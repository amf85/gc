# Graph coarsening (GC) #

MPhil research project for amf85

### Setup ###

Correct Dockerfile is in docker/gc/

### Structure of src/ ###

Note a lot of the structure is designed to facilitate recording experiments in a local sqlite database for some experiment(s), which can be merged back into a master version. Works well in practice but might admittedly seem a little [heavy-handed](https://xkcd.com/974/) at first. Anything not listed below can be safely ignored

* dcp_exps.py and silhouette_exps.py are the main methods used to run 
* data/ -- datasets + samples inherit from the various classes in gc_data.py 
* experiments/ -- training loops etc. losses.py is where most of the difficulty + failed tests have been 
* models/ -- as expected
* spectral/ -- mostly previous models, tests etc. Possibly decompose.py might be used somewhere
* sql/ -- db setup + methods
* util/ -- used but not interesting
* vis/ -- only using communities.py
