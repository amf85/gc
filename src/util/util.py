import os
import sys
import time
from time import time, process_time, perf_counter
import pandas as pd
import torch
from datetime import timedelta
import math


def more_pandas(width=1000, max_cols=100, max_rows=200):
  pd.set_option('display.width', width)
  pd.set_option('display.max_columns', max_cols)
  pd.set_option('display.max_rows', max_rows)


def validate_directory(directory):
  if directory is not None and not os.path.exists(directory):
    os.makedirs(directory)
  return directory

def round_sf(val, sf):
  return round(val, sf-int(math.floor(math.log10(abs(val))))-1)

WHITE = (1.0, 1.0, 1.0)
def rgba_to_rgb(foreground, alpha, background=WHITE):
  return [f * alpha + b * (1 - alpha) for f, b in zip(foreground, background)]


cpu = torch.device('cpu')
gpu = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def format_seconds(seconds):
  return "{:2}:{:02.0f}".format(int(divmod(seconds, 60)[0]), divmod(seconds, 60)[1])


class GCTime(object):
  """
  times[0] (time) for clock time
  times[1] (process time) updates only when process is running
  times[2] (perf counter) preferred for benchmarking, as uses the absolute cpu clock freq. So will
  incorporate any access to external resources (e.g. GPU)
  """
  
  def __init__(self):
    super(GCTime, self).__init__()
    self.times = {}
  
  def start_timer(self, timer_id):
    self.times[timer_id] = (time(), process_time(), perf_counter())
  
  def elapsed_time(self, timer_id, as_timedelta=False):
    t = time() - self.times[timer_id][0]
    if as_timedelta:
      t = timedelta(seconds=t)
    return t
  
  def elapsed_process_time(self, timer_id, as_timedelta=False):
    t= process_time() - self.times[timer_id][1]
    if as_timedelta:
      t = timedelta(seconds=t)
    return t
  
  def elapsed_perf_time(self, timer_id, as_timedelta=False):
    t= perf_counter() - self.times[timer_id][2]
    if as_timedelta:
      t = timedelta(seconds=t)
    return t
  
  def elapsed_all(self, timer_id, as_timedelta=False):
    now = (time(), process_time(), perf_counter())
    timer = self.times[timer_id]
    ts = [now[0] - timer[0], now[1] - timer[1], now[2] - timer[2]]
    if as_timedelta:
      ts = [timedelta(seconds=t) for t in ts]
    return ts


# Print iterations progress
def printProgressBar (iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Modified from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    sys.stdout.write(f'\r{prefix} |{bar}| {percent}% {suffix}')
    sys.stdout.flush()
    # print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # print('test')
    # Print New Line on Complete
    if iteration == total:
        sys.stdout.write('\n')
        sys.stdout.flush()
