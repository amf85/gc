from sqlalchemy import create_engine
from sql_base import Base
from sqlalchemy.orm import sessionmaker
from pathlib import Path
import os



if __name__ == '__main__':
  _sqlite = Path('/opt/project/sql_store/gc2.sqlite')
  os.remove(_sqlite)
  engine = create_engine(f"sqlite+pysqlite:////opt/project/sql_store/gc2.sqlite", echo=True,
                         future=True)
  Base.metadata.create_all(engine, checkfirst=True)
  Session = sessionmaker(bind=engine)
  session = Session()

