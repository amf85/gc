import _sqlite3 as sqlite3
import os
import shutil
from datetime import datetime
from pathlib import Path
import pickle

from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import SingletonThreadPool
# from sqlalchemy.sql import null
from sqlalchemy import null

# from sql.sql_base import Base, GCSampleBase, GCDataSplitBase, GCModelBase, GCOptimiserBase, \
#   GCExperimentBase, GCEpochBase, MUEpochBase, DCPEpochBase, SCPEpochBase
from sql.sql_base import *
from util.constants import *
from util.util import validate_directory



class GCDB(object):
  
  # samples_table_names = "('samples', 'samples_re', 'samples_hn', 'samples_sbm', 'samples_me', " \
  #                       "'sample_mr', 'samples_sm', 'samples_sm_motifs')"
  
  data_table_names = "('samples', 'samples_re', 'samples_hn', 'samples_sbm', 'samples_me', " \
                     "'samples_mr', 'datasplits', 'datasplits_hn', 'datasplits_me', " \
                     "'datasplits_re', 'datasplits_sbm', 'datasplits_mr', 'sample_datasplit', " \
                     "'samples_sm', 'datasplits_sm', 'samples_sm_motifs', 'samples_sm_pools', " \
                     "'samples_sm_pool_motif', 'samples_enz', 'datasplits_enz')"
  
  epoch_table_names = "('epoch_direct_community_prediction', 'epoch_implicit_aux_tgt', " \
                      "'epoch_measured_unsupervised', 'epoch_silhouette_community_prediction', " \
                      "'epoch_supervised_communities', 'epoch_supervised_edge_communities', " \
                      "'epoch_supervised_graph_classify')"
  
  epoch_names_tables = {
    'epoch_direct_community_prediction': (DCPEpochBase, dcp_epoch_drop_cols),
    'epoch_implicit_aux_tgt': (IATEpochBase, iat_epoch_drop_cols),
    'epoch_measured_unsupervised': (MUEpochBase, mu_epoch_drop_cols),
    'epoch_silhouette_community_prediction': (SCPEpochBase, scp_epoch_drop_cols),
    'epoch_supervised_communities': (SCEpochBase, sc_epoch_drop_cols),
    'epoch_supervised_edge_communities': (SECEpochBase, sec_epoch_drop_cols),
    'epoch_supervised_graph_classify': (SGCEpochBase, sgc_epoch_drop_cols),
  }

  exp_names_tables = {
    'exp_supervised_communities': (SupervisedCommunitiesBase, SCEpochBase),
    'exp_supervised_edge_communities': (SupervisedEdgeCommunitiesBase, SECEpochBase),
  }
  
  non_standard_table_names = "('sample_datasplit', 'samples_sm_pool_motif')"
  gc_base_classes = [GCDataSplitBase, GCEpochBase, GCExperimentBase, GCModelBase,
                     GCOptimiserBase, GCSampleBase]
  def __init__(self, ):
    super(GCDB, self).__init__()
    self.local_sess = None
    self.master_sess = None
  
  @staticmethod
  def get_sessionmaker(db_loc, verbose=False):
    engine = create_engine(url=f'sqlite+pysqlite:///{db_loc}',
                           echo=verbose,
                           future=True,
                           poolclass=SingletonThreadPool)
    return sessionmaker(bind=engine)
  
  def init_master(self, master_loc, verbose=False, fresh=False):
    if fresh:
      if os.path.exists(f'{master_loc}'):
        os.remove(master_loc)
    engine = create_engine(f'sqlite+pysqlite:///{master_loc}', echo=verbose, future=True,
                           poolclass=SingletonThreadPool)
    self.master_sess = sessionmaker(bind=engine)
    Base.metadata.create_all(engine, checkfirst=True)
  
  # def init_local(self,
  #                local_dir,
  #                verbose=False,
  #                add_samples_from_master=True,
  #                master_loc=None):
  #   validate_directory(local_dir)
  #   local_loc = datetime.now().strftime(f'{local_dir}/local_%Y-%m-%d_%H-%M-%S-%f.sqlite')
  #   engine = create_engine(f'sqlite+pysqlite:///{local_loc}',
  #                          echo=verbose,
  #                          future=True,
  #                          poolclass=SingletonThreadPool)
  #   self.local_sess = sessionmaker(bind=engine)
  #   Base.metadata.create_all(engine, checkfirst=True)
  #   if add_samples_from_master:
  #     conn = sqlite3.connect(local_loc)
  #     conn.execute(f"ATTACH '{master_loc}' as master")
  #     conn.execute("BEGIN")
  #     curs = conn.execute(
  #       f"SELECT * FROM master.sqlite_master WHERE name in {self.samples_table_names}")
  #     for row in curs:
  #       local = f"INSERT INTO {row[1]} SELECT * FROM master.{row[1]}"
  #       print(local)
  #       conn.execute(local)
  #     conn.commit()
  #     conn.execute("DETACH DATABASE master")
      
  
  
  def init_local(self,
                 local_loc,
                 fresh=False,
                 copy_most_recent=True,
                 verbose=False,
                 add_data_from_master=True,
                 add_all_from_master=False,
                 master_loc=None,
                 use_master_as_local=False
                 ):
    if use_master_as_local:
      self.local_sess = self.master_sess
      return
    if fresh:
      if os.path.exists(local_loc):
        if copy_most_recent:
          with open(LAST_EXP_DIR_LOC, 'r') as f:
            last_exp_dir = f.read()
          shutil.copy(local_loc, f'{last_exp_dir}/gc.sqlite')
        os.remove(local_loc)
    engine = create_engine(f'sqlite+pysqlite:///{local_loc}',
                           echo=verbose,
                           future=True,
                           poolclass=SingletonThreadPool)
    self.local_sess = sessionmaker(bind=engine)
    Base.metadata.create_all(engine, checkfirst=True)
    if fresh and add_all_from_master and add_data_from_master:
      # conn = sqlite3.connect(local_loc)
      # conn.execute(f"ATTACH '{master_loc}' as master")
      # conn.execute("BEGIN")
      l_sess = self.local_sess()
      l_sess.execute("BEGIN")
      l_sess.execute(f"ATTACH '{master_loc}' as master")
      tables = l_sess.execute(f"SELECT * FROM master.sqlite_master WHERE type='table'")
      for table in tables:
        l_sess.execute(f"INSERT INTO {table[1]} SELECT * FROM master.{table[1]}")
        l_sess.commit()
      l_sess.execute("DETACH DATABASE master")

    elif fresh and add_data_from_master:
      conn = sqlite3.connect(local_loc)
      conn.execute(f"ATTACH '{master_loc}' as master")
      conn.execute("BEGIN")
      curs = conn.execute(
        f"SELECT * FROM master.sqlite_master WHERE name in {self.data_table_names}")
      for row in curs:
        local = f"INSERT INTO {row[1]} SELECT * FROM master.{row[1]}"
        # print(local)
        conn.execute(local)
      conn.commit()
      conn.execute("DETACH DATABASE master")
      
  @staticmethod
  def merge_local_to_master(master_loc,
                            local_loc,
                            only_keep_every_nth_epoch_images=1,
                            keep_specific_epoch_images=None,
                            ):
    if master_loc == local_loc:
      raise ValueError()
    master = create_engine(f'sqlite+pysqlite:///{master_loc}',
                           echo=False,
                           future=True,
                           poolclass=SingletonThreadPool)
    local = create_engine(f'sqlite+pysqlite:///{local_loc}',
                          echo=False,
                          future=True,
                          poolclass=SingletonThreadPool)
    Base.metadata.create_all(master, checkfirst=True)
    Base.metadata.create_all(local, checkfirst=True)
    l_sess = sessionmaker(bind=local)()
    m_sess = sessionmaker(bind=master)()
    
    # increment ids so no primary key conflicts, in case any merges since last pull
    for gc_class in GCDB.gc_base_classes:
      max_master_record = m_sess.execute(
        select(gc_class).order_by(gc_class.id.desc()).limit(1)).first()
      max_master_id = 0 if max_master_record is None else max_master_record[0].id
      max_non_fresh_local = l_sess.execute(select(gc_class)
                                           .where(gc_class.fresh == False)
                                           .order_by(gc_class.id.desc())
                                           .limit(1)).first()
      max_non_fresh_local_id = 0 if max_non_fresh_local is None else max_non_fresh_local[0].id
      increment = max_master_id - max_non_fresh_local_id
      fresh_locals = l_sess.execute(select(gc_class)
                                    .where(gc_class.fresh == True)
                                    .order_by(gc_class.id.desc())).all()
      if increment > 0:
        for record in fresh_locals:
          record = record[0]
          for cascade in db_cascades[gc_class]:
            child_fkey = cascade[0]
            child_class = cascade[1]
            child_records = l_sess.execute(
              select(child_class)
                .where(child_class.__dict__[child_fkey] == record.id)).all()
            for child_record in child_records:
              child_record[0].__dict__[child_fkey] += increment
              l_sess.commit()

          if gc_class == GCSampleBase and increment > 0:
            GCDB.rename_sample_components(record, increment)
          record.id += increment
          record.fresh = False
          l_sess.commit()
      
    

    # attach local to master
    m_sess.execute(f"ATTACH '{local_loc}' as local")
    m_sess.execute(f"BEGIN")
    
    # first check if named datasplit was overwritten, if so take precedence from local
    local_dss = l_sess.execute('select * from datasplits where name is not null').all()
    master_dss = m_sess.execute('select * from datasplits where name is not null').all()
    for named_local in local_dss:
      matching_master = [m for m in master_dss if m.name == named_local.name]
      assert len(matching_master) < 2, \
        f'found duplicate named split in master with name "{named_local.name}"'
      if len(matching_master) == 0:
        continue
      if matching_master[0].id != named_local.id:
        m = m_sess.execute(select(GCDataSplitBase).where(GCDataSplitBase.id == matching_master[0].id)).all()
        assert len(m) < 2
        m[0][0].name = None
        # matching_master[0].name = None
        m_sess.commit()
      
    
    # any records in local that are not in master get merged (standard tables)
    tables = m_sess.execute(
      f"SELECT * FROM local.sqlite_master WHERE type='table' "
      f"AND name not in {GCDB.epoch_table_names} "
      f"AND name not in {GCDB.non_standard_table_names}")
    for table in tables:
      stmt = f"INSERT INTO {table[1]} SELECT * FROM local.{table[1]} " \
             f"WHERE id not in (SELECT id FROM {table[1]})"
      m_sess.execute(stmt)
      m_sess.commit()
      
    # do association tables after so foreign keys definitely exist
    local_sds = set(l_sess.execute('select * from sample_datasplit').all())
    master_sds = set(m_sess.execute('select * from sample_datasplit').all())
    to_add = local_sds.difference(master_sds)
    stmt = f'INSERT INTO sample_datasplit (sample_id, datasplit_id) VALUES ' \
           f'{",".join(map(str, to_add))}'
    if len(to_add) > 0:
      m_sess.execute(stmt)
      m_sess.commit()
      
    local_pms = set(l_sess.execute('select * from samples_sm_pool_motif').all())
    master_pms = set(m_sess.execute('select * from samples_sm_pool_motif').all())
    to_add = local_pms.difference(master_pms)
    stmt = f'INSERT INTO samples_sm_pool_motif (motif_id, pool_id) VALUES ' \
           f'{",".join(map(str, to_add))}'
    if len(to_add) > 0:
      m_sess.execute(stmt)
      m_sess.commit()

    image_epochs = ",".join(map(str, keep_specific_epoch_images))
    image_epochs = f'({image_epochs})'
    for table_name, (base_class, drop_cols) in GCDB.epoch_names_tables.items():
      cols = [col for col in base_class.__dict__.keys()
              if not col.startswith('_') and col not in drop_cols]

      # print('test')
      m_sess.execute(
        f"INSERT INTO {table_name} ({','.join(cols)}) "
        f"SELECT {','.join(cols)} FROM local.{table_name} "
        f"WHERE id not in (SELECT id from {table_name}) "
        f"AND epoch_num not in {image_epochs} "
        f"AND epoch_num % {only_keep_every_nth_epoch_images} != 0"
      )

      m_sess.execute(
        f"INSERT INTO {table_name} SELECT * FROM local.{table_name} "
        f"WHERE id not in (SELECT id FROM {table_name}) "
        f"AND (epoch_num in {image_epochs} "
        f"OR epoch_num % {only_keep_every_nth_epoch_images} = 0)"
      )
      m_sess.commit()
    
  @staticmethod
  def rename_sample_components(sample, increment):
    sample_root = '/'.join(Path(sample.file).parts[:-2])[1:]
    all_sample_files = list(Path(sample_root).glob(f'**/{sample.id}.*'))
    sample_pkl = pickle.load(open(f'{sample_root}/{SAMPLES_SFX}/{sample.id}.pkl', 'rb'))
    for file in all_sample_files:
      dest = f'{file.parent}/{int(file.stem) + increment}{file.suffix}'
      if os.path.exists(dest):
        raise ValueError(f'found existing file for {sample.id} -> {sample.id + increment}')
      sample_pkl.components[[k for k, v in sample_pkl.components.items()
                             if v == str(file)][0]] = dest
      shutil.move(src=file, dst=dest)
    sample_pkl.id += increment
    sample_pkl.filename = f'{sample_root}/{SAMPLES_SFX}/{sample.id}.pkl'
    # will already have been moved, just saving components + id update
    pickle.dump(sample_pkl, open(sample_pkl.filename, 'wb'))
    