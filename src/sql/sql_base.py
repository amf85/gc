from sqlalchemy import (
  Column,
  Integer,
  String,
  Boolean,
  Float,
  BLOB,
  Interval,
  DateTime,
  Table,
  ForeignKey,
)
from sqlalchemy.orm import declarative_base, relationship
# from sqlalchemy_imageattach.entity import Image, image_attachment
# from analysis.dataset_composition import DCAnalysis

Base = declarative_base()

GRAPH_LEVEL_FEATURES = [
  'density',
  'transitivity',
  'transitivity_sq',
  'diameter',
  'radius',
  'n_nodes',
  'n_edges',
  'periphery_coeff',
  # 'avg_clustering_coeff',
  # 'avg_clustering_coeff_sq',
]
STATS = ['mean', 'median', 'min', 'max', 'std', 'skew', 'kurtosis']

db_cascades = {}
tablenames_to_base = {}

class GCSampleBase(Base):
  __tablename__ = 'samples'
  
  id = Column(Integer, primary_key=True)
  sample_table = Column(String(64))
  # eigen_conf = Column(String)
  
  file = Column(String(256))
  fresh = Column(Boolean)
  
  datasplits = relationship('GCDataSplitBase', secondary=lambda: SampleDatasplit, cascade='all,delete')
db_cascades[GCSampleBase] = []

class HierarchicalNetworkBase(Base):
  __tablename__ = 'samples_hn'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  motif_size = Column(Integer)
  branching_factor = Column(Integer)
  depth = Column(Integer)
db_cascades[GCSampleBase].append(('id', HierarchicalNetworkBase))

class RandomlyExpandedNetworkBase(Base):
  __tablename__ = 'samples_re'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  init_g_size = Column(Integer)
  motif_min_nodes = Column(Integer)
  motif_max_nodes = Column(Integer)
  n_passes = Column(Integer)
  expansion_prob = Column(Float)
  init_g_sparsity = Column(Float)
  motif_sparsity = Column(Float)
  maintain_motif_degree = Column(Boolean)
db_cascades[GCSampleBase].append(('id', RandomlyExpandedNetworkBase))

class MotifExpandedNetworkBase(Base):
  __tablename__ = 'samples_me'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  init_g_size = Column(Integer)
  init_g_exp_node_edges = Column(Float)
  init_g_sparsity = Column(Float)
  motif_min_nodes = Column(Integer)
  motif_max_nodes = Column(Integer)
  motif_exp_node_edges = Column(Float)
  motif_pool_size = Column(Integer)
  expansion_prob = Column(Float)
  maintain_motif_degree = Column(Boolean)
  n_passes = Column(Integer)
db_cascades[GCSampleBase].append(('id', MotifExpandedNetworkBase))

class GCEnzymeBase(Base):
  __tablename__ = 'samples_enz'

  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
db_cascades[GCSampleBase].append(('id', GCEnzymeBase))

class GCDDBase(Base):
  __tablename__ = 'samples_dd'

  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
db_cascades[GCSampleBase].append(('id', GCDDBase))


class SMPoolBase(Base):
  __tablename__ = 'samples_sm_pools'
  
  id = Column(Integer, primary_key=True)
  pool_size = Column(Integer)
  min_motif_nodes = Column(Integer)
  max_motif_nodes = Column(Integer)
  randoms = Column(Boolean)
  rings = Column(Boolean)
  cliques = Column(Boolean)
  stars = Column(Boolean)
  lines = Column(Boolean)
  exp_node_edges = Column(Float)
  file = Column(String)
db_cascades[SMPoolBase] = []

class SharedMotifsNetworkBase(Base):
  __tablename__ = 'samples_sm'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  pool_id = Column(Integer, ForeignKey('samples_sm_pools.id', onupdate='cascade'), primary_key=True)
  
  init_size = Column(Integer)
  init_structure = Column(String)
  init_exp_node_edges = Column(Float)
  maintain_motif_degree = Column(Boolean)
  n_passes = Column(Integer)
  expansion_prob = Column(Float)
  
db_cascades[GCSampleBase].append(('id', SharedMotifsNetworkBase))
db_cascades[SMPoolBase].append(('pool_id', SharedMotifsNetworkBase))

class SMMotifBase(Base):
  __tablename__ = 'samples_sm_motifs'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  
  n_nodes = Column(Integer)
  structure = Column(String)
  exp_node_edges = Column(Float)
db_cascades[GCSampleBase].append(('id', SMMotifBase))

class MSPResidueBase(Base):
  __tablename__ = 'samples_mr'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  sample_name = Column(String)
  raw_res_root = Column(String)
  res_config_version = Column(String)
  sample_type = Column(String)
  
db_cascades[GCSampleBase].append(('id', MSPResidueBase))
  
class StochasticBlockModelBase(Base):
  __tablename__ = 'samples_sbm'
  
  id = Column(Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True)
  n_nodes = Column(Integer)
  n_clusters = Column(Integer)
  priors_a = Column(Float)
  priors_b = Column(Float)
  intra_connect_min = Column(Float)
  intra_connect_max = Column(Float)
  inter_connect_min = Column(Float)
  inter_connect_max = Column(Float)
db_cascades[GCSampleBase].append(('id', StochasticBlockModelBase))


  
PoolMotifBase = Table(
  'samples_sm_pool_motif',
  Base.metadata,
  Column(
    'motif_id',
    Integer,
    ForeignKey('samples_sm_motifs.id', onupdate='cascade'),
    primary_key=True),
  Column(
    'pool_id',
    Integer,
    ForeignKey('samples_sm_pools.id', onupdate='cascade'),
    primary_key=True)
)

class GCDataSplitBase(Base):
  __tablename__ = 'datasplits'
  
  id = Column(Integer, primary_key=True)
  name = Column(String(128), nullable=True)
  datasplit_class = Column(String(64))
  table_name = Column(String(64))
  fresh = Column(Boolean)

  samples = relationship('GCSampleBase', secondary=lambda: SampleDatasplit, cascade='all,delete')
db_cascades[GCDataSplitBase] = []

SampleDatasplit = Table(
  'sample_datasplit',
  Base.metadata,
  Column('sample_id', Integer, ForeignKey('samples.id', onupdate='cascade'), primary_key=True),
  Column('datasplit_id', Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
)

dsa_cols = {'__tablename__': 'datasplit_analyses',
            'id': Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)}
for feature in GRAPH_LEVEL_FEATURES:
  for stat in STATS:
    dsa_cols[f'{feature}_{stat}'] = Column(Float)
DataSplitAnalysisBase = type('DataSplitAnalyses', (Base,), dsa_cols)
db_cascades[GCDataSplitBase].append(('id', DataSplitAnalysisBase))

class HNDataSplitBase(Base):
  __tablename__ = 'datasplits_hn'
  
  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = String(32)
  n_samples = Column(Integer)
  motif_sizes = Column(String(256))
  branching_factors = Column(String(64))
  depths = Column(String(64))
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', HNDataSplitBase))

class REDataSplitBase(Base):
  __tablename__ = 'datasplits_re'
  
  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)
  init_g_sizes = Column(String(128))
  motif_min_nodes = Column(Integer)
  motif_max_nodes = Column(Integer)
  n_passes = Column(String(64))
  expansion_prob = Column(Float)
  init_g_sparsity = Column(Float)
  motif_sparsity = Column(Float)
  maintain_motif_degree = Column(Boolean)
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', REDataSplitBase))

class MEDataSplitBase(Base):
  __tablename__ = 'datasplits_me'
  
  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)
  init_g_min_nodes = Column(Integer)
  init_g_max_nodes = Column(Integer)
  init_g_exp_node_edges = Column(Integer)
  motif_min_nodes = Column(Integer)
  motif_max_nodes = Column(Integer)
  motif_exp_node_edges = Column(Float)
  motif_pool_size = Column(Float)
  expansion_prob = Column(Float)
  maintain_motif_degree = Column(Boolean)
  n_passes = Column(String(64))
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', MEDataSplitBase))

class EnzymesDataSplitBase(Base):
  __tablename__ = 'datasplits_enz'

  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', EnzymesDataSplitBase))

class DDDataSplitBase(Base):
  __tablename__ = 'datasplits_dd'

  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', DDDataSplitBase))

class SMDataSplitBase(Base):
  __tablename__ = 'datasplits_sm'
  
  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)

  init_min_nodes = Column(Integer)
  init_max_nodes = Column(Integer)
  init_randoms = Column(Boolean)
  init_rings = Column(Boolean)
  init_cliques = Column(Boolean)
  init_stars = Column(Boolean)
  init_lines = Column(Boolean)
  init_exp_node_edges = Column(Float)
  expansion_prob = Column(Float)
  maintain_motif_degree = Column(Boolean)
  n_passes = Column(String)
  
  pool_id = Column(Integer, ForeignKey('samples_sm_pools.id', onupdate='CASCADE'))
  
  # pool_min_nodes = Column(Integer)
  # pool_max_nodes = Column(Integer)
  # pool_size = Column(Integer)
  # pool_randoms = Column(Boolean)
  # pool_rings = Column(Boolean)
  # pool_cliques = Column(Boolean)
  # pool_stars = Column(Boolean)
  # pool_lines = Column(Boolean)
  # pool_exp_node_edges = Column(Float)
  
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', SMDataSplitBase))
db_cascades[SMPoolBase].append(('id', SMDataSplitBase))

class MRDataSplitBase(Base):
  __tablename__ = 'datasplits_mr'
  
  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)
  
  raw_res_root = Column(String)
  res_config_version = Column(String)
  selection_method = Column(String)
  sample_type = Column(String)
  
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', MEDataSplitBase))

class SBMDataSplitBase(Base):
  __tablename__ = 'datasplits_sbm'
  
  id = Column(Integer, ForeignKey('datasplits.id', onupdate='cascade'), primary_key=True)
  split = Column(String(32))
  n_samples = Column(Integer)
  
  min_nodes = Column(Integer)
  max_nodes = Column(Integer)
  min_clusters = Column(Integer)
  max_clusters = Column(Integer)
  priors_a = Column(Float)
  priors_b = Column(Float)
  intra_connect_min = Column(Float)
  intra_connect_max = Column(Float)
  inter_connect_min = Column(Float)
  inter_connect_max = Column(Float)
  directory = Column(String(256))
db_cascades[GCDataSplitBase].append(('id', SBMDataSplitBase))


class GCModelBase(Base):
  __tablename__ = 'models'
  
  id = Column(Integer, primary_key=True)
  model_class = Column(String(64))
  table_name = Column(String(64))
  fresh = Column(Boolean)
db_cascades[GCModelBase] = []


class ClusteringGNNBase(Base):
  __tablename__ = 'model_clustering_gnn'
  
  id = Column(Integer, ForeignKey('models.id', onupdate='cascade'), primary_key=True)
  input_dim = Column(Integer)
  has_identical_layers = Column(Boolean)
  mid_layer_output_dims = Column(String(256))
  output_dim = Column(Integer)
  dropout = Column(Float)
  use_batchnorm = Column(Boolean)
  activation = Column(String(32))
  self_loops = Column(Boolean)
  # params = Column(BLOB)
db_cascades[GCModelBase].append(('id', ClusteringGNNBase))


class GNNClassifyBase(Base):
  __tablename__ = 'model_gnn_classify'

  id = Column(Integer, ForeignKey('models.id', onupdate='cascade'), primary_key=True)
  input_dim = Column(Integer)

  gnn_out_dims = Column(String)
  mlp_out_dims = Column(String)

  dropout_gnn = Column(Float)
  dropout_mlp = Column(Float)
  use_batchnorm_gnn = Column(Boolean)
  use_batchnorm_mlp = Column(Boolean)
  activation = Column(String(32))
  output_aggr = Column(String)
  self_loops = Column(Boolean)
  # params = Column(BLOB)
db_cascades[GCModelBase].append(('id', GNNClassifyBase))


class EdgeClusterClassifyBase(Base):
  __tablename__ = 'model_edge_cluster_classify'

  id = Column(Integer, ForeignKey('models.id', onupdate='cascade'), primary_key=True)
  input_dim = Column(Integer)
  add_self_loops = Column(Boolean)

  edge_gnn_out_dims = Column(String)
  edge_gnn_conv_fn = Column(String)
  edge_gnn_conv_conf = Column(String)

  edge_pred_out_dims = Column(String)
  classify_from_hidden_representation = Column(Boolean)

  clsf_gnn_out_dims = Column(String)
  clsf_gnn_conv_fn = Column(String)
  clsf_gnn_conv_conf = Column(String)

  output_aggr = Column(String)
  clsf_mlp_out_dims = Column(String)

  batchnorm_edge_gnn = Column(Boolean)
  batchnorm_edge_pred = Column(Boolean)
  batchnorm_clsf_gnn = Column(Boolean)
  batchnorm_clsf_mlp = Column(Boolean)
  activation_edge_gnn = Column(String)
  activation_edge_pred = Column(String)
  activation_edge_pred_score = Column(String)
  activation_clsf_gnn = Column(String)
  activation_clsf_mlp = Column(String)
  dropout_edge_gnn = Column(Float)
  dropout_edge_pred = Column(Float)
  dropout_clsf_gnn = Column(Float)
  dropout_clsf_mlp = Column(Float)
db_cascades[GCModelBase].append(('id', EdgeClusterClassifyBase))


class EdgeClusteringGNNBase(Base):
  __tablename__ = 'model_edge_clustering'
  
  id = Column(Integer, ForeignKey('models.id', onupdate='cascade'), primary_key=True)
  input_dim = Column(Integer)
  add_self_loops = Column(Boolean)

  gnn_out_dims = Column(String)
  gnn_conv_fn = Column(String)
  gnn_conv_conf = Column(String)

  edge_pred_out_dims = Column(String)

  batchnorm_gnn = Column(Boolean)
  batchnorm_edge_pred = Column(Boolean)
  activation_gnn = Column(String)
  activation_edge_pred = Column(String)
  dropout_gnn = Column(Float)
  dropout_edge_pred = Column(Float)
db_cascades[GCModelBase].append(('id', EdgeClusteringGNNBase))


class ClusteringMLPBase(Base):
  __tablename__ = 'model_clustering_mlp'
  
  id = Column(Integer, ForeignKey('models.id', onupdate='cascade'), primary_key=True)
  input_dim = Column(Integer)
  has_identical_layers = Column(Boolean)
  mid_layer_output_dims = Column(String(256))
  output_dim = Column(Integer)
  dropout = Column(Float)
  use_batchnorm = Column(Boolean)
  activation = Column(String(32))
  # self_loops = Column(Boolean)
  # params = Column(BLOB)
db_cascades[GCModelBase].append(('id', ClusteringMLPBase))


class ImplicitAuxTgtGNNBase(Base):
  __tablename__ = 'model_implicit_aux_tgt'
  
  id = Column(Integer, ForeignKey('models.id', onupdate='cascade'), primary_key=True)
  
  dropout = Column(Float)
  
  X_pre_node_input_dim = Column(Integer)
  X_pre_use_node_inputs = Column(Boolean)
  X_pre_n_spectral = Column(Integer)
  X_pre_derived_feat_dim = Column(Integer)
  
  clustering_layers = Column(String(256))
  clustering_add_self_loops = Column(Boolean)
  clustering_use_batchnorm = Column(Boolean)
  clustering_activation = Column(String(32))
  
  X_post_use_clusters = Column(Boolean)
  X_post_use_spectral = Column(Boolean)
  X_post_use_node_inputs = Column(Boolean)
  X_post_use_derived_feats = Column(Boolean)
  
  intra_layers = Column(String(256))
  intra_adj = Column(String(32))
  intra_softmax = Column(Boolean)
  intra_add_self_loops = Column(Boolean)
  intra_use_batchnorm = Column(Boolean)
  intra_activation = Column(String(32))
  
  
  inter_layers = Column(String(256))
  inter_adj = Column(String(32))
  inter_softmax = Column(Boolean)
  inter_add_self_loops = Column(Boolean)
  inter_use_batchnorm = Column(Boolean)
  inter_activation = Column(String(32))
  
  output_aggregation = Column(String(32))
  output_layers = Column(String(256))
  output_use_batchnorm = Column(Boolean)
  output_activation = Column(String(32))
  output_softmax = Column(Boolean)
db_cascades[GCModelBase].append(('id', ImplicitAuxTgtGNNBase))


class GCOptimiserBase(Base):
  __tablename__ = 'optimisers'
  
  id = Column(Integer, primary_key=True)
  optimiser_class = Column(String(64))
  table_name = Column(String(64))
  fresh = Column(Boolean)
db_cascades[GCOptimiserBase] = []


class GCAdamOptBase(Base):
  __tablename__ = 'opt_adam'
  
  id = Column(Integer, ForeignKey('optimisers.id', onupdate='cascade'), primary_key=True)
  optimiser_class = Column(String(64))
  LR = Column(Float)
  beta_1 = Column(Float)
  beta_2 = Column(Float)
  eps = Column(Float)
  weight_decay = Column(Float)
  amsgrad = Column(Boolean)
db_cascades[GCOptimiserBase].append(('id', GCAdamOptBase))


class GCExperimentBase(Base):
  __tablename__ = 'experiments'
  
  id = Column(Integer, primary_key=True)
  experiment_class = Column(String(64))
  table_name = Column(String(64))
  fresh = Column(Boolean)
db_cascades[GCExperimentBase] = []


class DirectCommunityPredictionBase(Base):
  __tablename__ = 'exp_direct_community_prediction'

  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE', ondelete='CASCADE'),
              primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  n_clusters = Column(Integer)
  lambda_tce = Column(Float)
  lambda_cae = Column(Float)
  n_spectral_vectors = Column(Integer)
db_cascades[GCExperimentBase].append(('id', DirectCommunityPredictionBase))
db_cascades[GCModelBase].append(('model_id', DirectCommunityPredictionBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', DirectCommunityPredictionBase))
db_cascades[GCDataSplitBase].append(('train_split_id', DirectCommunityPredictionBase))
db_cascades[GCDataSplitBase].append(('test_split_id', DirectCommunityPredictionBase))


class MeasuredUnsupervisedBase(Base):
  __tablename__ = 'exp_measured_unsupervised'

  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE', ondelete='CASCADE'),
              primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  n_clusters = Column(Integer)
  lambda_silh = Column(Float)
  lambda_mod = Column(Float)
  lambda_tce = Column(Float)
  lambda_cae = Column(Float)
  
  label = Column(String(64))
  n_spectral_vectors = Column(Integer)
  degree_centrality = Column(Boolean)
  closeness_centrality = Column(Boolean)
  betweenness_centrality = Column(Boolean)
  current_flow_closeness_centrality = Column(Boolean)
  current_flow_betweenness_centrality = Column(Boolean)
  approximate_current_flow_betweenness_centrality = Column(Boolean)
  load_centrality = Column(Boolean)
  average_neighbor_degree = Column(Boolean)
  triangles = Column(Boolean)
  clustering_coeff_tri = Column(Boolean)
  clustering_coeff_sq = Column(Boolean)
  eccentricity = Column(Boolean)
db_cascades[GCExperimentBase].append(('id', MeasuredUnsupervisedBase))
db_cascades[GCModelBase].append(('model_id', MeasuredUnsupervisedBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', MeasuredUnsupervisedBase))
db_cascades[GCDataSplitBase].append(('train_split_id', MeasuredUnsupervisedBase))
db_cascades[GCDataSplitBase].append(('test_split_id', MeasuredUnsupervisedBase))


class SupervisedCommunitiesBase(Base):
  __tablename__ = 'exp_supervised_communities'
  
  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE'), primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  n_clusters = Column(Integer)
  
  lambda_cxe = Column(Float)
  lambda_cmse = Column(Float)
  lambda_cmae = Column(Float)
  lambda_silh = Column(Float)
  lambda_mod = Column(Float)
  lambda_tce = Column(Float)
  lambda_cae = Column(Float)
  lambda_nll = Column(Float)
  # lambda_l2 = Column(Float)
  # lambda_l1 = Column(Float)

  cross_val_group_start = Column(DateTime)
  cross_val_num_folds = Column(Integer)
  cross_val_which_fold = Column(Integer)
  cross_val_split_method = Column(String)
  cross_val_seed = Column(Integer)

  val_seed = Column(Integer)
  early_stop_patience = Column(Integer)
  early_stop_on = Column(String)

  eig_conf = Column(String)
  local_features = Column(String)
  node_features = Column(String)
  edge_features = Column(String)

  node_labels = Column(String)
  graph_labels = Column(String)
  edge_labels = Column(String)
db_cascades[GCExperimentBase].append(('id', SupervisedCommunitiesBase))
db_cascades[GCModelBase].append(('model_id', SupervisedCommunitiesBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', SupervisedCommunitiesBase))
db_cascades[GCDataSplitBase].append(('train_split_id', SupervisedCommunitiesBase))
db_cascades[GCDataSplitBase].append(('test_split_id', SupervisedCommunitiesBase))


class SupervisedGraphClassifyBase(Base):
  __tablename__ = 'exp_supervised_graph_classify'

  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE', ondelete='CASCADE'), primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  n_clusters = Column(Integer)

  # lambda_cxe = Column(Float)
  # lambda_cmse = Column(Float)
  # lambda_cmae = Column(Float)
  lambda_silh = Column(Float)
  lambda_mod = Column(Float)
  lambda_tce = Column(Float)
  lambda_cae = Column(Float)
  lambda_nll = Column(Float)
  # lambda_l2 = Column(Float)
  # lambda_l1 = Column(Float)

  cross_val_group_start = Column(DateTime)
  cross_val_num_folds = Column(Integer)
  cross_val_which_fold = Column(Integer)
  cross_val_split_method = Column(String)
  cross_val_seed = Column(Integer)

  val_seed = Column(Integer)
  early_stop_patience = Column(Integer)
  early_stop_on = Column(String)

  eig_conf = Column(String)
  local_features = Column(String)
  node_features = Column(String)
  edge_features = Column(String)

  node_labels = Column(String)
  graph_labels = Column(String)
  edge_labels = Column(String)
db_cascades[GCExperimentBase].append(('id', SupervisedGraphClassifyBase))
db_cascades[GCModelBase].append(('model_id', SupervisedGraphClassifyBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', SupervisedGraphClassifyBase))
db_cascades[GCDataSplitBase].append(('train_split_id', SupervisedGraphClassifyBase))
db_cascades[GCDataSplitBase].append(('test_split_id', SupervisedGraphClassifyBase))


class SupervisedEdgeCommunitiesBase(Base):
  __tablename__ = 'exp_supervised_edge_communities'
  
  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE', ondelete='CASCADE'), primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  
  lambda_nll = Column(Float)
  lambda_l2 = Column(Float)
  lambda_l1 = Column(Float)

  cross_val_group_start = Column(DateTime)
  cross_val_num_folds = Column(Integer)
  cross_val_which_fold = Column(Integer)
  cross_val_split_method = Column(String)
  cross_val_seed = Column(Integer)

  val_seed = Column(Integer)
  early_stop_patience = Column(Integer)
  early_stop_on = Column(String)
  
  eig_conf = Column(String)
  local_features = Column(String)
  node_features = Column(String)
  edge_features = Column(String)

  node_labels = Column(String)
  graph_labels = Column(String)
  edge_labels = Column(String)
  
db_cascades[GCExperimentBase].append(('id', SupervisedEdgeCommunitiesBase))
db_cascades[GCModelBase].append(('model_id', SupervisedEdgeCommunitiesBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', SupervisedEdgeCommunitiesBase))
db_cascades[GCDataSplitBase].append(('train_split_id', SupervisedEdgeCommunitiesBase))
db_cascades[GCDataSplitBase].append(('test_split_id', SupervisedEdgeCommunitiesBase))


class ImplicitAuxTgtBase(Base):
  __tablename__ = 'exp_implicit_aux_tgt'

  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE', ondelete='CASCADE'),
              primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  n_clusters = Column(Integer)
  
  lambda_aux_nll = Column(Float)
  lambda_nll = Column(Float)
  lambda_silh = Column(Float)
  lambda_mod = Column(Float)
  lambda_tce = Column(Float)
  lambda_cae = Column(Float)
  
  task_label = Column(String(64))
  aux_label = Column(String(64))
  
  n_spectral_vectors = Column(Integer)
  degree_centrality = Column(Boolean)
  closeness_centrality = Column(Boolean)
  betweenness_centrality = Column(Boolean)
  current_flow_closeness_centrality = Column(Boolean)
  current_flow_betweenness_centrality = Column(Boolean)
  approximate_current_flow_betweenness_centrality = Column(Boolean)
  load_centrality = Column(Boolean)
  average_neighbor_degree = Column(Boolean)
  triangles = Column(Boolean)
  clustering_coeff_tri = Column(Boolean)
  clustering_coeff_sq = Column(Boolean)
  eccentricity = Column(Boolean)
db_cascades[GCExperimentBase].append(('id', ImplicitAuxTgtBase))
db_cascades[GCModelBase].append(('model_id', ImplicitAuxTgtBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', ImplicitAuxTgtBase))
db_cascades[GCDataSplitBase].append(('train_split_id', ImplicitAuxTgtBase))
db_cascades[GCDataSplitBase].append(('test_split_id', ImplicitAuxTgtBase))


class SilhouetteCommunityPredictionBase(Base):
  __tablename__ = 'exp_silhouette_community_prediction'

  id = Column(Integer, ForeignKey('experiments.id', onupdate='CASCADE', ondelete='CASCADE'),
              primary_key=True)
  model_id = Column(Integer, ForeignKey('models.id', onupdate='CASCADE'))
  optimiser_id = Column(Integer, ForeignKey('optimisers.id', onupdate='CASCADE'))
  train_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  test_split_id = Column(Integer, ForeignKey('datasplits.id', onupdate='CASCADE'))
  val_size = Column(Float)
  batch_size = Column(Integer)
  n_clusters = Column(Integer)
  lambda_tce = Column(Float)
  lambda_cae = Column(Float)
  n_spectral_vectors = Column(Integer)
db_cascades[GCExperimentBase].append(('id', SilhouetteCommunityPredictionBase))
db_cascades[GCModelBase].append(('model_id', SilhouetteCommunityPredictionBase))
db_cascades[GCOptimiserBase].append(('optimiser_id', SilhouetteCommunityPredictionBase))
db_cascades[GCDataSplitBase].append(('train_split_id', SilhouetteCommunityPredictionBase))
db_cascades[GCDataSplitBase].append(('test_split_id', SilhouetteCommunityPredictionBase))


class GCEpochBase(Base):
  __tablename__ = 'epochs'
  
  id = Column(Integer, primary_key=True)
  experiment_class = Column(String(64))
  table_name = Column(String(64))
  fresh = Column(Boolean)
db_cascades[GCEpochBase] = []


class DCPEpochBase(Base):
  __tablename__ = 'epoch_direct_community_prediction'
  
  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)
  train_loss = Column(Float)
  train_raw = Column(Float)
  train_accuracy = Column(Float)
  train_tce = Column(Float)
  train_cae = Column(Float)
  train_silhouette = Column(Float)
  val_loss = Column(Float)
  val_raw = Column(Float)
  val_accuracy = Column(Float)
  val_tce = Column(Float)
  val_cae = Column(Float)
  val_silhouette = Column(Float)
  test_loss = Column(Float)
  test_raw = Column(Float)
  test_accuracy = Column(Float)
  test_tce = Column(Float)
  test_cae = Column(Float)
  test_silhouette = Column(Float)
  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)
  train_example = Column(BLOB)
  test_example = Column(BLOB)
db_cascades[GCEpochBase].append(('id', DCPEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', DCPEpochBase))
dcp_epoch_drop_cols = [
  'train_example', 'test_example'
]


class MUEpochBase(Base):
  __tablename__ = 'epoch_measured_unsupervised'
  
  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)
  
  train_loss = Column(Float)
  train_silh = Column(Float)
  train_mod = Column(Float)
  train_tce = Column(Float)
  train_cae = Column(Float)
  train_acc = Column(Float)
  train_nll = Column(Float)
  test_loss = Column(Float)
  test_silh = Column(Float)
  test_mod = Column(Float)
  test_tce = Column(Float)
  test_cae = Column(Float)
  test_acc = Column(Float)
  test_nll = Column(Float)
  
  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)
  
  train_eg_conf = Column(BLOB, nullable=True)
  train_eg_hmax = Column(BLOB, nullable=True)
  train_eg_labels = Column(BLOB, nullable=True)
  test_eg_conf = Column(BLOB, nullable=True)
  test_eg_hmax = Column(BLOB, nullable=True)
  test_eg_labels = Column(BLOB, nullable=True)
db_cascades[GCEpochBase].append(('id', MUEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', MUEpochBase))
mu_epoch_drop_cols = [
  'train_eg_conf', 'train_eg_hmax', 'train_eg_labels',
  'test_eg_conf', 'test_eg_hmax', 'test_eg_labels'
]

class SCEpochBase(Base):
  __tablename__ = 'epoch_supervised_communities'
  
  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)
  
  train_loss = Column(Float)
  train_acc = Column(Float)
  train_edge_acc = Column(Float)
  train_cxe = Column(Float)
  train_cmse = Column(Float)
  train_cmae = Column(Float)
  train_silh = Column(Float)
  train_mod = Column(Float)
  train_tce = Column(Float)
  train_cae = Column(Float)
  train_nll = Column(Float)

  val_loss = Column(Float)
  val_acc = Column(Float)
  val_edge_acc = Column(Float)
  val_cxe = Column(Float)
  val_cmse = Column(Float)
  val_cmae = Column(Float)
  val_silh = Column(Float)
  val_mod = Column(Float)
  val_tce = Column(Float)
  val_cae = Column(Float)
  val_nll = Column(Float)

  test_loss = Column(Float)
  test_acc = Column(Float)
  test_edge_acc = Column(Float)
  test_cxe = Column(Float)
  test_cmse = Column(Float)
  test_cmae = Column(Float)
  test_silh = Column(Float)
  test_mod = Column(Float)
  test_tce = Column(Float)
  test_cae = Column(Float)
  test_nll = Column(Float)
  
  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)
  
  train_eg_conf = Column(BLOB, nullable=True)
  train_eg_hmax = Column(BLOB, nullable=True)
  train_eg_labels = Column(BLOB, nullable=True)
  test_eg_conf = Column(BLOB, nullable=True)
  test_eg_hmax = Column(BLOB, nullable=True)
  test_eg_labels = Column(BLOB, nullable=True)
db_cascades[GCEpochBase].append(('id', SCEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', SCEpochBase))
sc_epoch_drop_cols = [
  'train_eg_conf', 'train_eg_hmax', 'train_eg_labels',
  'test_eg_conf', 'test_eg_hmax', 'test_eg_labels'
]

class SGCEpochBase(Base):
  __tablename__ = 'epoch_supervised_graph_classify'

  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)

  train_loss = Column(Float)
  train_acc = Column(Float)
  # train_cxe = Column(Float)
  # train_cmse = Column(Float)
  # train_cmae = Column(Float)
  train_silh = Column(Float)
  train_mod = Column(Float)
  train_tce = Column(Float)
  train_cae = Column(Float)
  train_nll = Column(Float)

  val_loss = Column(Float)
  val_acc = Column(Float)
  # val_cxe = Column(Float)
  # val_cmse = Column(Float)
  # val_cmae = Column(Float)
  val_silh = Column(Float)
  val_mod = Column(Float)
  val_tce = Column(Float)
  val_cae = Column(Float)
  val_nll = Column(Float)

  test_loss = Column(Float)
  test_acc = Column(Float)
  # test_cxe = Column(Float)
  # test_cmse = Column(Float)
  # test_cmae = Column(Float)
  test_silh = Column(Float)
  test_mod = Column(Float)
  test_tce = Column(Float)
  test_cae = Column(Float)
  test_nll = Column(Float)

  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)

  train_eg_conf = Column(BLOB, nullable=True)
  train_eg_hmax = Column(BLOB, nullable=True)
  train_eg_labels = Column(BLOB, nullable=True)
  test_eg_conf = Column(BLOB, nullable=True)
  test_eg_hmax = Column(BLOB, nullable=True)
  test_eg_labels = Column(BLOB, nullable=True)
db_cascades[GCEpochBase].append(('id', SGCEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', SGCEpochBase))
sgc_epoch_drop_cols = [
  'train_eg_conf', 'train_eg_hmax', 'train_eg_labels',
  'test_eg_conf', 'test_eg_hmax', 'test_eg_labels'
]

class SECEpochBase(Base):
  __tablename__ = 'epoch_supervised_edge_communities'
  
  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)
  
  train_loss = Column(Float)
  train_edge_acc = Column(Float)
  train_nll = Column(Float)

  val_loss = Column(Float)
  val_edge_acc = Column(Float)
  val_nll = Column(Float)

  test_loss = Column(Float)
  test_edge_acc = Column(Float)
  test_nll = Column(Float)
  
  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)
  
  train_eg_conf = Column(BLOB, nullable=True)
  train_eg_hmax = Column(BLOB, nullable=True)
  train_eg_labels = Column(BLOB, nullable=True)
  test_eg_conf = Column(BLOB, nullable=True)
  test_eg_hmax = Column(BLOB, nullable=True)
  test_eg_labels = Column(BLOB, nullable=True)
db_cascades[GCEpochBase].append(('id', SECEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', SECEpochBase))
sec_epoch_drop_cols = [
  'train_eg_conf', 'train_eg_hmax', 'train_eg_labels',
  'test_eg_conf', 'test_eg_hmax', 'test_eg_labels'
]

class IATEpochBase(Base):
  __tablename__ = 'epoch_implicit_aux_tgt'
  
  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)
  
  train_loss = Column(Float)
  train_acc = Column(Float)
  train_nll = Column(Float)
  train_aux_acc = Column(Float)
  train_aux_nll = Column(Float)
  train_silh = Column(Float)
  train_mod = Column(Float)
  train_tce = Column(Float)
  train_cae = Column(Float)
  
  test_loss = Column(Float)
  test_acc = Column(Float)
  test_nll = Column(Float)
  test_aux_acc = Column(Float)
  test_aux_nll = Column(Float)
  test_silh = Column(Float)
  test_mod = Column(Float)
  test_tce = Column(Float)
  test_cae = Column(Float)
  
  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)
  
  train_eg_conf = Column(BLOB, nullable=True)
  train_eg_hmax = Column(BLOB, nullable=True)
  train_eg_labels = Column(BLOB, nullable=True)
  test_eg_conf = Column(BLOB, nullable=True)
  test_eg_hmax = Column(BLOB, nullable=True)
  test_eg_labels = Column(BLOB, nullable=True)
db_cascades[GCEpochBase].append(('id', IATEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', IATEpochBase))

iat_epoch_drop_cols = [
  'train_eg_conf', 'train_eg_hmax', 'train_eg_labels',
  'test_eg_conf', 'test_eg_hmax', 'test_eg_labels'
]


class SCPEpochBase(Base):
  __tablename__ = 'epoch_silhouette_community_prediction'
  
  id = Column(Integer, ForeignKey('epochs.id', onupdate='cascade'), primary_key=True)
  experiment_id = Column(Integer, ForeignKey('experiments.id', onupdate='cascade'))
  epoch_num = Column(Integer)
  train_loss = Column(Float)
  train_raw = Column(Float)
  train_tce = Column(Float)
  train_cae = Column(Float)
  val_loss = Column(Float)
  val_raw = Column(Float)
  val_tce = Column(Float)
  val_cae = Column(Float)
  test_loss = Column(Float)
  test_raw = Column(Float)
  test_tce = Column(Float)
  test_cae = Column(Float)
  epoch_perf_time = Column(Interval)
  epoch_time = Column(Interval)
  elapsed_perf_time = Column(Interval)
  elapsed_time = Column(Interval)
  train_example = Column(BLOB)
  test_example = Column(BLOB)
db_cascades[GCEpochBase].append(('id', SCPEpochBase))
db_cascades[GCExperimentBase].append(('experiment_id', SCPEpochBase))
scp_epoch_drop_cols = [
  'train_example', 'test_example'
]

class_to_base = {
  'SupervisedCommunities': SupervisedCommunitiesBase
}

all_tables = list(db_cascades.keys())
for tbls in list(db_cascades.values()):
  all_tables += [tbl[1] for tbl in tbls]
tablenames_to_base = {t.__tablename__: t for t in all_tables}
