drop table if exists samples;
create table samples
(
    id integer primary key,
    sample_table varchar(64),
    n_nodes integer,
    n_edges integer,
    preprocessed_spectral integer,
    preprocessed_distance boolean,
    degree_centrality boolean,
    closeness_centrality boolean,
    betweenness_centrality boolean,
    current_flow_closeness_centrality boolean,
    current_flow_betweenness_centrality boolean,
    approximate_current_flow_betweenness_centrality boolean,
    load_centrality boolean,
    average_neighbor_degree boolean,
    triangles boolean,
    clustering_coeff_tri boolean,
    clustering_coeff_sq boolean,
    eccentricity boolean,
    file varchar(256),
    fresh boolean
);