drop table if exists models;
create table models
(
    id integer primary key,
    model_class varchar(64),
    table_name varchar(64)
);
