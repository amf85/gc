drop table if exists opt_adam;
create table opt_adam
(
    id integer primary key references optimisers(id),
    optimiser_class varchar(64),
    LR float,
    beta_1 float,
    beta_2 float,
    eps float,
    weight_decay float,
    amsgrad boolean
);