drop table if exists epochs;
create table epochs
(
    id integer primary key,
    experiment_class varchar(64),
    table_name varchar(64)
);
