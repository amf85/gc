drop table if exists samples_re;
create table samples_re
(
    id integer references samples(id) on update cascade primary key,
    init_g_size integer,
    motif_min_nodes integer,
    motif_max_nodes integer,
    n_passes integer,
    expansion_prob float,
    init_g_sparsity float,
    motif_sparsity float,
    maintain_motif_degree boolean
);