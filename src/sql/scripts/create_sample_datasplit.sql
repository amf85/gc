drop table if exists sample_datasplit;
create table sample_datasplit
(
    sample_id integer references samples(id),
    datasplit_id integer references datasplits(id),
    primary key (sample_id, datasplit_id)
);
