drop table if exists epoch_direct_community_prediction;
create table epoch_direct_community_prediction
(
    id integer references epochs(id) on update cascade primary key,
    experiment_id integer references experiments(id) on update cascade,
    epoch_num integer,
    train_loss float,
    train_raw float,
    train_accuracy float,
    train_tce float,
    train_cae float,
    val_loss float,
    val_raw float,
    val_accuracy float,
    val_tce float,
    val_cae float,
    test_loss float,
    test_raw float,
    test_accuracy float,
    test_tce float,
    test_cae float,
    epoch_perf_time time,
    epoch_time time,
    elapsed_perf_time time,
    elapsed_time time,
    train_example blob,
    test_example blob
);