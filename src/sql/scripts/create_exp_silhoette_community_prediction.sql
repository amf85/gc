drop table if exists exp_silhouette_community_prediction;
create table exp_silhouette_community_prediction
(
    id integer references experiments(id) on update cascade primary key,
    model_id references models(id) on update cascade,
    optimiser_id references optimisers(id) on update cascade,
    train_split_id references datasplits(id) on update cascade,
    test_split_id references datasplits(id) on update cascade,
    val_size float,
    batch_size integer,
    n_clusters integer,
    lambda_tce float,
    lambda_cae float,
    n_spectral_vectors integer
);