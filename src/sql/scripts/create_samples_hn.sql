drop table if exists samples_hn;
create table samples_hn
(
    id integer references samples(id) on update cascade primary key,
    motif_size integer,
    branching_factor integer,
    depth integer
);