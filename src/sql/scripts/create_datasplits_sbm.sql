drop table if exists datasplits_sbm;
create table datasplits_sbm
(
    id integer references datasplits(id) on update cascade primary key,
    split varchar(32),
    n_samples integer,

    min_nodes integer,
    max_nodes integer,
    min_clusters integer,
    max_clusters integer,
    priors_a float,
    priors_b float,
    intra_connect_min float,
    intra_connect_max float,
    inter_connect_min float,
    inter_connect_max float,

    directory varchar(256)
);
