drop table if exists model_clustering_gnn;
create table model_clustering_gnn
(
    id integer primary key references models(id),
    input_dim integer,
    has_identical_layers boolean,
    mid_layer_output_dims varchar(256),
    output_dim integer,
    activation varchar(32),
    self_loops boolean,
    params blob
);