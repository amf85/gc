drop table if exists experiments;
create table experiments
(
    id integer primary key,
    experiment_class varchar(64),
    table_name varchar(64)
);
