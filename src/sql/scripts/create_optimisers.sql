drop table if exists optimisers;
create table optimisers
(
    id integer primary key,
    optimiser_class varchar(64),
    table_name varchar(64)
);