drop table if exists datasplits_re;
create table datasplits_re
(
    id integer references datasplits(id) on update cascade primary key,
    split varchar(32),
    n_samples integer,
    init_g_sizes varchar(128),
    motif_min_nodes integer,
    motif_max_nodes integer,
    n_passes varchar(64),
    expansion_prob float,
    init_g_sparsity float,
    motif_sparsity float,
    maintain_motif_degree boolean,
    directory varchar(256)
);
