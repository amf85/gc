drop table if exists datasplits_hn;
create table datasplits_hn
(
    id integer references datasplits(id) on update cascade primary key,
    split varchar(32),
    n_samples integer,
    motif_sizes varchar(256),
    branching_factors varchar(64),
    depths varchar(64),
    directory varchar(256)
);
