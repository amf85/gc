drop table if exists samples_re;
create table samples_re
(
    id integer references samples(id) on update cascade primary key,
    n_nodes integer,
    n_clusters integer,
    priors_a float,
    priors_b float,
    intra_connect_min float,
    intra_connect_max float,
    inter_connect_min float,
    inter_connect_max float
);