drop table if exists datasplits;
create table datasplits
(
    id integer primary key,
    datasplit_class varchar(64),
    table_name varchar(64)
);

drop table if exists samples;
create table samples
(
    id integer primary key,
    n_nodes integer,
    n_edges integer,
    preprocessed_spectral integer,
    preprocessed_distance boolean,
    degree_centrality boolean,
    closeness_centrality boolean,
    betweenness_centrality boolean,
    current_flow_closeness_centrality boolean,
    current_flow_betweenness_centrality boolean,
    approximate_current_flow_betweenness_centrality boolean,
    load_centrality boolean,
    average_neighbor_degree boolean,
    triangles boolean,
    clustering_coeff_tri boolean,
    clustering_coeff_sq boolean,
    eccentricity boolean,
    file varchar(256)
);

drop table if exists sample_datasplit;
create table sample_datasplit
(
    sample_id integer references samples(id),
    datasplit_id integer references datasplits(id),
    primary key (sample_id, datasplit_id)
);


drop table if exists models;
create table models
(
    id integer primary key,
    model_class varchar(64),
    table_name varchar(64)
);

drop table if exists optimisers;
create table optimisers
(
    id integer primary key,
    optimiser_class varchar(64),
    table_name varchar(64)
);

drop table if exists experiments;
create table experiments
(
    id integer primary key,
    experiment_class varchar(64),
    table_name varchar(64)
);

drop table if exists epochs;
create table epochs
(
    id integer primary key,
    epoch_class varchar(64),
    table_name varchar(64)
);







drop table if exists datasplits_hn;
create table datasplits_hn
(
    id integer primary key references datasplits(id),
    split varchar(32),
    n_samples integer,
    motif_sizes varchar(256),
    branching_factors varchar(64),
    depths varchar(64),
    directory varchar(256)
);

drop table if exists datasplits_re;
create table datasplits_re
(
    id integer primary key references datasplits(id),
    split varchar(32),
    n_samples integer,
    init_g_sizes varchar(128),
    motif_min_nodes integer,
    motif_max_nodes integer,
    n_passes varchar(64),
    expansion_prob float,
    init_g_sparsity float,
    motif_sparsity float,
    maintain_motif_degree boolean,
    directory varchar(256)
);


drop table if exists samples_hn;
create table samples_hn
(
    id integer primary key references samples(id),
    motif_size integer,
    branching_factor integer,
    depth integer
);

drop table if exists samples_re;
create table samples_re
(
    id integer primary key,
    split_id integer references datasplits_re(id),
    init_g_size integer,
    n_passes integer
);

drop table if exists model_clustering_gnn;
create table model_clustering_gnn
(
    id integer primary key references models(id),
    input_dim integer,
    has_identical_layers boolean,
    mid_layer_output_dims varchar(256),
    output_dim integer,
    activation varchar(32),
    self_loops boolean,
    params blob
);

drop table if exists opt_adam;
create table opt_adam
(
    id integer primary key references optimisers(id),
    optimiser_class varchar(64),
    LR float,
    beta_1 float,
    beta_2 float,
    eps float,
    weight_decay float,
    amsgrad boolean
);

drop table if exists exp_direct_community_prediction;
create table exp_direct_community_prediction
(
    id integer primary key,
    model_id references models(id),
    optimiser_id references optimisers(id),
    train_split references datasplits(id),
    val_size float,
    batch_size integer
);

drop table if exists epoch_direct_community_prediction;
create table epoch_direct_community_prediction
(
    id integer references epochs(id),
    experiment_id integer references experiments(id),
    epoch_num integer,
    train_loss float,
    train_accuracy float,
    val_loss float,
    val_accuracy float,
    test_loss float,
    test_accuracy float,
    is_checkpoint boolean,
    train_example blob,
    test_example blob
);

drop table if exists exp_silhoette_community_prediction;
create table exp_silhoette_community_prediction
(
    id integer primary key,
    model_id references models(id),
    optimiser_id references optimisers(id),
    train_split_id references datasplits(id),
    test_split_id references datasplits(id),
    val_size float,
    batch_size integer,
    n_clusters integer,
    lambda_TCE float,
    n_spectral_vectors integer,
    include_binary_node_feature boolean,
    label varchar(64)
);