drop table if exists datasplits;
create table datasplits
(
    id integer primary key,
    datasplit_class varchar(64),
    table_name varchar(64)
);
