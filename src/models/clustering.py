import pickle

import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Dropout
from torch_geometric.nn import GraphConv
from torch_geometric.utils import add_remaining_self_loops, add_self_loops

from models.gc_model import GCModel
from sql.sql_base import ClusteringGNNBase
from functools import partial
from copy import copy


class ClusteringGNN(GCModel):
  @property
  def db_table(self):
    return 'model_clustering_gnn'
  
  @property
  def model_class(self):
    return 'ClusteringGNN'
  
  class ClusteringGNNParams(GCModel.GCModelParams):
    def __init__(self,
                 input_dim,
                 apply_input_dropout,
                 gnn_out_dims,
                 conv_fn=None,
                 conv_conf=None,
                 activation=F.relu,
                 self_loops=True,
                 use_batchnorm=True,
                 dropout=0.0,
                 ):
      super(ClusteringGNN.ClusteringGNNParams, self).__init__()
      self.input_dim = input_dim
      self.apply_input_dropout = apply_input_dropout
      self.gnn_out_dims = gnn_out_dims

      self.gnn_conv_conf = copy(conv_conf)
      self.gnn_aggr_hidden = self.gnn_conv_conf.pop('gnn_aggr_hidden', None)
      self.gnn_n_heads = self.gnn_conv_conf.pop('edge_gnn_n_heads', 1)
      self.gnn_conv_fn = partial(conv_fn, **self.gnn_conv_conf)

      self.activation = activation
      self.self_loops = self_loops
      self.use_batchnorm = use_batchnorm
      self.dropout = dropout
  
  def __init__(self, gcdb, model_dir, params: ClusteringGNNParams):
    super(ClusteringGNN, self).__init__(model_dir=model_dir, params=params, gcdb=gcdb)
    self.gnn_dimensions = [params.input_dim] + params.gnn_out_dims

    self.gnn_out_dims = params.gnn_out_dims
    if params.gnn_aggr_hidden == 'concat':
      self.gnn_in_dims = [params.input_dim] + [d * params.gnn_n_heads for d in params.gnn_out_dims]
    else:
      self.gnn_in_dims = [params.input_dim] + params.gnn_out_dims

    self.gnn_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    self.dropouts = nn.ModuleList()
    for L in range(len(params.gnn_out_dims)):
      self.dropouts.append(Dropout(p=params.dropout))
      self.gnn_layers.append(self.params.gnn_conv_fn(in_feats=self.gnn_in_dims[L],
                                                     out_feats=self.gnn_out_dims[L]))
      if params.use_batchnorm:
        self.batchnorms.append(BatchNorm1d(self.gnn_in_dims[L+1]))
    self.activation = params.activation
    self.self_loops = params.self_loops
    # self.cluster_reg_weights = nn.Parameter(torch.empty(self.gnn_dimensions[-1],), requires_grad=True)
    # nn.init.xavier_uniform_(self.cluster_reg_weights)
    # nn.init.uniform_(self.cluster_reg_weights, a=0.0, b=1.0)

  def forward(self, dgl_batch, softmax_output=True, device=torch.device('cpu')):
    if self.self_loops:
      dgl_batch = dgl.add_self_loop(dgl_batch)
    X = dgl_batch.ndata['X']

    for L in range(len(self.gnn_layers)):
      if L != 0 or self.params.apply_input_dropout:
        X = self.dropouts[L](X)
      X = self.gnn_layers[L](dgl_batch, X)

      if self.params.gnn_aggr_hidden == 'concat':
        X = torch.cat([X[:, t, :] for t in range(self.params.gnn_n_heads)], dim=1)
      elif self.params.gnn_aggr_hidden == 'mean':
        X = torch.mean(X, dim=1)
      elif self.params.gnn_aggr_hidden == 'sum':
        X = torch.sum(X, dim=1)
      elif self.params.gnn_aggr_hidden == 'max':
        X = torch.max(X, dim=1).values

      X = self.params.activation(X)
      if self.params.use_batchnorm:
        X = self.batchnorms[L](X)

    if softmax_output:
      X = F.softmax(X, dim=1)

    return X, None
  
  # def forward(self, X, A_edge, A_attr, softmax_output=True):
  #   if self.params.unsqueeze_X:
  #     X = torch.unsqueeze(X, dim=0)
  #   if self.add_self_loops:
  #     # A_edge, A_attr = add_remaining_self_loops(A_edge, A_attr)
  #     A_edge, A_attr = add_self_loops(A_edge, A_attr)
  #   for L in range(len(self.gnn_layers)):
  #     if L != 0:
  #       X = self.dropouts[L](X)
  #     X = self.gnn_layers[L](X, A_edge, A_attr)
  #     if L < len(self.gnn_layers) - 1:
  #       X = self.activation(X)
  #     if self.params.use_batchnorm_gnn:
  #       X = self.batchnorms[L](X)
  #   # X = self.cluster_reg_weights * X
  #   if self.params.unsqueeze_X:
  #     X = torch.squeeze(X)
  #   if softmax_output:
  #     X = F.softmax(X, dim=1)
  #     # X = torch.softmax(X, dim=1)
  #
  #   return X, None
  
  def add_sql_entry(self):
    has_identical_layers = all(L == self.params.gnn_out_dims[0]
                               for L in self.params.gnn_out_dims[:-1])
    class_base = ClusteringGNNBase(
      id=self.id,
      input_dim=self.params.input_dim,
      has_identical_layers=has_identical_layers,
      mid_layer_output_dims=self.params.gnn_out_dims[0] if has_identical_layers else
      ','.join(map(str, self.params.gnn_out_dims[:-1])),
      output_dim=self.params.gnn_out_dims[-1],
      activation=self.params.activation.__name__,
      self_loops=self.params.self_loops,
      dropout=self.params.dropout,
      use_batchnorm=self.params.use_batchnorm
    )
    session = self.gcdb.local_sess()
    session.add(class_base)
    session.commit()
