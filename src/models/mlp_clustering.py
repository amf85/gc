import pickle

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Dropout, Linear
from torch_geometric.nn import GraphConv
from torch_geometric.utils import add_remaining_self_loops, add_self_loops

from models.gc_model import GCModel
from sql.sql_base import ClusteringMLPBase


class ClusteringMLP(GCModel):
  @property
  def db_table(self):
    return 'model_clustering_mlp'
  
  @property
  def model_class(self):
    return 'ClusteringMLP'
  
  class ClusteringMLPParams(GCModel.GCModelParams):
    def __init__(self,
                 input_dim,
                 apply_input_dropout,

                 layer_out_dims,
                 activation=F.relu,
                 use_batchnorm=True,
                 dropout=0.0
                 ):
      super(ClusteringMLP.ClusteringMLPParams, self).__init__()
      self.input_dim = input_dim
      self.layer_out_dims = layer_out_dims
      self.activation = activation
      # self.add_self_loops = add_self_loops
      self.use_batchnorm = use_batchnorm
      self.dropout = dropout
  
  def __init__(self, gcdb, model_dir, params: ClusteringMLPParams):
    super(ClusteringMLP, self).__init__(model_dir=model_dir, params=params, gcdb=gcdb)
    self.layer_dimensions = [params.input_dim] + params.layer_out_dims
    self.mlp_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    self.dropouts = nn.ModuleList()
    for L in range(len(params.layer_out_dims)):
      self.dropouts.append(Dropout(p=params.dropout))
      # self.mlp_layers.append(GraphConv(self.layer_dimensions[L], self.layer_dimensions[L + 1]))
      self.mlp_layers.append(Linear(self.layer_dimensions[L], self.layer_dimensions[L + 1]))
      if params.use_batchnorm:
        self.batchnorms.append(BatchNorm1d(self.layer_dimensions[L + 1]))
    self.activation = params.activation
    # self.add_self_loops = params.add_self_loops
    # self.cluster_reg_weights = nn.Parameter(torch.empty(self.gnn_dimensions[-1],), requires_grad=True)
    # nn.init.xavier_uniform_(self.cluster_reg_weights)
    # nn.init.uniform_(self.cluster_reg_weights, a=0.0, b=1.0)
  
  def forward(self, dgl_batch, softmax_output=True):
    X = dgl_batch.ndata['X']
    for L in range(len(self.mlp_layers)):
      if L != 0:
        X = self.dropouts[L](X)
      X = self.mlp_layers[L](X)#
      if L < len(self.mlp_layers) - 1:
        X = self.activation(X)
      if self.params.use_batchnorm:
        X = self.batchnorms[L](X)
    # X = self.cluster_reg_weights * X
    if softmax_output:
      X = F.softmax(X, dim=1)
      # X = torch.softmax(X, dim=1)
    return X, None
  
  def add_sql_entry(self):
    has_identical_layers = all(L == self.params.layer_out_dims[0]
                               for L in self.params.layer_out_dims[:-1])
    class_base = ClusteringMLPBase(
      id=self.id,
      input_dim=self.params.input_dim,
      has_identical_layers=has_identical_layers,
      mid_layer_output_dims=self.params.layer_out_dims[0] if has_identical_layers else
      ','.join(map(str, self.params.layer_out_dims[:-1])),
      output_dim=self.params.layer_out_dims[-1],
      activation=self.params.activation.__name__,
      # self_loops=self.params.add_self_loops,
      dropout=self.params.dropout,
      use_batchnorm=self.params.use_batchnorm
    )
    session = self.gcdb.local_sess()
    session.add(class_base)
    session.commit()
