import pickle
import json
from copy import copy
from functools import partial
import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Dropout, Linear
from torch_geometric.nn import GraphConv
from torch_geometric.utils import add_remaining_self_loops, add_self_loops, remove_self_loops

from models.gc_model import GCModel
from sql.sql_base import ClusteringGNNBase, EdgeClusteringGNNBase


class EdgeClusteringGNN(GCModel):
  @property
  def db_table(self):
    return 'model_edge_clustering'
  
  @property
  def model_class(self):
    return 'EdgeClusteringGNN'
  
  class EdgeClusteringGNNParams(GCModel.GCModelParams):
    def __init__(self,
                 input_dim,
                 apply_input_dropout,
                 add_self_loops,

                 gnn_out_dims,
                 gnn_conv_fn,
                 gnn_conv_conf,

                 edge_pred_out_dims,

                 batchnorm_gnn,
                 batchnorm_edge_pred,
                 activation_gnn,
                 activation_edge_pred,
                 dropout_gnn,
                 dropout_edge_pred,

                 ):
      super(EdgeClusteringGNN.EdgeClusteringGNNParams, self).__init__()
      self.input_dim = input_dim
      self.apply_input_dropout = apply_input_dropout
      self.add_self_loops = add_self_loops

      self.gnn_out_dims = gnn_out_dims
      self.gnn_conv_conf = copy(gnn_conv_conf)
      self.gnn_aggr_hidden = self.gnn_conv_conf.pop('gnn_aggr_hidden', None)
      self.gnn_n_heads = self.gnn_conv_conf.pop('gnn_n_heads', 1)
      self.gnn_conv_fn = partial(gnn_conv_fn, **self.gnn_conv_conf)

      self.edge_pred_out_dims = edge_pred_out_dims

      self.batchnorm_gnn = batchnorm_gnn
      self.batchnorm_edge_pred = batchnorm_edge_pred
      self.activation_gnn = activation_gnn
      self.activation_edge_pred = activation_edge_pred
      self.dropout_gnn = dropout_gnn
      self.dropout_edge_pred = dropout_edge_pred

  
  def __init__(self, gcdb, model_dir, params: EdgeClusteringGNNParams):
    super(EdgeClusteringGNN, self).__init__(model_dir=model_dir, params=params, gcdb=gcdb)

    self.gnn_out_dims = params.gnn_out_dims
    self.gnn_in_dims = [params.input_dim]
    if params.gnn_aggr_hidden == 'concat':
      self.gnn_in_dims += [d * params.gnn_n_heads for d in params.gnn_out_dims]
    else:
      self.gnn_in_dims += params.gnn_out_dims

    self.gnn_layers = nn.ModuleList()
    self.gnn_dropouts = nn.ModuleList()
    self.gnn_batchnorms = nn.ModuleList()
    for L in range(len(self.gnn_out_dims)):
      self.gnn_dropouts.append(Dropout(p=params.dropout_gnn))
      self.gnn_layers.append(params.gnn_conv_fn(
        in_feats=self.gnn_in_dims[L], out_feats=self.gnn_out_dims[L]))
      self.gnn_batchnorms.append(BatchNorm1d(self.gnn_in_dims[L+1]))

    self.edge_predictor = EdgePredictor(
      in_dim=self.gnn_in_dims[-1],
      out_dims=params.edge_pred_out_dims,
      use_batchnorm=params.batchnorm_edge_pred,
      dropout=params.dropout_edge_pred,
      activation=params.activation_edge_pred
    )

  def forward(self, dgl_batch, softmax_output=True):
    batch_num_nodes = dgl_batch.batch_num_nodes()
    batch_num_edges = dgl_batch.batch_num_edges()
    if self.params.add_self_loops:
      dgl_batch = dgl.add_self_loop(dgl_batch)
    X = dgl_batch.ndata['X']
    for L in range(len(self.gnn_layers)):
      if L != 0 or self.params.apply_input_dropout:
        X = self.gnn_dropouts[L](X)
      X = self.gnn_layers[L](dgl_batch, X)

      if self.params.gnn_aggr_hidden == 'concat':
        X = torch.cat([X[:, t, :] for t in range(self.params.gnn_n_heads)], dim=1)
      elif self.params.gnn_aggr_hidden == 'mean':
        X = torch.mean(X, dim=1)
      elif self.params.gnn_aggr_hidden == 'sum':
        X = torch.sum(X, dim=1)
      elif self.params.gnn_aggr_hidden == 'max':
        X = torch.max(X, dim=1).values

      X = self.params.activation_gnn(X)
      if self.params.batchnorm_gnn:
        X = self.gnn_batchnorms[L](X)

    edge_score = self.edge_predictor(dgl_batch, X)
    if softmax_output:
      edge_score = F.softmax(edge_score, dim=1)

    # self loops always added to end, so any original self loops kept this way
    n_self_loops_added = dgl_batch.batch_num_edges().item() - torch.sum(batch_num_edges).item()
    return edge_score[:-n_self_loops_added, :], None

  
  def add_sql_entry(self):
    class_base = EdgeClusteringGNNBase(
      id=self.id,
      input_dim=self.params.input_dim,
      add_self_loops=self.params.add_self_loops,
      
      gnn_out_dims=','.join(map(str, self.params.gnn_out_dims)),
      gnn_conv_fn=self.params.gnn_conv_fn.func.__name__,
      gnn_conv_conf=json.dumps(self.params.gnn_conv_conf, indent=2),

      edge_pred_out_dims=','.join(map(str, self.params.edge_pred_out_dims)),

      batchnorm_gnn=self.params.batchnorm_gnn,
      batchnorm_edge_pred=self.params.batchnorm_edge_pred,
      activation_gnn=self.params.activation_gnn.__name__,
      activation_edge_pred=self.params.activation_edge_pred.__name__,
      dropout_gnn=self.params.dropout_gnn,
      dropout_edge_pred=self.params.dropout_edge_pred,

    )
    session = self.gcdb.local_sess()
    session.add(class_base)
    session.commit()


class EdgePredictor(nn.Module):
  def __init__(self, in_dim, out_dims, use_batchnorm, dropout, activation):
    super(EdgePredictor, self).__init__()
    self.use_batchnorm = use_batchnorm
    self.activation = activation
    self.dims = [in_dim * 2] + out_dims
    self.mlp_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    self.dropouts = nn.ModuleList()
    for L in range(len(self.dims)-1):
      self.dropouts.append(Dropout(p=dropout))
      self.mlp_layers.append(nn.Linear(self.dims[L], self.dims[L+1]))
      self.batchnorms.append(BatchNorm1d(self.dims[L+1]))


  def apply_edges(self, edges):
    h = torch.cat([edges.src['h'], edges.dst['h']], 1)
    for L in range(len(self.mlp_layers)):
      h = self.dropouts[L](h)
      h = self.mlp_layers[L](h)
      if L < len(self.mlp_layers) - 1:
        h = self.activation(h)
        if self.use_batchnorm:
          h = self.batchnorms[L](h)
    return {'pred': h.squeeze(1)}


  def forward(self, g, h):
    with g.local_scope():
      g.ndata['h'] = h
      g.apply_edges(self.apply_edges)
      return g.edata['pred']