from abc import abstractmethod
from typing import Any
import torch
import torch.nn as nn

from sql.sql_base import GCModelBase
from util.util import validate_directory


class GCModel(nn.Module):
  @property
  def models_table(self):
    return 'models'
  
  @property
  @abstractmethod
  def model_class(self):
    ...
  
  @property
  @abstractmethod
  def db_table(self):
    ...
  
  class GCModelParams(object):
    def __init__(self):
      super().__init__()
  
  def __init__(self, model_dir, gcdb, params: GCModelParams):
    super(GCModel, self).__init__()
    self.model_dir = model_dir
    self.gcdb = gcdb
    self.params = params
    self.id = self.add_models_sql_entry()
    self.add_sql_entry()
  
  def save_model_state(self, directory):
    validate_directory(directory)
    torch.save(self.state_dict(), f'{directory}/model_state.pyt')
  
  def load_model_state(self, directory):
    self.load_model_state(torch.load(f'{directory}/model_state.pyt'))
  
  @abstractmethod
  def forward(self, *args: Any):
    ...
  
  @abstractmethod
  def add_sql_entry(self):
    ...
  
  @abstractmethod
  def add_models_sql_entry(self):
    base = GCModelBase(
      model_class=self.model_class,
      table_name=self.db_table,
      fresh=True,
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.flush()
    id = base.id
    session.commit()
    return id
