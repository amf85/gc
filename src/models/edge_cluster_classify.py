import pickle
import json
import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Dropout
from torch_geometric.nn import GraphConv
from torch_geometric.utils import add_remaining_self_loops, add_self_loops

from models.gc_model import GCModel
from sql.sql_base import EdgeClusterClassifyBase
from functools import partial
from copy import copy


class EdgeClusterClassify(GCModel):
  @property
  def db_table(self):
    return 'model_edge_cluster_classify'
  
  @property
  def model_class(self):
    return 'EdgeClusterClassify'
  
  class EdgeClusterClassifyParams(GCModel.GCModelParams):
    def __init__(self,
                 input_feature_dim,
                 apply_input_dropout,
                 add_self_loops,

                 edge_gnn_out_dims,
                 edge_gnn_conv_fn,
                 edge_gnn_conv_conf,

                 edge_pred_out_dims,
                 classify_from_hidden_representation,

                 clsf_gnn_out_dims,
                 clsf_gnn_conv_fn,
                 clsf_gnn_conv_conf,

                 output_aggr,
                 clsf_mlp_out_dims,

                 batchnorm_edge_gnn,
                 batchnorm_edge_pred,
                 batchnorm_clsf_gnn,
                 batchnorm_clsf_mlp,
                 activation_edge_gnn,
                 activation_edge_pred,
                 activation_edge_pred_score,
                 activation_clsf_gnn,
                 activation_clsf_mlp,
                 dropout_edge_gnn,
                 dropout_edge_pred,
                 dropout_clsf_gnn,
                 dropout_clsf_mlp,
                 ):
      super(EdgeClusterClassify.EdgeClusterClassifyParams, self).__init__()
      self.input_feature_dim = input_feature_dim
      self.apply_input_dropout = apply_input_dropout
      self.add_self_loops = add_self_loops

      self.edge_gnn_out_dims = edge_gnn_out_dims
      self.edge_gnn_conv_conf = copy(edge_gnn_conv_conf)
      self.edge_gnn_aggr_hidden = self.edge_gnn_conv_conf.pop('edge_gnn_aggr_hidden', None)
      self.edge_gnn_n_heads = self.edge_gnn_conv_conf.pop('edge_gnn_n_heads', 1)
      self.edge_gnn_conv_fn = partial(edge_gnn_conv_fn, **self.edge_gnn_conv_conf)

      self.classify_from_hidden_representation = classify_from_hidden_representation
      self.edge_pred_out_dims = edge_pred_out_dims

      self.clsf_gnn_out_dims = clsf_gnn_out_dims
      self.clsf_gnn_conv_conf = copy(clsf_gnn_conv_conf)
      self.clsf_gnn_aggr_hidden = self.clsf_gnn_conv_conf.pop('clsf_gnn_aggr_hidden', None)
      self.clsf_gnn_n_heads = self.clsf_gnn_conv_conf.pop('clsf_gnn_n_heads', 1)
      self.clsf_gnn_conv_fn = partial(clsf_gnn_conv_fn, **self.clsf_gnn_conv_conf)

      self.output_aggr = output_aggr
      self.clsf_mlp_out_dims = clsf_mlp_out_dims

      self.batchnorm_edge_gnn = batchnorm_edge_gnn
      self.batchnorm_edge_pred = batchnorm_edge_pred
      self.batchnorm_clsf_gnn = batchnorm_clsf_gnn
      self.batchnorm_clsf_mlp = batchnorm_clsf_mlp
      self.activation_edge_gnn = activation_edge_gnn
      self.activation_edge_pred = activation_edge_pred
      self.activation_edge_pred_score = activation_edge_pred_score
      self.activation_clsf_gnn = activation_clsf_gnn
      self.activation_clsf_mlp = activation_clsf_mlp
      self.dropout_edge_gnn = dropout_edge_gnn
      self.dropout_edge_pred = dropout_edge_pred
      self.dropout_clsf_gnn = dropout_clsf_gnn
      self.dropout_clsf_mlp = dropout_clsf_mlp

  
  def __init__(self, gcdb, model_dir, params: EdgeClusterClassifyParams):
    super(EdgeClusterClassify, self).__init__(model_dir=model_dir, params=params, gcdb=gcdb)
    self.add_self_loops = params.add_self_loops

    self.edge_gnn_out_dims = params.edge_gnn_out_dims
    if params.edge_gnn_aggr_hidden == 'concat':
      self.edge_gnn_in_dims = [params.input_feature_dim] + \
                              [d*params.edge_gnn_n_heads for d in params.edge_gnn_out_dims]
    else:
      self.edge_gnn_in_dims = [params.input_feature_dim] + params.edge_gnn_out_dims
    self.edge_gnn_dimensions = [params.input_feature_dim] + params.edge_gnn_out_dims
    self.edge_gnn_layers = nn.ModuleList()
    self.edge_gnn_dropouts = nn.ModuleList()
    self.edge_gnn_batchnorms = nn.ModuleList()
    self.batchnorm_edge_gnn = params.batchnorm_edge_gnn
    for L in range(len(self.edge_gnn_dimensions)-1):
      self.edge_gnn_dropouts.append(Dropout(p=params.dropout_edge_gnn))
      self.edge_gnn_layers.append(self.params.edge_gnn_conv_fn(
        in_feats=self.edge_gnn_in_dims[L], out_feats=self.edge_gnn_out_dims[L]))
      self.edge_gnn_batchnorms.append(BatchNorm1d(self.edge_gnn_in_dims[L+1]))

    self.edge_predictor = EdgePredictor(
      in_dim=self.edge_gnn_in_dims[-1],
      out_dims=params.edge_pred_out_dims,
      use_batchnorm=params.batchnorm_edge_pred,
      dropout=params.dropout_edge_pred,
      activation=params.activation_edge_pred
    )

    self.classify_from_hidden_representation = params.classify_from_hidden_representation

    if self.classify_from_hidden_representation:
      clsf_in_dim = self.edge_gnn_in_dims[-1]
    else:
      clsf_in_dim = params.input_feature_dim
    self.clsf_gnn_out_dims = params.clsf_gnn_out_dims
    if params.clsf_gnn_aggr_hidden == 'concat':
      self.clsf_gnn_in_dims = [clsf_in_dim] + \
                              [d * params.clsf_gnn_n_heads for d in params.clsf_gnn_out_dims]
    else:
      self.clsf_gnn_in_dims = [clsf_in_dim] + params.clsf_gnn_out_dims
    self.clsf_gnn_dimensions = [clsf_in_dim] + params.clsf_gnn_out_dims
    self.clsf_gnn_layers = nn.ModuleList()
    self.clsf_gnn_dropouts = nn.ModuleList()
    self.clsf_gnn_batchnorms = nn.ModuleList()
    self.batchnorm_clsf_gnn = params.batchnorm_clsf_gnn
    for L in range(len(self.clsf_gnn_dimensions) - 1):
      self.clsf_gnn_dropouts.append(Dropout(p=params.dropout_clsf_gnn))
      self.clsf_gnn_layers.append(self.params.clsf_gnn_conv_fn(
        in_feats=self.clsf_gnn_in_dims[L], out_feats=self.clsf_gnn_out_dims[L]))
      self.clsf_gnn_batchnorms.append(BatchNorm1d(self.clsf_gnn_in_dims[L + 1]))

    self.clsf_mlp_dimensions = [self.clsf_gnn_out_dims[-1]] + params.clsf_mlp_out_dims
    self.clsf_mlp_layers = nn.ModuleList()
    self.clsf_mlp_dropouts = nn.ModuleList()
    self.clsf_mlp_batchnorms = nn.ModuleList()
    self.batchnorm_clsf_mlp = params.batchnorm_clsf_mlp
    for L in range(len(self.clsf_mlp_dimensions)-1):
      self.clsf_mlp_dropouts.append(Dropout(p=params.dropout_clsf_mlp))
      self.clsf_mlp_layers.append(nn.Linear(self.clsf_mlp_dimensions[L], self.clsf_mlp_dimensions[L+1]))
      self.clsf_mlp_batchnorms.append(BatchNorm1d(self.clsf_mlp_dimensions[L+1]))


  def forward(self, dgl_batch, softmax_output=True, device=torch.device('cpu')):
    batch_num_nodes = dgl_batch.batch_num_nodes()
    if self.add_self_loops:
      dgl_batch = dgl.add_self_loop(dgl_batch)
    X = dgl_batch.ndata['X']

    for L in range(len(self.edge_gnn_layers)):
      if L != 0 or self.params.apply_input_dropout:
        X = self.edge_gnn_dropouts[L](X)
      X = self.edge_gnn_layers[L](dgl_batch, X)

      if self.params.edge_gnn_aggr_hidden == 'concat':
        X = torch.cat([X[:, t, :] for t in range(self.params.edge_gnn_n_heads)], dim=1)
      elif self.params.edge_gnn_aggr_hidden == 'mean':
        X = torch.mean(X, dim=1)
      elif self.params.edge_gnn_aggr_hidden == 'sum':
        X = torch.sum(X, dim=1)
      elif self.params.edge_gnn_aggr_hidden == 'max':
        X = torch.max(X, dim=1).values

      X = self.params.activation_edge_gnn(X)
      if self.params.batchnorm_edge_gnn:
        X = self.edge_gnn_batchnorms[L](X)

    edge_score = self.edge_predictor(dgl_batch, X)
    edge_score = self.params.activation_edge_pred_score(edge_score)

    if not self.classify_from_hidden_representation:
      X = dgl_batch.ndata['X']

    for L in range(len(self.clsf_gnn_layers)):
      if L != 0 or self.classify_from_hidden_representation:
        X = self.clsf_gnn_dropouts[L](X)
      # X = self.clsf_gnn_layers[L](dgl_batch, X, edge_weight=None)
      X = self.clsf_gnn_layers[L](dgl_batch, X, edge_weight=edge_score)

      if self.params.clsf_gnn_aggr_hidden == 'concat':
        X = torch.cat([X[:, t, :] for t in range(self.params.clsf_gnn_n_heads)], dim=1)
      elif self.params.clsf_gnn_aggr_hidden == 'mean':
        X = torch.mean(X, dim=1)
      elif self.params.clsf_gnn_aggr_hidden == 'sum':
        X = torch.sum(X, dim=1)
      elif self.params.clsf_gnn_aggr_hidden == 'max':
        X = torch.max(X, dim=1).values

      X = self.params.activation_clsf_gnn(X)
      if self.params.batchnorm_clsf_gnn:
        X = self.clsf_gnn_batchnorms[L](X)

    dgl_batch.ndata['X'] = X
    dgl_batch = dgl_batch.set_batch_num_nodes(batch_num_nodes)
    dgl_batch.set_batch_num_nodes(batch_num_nodes)

    if self.params.output_aggr == 'sum':
      X = dgl.sum_nodes(dgl_batch, 'X')
    elif self.params.output_aggr == 'mean':
      X = dgl.mean_nodes(dgl_batch, 'X')
    elif self.params.output_aggr == 'max':
      X = dgl.max_nodes(dgl_batch, 'X')
    else:
      raise NotImplementedError()

    for L in range(len(self.clsf_mlp_layers)):
      X = self.clsf_mlp_dropouts[L](X)
      X = self.clsf_mlp_layers[L](X)
      if L < len(self.clsf_mlp_layers) - 1:
        X = self.params.activation_clsf_mlp(X)
        if self.params.batchnorm_clsf_mlp:
          X = self.clsf_mlp_batchnorms[L](X)

    # if self.params.unsqueeze_X:
    #   X = torch.squeeze(X)
    if softmax_output:
      X = F.softmax(X, dim=1)

    return X, edge_score

  
  def add_sql_entry(self):

    class_base = EdgeClusterClassifyBase(
      id=self.id,
      input_dim=self.params.input_dim,
      add_self_loops=self.params.add_self_loops,
      edge_gnn_out_dims=','.join(map(str, self.params.edge_gnn_out_dims)),
      edge_gnn_conv_fn=self.params.edge_gnn_conv_fn.func.__name__,
      edge_gnn_conv_conf=json.dumps(self.params.edge_gnn_conv_conf, indent=2),
      edge_pred_out_dims=','.join(map(str, self.params.edge_pred_out_dims)),
      classify_from_hidden_representation=self.params.classify_from_hidden_representation,
      clsf_gnn_out_dims=','.join(map(str, self.params.clsf_gnn_out_dims)),
      clsf_gnn_conv_fn=self.params.clsf_gnn_conv_fn.func.__name__,
      clsf_gnn_conv_conf=json.dumps(self.params.clsf_gnn_conv_conf, indent=2),
      output_aggr=self.params.output_aggr,
      clsf_mlp_out_dims=','.join(map(str, self.params.clsf_mlp_out_dims)),
      batchnorm_edge_gnn=self.params.batchnorm_edge_gnn,
      batchnorm_edge_pred=self.params.batchnorm_edge_pred,
      batchnorm_clsf_gnn=self.params.batchnorm_clsf_gnn,
      batchnorm_clsf_mlp=self.params.batchnorm_clsf_mlp,
      activation_edge_gnn=self.params.activation_edge_gnn.__name__,
      activation_edge_pred=self.params.activation_edge_pred.__name__,
      activation_edge_pred_score=self.params.activation_edge_pred_score.__name__,
      activation_clsf_gnn=self.params.activation_clsf_gnn.__name__,
      activation_clsf_mlp=self.params.activation_clsf_mlp.__name__,
      dropout_edge_gnn=self.params.dropout_edge_gnn,
      dropout_edge_pred=self.params.dropout_edge_pred,
      dropout_clsf_gnn=self.params.dropout_clsf_gnn,
      dropout_clsf_mlp=self.params.dropout_clsf_mlp,
    )
    session = self.gcdb.local_sess()
    session.add(class_base)
    session.commit()


class EdgePredictor(nn.Module):
  def __init__(self, in_dim, out_dims, use_batchnorm, dropout, activation):
    super(EdgePredictor, self).__init__()
    self.use_batchnorm = use_batchnorm
    self.activation = activation
    self.dims = [in_dim * 2] + out_dims
    self.mlp_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    self.dropouts = nn.ModuleList()
    for L in range(len(self.dims)-1):
      self.dropouts.append(Dropout(p=dropout))
      self.mlp_layers.append(nn.Linear(self.dims[L], self.dims[L+1]))
      self.batchnorms.append(BatchNorm1d(self.dims[L+1]))


  def apply_edges(self, edges):
    h = torch.cat([edges.src['h'], edges.dst['h']], 1)
    for L in range(len(self.mlp_layers)):
      h = self.dropouts[L](h)
      h = self.mlp_layers[L](h)
      if L < len(self.mlp_layers) - 1:
        h = self.activation(h)
        if self.use_batchnorm:
          h = self.batchnorms[L](h)
    return {'score': h.squeeze(1)}


  def forward(self, g, h):
    with g.local_scope():
      g.ndata['h'] = h
      g.apply_edges(self.apply_edges)
      return g.edata['score']