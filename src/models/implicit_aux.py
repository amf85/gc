import pickle

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Dropout
from torch_geometric.nn import GraphConv
from torch_geometric.utils import add_remaining_self_loops, dense_to_sparse, to_dense_adj, add_self_loops

from models.gc_model import GCModel
from sql.sql_base import ImplicitAuxTgtGNNBase


class ImplicitAuxTgtGNN(GCModel):
  @property
  def db_table(self):
    return 'model_implicit_aux_tgt'
  
  @property
  def model_class(self):
    return 'ImplicitAuxTgtGNN'
  
  class IATGNNParams(GCModel.GCModelParams):
    def __init__(self,
                 dropout=0.0,
                 
                 X_pre_node_input_dim=0,
                 X_pre_use_node_inputs=False,
                 X_pre_n_spectral=0,
                 X_pre_derived_feat_dim=0,
                 
                 clustering_layers=None,
                 clustering_add_self_loops=True,
                 clustering_use_batchnorm=False,
                 clustering_activation=F.relu,
                 
                 X_post_use_clusters=True,
                 X_post_use_spectral=False,
                 X_post_use_node_inputs=True,
                 X_post_use_derived_feats=False,
                 
                 intra_layers=None,
                 intra_adj='A',
                 intra_softmax=False,
                 intra_add_self_loops=False,
                 intra_use_batchnorm=False,
                 intra_activation=F.relu,
                 
                 inter_layers=None,
                 inter_adj='weighted',
                 inter_softmax=False,
                 inter_add_self_loops=False,
                 inter_use_batchnorm=False,
                 inter_activation=F.relu,
                 
                 output_aggregation='mean',
                 output_layers=None,
                 output_use_batchnorm=False,
                 output_activation=F.relu,
                 output_softmax=False,
                 ):
      super(ImplicitAuxTgtGNN.IATGNNParams, self).__init__()
      self.dropout = dropout
      self.X_pre_node_input_dim = X_pre_node_input_dim
      self.X_pre_use_node_inputs = X_pre_use_node_inputs
      self.X_pre_n_spectral = X_pre_n_spectral
      self.X_pre_derived_feat_dim = X_pre_derived_feat_dim
      self.clustering_layers = clustering_layers
      self.clustering_add_self_loops = clustering_add_self_loops
      self.clustering_use_batchnorm = clustering_use_batchnorm
      self.clustering_activation = clustering_activation
      self.X_post_use_clusters = X_post_use_clusters
      self.X_post_use_spectral = X_post_use_spectral
      self.X_post_use_node_inputs = X_post_use_node_inputs
      self.X_post_use_derived_feats = X_post_use_derived_feats
      self.intra_layers = intra_layers
      self.intra_adj = intra_adj
      self.intra_softmax = intra_softmax
      self.intra_add_self_loops = intra_add_self_loops
      self.intra_use_batchnorm = intra_use_batchnorm
      self.intra_activation = intra_activation
      self.inter_layers = inter_layers
      self.inter_adj = inter_adj
      self.inter_softmax = inter_softmax
      self.inter_add_self_loops = inter_add_self_loops
      self.inter_use_batchnorm = inter_use_batchnorm
      self.inter_activation = inter_activation
      self.output_aggregation = output_aggregation
      self.output_layers = output_layers
      self.output_use_batchnorm = output_use_batchnorm
      self.output_activation = output_activation
      self.output_softmax = output_softmax
  
  def __init__(self, gcdb, model_dir, params: IATGNNParams):
    super(ImplicitAuxTgtGNN, self).__init__(model_dir=model_dir, params=params, gcdb=gcdb)
    self.params = params   # (mostly harmless) ide hack...
    
    # n_features should always have at least one nonzero component
    # X_feat_dim = params.X_pre_n_spectral + \
    #              params.X_pre_node_input_dim + \
    #              params.X_pre_derived_feat_dim
    
    if params.clustering_layers is not None:
      X_feat_dim = 0
      X_feat_dim += params.X_pre_node_input_dim if params.X_pre_use_node_inputs else 0
      X_feat_dim += params.X_pre_n_spectral
      X_feat_dim += params.X_pre_derived_feat_dim
      
      cluster_layer_dims = [X_feat_dim]
      cluster_layer_dims += params.clustering_layers
      X_feat_dim = params.clustering_layers[-1]
    
      self.cluster_gnn_dropouts = nn.ModuleList()
      self.cluster_gnn_layers = nn.ModuleList() if cluster_layer_dims != [] else None
      self.cluster_batchnorms = nn.ModuleList() if params.clustering_use_batchnorm else None
      for L in range(len(cluster_layer_dims) - 1):
        self.cluster_gnn_dropouts.append(Dropout(p=params.dropout))
        self.cluster_gnn_layers.append(
          GraphConv(cluster_layer_dims[L], cluster_layer_dims[L+1]))
        if params.clustering_use_batchnorm:
          self.cluster_batchnorms.append(BatchNorm1d(cluster_layer_dims[L+1]))
    
    
    
    # self.intra_layer_dims = [self.X_post_dim]
    if params.intra_layers is not None:
      X_feat_dim = 0
      X_feat_dim += params.X_pre_node_input_dim if params.X_post_use_node_inputs else 0
      X_feat_dim += params.X_pre_n_spectral if params.X_post_use_spectral else 0
      X_feat_dim += params.X_pre_derived_feat_dim if params.X_post_use_derived_feats else 0
      if params.X_post_use_clusters and params.clustering_layers is not None:
        X_feat_dim += cluster_layer_dims[-1]
      
      intra_layer_dims = [X_feat_dim]
      intra_layer_dims += params.intra_layers
      self.intra_gnn_dropouts = nn.ModuleList()
      self.intra_gnn_layers = nn.ModuleList()
      self.intra_batchnorms = nn.ModuleList() if params.intra_use_batchnorm else None
      for L in range(len(intra_layer_dims) - 1):
        self.intra_gnn_dropouts.append(Dropout(p=params.dropout))
        self.intra_gnn_layers.append(
          GraphConv(intra_layer_dims[L], intra_layer_dims[L+1]))
        if params.intra_use_batchnorm:
          self.intra_batchnorms.append(BatchNorm1d(intra_layer_dims[L+1]))
      X_feat_dim = intra_layer_dims[-1]
      
    if params.inter_layers is not None:
      if params.intra_layers is None:
        X_feat_dim = 0
        X_feat_dim += params.X_pre_node_input_dim if params.X_post_use_node_inputs else 0
        X_feat_dim += params.X_pre_n_spectral if params.X_post_use_spectral else 0
        X_feat_dim += params.X_pre_derived_feat_dim if params.X_post_use_derived_feats else 0
        X_feat_dim += self.cluster_layer_dims[-1] if params.X_post_use_clusters else 0
      
      inter_layer_dims = [X_feat_dim]
      inter_layer_dims += params.inter_layers
      self.inter_gnn_dropouts = nn.ModuleList()
      self.inter_gnn_layers = nn.ModuleList()
      self.inter_batchnorms = nn.ModuleList() if params.inter_use_batchnorm else None
      for L in range(len(inter_layer_dims) - 1):
        self.inter_gnn_dropouts.append(Dropout(params.dropout))
        self.inter_gnn_layers.append(
          GraphConv(inter_layer_dims[L], inter_layer_dims[L+1]))
        if params.inter_use_batchnorm:
          self.inter_batchnorms.append(BatchNorm1d(inter_layer_dims[L+1]))
      X_feat_dim = inter_layer_dims[-1]
    
    if params.output_layers is not None:
      self.output_mlp_dropouts = nn.ModuleList()
      self.output_mlp_layers = nn.ModuleList()
      self.output_batchnorms = nn.ModuleList() if params.output_use_batchnorm else None
      output_layer_dims = [X_feat_dim]
      for L in range(len(params.output_layers) - 1):
        self.output_mlp_dropouts.append(Dropout(p=params.dropout))
        self.output_mlp_layers.append(
          nn.Linear(output_layer_dims[L], output_layer_dims[L+1]))
        if params.output_use_batchnorm:
          self.output_batchnorms.append(BatchNorm1d(output_layer_dims[L+1]))
        
        
    
  
  def forward(self, X, A_edge, A_attr):
    '''
    
    :param X: whenever present, order of features should go: node_inputs; spectral; derived
    :param A_edge:
    :param A_attr:
    :return:
    '''
    X_node_inputs = X[:, :self.params.X_pre_node_input_dim]
    X_spectral = X[:, self.params.X_pre_node_input_dim: self.params.X_pre_n_spectral]
    X_derived = X[:, self.params.X_pre_node_input_dim + self.params.X_pre_n_spectral:]
    
    if not self.params.X_pre_use_node_inputs:
      X = X[:, self.params.X_pre_node_input_dim:]
    
    if self.params.clustering_layers is not None:
      if self.params.clustering_add_self_loops:
        A_edge, A_attr = add_self_loops(A_edge, A_attr)
      for L in range(len(self.cluster_gnn_layers)):
        if L != 0:
          X = self.cluster_gnn_dropouts[L](X)
        X = self.cluster_gnn_layers[L](X, A_edge, A_attr)
        if L < len(self.cluster_gnn_layers) - 1:
          X = self.params.clustering_activation(X)
        if self.params.clustering_use_batchnorm:
          X = self.cluster_batchnorms[L](X)
      S = X
    else:
      S = torch.zeros(size=(X.size()[0], 1))
    
    if self.params.intra_layers is not None:
      intra_features = []
      if self.params.X_post_use_node_inputs:
        intra_features.append(X_node_inputs)
      if self.params.X_post_use_spectral:
        intra_features.append(X_spectral)
      if self.params.X_post_use_derived_feats:
        intra_features.append(X_derived)
      if self.params.X_post_use_clusters:
        if self.params.intra_softmax:
          intra_features.append(torch.softmax(S, dim=1))
        else:
          intra_features.append(S)
      X = torch.cat(intra_features, dim=1)
      if self.params.intra_adj == 'disconnected':
        A_intra = torch.matmul(S, S.T)
        A_intra_edge, A_intra_attr = dense_to_sparse(A_intra)
      elif self.params.intra_adj == 'weighted':
        A = to_dense_adj(edge_index=A_edge, edge_attr=A_attr).squeeze()
        A_intra = torch.matmul(S, torch.matmul(S.T, A))
        A_intra_edge, A_intra_attr = dense_to_sparse(A_intra)
      elif self.params.intra_adj == 'A':
        A_intra_edge, A_intra_attr = A_edge, A_attr
      else:
        raise ValueError()
      if self.params.intra_add_self_loops:
        A_intra_edge, A_intra_attr = add_self_loops(A_intra_edge, A_intra_attr)
      
      for L in range(len(self.intra_gnn_layers)):
        if L != 0:
          X = self.intra_gnn_dropouts[L](X)
        X = self.intra_gnn_layers[L](X, A_intra_edge, A_intra_attr)
        X = self.params.intra_activation(X)
        if self.params.intra_use_batchnorm:
          X = self.intra_batchnorms[L](X)
    
    if self.params.inter_layers is not None:
      if self.params.intra_layers is not None:
        inter_features = X
      else:
        inter_features = []
        if self.params.X_post_use_node_inputs:
          inter_features.append(X_node_inputs)
        if self.params.X_post_use_spectral:
          inter_features.append(X_spectral)
        if self.params.X_post_use_derived_feats:
          inter_features.append(X_derived)
        if self.params.X_post_use_clusters:
          if self.params.intra_softmax:
            inter_features.append(torch.softmax(S, dim=1))
          else:
            inter_features.append(S)
        inter_features = torch.cat(inter_features, dim=1)
      inter_features = torch.matmul(S.T, inter_features)
      
      if self.params.inter_adj == 'agreement':
        A_inter = torch.matmul(S.T, S)
      elif self.params.inter_adj == 'weighted':
        A = to_dense_adj(edge_index=A_edge, edge_attr=A_attr).squeeze()
        A_inter = torch.matmul(S.T, torch.matmul(A, S))
      else:
        raise ValueError()
      A_inter_edge, A_inter_attr = dense_to_sparse(A_inter)
      if self.params.inter_add_self_loops:
        A_inter_edge, A_inter_attr = add_self_loops(A_inter_edge, A_inter_attr)
      
      for L in range(len(self.inter_gnn_layers)):
        if L != 0:
          inter_features = self.inter_gnn_dropouts[L](inter_features)
        inter_features = self.inter_gnn_layers[L](inter_features, A_inter_edge, A_inter_attr)
        inter_features = self.params.inter_activation(inter_features)
        if self.params.inter_use_batchnorm:
          inter_features = self.inter_batchnorms[L](inter_features)
      X = inter_features
      
    if self.params.output_aggregation == 'mean':
      X = torch.mean(X, dim=0)
    elif self.params.output_aggregation == 'sum':
      X = torch.sum(X, dim=0)
    elif self.params.output_aggregation == 'max':
      X = torch.max(X, dim=0)
    elif self.params.output_aggregation == 'min':
      X = torch.min(X, dim=0)
    elif self.params.output_aggregation == 'std':
      X = torch.std(X, dim=0)
    elif self.params.output_aggregation == 'none':
      if self.params.output_layers is not None:
        raise ValueError('Need aggregation if using output mlp layers')
      ...
    else:
      raise ValueError()
  
    if self.params.output_layers is not None:
      for L in range(len(self.output_mlp_layers)):
        if L != 0:
          X = self.output_mlp_dropouts[L](X)
        X = self.output_mlp_layers[L](X)
        if L < len(self.output_mlp_layers):
          X = self.params.output_activation(X)
        if self.params.output_use_batchnorm:
          X = self.output_batchnorms[L](X)
      
      if self.params.output_softmax:
        X = torch.softmax(X, dim=0)
    
    return X, S
    
  
  def add_sql_entry(self):
    
    clustering_layers = ''
    if self.params.clustering_layers is not None:
      clustering_layers = ','.join(map(str, self.params.clustering_layers))
    intra_layers = ''
    if self.params.intra_layers is not None:
      intra_layers = ','.join(map(str, self.params.intra_layers))
    inter_layers = ''
    if self.params.inter_layers is not None:
      inter_layers = ','.join(map(str, self.params.inter_layers))
    output_layers = ''
    if self.params.output_layers is not None:
      output_layers = ','.join(map(str, (self.params.output_layers)))
    
    class_base = ImplicitAuxTgtGNNBase(
      id=self.id,
      dropout=self.params.dropout,
      X_pre_node_input_dim=self.params.X_pre_node_input_dim,
      X_pre_use_node_inputs=self.params.X_pre_use_node_inputs,
      X_pre_n_spectral=self.params.X_pre_n_spectral,
      X_pre_derived_feat_dim=self.params.X_pre_derived_feat_dim,
      clustering_layers=clustering_layers,
      clustering_add_self_loops=self.params.clustering_add_self_loops,
      clustering_use_batchnorm=self.params.clustering_use_batchnorm,
      clustering_activation=self.params.clustering_activation.__name__,
      X_post_use_clusters=self.params.X_post_use_clusters,
      X_post_use_spectral=self.params.X_post_use_spectral,
      X_post_use_node_inputs=self.params.X_post_use_node_inputs,
      X_post_use_derived_feats=self.params.X_post_use_derived_feats,
      intra_layers=intra_layers,
      intra_adj=self.params.intra_adj,
      intra_softmax=self.params.intra_softmax,
      intra_add_self_loops=self.params.intra_add_self_loops,
      intra_use_batchnorm=self.params.intra_use_batchnorm,
      intra_activation=self.params.intra_activation.__name__,
      inter_layers=inter_layers,
      inter_adj=self.params.inter_adj,
      inter_softmax=self.params.inter_softmax,
      inter_add_self_loops=self.params.inter_add_self_loops,
      inter_use_batchnorm=self.params.inter_use_batchnorm,
      inter_activation=self.params.inter_activation.__name__,
      output_aggregation=self.params.output_aggregation,
      output_layers=output_layers,
      output_use_batchnorm=self.params.output_use_batchnorm,
      output_activation=self.params.output_activation.__name__,
      output_softmax=self.params.output_softmax
    )
    
    session = self.gcdb.local_sess()
    session.add(class_base)
    session.commit()
