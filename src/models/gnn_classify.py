import pickle

import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Dropout
from torch_geometric.nn import GraphConv
from torch_geometric.utils import add_remaining_self_loops, add_self_loops

from models.gc_model import GCModel
from sql.sql_base import GNNClassifyBase
from functools import partial
from copy import copy


class GNNClassify(GCModel):
  @property
  def db_table(self):
    return 'model_gnn_classify'
  
  @property
  def model_class(self):
    return 'GNNClassify'
  
  class GNNClassifyParams(GCModel.GCModelParams):
    def __init__(self,
                 input_feature_dim,
                 gnn_out_dims,
                 mlp_out_dims,
                 conv_fn=None,
                 conv_conf=None,
                 activation=F.relu,
                 add_self_loops=True,
                 use_batchnorm_gnn=True,
                 use_batchnorm_mlp=True,
                 dropout_gnn=0.0,
                 dropout_mlp=0.0,
                 output_aggr='mean',

                 ):
      super(GNNClassify.GNNClassifyParams, self).__init__()
      self.input_feature_dim = input_feature_dim
      self.gnn_out_dims = gnn_out_dims
      self.mlp_out_dims = mlp_out_dims
      self.conv_conf = copy(conv_conf)
      self.unsqueeze_X = self.conv_conf.pop('unsqueeze_X', False)
      self.aggregate_hidden = self.conv_conf.pop('aggregate_hidden', None)
      self.n_heads = self.conv_conf.get('num_heads', 1)
      # if self.aggregate_hidden == 'concat':
      #   self.gnn_out_dims = [d*self.n_heads for d in self.gnn_out_dims]
      # if conv_conf is None:
      #   self.conv_conf = {'aggr': 'add', 'bias': True}
      # else:
      #   self.conv_conf = conv_conf
      # if conv_fn is None:
      #   self.conv_fn = partial(GraphConv, **self.conv_conf)
      # else:
      self.conv_fn = partial(conv_fn, **self.conv_conf)
      self.activation = activation
      self.add_self_loops = add_self_loops
      self.use_batchnorm_gnn = use_batchnorm_gnn
      self.use_batchnorm_mlp = use_batchnorm_mlp
      self.dropout_gnn = dropout_gnn
      self.dropout_mlp = dropout_mlp
      self.output_aggr = output_aggr
  
  def __init__(self, gcdb, model_dir, params: GNNClassifyParams):
    super(GNNClassify, self).__init__(model_dir=model_dir, params=params, gcdb=gcdb)
    self.gnn_out_dims = params.gnn_out_dims
    if params.aggregate_hidden == 'concat':
      self.gnn_in_dims = [params.input_feature_dim] + [d*params.n_heads for d in params.gnn_out_dims]
    else:
      self.gnn_in_dims = [params.input_feature_dim] + params.gnn_out_dims
    self.mlp_dimensions = [self.gnn_in_dims[-1]] + params.mlp_out_dims
    self.gnn_layers = nn.ModuleList()
    self.mlp_layers = nn.ModuleList()
    self.batchnorms_gnn = nn.ModuleList()
    self.batchnorms_mlp = nn.ModuleList()
    self.dropouts_gnn = nn.ModuleList()
    self.dropouts_mlp = nn.ModuleList()
    for L in range(len(params.gnn_out_dims)):
      self.dropouts_gnn.append(Dropout(p=params.dropout_gnn))
      # self.gnn_layers.append(self.params.conv_fn(in_channels=self.gnn_dimensions[L],
      #                                            out_channels=self.gnn_dimensions[L + 1]))
      self.gnn_layers.append(self.params.conv_fn(in_feats=self.gnn_in_dims[L],
                                                 out_feats=self.gnn_out_dims[L]))
      if params.use_batchnorm_gnn:
        self.batchnorms_gnn.append(BatchNorm1d(self.gnn_in_dims[L+1]))

    for L in range(len(params.mlp_out_dims)):
      self.dropouts_mlp.append(Dropout(p=params.dropout_mlp))
      self.mlp_layers.append(nn.Linear(self.mlp_dimensions[L], self.mlp_dimensions[L+1]))
      if params.use_batchnorm_mlp:
        self.batchnorms_mlp.append(BatchNorm1d(self.mlp_dimensions[L+1]))


    self.activation = params.activation
    self.add_self_loops = params.add_self_loops
    # self.cluster_reg_weights = nn.Parameter(torch.empty(self.gnn_dimensions[-1],), requires_grad=True)
    # nn.init.xavier_uniform_(self.cluster_reg_weights)
    # nn.init.uniform_(self.cluster_reg_weights, a=0.0, b=1.0)
  
  # def forward(self, X, A_edge, A_attr, softmax_output=True):
  def forward(self, dgl_batch, softmax_output=True, device=torch.device('cpu')):
    # X = dgl_batch.ndata['X']
    # A_edge = dgl_batch.edges()
    # A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
    # A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(device)

    # if self.params.unsqueeze_X:
    #   X = torch.unsqueeze(X, dim=0)
    batch_num_nodes = dgl_batch._batch_num_nodes
    batch_num_edges = dgl_batch._batch_num_edges
    if self.add_self_loops:
      # A_edge, A_attr = add_remaining_self_loops(A_edge, A_attr)
      # pass
      dgl.add_self_loop(dgl_batch)
      # dgl_batch.add_edges(dgl_batch.nodes(), dgl_batch.nodes())
      # A_edge, A_attr = add_self_loops(A_edge, A_attr)
    X = dgl_batch.ndata['X']
    for L in range(len(self.gnn_layers)):
      if L != 0:
        X = self.dropouts_gnn[L](X)
      X = self.gnn_layers[L](dgl_batch, X)
      if self.params.aggregate_hidden == 'concat':
        X = torch.cat([X[:, t, :] for t in range(self.params.n_heads)], dim=1)
      elif self.params.aggregate_hidden == 'mean':
        X = torch.mean(X, dim=1)
      elif self.params.aggregate_hidden == 'sum':
        X = torch.sum(X, dim=1)
      elif self.params.aggregate_hidden == 'max':
        X = torch.max(X, dim=1).values

      # X = self.gnn_layers[L](X, A_edge, A_attr)

      # if L < len(self.gnn_layers) - 1:
      X = self.activation(X)
      if self.params.use_batchnorm_gnn:
       X = self.batchnorms_gnn[L](X)
    # X = self.cluster_reg_weights * X
    dgl_batch.ndata['X'] = X
    dgl_batch.set_batch_num_nodes(batch_num_nodes)
    # dgl_batch.set_batch_num_edges(batch_num_edges)  # not actually correct, original had no self loops, but doesn't matter


    if self.params.output_aggr == 'sum':
      X = dgl.sum_nodes(dgl_batch, 'X')
    elif self.params.output_aggr == 'mean':
      X = dgl.mean_nodes(dgl_batch, 'X')
    elif self.params.output_aggr == 'max':
      X = dgl.max_nodes(dgl_batch, 'X')
    else:
      raise NotImplementedError()

    for L in range(len(self.mlp_layers)):
      X = self.dropouts_mlp[L](X)
      X = self.mlp_layers[L](X)
      if L < len(self.mlp_layers) - 1:
        X = self.activation(X)
        if self.params.use_batchnorm_mlp:
          X = self.batchnorms_mlp[L](X)


    if self.params.unsqueeze_X:
      X = torch.squeeze(X)
    if softmax_output:
      X = F.softmax(X, dim=1)
      # X = torch.softmax(X, dim=1)

    return X, None

  # def forward(self, X, A_edge, A_attr, softmax_output=True):
  # def forward(self, dgl_batch, softmax_output=True, device=torch.device('cpu')):
  #   X = dgl_batch.ndata['X']
  #   A_edge = dgl_batch.edges()
  #   A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
  #   A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(device)
  #
  #   if self.params.unsqueeze_X:
  #     X = torch.unsqueeze(X, dim=0)
  #   if self.add_self_loops:
  #     # A_edge, A_attr = add_remaining_self_loops(A_edge, A_attr)
  #     A_edge, A_attr = add_self_loops(A_edge, A_attr)
  #   for L in range(len(self.gnn_layers)):
  #     if L != 0:
  #       X = self.dropouts_gnn[L](X)
  #     X = self.gnn_layers[L](X, A_edge, A_attr)
  #     # if L < len(self.gnn_layers) - 1:
  #     X = self.activation(X)
  #     if self.params.use_batchnorm_gnn:
  #       X = self.batchnorms_gnn[L](X)
  #   # X = self.cluster_reg_weights * X
  #   dgl_batch.ndata['X'] = X
  #
  #   if self.params.output_aggr == 'sum':
  #     X = dgl.sum_nodes(dgl_batch, 'X')
  #   elif self.params.output_aggr == 'mean':
  #     X = dgl.mean_nodes(dgl_batch, 'X')
  #   elif self.params.output_aggr == 'max':
  #     X = dgl.max_nodes(dgl_batch, 'X')
  #   else:
  #     raise NotImplementedError()
  #
  #   for L in range(len(self.mlp_layers)):
  #     X = self.dropouts_mlp[L](X)
  #     X = self.mlp_layers[L](X)
  #     if L < len(self.gnn_layers) - 1:
  #       X = self.activation(X)
  #       if self.params.use_batchnorm_mlp:
  #         X = self.batchnorms_mlp[L](X)
  #
  #
  #   if self.params.unsqueeze_X:
  #     X = torch.squeeze(X)
  #   if softmax_output:
  #     X = F.softmax(X, dim=1)
  #     # X = torch.softmax(X, dim=1)
  #
  #   return X, None
  
  def add_sql_entry(self):

    class_base = GNNClassifyBase(
      id=self.id,
      input_dim=self.params.input_dim,
      gnn_out_dims=','.join(map(str, self.params.gnn_out_dims)),
      mlp_out_dims=','.join(map(str, self.params.mlp_out_dims)),
      dropout_gnn=self.params.dropout_gnn,
      dropout_mlp=self.params.dropout_mlp,
      use_batchnorm_gnn=self.params.use_batchnorm_gnn,
      use_batchnorm_mlp=self.params.use_batchnorm_mlp,
      activation=self.params.activation.__name__,
      self_loops=self.params.self_loops,
      output_aggr=self.params.output_aggr
    )
    session = self.gcdb.local_sess()
    session.add(class_base)
    session.commit()
