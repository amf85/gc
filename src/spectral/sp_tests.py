import itertools

import numpy as np
import torch

import divisive_spectral_pool as dsp
import graphgen.gc_hierarch as hn
import spectral.decompose as decompose
# import g_classify as gclassify
import spectral_pool as spool
import test_frame as frame


class SPTestFramework(frame.TestFramework):
  def __init__(self, directory, data_path, model, n_classes=None):
    super(SPTestFramework, self).__init__(directory, data_path, model)
    self.nllloss = torch.nn.NLLLoss()
    self.n_classes = n_classes
  
  class ModelParams(dict):
    def __init__(self, node_features, spectral_features, out_dims):
      super(SPTestFramework.ModelParams, self).__init__()
      self['node_features'] = node_features
      self['spectral_features'] = spectral_features
      self['out_dims'] = out_dims
  
  def loss_fn(self, preds, labels):
    return std_reg_lbl_loss_fn(preds, labels)
    # return std_cat_lbl_loss_fn(preds, labels, nllloss=self.nllloss)
    # return num_clusters_loss_fn(preds, labels)
    # return ground_truth_communities_loss_fn(preds, labels, n_classes=self.n_classes)
    # return max_cluster_loss_fn(preds, labels, nllloss=self.nllloss)
  
  def preprocess_features(self, dataset, model_params):
    processed_dataset = []
    for sample in dataset:
      label = [sample.graph_label]
      # XS = decompose.pyt_compute_laplacian_eigs(model_params['spectral_features'], A, L_normalization=None)
      A = sample.A
      XS = decompose.compute_laplacian_eigenvectors(A, model_params['spectral_features'])
      
      A = torch.tensor(sample.A).float()
      X_struct, X_subgraphs = dsp.split_by_fiedler(A, splits=9, return_eigenvectors=False)
      XS = torch.cat((X_struct, X_subgraphs, torch.tensor(XS)), dim=1)
      X = np.expand_dims(sample.node_info, axis=1)
      # X = np.concatenate([node_info, X], axis=1)
      XS = torch.tensor(XS).float()
      X = torch.tensor(X).float()
      # coo_A = sp.sparse.coo_matrix(A)
      # A = torch.tensor(np.stack([coo_A.row, coo_A.col])).long()
      processed_dataset.append((X, A, XS, label))
    return processed_dataset


def std_cat_lbl_loss_fn(preds, labels, nllloss=None, label_translation=0):
  nllloss = torch.nn.NLLLoss() if nllloss is None else nllloss
  labels = torch.tensor(labels[0] + label_translation)
  preds = torch.softmax(preds, dim=0)
  loss = nllloss(torch.unsqueeze(preds, dim=0), labels.unsqueeze(dim=0))
  pred = torch.argmax(preds)
  correct = torch.sum(pred == labels)
  return loss, correct


def std_reg_lbl_loss_fn(preds, labels, device=torch.device('cpu')):
  labels = torch.tensor(labels).float().to(device)
  # loss = torch.nn.MSELoss()(torch.unsqueeze(preds, dim=0), labels)
  # loss = torch.pow(labels.item() - preds.item(), torch.tensor(2., requires_grad=True))
  loss = torch.nn.MSELoss()(preds, labels)
  correct = torch.sum(torch.round(preds) == labels)
  return loss, correct


def num_clusters_loss_fn(preds, labels):
  # print('test')
  labels = torch.tensor(labels)
  num_clusters_used = torch.unique(torch.argmax(preds, dim=1)).size()[0]
  loss = torch.pow(num_clusters_used - labels.item(), torch.tensor(2., requires_grad=True))
  correct = torch.sum(num_clusters_used == labels)
  return loss, correct


def max_cluster_loss_fn(preds, labels, nllloss=None):
  nllloss = torch.nn.NLLLoss() if nllloss is None else nllloss
  loss = nllloss(torch.unsqueeze(torch.softmax(preds, dim=0), dim=0), torch.tensor(labels))
  pred = torch.argmax(torch.softmax(preds, dim=0))
  correct = torch.sum(torch.argmax(torch.softmax(preds, dim=0)) == torch.tensor(labels))
  return loss, correct


def ground_truth_communities_loss_fn(predictions,
                                     labels,
                                     n_classes,
                                     return_correct=True,
                                     device=torch.device('cpu')):
  n_classes = n_classes
  loss = np.inf
  correct = 0
  permutations = itertools.permutations(np.arange(n_classes))
  for perm in permutations:
    p_labels = np.choose(labels, perm)
    t_labels = torch.tensor(p_labels)
    likelihoods = torch.gather(predictions, dim=1, index=torch.unsqueeze(t_labels, 1))
    # likelihoods = torch.gather(predictions, dim=1, index=t_labels)
    log_likelihoods = torch.log(likelihoods + 1e-8)
    nll = -log_likelihoods.sum()
    if nll < loss:
      loss = nll
      correct = torch.sum(t_labels == torch.max(predictions, dim=1)[1])
  if return_correct:
    return loss, correct / predictions.size()[0]
  else:
    return loss


def test():
  dataset_params = {
    'branching_factors': [2, 3, 4, 5],
    'depths': [0],
    'motif_sizes': [3, 4, 5, 6]
  }
  
  data_dir = '../../output/tests/sp/datasets/set0'
  test_dir = '../../output/tests/sp/tests/test0'
  hn.create_dataset(1000, 1000, dataset_params, data_dir)
  
  epochs = 500
  LR = 1e-3
  opt_class = torch.optim.Adam
  node_features = 1
  spectral_features = 100
  out_dims = [50, 50, 100, 100]
  model_params = SPTestFramework.ModelParams(node_features=node_features,
                                             spectral_features=spectral_features,
                                             out_dims=out_dims)
  
  pool_1 = spool.SpectralPoolingModule(n_node_features=1,
                                       n_spectral_vectors=100 + 18,
                                       node_gnn_out_dims=[10],
                                       node_mlp_out_dims=[20],
                                       spectral_out_dims=[30, 6])
  
  # pool_2 = spool.SpectralPoolingModule(n_node_features=10,
  #                                      n_spectral_vectors=30,
  #                                      node_gnn_out_dims=[10],
  #                                      node_mlp_out_dims=[20],
  #                                      spectral_out_dims=[30, 6])
  
  pooling_modules = torch.nn.ModuleList()
  pooling_modules.append(pool_1)
  # pooling_modules.append(pool_2)
  
  model = spool.SpectralPoolingClassifier(pooling_modules=[],
                                          mlp_in_dim=1,
                                          mlp_out_dims=[50, 20, 1])
  
  from pprint import pprint
  pprint(dict(model.named_parameters()))
  # grads = {k: 0 for k in dict(model.named_parameters()).keys()}
  # for k, p in dict(model.named_parameters()).items():
  #     t = p.register_hook(lambda grad: grads[k] = grad)
  # model.grads = grads
  
  # model = cluster.SpectralSoftAssignment(inp_dim=100, out_dims=[30, 30, 30, 5])
  
  # model = gclassify.SpectralClassifier(**model_params)
  framework = SPTestFramework(directory=test_dir, data_path=data_dir, model=model, n_classes=1)
  
  framework.train_model(model_params, epochs=epochs, LR=LR, opt_class=opt_class,
                        permute_samples=False, remove_structure=False)


if __name__ == '__main__':
  test()
  exit(0)
