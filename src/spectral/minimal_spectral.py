import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import GraphConv
from torch_geometric.utils import dense_to_sparse, add_remaining_self_loops

import decompose as decompose
import graphgen.gc_hierarch as harch
import test_frame as frame
from sp_tests import (std_reg_lbl_loss_fn,
                      ground_truth_communities_loss_fn)

tgt_dev = 'cpu'
device = torch.device('cuda' if torch.cuda.is_available() and tgt_dev == 'gpu' else 'cpu')


class MinSpectralGNN(nn.Module):
  def __init__(self,
               input_feature_dimension,
               n_spectral_features,
               gnn_cluster_out_dimensions,
               top_gnn_out_dimensions,
               top_mlp_out_dimensions,
               concat_x_and_xs=True,
               decompose_clusters=True,
               device=torch.device('cpu'),
               intra_cluster_gnn_out_dimensions=None,
               concat_x_and_xs_intra_cluster=False,
               stxb_attn_out_dim=None,
               concat_x_xs_pre_attn=False,
               return_S_only=False
               ):
    super(MinSpectralGNN, self).__init__()
    self.n_spectral_features = n_spectral_features
    self.concat_x_and_xs = concat_x_and_xs
    self.decompose_clusters = decompose_clusters
    self.return_S_only = return_S_only
    self.intra_cluster_processing = True if intra_cluster_gnn_out_dimensions is not None else False
    self.concat_x_and_xs_intra_cluster = concat_x_and_xs_intra_cluster
    self.device = device
    self.gnn_cluster_dimensions = [n_spectral_features] + gnn_cluster_out_dimensions
    self.gnn_cluster_layers = nn.ModuleList()
    self.gnn_cluster_bns = nn.ModuleList()
    for L in range(len(gnn_cluster_out_dimensions)):
      self.gnn_cluster_layers.append(
        GraphConv(self.gnn_cluster_dimensions[L], self.gnn_cluster_dimensions[L + 1]))
      self.gnn_cluster_bns.append(
        nn.BatchNorm1d(self.gnn_cluster_dimensions[L]))
    
    if intra_cluster_gnn_out_dimensions:
      intra_cluster_gnn_dims = input_feature_dimension
      if concat_x_and_xs_intra_cluster:
        intra_cluster_gnn_dims += n_spectral_features
      self.intra_cluster_gnn_dimensions = [
                                            intra_cluster_gnn_dims] + intra_cluster_gnn_out_dimensions
      self.intra_cluster_gnn_layers = nn.ModuleList()
      for L in range(len(intra_cluster_gnn_out_dimensions)):
        self.intra_cluster_gnn_layers.append(
          GraphConv(self.intra_cluster_gnn_dimensions[L], self.intra_cluster_gnn_dimensions[L + 1]))
    
    self.stxb_attn_out_dim = stxb_attn_out_dim
    self.concat_x_xs_pre_attn = concat_x_xs_pre_attn
    if stxb_attn_out_dim:
      attn_in_dim = input_feature_dimension
      if concat_x_xs_pre_attn:
        attn_in_dim += n_spectral_features
      self.B = torch.empty(attn_in_dim, stxb_attn_out_dim, requires_grad=True)
      nn.init.xavier_normal_(self.B)
    
    top_gnn_in_dimension = input_feature_dimension if not stxb_attn_out_dim else stxb_attn_out_dim
    if self.intra_cluster_processing:
      top_gnn_in_dimension = intra_cluster_gnn_out_dimensions[-1]
    elif concat_x_and_xs and not stxb_attn_out_dim:
      top_gnn_in_dimension += n_spectral_features
    if decompose_clusters:
      top_gnn_in_dimension += gnn_cluster_out_dimensions[-1]
    self.top_gnn_dimensions = [top_gnn_in_dimension] + top_gnn_out_dimensions
    self.top_gnn_layers = nn.ModuleList()
    for L in range(len(top_gnn_out_dimensions)):
      self.top_gnn_layers.append(
        GraphConv(self.top_gnn_dimensions[L], self.top_gnn_dimensions[L + 1])
      )
    
    self.top_mlp_dimensions = [top_gnn_out_dimensions[-1]] + top_mlp_out_dimensions
    self.top_mlp_layers = nn.ModuleList()
    for L in range(len(top_mlp_out_dimensions)):
      self.top_mlp_layers.append(
        nn.Linear(self.top_mlp_dimensions[L], self.top_mlp_dimensions[L + 1])
      )
    
    self.intra_cluster_gnn_dimensions = []
    
    self.done_one = False
  
  def forward(self, X, A, XS=None):
    hnet = XS
    XS = decompose.pyt_compute_laplacian_eigs(n_vectors=self.n_spectral_features, A=A)
    
    A_edge, A_attr = dense_to_sparse(A)
    A_edge, A_attr = add_remaining_self_loops(A_edge, A_attr)
    S = XS
    if self.done_one:
      pass
      # print('gradient:', list(self.gnn_cluster_layers[0].parameters())[0].grad[0])
      # print('max param:', max([torch.max(p) for p in self.parameters()]) )
    for L in range(len(self.gnn_cluster_layers)):
      ss = S
      # print(list(self.gnn_cluster_layers[0].parameters())[0][0])
      # S = self.gnn_cluster_bns[L](S)
      if torch.any(torch.isnan(S)) or torch.any(torch.isnan(ss)):
        print('nan')
      S = self.gnn_cluster_layers[L](S, A_edge, A_attr)
      if torch.any(torch.isnan(S)) or torch.any(torch.isnan(ss)):
        print('nan')
      S = F.relu(S)
      # S = torch.tanh(S)
      if torch.any(torch.isnan(S)) or torch.any(torch.isnan(ss)):
        print('nan')
    S = F.softmax(S, dim=1)
    if torch.any(torch.isnan(S)) or torch.any(torch.isnan(ss)):
      print('nan')
    # todo: for debug only
    clusters = torch.argmax(S, dim=1)
    self.done_one = True
    strongest_cluster_preferences = torch.max(S, dim=1).values
    
    # regularisation for encouraging single cluster choice
    l1_reg_cluster_choice = torch.sum(torch.ones(S.size()[0], requires_grad=True).to(self.device) -
                                      torch.max(S, dim=1).values)
    cluster_entropy = -torch.mean(torch.sum(torch.log_softmax(S, dim=1) * S, dim=1), dim=0)
    if self.return_S_only:
      return S, cluster_entropy
    
    X_cluster = X
    if self.intra_cluster_processing:
      A_intra = torch.matmul(S, S.T)
      # A_intra = torch.matmul(A, A_intra)
      A_intra = torch.matmul(A_intra, A)
      A_intra_edge, A_intra_attr = dense_to_sparse(A_intra)
      A_intra_edge, A_intra_attr = add_remaining_self_loops(A_intra_edge, A_intra_attr)
      X_intra = X
      if self.concat_x_and_xs_intra_cluster:
        X_intra = torch.cat([X, XS], dim=1)
      for L in range(len(self.intra_cluster_gnn_layers)):
        X_intra = self.intra_cluster_gnn_layers[L](X_intra, A_intra_edge, A_intra_attr)
        X_intra = F.relu(X_intra)
      X_cluster = X_intra
      # X_cluster = torch.matmul(S.T, X_intra)
    elif self.stxb_attn_out_dim:
      X_attn = X
      if self.concat_x_xs_pre_attn:
        X_attn = torch.cat([X, XS], dim=1)
      X_attn = torch.matmul(X_attn, self.B)
      X_cluster = X_attn
    
    elif self.concat_x_and_xs:
      X_cluster = torch.cat([X, XS], dim=1)
    
    # note no self loops here
    # A_cluster = torch.matmul(S.T, torch.matmul(A, S))
    # alternative that just uses assignment matrix S to produce $S \in R^{c \times c}$
    A_cluster = torch.matmul(S.T, S)
    
    if self.decompose_clusters:
      # calculating eigenvectors uses power iteration, quickly results in gradient explosion
      # so detach from autograd to compute this. Let model learn to predict the effects instead.
      with torch.no_grad():
        XS_cluster = decompose.pyt_compute_laplacian_eigs(n_vectors=self.gnn_cluster_dimensions[-1],
                                                          A=A_cluster,
                                                          device=self.device)
      X_cluster = torch.cat([X_cluster, XS_cluster], dim=1)
    
    A_cluster_edge, A_cluster_attr = dense_to_sparse(A_cluster)
    A_cluster_edge, A_cluster_attr = add_remaining_self_loops(A_cluster_edge, A_cluster_attr)
    for layer in self.top_gnn_layers:
      X_cluster = layer(X_cluster, A_cluster_edge, A_cluster_attr)
      X_cluster = F.relu(X_cluster)
    
    X_cluster = torch.mean(X_cluster, dim=0)
    for L in range(len(self.top_mlp_layers)):
      X_cluster = self.top_mlp_layers[L](X_cluster)
      if L < len(self.top_mlp_layers) - 1:
        X_cluster = F.relu(X_cluster)
    
    return (X_cluster, S), cluster_entropy


class MinSpectralFrame(frame.TestFramework):
  def __init__(self, directory, data_path, model,
               n_classes=None,
               n_spectral_features=None,
               add_spectral_v1_to_X=False,
               set_XS_to_zeros=False,
               device=torch.device('cpu')):
    super(MinSpectralFrame, self).__init__(directory, data_path, model, device)
    self.n_classes = n_classes
    self.nllloss = torch.nn.NLLLoss()
    self.n_spectral_features = n_spectral_features
    self.add_spectral_v1_to_X = add_spectral_v1_to_X
    self.set_XS_to_zeros = set_XS_to_zeros
  
  def preprocess_features(self, dataset, model_params):
    start = time.time()
    processed_dataset = []
    for sample in dataset:
      label = [sample.graph_label]
      A = sample.A
      X = np.expand_dims(sample.node_info, axis=1)
      X = torch.tensor(X).float()
      XS = None
      if self.add_spectral_v1_to_X:
        XS = decompose.compute_laplacian_eigenvectors(A, self.n_spectral_features)
        XS = torch.tensor(XS).float()
        X = torch.cat([X, XS], dim=1)
        XS = None
      A = torch.tensor(sample.A).float()
      XS = decompose.pyt_compute_laplacian_eigs(self.n_spectral_features, A)
      labels = [label]
      labels.append(sample.node_l1_communities)
      if self.set_XS_to_zeros:
        XS = torch.zeros_like(XS)
      processed_dataset.append((X, A, XS, labels))
    print(f'Preprocessing time: {time.time() - start}s')
    return processed_dataset
  
  def loss_fn(self, preds, labels, device=torch.device('cpu')):
    loss, correct = std_reg_lbl_loss_fn(preds[0], labels[0], device)
    # loss = std_cat_lbl_loss_fn(preds, labels, nllloss=self.nllloss, label_translation=0)
    # loss = num_clusters_loss_fn(preds, labels)
    # loss, correct = ground_truth_communities_loss_fn(preds, labels[1], n_classes=self.n_classes)
    # loss = max_cluster_loss_fn(preds, labels, nllloss=self.nllloss)
    # print('loss:', loss)
    auxiliary, aux_correct = ground_truth_communities_loss_fn(preds[1], labels[1],
                                                              n_classes=
                                                              self.model.gnn_cluster_dimensions[-1],
                                                              device=device)
    return loss + 0.00 * auxiliary, correct


def test():
  dataset_params = {
    'branching_factors': [2, 3],
    'depths': [1],
    # 'motif_sizes': [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
    'motif_sizes': [3, 4, 5]
  }
  
  data_dir = '../../output/tests/baseline_gnn/datasets/set0'
  test_dir = '../../output/tests/baseline_gnn/tests/test0'
  harch.create_dataset(1000, 1000, dataset_params, data_dir)
  
  epochs = 500
  LR = 3e-3
  L2_weights = 0.00
  L2_cluster = 0.00
  opt_class = torch.optim.Adam
  
  node_features = 1
  n_spectral_features = 50
  add_spectral_v1_to_X = True
  set_XS_to_zeros = False
  gnn_cluster_out_dimensions = [4]
  top_gnn_out_dimensions = [30, 30, 30]
  out_dimension = 1
  top_mlp_out_dimensions = [out_dimension]
  concat_x_and_xs = True
  decompose_clusters = False
  intra_cluster_gnn_out_dimensions = None
  concat_x_and_xs_intra_cluster = True
  stxb_attn_out_dim = 30
  concat_x_xs_pre_attn = True
  return_S_only = False
  
  if add_spectral_v1_to_X:
    node_features = node_features + n_spectral_features
  device = torch.device('cpu')
  
  model = MinSpectralGNN(input_feature_dimension=node_features,
                         n_spectral_features=n_spectral_features,
                         gnn_cluster_out_dimensions=gnn_cluster_out_dimensions,
                         top_gnn_out_dimensions=top_gnn_out_dimensions,
                         top_mlp_out_dimensions=top_mlp_out_dimensions,
                         concat_x_and_xs=concat_x_and_xs,
                         decompose_clusters=decompose_clusters,
                         intra_cluster_gnn_out_dimensions=intra_cluster_gnn_out_dimensions,
                         concat_x_and_xs_intra_cluster=concat_x_and_xs_intra_cluster,
                         stxb_attn_out_dim=stxb_attn_out_dim,
                         concat_x_xs_pre_attn=concat_x_xs_pre_attn,
                         return_S_only=return_S_only,
                         device=device)
  
  optimiser = torch.optim.Adam(params=model.parameters(),
                               lr=LR,
                               weight_decay=L2_weights)
  
  # optimiser = torch.optim.SGD(params=model.parameters(),
  #                              lr=LR,
  #                              weight_decay=L2_weights)
  
  framework = MinSpectralFrame(directory=test_dir,
                               data_path=data_dir,
                               model=model,
                               n_classes=out_dimension,
                               n_spectral_features=n_spectral_features,
                               add_spectral_v1_to_X=add_spectral_v1_to_X,
                               set_XS_to_zeros=set_XS_to_zeros,
                               device=device)
  
  framework.train_model(model_params=None,
                        epochs=epochs,
                        LR=LR,
                        optimiser=optimiser,
                        opt_class=opt_class,
                        max_grad=0.5,
                        permute_samples=False,
                        remove_structure=False,
                        p_lambda=L2_cluster,
                        opt_update_freq=1)


if __name__ == '__main__':
  test()
