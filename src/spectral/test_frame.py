import pickle
import time
from abc import abstractmethod, ABC

import numpy as np
import torch
from prettytable import PrettyTable

import vis.hn_vis as hnv
from util import util


class TestFramework(ABC):
  def __init__(self, directory, data_path, model, device=torch.device('cpu')):
    super(TestFramework, self).__init__()
    self.directory = util.validate_directory(directory)
    self.data_path = data_path
    self.model = model.to(device)
    self.device = device
    
  
  class ModelParams(dict):
    def __init__(self):
      super(TestFramework.ModelParams, self).__init__()
  
  @abstractmethod
  def preprocess_features(self, dataset, model_params):
    raise NotImplementedError()
  
  @abstractmethod
  def loss_fn(self, preds, labels, device):
    raise NotImplementedError()
  
  def permute_x_a(self, X, A, XS=None):
    P = torch.eye(X.size()[0])[np.random.permutation(X.size()[0])]
    X = torch.matmul(P, X)
    if XS is not None:
      XS = torch.matmul(P, XS)
    
    A = torch.matmul(P, torch.matmul(A, P))
    
    return X, A, XS
  
  def train_model(self,
                  model_params: ModelParams,
                  epochs,
                  LR,
                  opt_class,
                  optimiser=None,
                  permute_samples=False,
                  remove_structure=False,
                  max_grad=None,
                  p_lambda=0,
                  opt_update_freq=1
                  ):
    start = time.time()
    
    vis = hnv.HierarchicalNetVisualiser()
    vis_path = f'../../output/tests/n0.png'
    
    train_set = pickle.load(open(f'{self.data_path}/train.pkl', 'rb'))
    train_set = self.preprocess_features(train_set, model_params=model_params)
    test_set = pickle.load(open(f'{self.data_path}/test.pkl', 'rb'))
    test_set = self.preprocess_features(test_set, model_params=model_params)
    optimiser = optimiser if optimiser is not None else opt_class(self.model.parameters(), lr=LR)
    # optimiser = torch.optim.SGD(params=self.model.parameters(), lr=1e-5)
    
    t = PrettyTable()
    t.field_names = ['Epoch', 'Train accuracy', 'Train loss', 'Test accuracy', 'Test loss',
                     'Time since start']
    t_init = False
    
    step = 0
    for i in range(epochs):
      optimiser.zero_grad()
      epoch_loss, epoch_correct, epoch_total = 0, 0, 0
      np.random.shuffle(train_set)
      s_idx = 0
      for sample in train_set:
        step += 1
        s_idx += 1
        if len(sample) == 3:
          X, A, labels = sample
          X = X.to(self.device)
          A = A.to(self.device)
          # labels.to(self.device)
          if permute_samples:
            X, A, _ = self.permute_x_a(X, A)
          if remove_structure:
            X = torch.zeros_like(X)
          preds, penalties = self.model.forward(X, A)
        elif len(sample) == 4:
          X, A, XS, labels = sample
          X = X.to(self.device)
          A = A.to(self.device)
          XS = XS.to(self.device)
          # labels.to(self.device)
          if permute_samples:
            X, A, XS = self.permute_x_a(X, A, XS)
          if remove_structure:
            XS = torch.zeros_like(XS)
          preds, penalties = self.model.forward(X, A, XS)
        else:
          raise ValueError()
        loss, correct = self.loss_fn(preds, labels, device=self.device)
        loss += penalties * p_lambda
        loss.backward()
        if max_grad is not None:
          torch.nn.utils.clip_grad_norm_(self.model.parameters(), max_grad, 'inf')
        if s_idx % opt_update_freq == 0:
          optimiser.step()
          optimiser.zero_grad()
        epoch_loss += loss
        epoch_correct += correct
        # epoch_total += len(labels[1])
        epoch_total += 1
      
      test_loss, test_correct, test_total = 0, 0, 0
      for sample in test_set:
        if len(sample) == 3:
          X, A, labels = sample
          X = X.to(self.device)
          A = A.to(self.device)
          if permute_samples:
            X, A, _ = self.permute_x_a(X, A)
          if remove_structure:
            X = torch.zeros_like(X)
          preds, penalties = self.model.forward(X, A)
        elif len(sample) == 4:
          X, A, XS, labels = sample
          X = X.to(self.device)
          A = A.to(self.device)
          XS = XS.to(self.device)
          if permute_samples:
            X, A, XS = self.permute_x_a(X, A, XS)
          if remove_structure:
            XS = torch.zeros_like(XS)
          preds, penalties = self.model.forward(X, A, XS)
        else:
          raise ValueError()
        loss, correct = self.loss_fn(preds, labels, device=self.device)
        loss += penalties * p_lambda
        test_loss += loss
        test_correct += correct
        # test_total += len(labels[1])
        test_total += 1
      
      row = [i + 1,
             round(epoch_correct.item() / epoch_total, 4),
             round(epoch_loss.item(), 4),
             round(test_correct.item() / test_total, 4),
             round(test_loss.item(), 4),
             time.time() - start]
      row[5] = "{:2}:{:02.0f}".format(int(divmod(row[5], 60)[0]), divmod(row[5], 60)[1])
      t.add_row(row)
      with open(f'{self.directory}/table.txt', 'w') as f:
        f.write(t.get_string())
      if not t_init:
        print('\n'.join(t.get_string().splitlines()[:3]))
        t_init = True
      print('\n'.join(t.get_string().splitlines()[-2:]))
    
    self.model.save(f'{self.directory}/model')
