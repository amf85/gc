import json

import torch
import torch.nn.functional as F
from torch import nn
from torch.nn import BatchNorm1d
from torch_geometric.nn import GraphConv
from torch_geometric.utils import dense_to_sparse, add_remaining_self_loops

import decompose as dc
from util import util


# class SpectralPoolingClassifier(nn.Module):
#     def __init__(self, pooling_modules, mlp_in_dim, mlp_out_dims):
#         super(SpectralPoolingClassifier, self).__init__()
#         self.pooling_modules = pooling_modules
#         self.mlp_in_dim = mlp_in_dim
#         self.mlp_out_dims = mlp_out_dims
#         self.mlp_dims = [self.mlp_in_dim] + self.mlp_out_dims
#         self.mlp_layers = nn.ModuleList()
#         self.mlp_batchnorms = nn.ModuleList()
#         for L in range(len(self.mlp_dims) - 1):
#             self.mlp_layers.append(Linear(self.mlp_dims[L], self.mlp_dims[L+1]))
#             self.mlp_batchnorms.append(BatchNorm1d(self.mlp_dims[L+1]))
#
#
#     def forward(self, X, A, XS=None):
#         for L in range(len(self.pooling_modules)):
#             if L == 0 and XS is not None:
#                 X, A = self.pooling_modules[L].forward(X, A, XS)
#             else:
#                 X, A = self.pooling_modules[L].forward(X, A)
#
#         X = torch.mean(X, dim=0)
#         for L in range(len(self.mlp_layers)):
#             X = self.mlp_layers[L](X)
#             if L < len(self.mlp_layers) - 1:
#                 X = F.relu(X)
#                 # X = self.mlp_batchnorms[L](X)
#
#         # X = F.softmax(X)
#         return X, A

class SimplerSpectralPoolingModule(nn.Module):
  
  def __init__(self, n_node_features,
               n_spectral_vectors,
               node_gnn_out_dims,
               node_mlp_out_dims,
               spectral_out_dims,
               add_structure_to_cluster_processing=True):
    super(SimplerSpectralPoolingModule, self).__init__()
    
    self.n_node_features = n_node_features
    self.n_spectral_vectors = n_spectral_vectors
    self.node_gnn_out_dims = node_gnn_out_dims
    self.node_mlp_out_dims = node_mlp_out_dims
    self.spectral_out_dims = spectral_out_dims
    self.add_structure_to_cluster_processing = add_structure_to_cluster_processing
    self.cluster_gnn_layers = nn.ModuleList()
    self.cluster_batchnorms = nn.ModuleList()
    self.feature_gnn_layers = nn.ModuleList()
    self.feature_gnn_batchnorms = nn.ModuleList()
    self.feature_mlp_layers = nn.ModuleList()
    self.feature_mlp_batchnorms = nn.ModuleList()
    
    self.spectral_dimensions = [self.n_spectral_vectors] + self.spectral_out_dims
    if self.add_structure_to_cluster_processing:
      self.feature_gnn_dimensions = [
                                      self.n_node_features + self.n_spectral_vectors] + self.node_gnn_out_dims
    else:
      self.feature_gnn_dimensions = [self.n_node_features] + self.node_gnn_out_dims
    self.feature_mlp_dimensions = [self.feature_gnn_dimensions[-1]] + self.node_mlp_out_dims
    
    for i in range(len(self.spectral_dimensions) - 1):
      self.cluster_gnn_layers.append(
        GraphConv(self.spectral_dimensions[i], self.spectral_dimensions[i + 1]))
      self.cluster_batchnorms.append(BatchNorm1d(self.spectral_dimensions[i + 1]))
    for i in range(len(self.feature_gnn_dimensions) - 1):
      self.feature_gnn_layers.append(
        GraphConv(self.feature_gnn_dimensions[i], self.feature_gnn_dimensions[i + 1]))
      self.feature_gnn_batchnorms.append(BatchNorm1d(self.feature_gnn_dimensions[i + 1]))
    for i in range(len(self.feature_mlp_dimensions) - 1):
      self.feature_mlp_layers.append(
        GraphConv(self.feature_mlp_dimensions[i], self.feature_mlp_dimensions[i + 1]))
      self.feature_mlp_batchnorms.append(BatchNorm1d(self.feature_mlp_dimensions[i + 1]))
  
  def forward(self, X, A, XS=None):
    """

    :param X: the node features
    :param A: should be dense adjacency matrix
    :param XS: precomputed laplacian eigenvectors if available
    :return: features for cluster X, and dense adjacency matrix of clusters A
    """
    # this just means can provide preprocessed eigenvectors for first layer to save time
    if XS is None:
      # XS = dc.compute_laplacian_eigenvectors(A, n_vectors=self.n_spectral_vectors)
      XS = dc.pyt_compute_laplacian_eigs(n_vectors=self.n_spectral_vectors, A=A,
                                         L_normalization=None)
    if self.add_structure_to_cluster_processing:
      original_XS = XS
    # apply clustering GNN to generate assignment matrix S
    A_edge_index, A_edge_weight = dense_to_sparse(A)
    A_edge_index, A_edge_weight = add_remaining_self_loops(A_edge_index, A_edge_weight)
    for L in range(len(self.cluster_gnn_layers)):
      XS = self.cluster_gnn_layers[L].forward(XS, A_edge_index,
                                              A_edge_weight)  # todo: add self loop option
      XS = F.relu(XS)
      XS = self.cluster_batchnorms[L](XS)
    
    # make assignment matrix on by applying softmax on a per-row basis
    S = F.softmax(XS, dim=1)
    
    # generate new adjacency matrix using assignment matrix
    # A = to_dense_adj(A)
    A = torch.matmul(S.T, torch.matmul(A, S))
    
    # this makes A_c encode within-cluster edges with very high weight, between-cluster edges with very low weight
    # so essentially like a collection of disconnected graphs
    A_c = torch.matmul(S, S.T)
    A_c_edge_index, A_c_edge_weight = dense_to_sparse(A_c)
    A_c_edge_index = A_c_edge_index.long()
    A_c_edge_weight = A_c_edge_weight.float()
    
    # this then allows for message passing to occur separately within the clusters themselves
    X_c = X
    if self.add_structure_to_cluster_processing:
      X_c = torch.cat([X, original_XS], dim=1)
    for L in range(len(self.feature_gnn_layers)):
      # should be no need to add self loops here as every node is assigned to same cluster as itself...
      X_c = self.feature_gnn_layers[L].forward(X_c, A_c_edge_index, A_c_edge_weight)
      X_c = F.relu(X_c)
      X_c = self.feature_gnn_batchnorms[L](X_c)
    
    # this takes a weighted mean of nodes in the cluster (for each feature)
    X_c = torch.matmul(S.T, X_c)
    # applying mlp to each cluster to get feature embedding essentially same as graphconv with identity as A
    c_eye = torch.eye(X_c.size()[0])
    c_eye_edge_index, _ = dense_to_sparse(c_eye)
    c_eye_edge_index = c_eye_edge_index.long()
    for L in range(len(self.feature_mlp_layers)):
      X_c = self.feature_mlp_layers[L].forward(X_c, c_eye_edge_index)
    
    # new graph represented by cluster features X_c and new adjacency A
    return X_c, A
  
  def save(self, directory):
    util.validate_directory(directory)
    torch.save(self.state_dict(), f'{directory}/model_params.pyt')
    params = {
      'inp_dim': self.inp_dim,
      'out_dims': self.out_dims
    }
    json.dump(params, open(f'{directory}/init_params.json', 'w'), indent=4)
  
  @staticmethod
  def load(directory):
    params = json.load(open(f'{directory}/init_params.json', 'r'))
    model = SimplerSpectralPoolingModule(**params)
    model.load_state_dict(torch.load(f'{directory}/model_params.pyt'))
    return model
