import torch
import torch.nn as nn

from graphgen.gc_hierarch import create_dataset

tgt_dev = 'cpu'
device = torch.device('cuda' if torch.cuda.is_available() and tgt_dev == 'gpu' else 'cpu')


def get_fiedler_vector(A, L_normalisation=None, eps=1e-4, return_eigenvectors=False):
  D = torch.diagflat(torch.sum(A, axis=0))  # TODO check this isn't causing errors...
  L = D - A
  eig_vals, eig_vecs = torch.symeig(L, eigenvectors=True)
  sorted_eig_val_idxs = torch.argsort(eig_vals)
  sorted_eig_vals = eig_vals[sorted_eig_val_idxs]
  fiedler_idx = torch.where(sorted_eig_vals < eps)[0][-1]
  sorted_eig_vecs = eig_vecs[:, sorted_eig_val_idxs]
  fiedler_vec = sorted_eig_vecs[:, 1]
  if return_eigenvectors:
    return fiedler_vec, sorted_eig_vecs
  else:
    return fiedler_vec, None


def split_by_fiedler(A, splits, return_eigenvectors=False):
  fiedler_vec, eigenvectors = get_fiedler_vector(A, return_eigenvectors=return_eigenvectors)
  X_new = torch.unsqueeze(fiedler_vec, dim=1)
  node_order = torch.argsort(fiedler_vec)
  
  subgraph_vector = torch.zeros(A.size()[0], 1)
  sg1_idxs = node_order[:int(len(node_order) / 2)]
  sg2_idxs = node_order[int(len(node_order) / 2):]
  subgraph_vector[sg1_idxs] = 0
  subgraph_vector[sg2_idxs] = 1
  
  if splits == 0:
    return X_new, subgraph_vector
  
  if A.size()[0] <= 4:
    subgraphs = torch.zeros(subgraph_vector.size()[0], splits - 1)
    subgraphs = torch.cat((subgraph_vector, subgraphs), dim=1)
    X = torch.zeros(X_new.size()[0], splits - 1)
    X = torch.cat((X_new, X), dim=1)
    return X, subgraphs
  
  sg1_A = A[sg1_idxs][:, sg1_idxs]
  sg2_A = A[sg2_idxs][:, sg2_idxs]
  
  sg1_X, sg1_subgraphs = split_by_fiedler(sg1_A, splits - 1, return_eigenvectors=False)
  sg2_X, sg2_subgraphs = split_by_fiedler(sg2_A, splits - 1, return_eigenvectors=False)
  
  feature_vector = torch.zeros(A.size()[0], sg1_X.size()[1]).float()
  feature_vector[sg1_idxs] = sg1_X
  feature_vector[sg2_idxs] = sg2_X
  
  X_new = torch.cat((X_new, feature_vector), dim=1)
  merged_subgraphs = torch.zeros(subgraph_vector.size()[0], sg1_subgraphs.size()[1])
  merged_subgraphs[sg1_idxs] = sg1_subgraphs
  merged_subgraphs[sg2_idxs] = sg2_subgraphs
  
  merged_subgraphs = torch.cat((subgraph_vector, merged_subgraphs), dim=1)
  
  if return_eigenvectors:
    return X_new, merged_subgraphs, eigenvectors
  else:
    return X_new, merged_subgraphs


def divisive_spectral_features(A, L_normalisation=None, eps=1e-12):
  # D = np.diagflat(np.sum(A, axis=0))
  # L = D - A
  # start = time.time()
  # start_prcs = time.process_time()
  # evals = np.linalg.eigvalsh(L)
  # print(f'evals = {time.time() - start}')
  # start = time.time()
  # eig_vals, eig_vecs = np.linalg.eigh(L)
  # print(f'linalg eig = {time.time() - start}')
  # # pyt_L = torch.tensor(L).to(device)
  # start = time.time()
  # pyt_A = torch.tensor(A).to(device)
  # print(f'move A to tensor on GPU = {time.time() - start}')
  # pyt_D = torch.diagflat(torch.sum(pyt_A, axis=0))
  # pyt_L = pyt_D - pyt_A
  # print(f'compute L on GPU = {time.time() - start}')
  # start = time.time()
  # pyt_eig_vals, pyt_eig_vecs = torch.symeig(pyt_L, eigenvectors=True)
  # print(f'pyt eig = {time.time() - start}')
  # # eig_vals, eig_vecs = sp.sparse.linalg.eigs(L, k=10, which='SR', tol=0)
  # # for Laplacian, eigenvalues should all be >= 0 and real, but floating point programming...
  # sorted_eig_val_idxs = np.argsort(np.real(eig_vals))
  # pyt_sorted_eig_val_idxs = torch.argsort(pyt_eig_vals)
  # sorted_eig_vals = eig_vals[sorted_eig_val_idxs]
  # pyt_sorted_eig_vals = pyt_eig_vals[pyt_sorted_eig_val_idxs]
  #
  #
  # graph_components = 0
  # for pyt_e_val_idx in pyt_sorted_eig_val_idxs:
  #     if np.abs(pyt_eig_vals[pyt_e_val_idx]) < eps:
  #         graph_components += 1
  #     else:
  #         break
  # assert graph_components > 0, 'Possible Laplacian error'
  #
  # pyt_sorted_eig_vecs = pyt_eig_vecs[:, pyt_sorted_eig_val_idxs]
  # # for connected graph (1 component), fiedler vec is second smallest eigenpair
  
  # fiedler_vec = pyt_sorted_eig_vecs[:, graph_components]
  
  # subgraphs = torch.zeros(A.size()[0], 0)
  # X = torch.zeros(A.size()[0], 0)
  X, subgraphs, eigenvectors = split_by_fiedler(A, splits=9, return_eigenvectors=True)
  return X, subgraphs


class DivisiveSpectralPoolModule(nn.Module):
  
  def __init__(self):
    super(DivisiveSpectralPoolModule, self).__init__()
  
  def forward(self, X, A, X_structural=None):
    if X_structural is None:
      X_structural


def test():
  dataset_params = {
    'branching_factors': [3],
    'depths': [2],
    'motif_sizes': [4]
  }
  
  data_dir = '../../output/tests/dsp/datasets/set0'
  test_dir = '../../output/tests/dsp/tests/test0'
  train_samples, test_samples = create_dataset(10, 10, dataset_params, data_dir)
  for s in train_samples:
    divisive_spectral_features(torch.tensor(s.A))
  # s = train_samples[0]
  # A = torch.tensor(s.A).float()
  # divisive_spectral_features(s.A)


if __name__ == '__main__':
  test()
