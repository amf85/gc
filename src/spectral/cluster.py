import itertools
import json

import numpy as np
import scipy as sp
import scipy.sparse
import torch
import torch.nn.functional as F
from torch import nn
from torch.nn import BatchNorm1d
from torch_geometric.nn import GraphConv
from torch_geometric.utils import dense_to_sparse, add_remaining_self_loops

# from decompose import compute_laplacian_eigenvectors
import decompose as dc
import vis.communities as viscom
from graphgen.sbm import SBM
from util import util


class SpectralSoftAssignment(nn.Module):
  
  def __init__(self, inp_dim, out_dims, j_hops=1):
    super(SpectralSoftAssignment, self).__init__()
    self.inp_dim = inp_dim
    self.out_dims = out_dims
    self.j_hops = j_hops
    self.gcn_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    self.n_classes = out_dims[-1]
    self.dimensions = [self.inp_dim] + self.out_dims
    for i in range(len(self.out_dims)):
      self.gcn_layers.append(GraphConv(self.dimensions[i], self.dimensions[i + 1]))
      self.batchnorms.append(BatchNorm1d(self.dimensions[i + 1]))
  
  def forward(self, X, A, XS=None):
    if XS is None:
      XS = dc.pyt_compute_laplacian_eigs(n_vectors=self.inp_dim, A=A, L_normalization=None)
    A = torch.pow(A, self.j_hops)
    A_edges, A_attrs = dense_to_sparse(A)
    A_edges, A_attrs = add_remaining_self_loops(A_edges, A_attrs)
    for i in range(len(self.gcn_layers)):
      XS = self.gcn_layers[i].forward(XS, A_edges, A_attrs)
      XS = F.relu(XS)
      XS = self.batchnorms[i](XS)
    S = F.softmax(XS, dim=1)
    return S, A
  
  def save(self, directory):
    util.validate_directory(directory)
    torch.save(self.state_dict(), f'{directory}/model_params.pyt')
    params = {
      'inp_dim': self.inp_dim,
      'out_dims': self.out_dims
    }
    json.dump(params, open(f'{directory}/init_params.json', 'w'), indent=4)
  
  @staticmethod
  def load(directory):
    params = json.load(open(f'{directory}/init_params.json', 'r'))
    model = SpectralSoftAssignment(**params)
    model.load_state_dict(torch.load(f'{directory}/model_params.pyt'))
    return model


def sbm_communities_loss_fn(predictions, labels, n_classes, return_correct=True):
  loss = np.inf
  correct = 0
  permutations = itertools.permutations(np.arange(n_classes))
  for perm in permutations:
    p_labels = np.choose(labels, perm)
    t_labels = torch.tensor(p_labels).unsqueeze(dim=1)
    likelihoods = torch.gather(predictions, dim=1, index=t_labels)
    log_likelihoods = torch.log(likelihoods + 1e-8)
    nll = -log_likelihoods.sum()
    if nll < loss:
      loss = nll
      correct = (t_labels[:, 0] == torch.max(predictions, dim=1)[1]).sum()
  if return_correct:
    return loss, correct
  else:
    return loss


def hn_graphlabel_loss_fn(prediction, label, return_correct=True):
  print('test')


def train():
  steps = 100000
  report_freq = 1000
  LR = 1e-4
  n_nodes = 50
  cluster_prior = [0.2, 0.8]
  connectivity = [[0.4, 0.05],
                  [0.05, 0.2]]
  sbm = SBM(n_nodes=n_nodes, cluster_prior=cluster_prior, connectivity=connectivity)
  n_features = 20
  model = SpectralSoftAssignment(inp_dim=n_features, out_dims=[10, 5, len(cluster_prior)])
  optimiser = torch.optim.Adam(model.parameters(), lr=LR)
  
  report_loss, report_correct, report_total = 0, 0, 0
  
  for i in range(steps):
    
    labels, A = sbm.generate()
    X = dc.compute_laplacian_eigenvectors(A, n_features)
    coo_A = sp.sparse.coo_matrix(A)
    X = torch.tensor(X).float()
    # X = torch.zeros(X.shape)
    A = torch.tensor(np.stack([coo_A.row, coo_A.col])).long()
    preds, _ = model.forward(X, A)
    loss, correct = model.loss_fn(preds, labels)
    loss.backward()
    optimiser.step()
    
    # logits, predicted = torch.max(preds.data, dim=1)
    # correct = (predicted == torch.tensor(labels)).sum().item()
    report_correct += correct.item()
    report_total += len(labels)
    report_loss += loss
    
    if i % report_freq == report_freq - 1:
      print(
        f'Sample {i + 1}, last {report_freq}: accuracy = {round(report_correct / report_total, 4)}%, loss = {report_loss}')
      report_loss, report_correct, report_total = 0, 0, 0
  
  model.save(f'../../output/tests/model_0')
  return model


def test():
  # train()
  n_nodes = 50
  n_features = 20
  # cluster_prior = [0.2, 0.8]
  cluster_prior = [0.5, 0.5]
  connectivity = [[0.4, 0.025],
                  [0.025, 0.4]]
  # connectivity = [[0.2, 0.01],
  #                 [0.01, 0.1]]
  # connectivity = [[1, 1],
  #                 [1, 1]]
  sbm = SBM(n_nodes=n_nodes, cluster_prior=cluster_prior, connectivity=connectivity)
  labels, A = sbm.generate()
  X = dc.compute_laplacian_eigenvectors(A, 20)
  coo_A = sp.sparse.coo_matrix(A)
  
  # ssa = SpectralSoftAssignment(inp_dim=5, out_dims=[10, 5, 2])
  ssa = SpectralSoftAssignment.load(f'../../output/tests/model_0')
  preds, _ = ssa.forward(torch.tensor(X).float(),
                         torch.tensor(np.stack([coo_A.row, coo_A.col])).long())
  # labels = torch.tensor(labels)
  loss, correct = ssa.loss_fn(preds, labels)
  
  visualiser = viscom.CommunitiesVisualiser()
  visualiser.visualise(A, torch.argmax(preds, dim=1).detach().numpy(),
                       f'../../output/tests/pred_communities.png')
  visualiser.visualise(A, labels, f'../../output/tests/community_labels.png')
  
  print('test')
  
  pass


if __name__ == '__main__':
  test()
