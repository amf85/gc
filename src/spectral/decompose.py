import numpy as np
import scipy.sparse
import torch

from graphgen.sbm import SBM


def compute_laplacian_eigenvectors(A, n_vectors, sorted='asc'):
  k = n_vectors if n_vectors <= A.shape[0] - 1 else A.shape[0] - 0
  spare = 0 if n_vectors <= A.shape[0] - 1 else n_vectors - A.shape[0]
  
  # D = np.sum(A, axis=0) * np.eye(A.shape[0])  # NO!
  D = np.diagflat(np.sum(A, axis=0))
  L = D - A
  
  # eig_vals, eig_vecs = scipy.sparse.linalg.eigs(L, k=k, which='SR', tol=5e-5)
  eig_vals, eig_vecs = np.linalg.eig(L)
  if not sorted:
    return np.real(eig_vecs)
  eig_vecs = eig_vecs[:, np.argsort([np.abs(e) for e in eig_vals])]
  if sorted == 'asc':
    return np.concatenate([np.real(eig_vecs[:, :n_vectors]), np.zeros(shape=(A.shape[0], spare))],
                          axis=1)
  elif sorted == 'desc':
    raise NotImplementedError()
    # check not inadvertently reversing node order rather than feature order...
    # return np.real[eig_vecs[::-1]] + [0]*spare
  else:
    raise ValueError(f'sorted must be one of ["asc", "desc", {False}]')


def pyt_compute_laplacian_eigs(n_vectors, A, device=torch.device('cpu')):
  D = torch.diagflat(torch.sum(A, dim=0))
  L = D - A
  eig_vals, eig_vecs = torch.symeig(L, eigenvectors=True)
  # TODO: could potentially NOT sort these... and just rely on the default output
  # todo may not be easily differentiable
  sorted_eig_val_idxs = torch.argsort(eig_vals)
  # sorted_eig_vals = eig_vals[sorted_eig_val_idxs]
  sorted_eig_vecs = eig_vecs[:, sorted_eig_val_idxs]
  z = n_vectors - sorted_eig_vecs.size()[1]
  if z < 0:
    return sorted_eig_vecs[:, :n_vectors]
  else:
    spare = torch.zeros(size=(sorted_eig_vecs.size()[0], z)).to(device)
    sorted_eig_vecs = torch.cat([sorted_eig_vecs, spare], dim=1)
    return sorted_eig_vecs


# def pyt_compute_laplacian_eigs(n_vectors, A, A_weight=None, sorted='asc', L_normalization=None):
#     # k = n_vectors if n_vectors <= A.size()[0] - 1 else A.size()[0]
#     spare = 0 if n_vectors <= A.size()[0] - 1 else n_vectors - A.size()[0]
#
#     if A_weight is None:
#         A_edge, A_weight = dense_to_sparse(A)
#     else:
#         A_edge = A
#
#     L_edge, L_weight = get_laplacian(A_edge, A_weight, normalization=L_normalization)
#     L = to_dense_adj(L_edge, edge_attr=L_weight)[0]
#     L = L.double()
#     eig_vals, eig_vecs = torch.eig(L, eigenvectors=True)
#     ordered_perm = torch.argsort(eig_vals[:, 0])
#     if sorted != 'asc':
#          raise NotImplementedError()
#     ordered_vecs = eig_vecs[:, ordered_perm]
#     X = torch.cat([ordered_vecs[:, :n_vectors], torch.zeros(size=(A.size()[0], spare))], dim=1).float()
#     return X


def compute_fiedler(A):
  D = np.sum(A, axis=0) * np.eye(A.shape[0])
  L = D - A
  eig_vals, _ = scipy.sparse.linalg.eigs(L, k=2, which='SR', tol=5e-1)
  return sorted(np.real(eig_vals))[1]


def test():
  n_nodes = 30
  cluster_prior = [0.2, 0.8]
  connectivity = [[0.4, 0.05],
                  [0.05, 0.2]]
  sbm = SBM(n_nodes=n_nodes, cluster_prior=cluster_prior, connectivity=connectivity)
  labels, A = sbm.generate()
  X = compute_laplacian_eigenvectors(A, 5)
  
  print('test')


if __name__ == '__main__':
  test()
