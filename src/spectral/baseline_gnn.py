import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import GraphConv
from torch_geometric.utils import dense_to_sparse, add_remaining_self_loops

import decompose as decompose
import graphgen.gc_hierarch as harch
import test_frame as frame
from sp_tests import (std_reg_lbl_loss_fn)


class BaselinGNN(nn.Module):
  
  def __init__(self,
               input_feature_dimension,
               gnn_out_dimensions,
               mlp_out_dimensions):
    super(BaselinGNN, self).__init__()
    self.gnn_dimensions = [input_feature_dimension] + gnn_out_dimensions
    self.gnn_layers = nn.ModuleList()
    self.mlp_dimensions = [gnn_out_dimensions[-1]] + mlp_out_dimensions
    self.mlp_layers = nn.ModuleList()
    for L in range(len(gnn_out_dimensions)):
      self.gnn_layers.append(GraphConv(self.gnn_dimensions[L], self.gnn_dimensions[L + 1]))
    for L in range(len(mlp_out_dimensions)):
      self.mlp_layers.append(nn.Linear(self.mlp_dimensions[L], self.mlp_dimensions[L + 1]))
  
  def forward(self, X, A, XS=None):
    A_edge, A_attr = dense_to_sparse(A)
    A_edge, A_attr = add_remaining_self_loops(A_edge, A_attr)
    for layer in self.gnn_layers:
      X = layer(X, A_edge, A_attr)
      X = F.relu(X)
    
    X = torch.mean(X, dim=0)
    for layer in self.mlp_layers:
      X = layer(X)
      X = F.relu(X)
    
    penalties = 0
    return X, penalties


class BaselineGNNFrame(frame.TestFramework):
  def __init__(self, directory, data_path, model,
               n_classes=None,
               n_spectral_features=None,
               add_spectral_v1_to_X=False):
    super(BaselineGNNFrame, self).__init__(directory, data_path, model)
    self.n_classes = n_classes
    self.nllloss = torch.nn.NLLLoss()
    self.n_spectral_features = n_spectral_features
    self.add_spectral_v1_to_X = add_spectral_v1_to_X
  
  def preprocess_features(self, dataset, model_params):
    start = time.time()
    processed_dataset = []
    for sample in dataset:
      label = [sample.graph_label]
      A = sample.A
      X = np.expand_dims(sample.node_info, axis=1)
      X = torch.tensor(X).float()
      XS = None
      if self.add_spectral_v1_to_X:
        XS = decompose.compute_laplacian_eigenvectors(A, self.n_spectral_features)
        XS = torch.tensor(XS).float()
        X = torch.cat([X, XS], dim=1)
        # XS = None
      A = torch.tensor(sample.A).float()
      processed_dataset.append((X, A, XS, label))
    print(f'Preprocessing time: {time.time() - start}s')
    return processed_dataset
  
  def loss_fn(self, preds, labels, device):
    return std_reg_lbl_loss_fn(preds, labels, device=device)
    # return std_cat_lbl_loss_fn(preds, labels, nllloss=self.nllloss, label_translation=1)
    # return num_clusters_loss_fn(preds, labels)
    # return ground_truth_communities_loss_fn(preds, labels, n_classes=self.n_classes)
    # return max_cluster_loss_fn(preds, labels, nllloss=self.nllloss)


def test():
  ms = list(np.arange(10, 20))
  dataset_params = {
    'branching_factors': [2, 3],
    'depths': [1],
    # 'motif_sizes': [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
    'motif_sizes': [3, 4, 5]
  }
  
  data_dir = '../../output/tests/baseline_gnn/datasets/set0'
  test_dir = '../../output/tests/baseline_gnn/tests/test0'
  harch.create_dataset(1000, 1000, dataset_params, data_dir)
  
  epochs = 500
  LR = 3e-3
  opt_class = torch.optim.Adam
  node_features = 1
  out_dimension = 1
  add_spectral_v1_to_X = True
  n_spectral_features = 40
  if add_spectral_v1_to_X:
    node_features = node_features + n_spectral_features
  
  model = BaselinGNN(input_feature_dimension=node_features,
                     gnn_out_dimensions=[30, 30, 30],
                     mlp_out_dimensions=[10, out_dimension])
  
  framework = BaselineGNNFrame(directory=test_dir,
                               data_path=data_dir,
                               model=model,
                               n_classes=out_dimension,
                               n_spectral_features=n_spectral_features,
                               add_spectral_v1_to_X=add_spectral_v1_to_X)
  
  framework.train_model(model_params=None,
                        epochs=epochs,
                        LR=LR,
                        opt_class=opt_class,
                        permute_samples=False,
                        remove_structure=False)


if __name__ == '__main__':
  test()
