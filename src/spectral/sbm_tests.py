import json
import pickle

import numpy as np
import scipy as sp
import scipy.sparse
import torch

import graphgen.sbm as sbm
import spectral.cluster as cluster
import spectral.decompose as decompose
import test_frame as frame
from util import util


def create_dataset(train_samples, test_samples, params, directory):
  util.validate_directory(directory)
  set_params = {
    'train_samples': train_samples,
    'test_samples': test_samples,
    'generator_params': params
  }
  json.dump(set_params, open(f'{directory}/dataset_params.json', 'w'), indent=2)
  block_model = sbm.SBM(**params)
  train_set = []
  test_set = []
  for i in range(train_samples):
    train_set.append(block_model.generate())
  for i in range(test_samples):
    test_set.append(block_model.generate())
  pickle.dump(train_set, open(f'{directory}/train.pkl', 'wb'))
  pickle.dump(test_set, open(f'{directory}/test.pkl', 'wb'))


class SBMTestFramework(frame.TestFramework):
  def __init__(self, directory, data_path, model):
    super(SBMTestFramework, self).__init__(directory, data_path, model)
  
  class ModelParams(dict):
    def __init__(self, n_features, out_dims):
      super(SBMTestFramework.ModelParams, self).__init__()
      self['inp_dim'] = n_features
      self['out_dims'] = out_dims
  
  def loss_fn(self, preds, labels):
    return cluster.sbm_communities_loss_fn(preds, labels, self.model.n_clusters)
  
  def preprocess_features(self, dataset, model_params):
    processed_dataset = []
    for sample in dataset:
      labels, A = sample
      X = decompose.compute_laplacian_eigenvectors(A, model_params['inp_dim'])
      X = torch.tensor(X).float()
      coo_A = sp.sparse.coo_matrix(A)
      A = torch.tensor(np.stack([coo_A.row, coo_A.col])).long()
      processed_dataset.append((X, A, labels))
    return processed_dataset


def test():
  n_nodes = 75
  # cluster_prior = [0.3, 0.7]
  # connectivity = [[0.4, 0.1],
  #                 [0.1, 0.2]]
  cluster_prior = [0.3, 0.5, 0.2]
  connectivity = [[0.4, 0.1, 0.05],
                  [0.1, 0.2, 0.15],
                  [0.05, 0.15, 0.3]]
  dataset_params = {
    'n_nodes': n_nodes,
    'cluster_prior': cluster_prior,
    'connectivity': connectivity
  }
  data_dir = '../../output/tests/datasets/set2'
  test_dir = '../../output/tests/test_frameworks/t2'
  create_dataset(train_samples=100, test_samples=100, params=dataset_params, directory=data_dir)
  
  epochs = 50
  LR = 1e-5
  n_features = 75
  
  out_dims = [50, 50, 25, 10, len(cluster_prior)]
  model_params = SBMTestFramework.ModelParams(n_features=n_features, out_dims=out_dims)
  model = cluster.SpectralSoftAssignment(**model_params)
  framework = SBMTestFramework(directory=test_dir, data_path=data_dir, model=model)
  
  framework.train_model(model_params, epochs=epochs, LR=LR, opt_class=torch.optim.Adam)


if __name__ == '__main__':
  test()
