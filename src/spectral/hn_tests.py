import json
import pickle

import numpy as np
import scipy as sp
import scipy.sparse
import torch

import g_classify as gclassify
import graphgen.gc_hierarch as hn
import spectral.decompose as decompose
import test_frame as frame
from util import util


def generate_motif(n_nodes):
  gotone = False
  while not gotone:
    A = np.random.randint(0, 2, size=(n_nodes, n_nodes))
    for i in range(n_nodes):
      A[i, i] = 0
    A = np.tril(A) + np.tril(A).T
    if decompose.compute_fiedler(A) > 0:  # true iff A is connected
      gotone = True
  X = ['c'] + ['p'] * (n_nodes - 1)
  return np.array(X), A


def create_dataset(train_samples, test_samples, params, directory):
  util.validate_directory(directory)
  set_params = {
    'train_samples': train_samples,
    'test_samples': test_samples,
    'generator_params': params
  }
  json.dump(set_params, open(f'{directory}/dataset_params.json', 'w'), indent=2)
  motif_sizes = params['motif_sizes']
  depths = params['depths']
  branching_factors = params['branching_factors']
  
  def create_and_save(n_samples, filename):
    samples = []
    for i in range(n_samples):
      motif_size = np.random.choice(motif_sizes)
      depth = np.random.choice(depths)
      branching_factor = np.random.choice(branching_factors)
      X, A = generate_motif(motif_size)
      samples.append(hn.HierarchicalNetwork(A, X, depth, branching_factor))
      if i % 1000 == 0:
        print(f'Generating {filename}, done {i}/{n_samples}')
    pickle.dump(samples, open(f'{directory}/{filename}', 'wb'))
  
  create_and_save(train_samples, 'train.pkl')
  create_and_save(test_samples, 'test.pkl')


class HNTestFramework(frame.TestFramework):
  def __init__(self, directory, data_path, model):
    super(HNTestFramework, self).__init__(directory, data_path, model)
    self.nllloss = torch.nn.NLLLoss()
  
  class ModelParams(dict):
    def __init__(self, node_features, spectral_features, out_dims):
      super(HNTestFramework.ModelParams, self).__init__()
      self['node_features'] = node_features
      self['spectral_features'] = spectral_features
      self['out_dims'] = out_dims
  
  def loss_fn(self, preds, labels):
    labels = torch.tensor(labels)
    loss = self.nllloss(torch.unsqueeze(preds, dim=0), labels)
    correct = torch.sum(torch.argmax(preds) == labels)
    return loss, correct
  
  def preprocess_features(self, dataset, model_params):
    processed_dataset = []
    for sample in dataset:
      label = [sample.graph_label]
      A = sample.A
      X = decompose.compute_laplacian_eigenvectors(A, model_params['spectral_features'])
      node_info = np.expand_dims(sample.node_info, axis=1)
      X = np.concatenate([node_info, X], axis=1)
      X = torch.tensor(X).float()
      coo_A = sp.sparse.coo_matrix(A)
      A = torch.tensor(np.stack([coo_A.row, coo_A.col])).long()
      processed_dataset.append((X, A, label))
    return processed_dataset


def test():
  dataset_params = {
    'branching_factors': [2, 3, 4],
    'depths': [0, 1, 2, 3],
    'motif_sizes': [3, 4, 5, 6]
  }
  
  data_dir = '../../output/tests/hm/datasets/set0'
  test_dir = '../../output/tests/hm/tests/test0'
  # create_dataset(1000, 1000, dataset_params, data_dir)
  
  epochs = 50
  LR = 1e-4
  opt_class = torch.optim.Adam
  node_features = 1
  spectral_features = 500
  out_dims = [50, 50, 100, 100]
  model_params = HNTestFramework.ModelParams(node_features=node_features,
                                             spectral_features=spectral_features,
                                             out_dims=out_dims)
  model = gclassify.SpectralClassifier(**model_params)
  framework = HNTestFramework(directory=test_dir, data_path=data_dir, model=model)
  
  framework.train_model(model_params, epochs=epochs, LR=LR, opt_class=opt_class)


if __name__ == '__main__':
  test()
  exit(0)
