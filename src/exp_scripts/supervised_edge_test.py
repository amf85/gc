from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES
from util.constants import EC_TESTS as EXPDIR
from util.util import validate_directory
validate_directory(EXPDIR)
from exp_scripts.default_setups import *
from sql.db_util import GCDB
import torch.nn.functional as F


#################### MOTIF EXPANDED NETWORKS ##################################

def get_me_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if
                       f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if
                        f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,

    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }

  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',

      'init_g_min_nodes': 10,
      'init_g_max_nodes': 30,
      'init_g_exp_node_edges': 3.5,
      'motif_min_nodes': 4,
      'motif_max_nodes': 12,
      'motif_exp_node_edges': 5.5,
      'motif_pool_size': 3,
      'expansion_prob': 1.0,
      'maintain_motif_degree': True,
      'n_passes': [1],

      'train_samples': 256 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 128 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },

    'train_identifier': 'sc_me_train_v0' if debug_n_samples_overwrite is None else 'sc_me_debug_train',
    'test_identifier': 'sc_me_test_v1' if debug_n_samples_overwrite is None else 'sc_me_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,

    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },

    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': [],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
    },

    'create_new_splits': False,
  }
  return data_config


def _0_supervised_edges_test_me():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_edges/test_sm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)

  remove_node_level_features = NODE_LEVEL_FEATURES

  # data_config = get_sm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)
  data_config = get_me_data_config(overwrite_identifiers=False,
                                   debug_n_samples_overwrite=None)

  eig_splits = 6
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['sign'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }

  feature_config = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': [],
    'graph_labels': [],
    'edge_labels': ['communities_L1'],
  }

  exp_conf = {
    'batch_size': 32,
    'val_size': 0.0,
    'lambda_nll': 1.0,
  }

  model_conf = {
    'gnn_out_dims': [128, 128, 128, 128, 128],
    'mlp_out_dims': [64, 32, 2],
    'activation': F.relu,
    'add_self_loops': True,
    'batchnorm_gnn': True,
    'batchnorm_mlp': True,
    'dropout_gnn': 0.25,
    'dropout_mlp': 0.4,
  }

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_me,

    'exp_method': exp_supervised_edge_communities,
    'exp_config': exp_conf,

    'model_method': model_edge_gnn,
    'model_config': model_conf,

    'LR': 3e-4,
    'epochs': 100,
    # 'dropout': 0.25,
    # 'lambda_l1': 0.0,
    'lambda_l2': 0.025,

    'feature_config': feature_config,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }

  run_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])


########################################  SHARED MOTIFS  ###########################################

def get_sm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',
  
      'init_min_nodes': 10,
      'init_max_nodes': 25,
      'init_randoms': True,
      'init_rings': False,
      'init_cliques': False,
      'init_stars': False,
      'init_lines': False,
      'init_exp_node_edges': 2.5,
      'expansion_prob': 0.4,
      'maintain_motif_degree': True,
      'n_passes': 1,
  
      'pool_min_nodes': 5,
      'pool_max_nodes': 10,
      'pool_size': 20,
      'pool_randoms': True,
      'pool_rings': True,
      'pool_cliques': False,
      'pool_stars': False,
      'pool_lines': False,
      'pool_exp_node_edges': 4.0,
      
      'train_samples': 512 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 256 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
  
    'train_identifier': 'edges_sm_train_v9' if debug_n_samples_overwrite is None else 'edges_sm_debug_train',
    'test_identifier': 'edges_sm_test_v9' if debug_n_samples_overwrite is None else 'edges_sm_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': [],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
    },
    
    'create_new_splits': False,
  }
  return data_config

def _1_supervised_edges_test_sm():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_edges/test_sm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_sm_data_config(overwrite_identifiers=True, debug_n_samples_overwrite=None)

  eig_splits = 6
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['sign'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  feature_config = {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'edge_features': [],
      'node_labels': [],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
  }
  
  exp_conf = {
    'batch_size': 32,
    'val_size': 0.0,
    'lambda_nll': 1.0,
  }
  
  model_conf = {
    'gnn_out_dims': [128, 128, 128],
    'mlp_out_dims': [128, 2],
    'activation': F.relu,
    'add_self_loops': True,
    'batchnorm_gnn': True,
    'batchnorm_mlp': True,
    'dropout_gnn': 0.2,
    'dropout_mlp': 0.4,
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_sm,
    
    'exp_method': exp_supervised_edge_communities,
    'exp_config': exp_conf,
    
    'model_method': model_edge_gnn,
    'model_config': model_conf,
    
    'LR': 4e-4,
    'epochs': 100,
    # 'dropout': 0.25,
    # 'lambda_l1': 0.0,
    'lambda_l2': 0.1,
    
    
    'feature_config': feature_config,
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

def _2_supervised_edges_test_sm_local_features():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_edges/test_sm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)

  remove_node_level_features = []

  data_config = get_sm_data_config(overwrite_identifiers=True, debug_n_samples_overwrite=None)

  eig_splits = 0
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['sign'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }

  feature_config = {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'edge_features': [],
      'node_labels': [],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
  }

  exp_conf = {
    'batch_size': 32,
    'val_size': 0.0,
    'lambda_nll': 1.0,
  }

  model_conf = {
    'gnn_out_dims': [128, 128, 128],
    'mlp_out_dims': [128, 2],
    'activation': F.relu,
    'add_self_loops': True,
    'batchnorm_gnn': True,
    'batchnorm_mlp': True,
    'dropout_gnn': 0.2,
    'dropout_mlp': 0.4,
  }

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_sm,

    'exp_method': exp_supervised_edge_communities,
    'exp_config': exp_conf,

    'model_method': model_edge_gnn,
    'model_config': model_conf,

    'LR': 5e-4,
    'epochs': 100,
    # 'dropout': 0.25,
    # 'lambda_l1': 0.0,
    'lambda_l2': 0.025,


    'feature_config': feature_config,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }

  run_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

  
if __name__ == '__main__':
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  # _0_supervised_comparisons_recursive_spectral_me()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _1_supervised_comparisons_local_features_me()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _2_supervised_comparisons_recursive_spectral_sbm()G
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _3_supervised_comparisons_local_features_sbm()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _4_supervised_comparisons_recursive_spectral_hn()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _5_supervised_comparisons_local_features_hn()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _6_supervised_comparisons_recursive_spectral_re()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _7_supervised_comparisons_local_features_re()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _0_supervised_edges_test_me()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  _1_supervised_edges_test_sm()
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _2_supervised_edges_test_sm_local_features()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _9_supervised_comparisons_local_features_sm()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  