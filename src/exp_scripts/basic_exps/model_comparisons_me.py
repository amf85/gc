import time

from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES, DISTANCE_DEPENDENT_FEATURES
from util.constants import BASIC_EXPS as EXPDIR
from util.util import validate_directory
validate_directory(EXPDIR)
from exp_scripts.default_setups import *
from sql.db_util import GCDB
from torch.nn import functional as F
from datetime import datetime
# from torch_geometric.nn import GraphConv, GCNConv
from dgl.nn.pytorch import GraphConv, SAGEConv, GATConv


########################################  MOTIF EXPANDED  ##########################################

def get_me_data_config(overwrite_identifiers=False):
  remove_node_level_features = ['betweenness_centrality']
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if
                       f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if
                        f not in remove_graph_level_features]

  eig_splits = 8
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    # 'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_eig_algorithms': ['np_eig'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,

    'per_layer_eigenvectors': [20] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [5] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }

  data_config = {
    'datasets_dir': f'{EXPDIR}/datasets',

    'verbose': 0,

    'init_g_min_nodes': 5,
    'init_g_max_nodes': 20,
    'init_g_exp_node_edges': 1.0,
    'motif_min_nodes': 6,
    'motif_max_nodes': 12,
    'motif_exp_node_edges': 3.5,
    'motif_pool_size': 20,
    'expansion_prob': 1.0,
    'maintain_motif_degree': True,
    'n_passes': [1],

    # 'train_samples': 50 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    'train_samples': 500,
    'test_samples': 0,

    'train_identifier': 'basic_exps_me_500',
    'test_identifier': None,
    'overwrite_identifiers': overwrite_identifiers,

    'test_split_changes': {},

    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
    },

    'create_new_splits': False,
  }
  return data_config

def get_exp_conf_clustering():
  return {
    'batch_size': 128,
    'cross_val_group_start': datetime.now(),
    'cross_val_num_folds': 10,
    'cross_val_split_method': 'random',
    'cross_val_which_fold': None,
    'cross_val_seed': 196759,

    'scale_eig_features': True,
    'scale_local_features': False,
    'scale_node_features': True,

    'val_size': 0.15,
    'val_seed': 196759,
    'early_stop_patience': 15,
    'early_stop_on': 'acc',
    'early_stop_direction': 'maxeq',

    'lambda_cxe': None,
    'lambda_cmse': None,
    'lambda_cmae': None,
    'lambda_tce': None,
    'lambda_cae': None,

    'lambda_mod': 0.,
    'lambda_silh': 0.,
    'lambda_nll': 1.,
  }

def get_eig_conf(eig_splits=0):
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    # 'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_eig_algorithms': ['np_eig'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [3] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  return eig_conf


def gnn_spectral():
  local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/gnn_spectral'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  # local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=None,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True,
    use_master_as_local=True,
  )

  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_me_data_config(overwrite_identifiers=False)
  eig_conf = get_eig_conf(eig_splits=6)

  feature_conf = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': ['communities_L1'],
  }

  exp_conf = get_exp_conf_clustering()
  n_clusters = 20
  exp_conf['n_clusters'] = n_clusters

  model_conf = {
    'gnn_out_dims': [64] * 3,
    'conv_fn': SAGEConv,
    'conv_conf': {'aggregator_type': 'gcn', 'feat_drop': 0, 'bias': True, 'norm': None, 'activation': None},

    'dropout': 0.6,
    'activation': F.relu,
    'self_loops': True,
    'use_batchnorm': True,
    'apply_input_dropout': False
  }
  model_conf['gnn_out_dims'] += [n_clusters]

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,

    'data_method': data_me,
    'data_config': data_config,
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    'model_method': model_node_gnn,
    'model_config': model_conf,

    'LR': 50e-4,
    'epochs': 100,
    'lambda_l2': 0.05,

    'feature_config': feature_conf,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 1,
    'test_acc_freq': 1,
  }

  copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
  for i in range(exp_conf['cross_val_num_folds']):
    print('#' * 60)
    print(f'Beginning cross_val iter {i}')
    exp_conf['cross_val_which_fold'] = i
    run_with_config(**run_config)
    copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
    # GCDB.merge_local_to_master(
    #   master_loc=master_loc,
    #   local_loc=local_loc,
    #   only_keep_every_nth_epoch_images=3,
    #   keep_specific_epoch_images=[i for i in range(0, 20)]
    # )


def gnn_locals():
  local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/gnn_locals'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  # local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=None,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True,
    use_master_as_local=True,
  )

  # remove_node_level_features = []
  remove_node_level_features = DISTANCE_DEPENDENT_FEATURES

  data_config = get_me_data_config(overwrite_identifiers=False)
  eig_conf = get_eig_conf(eig_splits=0)

  feature_conf = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': ['communities_L1'],
  }

  exp_conf = get_exp_conf_clustering()
  n_clusters = 20
  exp_conf['n_clusters'] = n_clusters

  model_conf = {
    'gnn_out_dims': [128] * 5,
    'conv_fn': SAGEConv,
    'conv_conf': {'aggregator_type': 'gcn', 'feat_drop': 0, 'bias': True, 'norm': None, 'activation': None},

    'dropout': 0.4,
    'activation': F.relu,
    'self_loops': True,
    'use_batchnorm': True,
    'apply_input_dropout': False
  }
  model_conf['gnn_out_dims'] += [n_clusters]

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,

    'data_method': data_me,
    'data_config': data_config,
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    'model_method': model_node_gnn,
    'model_config': model_conf,

    'LR': 10e-4,
    'epochs': 100,
    'lambda_l2': 0.05,

    'feature_config': feature_conf,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 1,
    'test_acc_freq': 1,
  }

  copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
  for i in range(exp_conf['cross_val_num_folds']):
    print('#' * 60)
    print(f'Beginning cross_val iter {i}')
    exp_conf['cross_val_which_fold'] = i
    run_with_config(**run_config)
    copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
    # GCDB.merge_local_to_master(
    #   master_loc=master_loc,
    #   local_loc=local_loc,
    #   only_keep_every_nth_epoch_images=3,
    #   keep_specific_epoch_images=[i for i in range(0, 20)]
    # )

def get_exp_conf_mlp_clustering():
  return {
    'batch_size': 100,
    'cross_val_group_start': datetime.now(),
    'cross_val_num_folds': 10,
    'cross_val_split_method': 'random',
    'cross_val_which_fold': None,
    'cross_val_seed': 196759,

    'scale_eig_features': True,
    'scale_local_features': False,
    'scale_node_features': True,

    'val_size': 0.15,
    'val_seed': 196759,
    'early_stop_patience': 15,
    'early_stop_on': 'acc',
    'early_stop_direction': 'maxeq',

    'lambda_cxe': None,
    'lambda_cmse': None,
    'lambda_cmae': None,
    'lambda_tce': None,
    'lambda_cae': None,

    'lambda_mod': 0.,
    'lambda_silh': 0.,
    'lambda_nll': 1.,

    'train_epoch_method': 'train_epoch',
    'visualiser': 'communities'
  }

def mlp_spectral():
  local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/mlp_spectral'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  # local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=None,
    erase_master=False,
    merge=False,
    samples_only=True,
    add_data_from_master=True,
    use_master_as_local=True,
  )

  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_me_data_config(overwrite_identifiers=False)
  eig_conf = get_eig_conf(eig_splits=6)

  feature_conf = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': ['communities_L1'],
  }

  exp_conf = get_exp_conf_mlp_clustering()
  n_clusters = 20
  exp_conf['n_clusters'] = n_clusters

  model_conf = {
    'apply_input_dropout': False,

    'layer_out_dims': [256, 128, 64, 32],
    'activation': F.relu,
    'use_batchnorm': True,
    'dropout': 0.4
  }
  model_conf['layer_out_dims'] += [n_clusters]


  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,

    'data_method': data_me,
    'data_config': data_config,
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    'model_method': model_node_mlp,
    'model_config': model_conf,

    'LR': 30e-4,
    'epochs': 100,
    'lambda_l2': 0.0125,

    'feature_config': feature_conf,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 1,
    'test_acc_freq': 1,
  }

  copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
  for i in range(exp_conf['cross_val_num_folds']):
    print('#' * 60)
    print(f'Beginning cross_val iter {i}')
    exp_conf['cross_val_which_fold'] = i
    run_with_config(**run_config)
    copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
    # GCDB.merge_local_to_master(
    #   master_loc=master_loc,
    #   local_loc=local_loc,
    #   only_keep_every_nth_epoch_images=3,
    #   keep_specific_epoch_images=[j for j in range(0, 20)]
    # )


def mlp_locals():
  local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/mlp_locals'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  # local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=None,
    erase_master=False,
    merge=False,
    samples_only=True,
    add_data_from_master=True,
    use_master_as_local=True,
  )

  # remove_node_level_features = []
  remove_node_level_features = DISTANCE_DEPENDENT_FEATURES

  data_config = get_me_data_config(overwrite_identifiers=False)
  eig_conf = get_eig_conf(eig_splits=0)

  feature_conf = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': ['communities_L1'],
  }

  exp_conf = get_exp_conf_mlp_clustering()
  n_clusters = 20
  exp_conf['n_clusters'] = n_clusters

  model_conf = {
    'apply_input_dropout': False,

    'layer_out_dims': [128, 64, 32, 32],
    'activation': F.relu,
    'use_batchnorm': True,
    'dropout': 0.2
  }
  model_conf['layer_out_dims'] += [n_clusters]

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,

    'data_method': data_me,
    'data_config': data_config,
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    'model_method': model_node_mlp,
    'model_config': model_conf,

    'LR': 10e-4,
    'epochs': 100,
    'lambda_l2': 0.025,

    'feature_config': feature_conf,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 1,
    'test_acc_freq': 1,
  }

  copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
  for i in range(exp_conf['cross_val_num_folds']):
    print('#' * 60)
    print(f'Beginning cross_val iter {i}')
    exp_conf['cross_val_which_fold'] = i
    run_with_config(**run_config)
    copy_db_loc_to_most_recent_exp_dir(master_loc, original=True)
    # GCDB.merge_local_to_master(
    #   master_loc=master_loc,
    #   local_loc=local_loc,
    #   only_keep_every_nth_epoch_images=3,
    #   keep_specific_epoch_images=[i for i in range(0, 20)]
    # )



# def get_exp_conf_edge_clustering():
#   return {
#     'batch_size': 128,
#     'cross_val_group_start': datetime.now(),
#     'cross_val_num_folds': 10,
#     'cross_val_split_method': 'random',
#     'cross_val_which_fold': None,
#     'cross_val_seed': 196759,
#
#     'scale_eig_features': True,
#     'scale_local_features': False,
#     'scale_node_features': True,
#
#     'val_size': 0.15,
#     'val_seed': 196759,
#     'early_stop_patience': 25,
#     'early_stop_on': 'nll',
#     'early_stop_direction': 'mineq',
#
#     # 'lambda_mod': 1.,
#     # 'lambda_silh': 1.,
#     'lambda_nll': 1.,
#
#     'train_epoch_method': 'train_epoch',
#     'visualiser': 'edge_motifs'
#   }
#
# def ec_spectral():
#   local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/ec_spectral'
#   master_loc = f'{EXPDIR}/master_gc.sqlite'
#   local_loc = f'{EXPDIR}/local_gc.sqlite'
#   gcdb = setup_gcdb(
#     master_loc=master_loc,
#     local_loc=local_loc,
#     erase_master=False,
#     merge=True,
#     samples_only=True,
#     add_data_from_master=True
#   )
#
#   remove_node_level_features = NODE_LEVEL_FEATURES
#
#   data_config = get_me_data_config(overwrite_identifiers=False)
#   eig_conf = get_eig_conf(eig_splits=6)
#
#   feature_conf = {
#     'eig_conf': eig_conf,
#     'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
#     'node_features': [],
#     'edge_features': [],
#     'node_labels': ['communities_L1'],
#     'graph_labels': [],
#     'edge_labels': ['communities_L1'],
#   }
#
#   exp_conf = get_exp_conf_edge_clustering()
#
#   model_conf = {
#     'apply_input_dropout': False,
#     'add_self_loops': True,
#
#     'gnn_out_dims': [64] * 3,
#     'gnn_conv_fn': SAGEConv,
#     'gnn_conv_conf': {'aggregator_type': 'gcn', 'feat_drop': 0, 'bias': True, 'norm': None, 'activation': None},
#
#     'edge_pred_out_dims': [64, 2],  # note first dimension gets doubled
#
#     'batchnorm_gnn': True,
#     'batchnorm_edge_pred': True,
#     'activation_gnn': F.relu,
#     'activation_edge_pred': F.relu,
#     'dropout_gnn': 0.3,
#     'dropout_edge_pred': 0.3,
#   }
#
#   run_config = {
#     'gcdb': gcdb,
#     'exp_dir': local_exp_dir,
#
#     'data_method': data_me,
#     'data_config': data_config,
#     'exp_method': exp_supervised_edge_communities,
#     'exp_config': exp_conf,
#     'model_method': model_edge_gnn,
#     'model_config': model_conf,
#
#     'LR': 3e-4,
#     'epochs': 300,
#     'lambda_l2': 0.0125,
#
#     'feature_config': feature_conf,
#
#     'chkp_freq': 1,
#     'vis_freq': 1,
#     'vis_n_samples': 1,
#     'val_freq': 1,
#     'test_freq': 1,
#     'train_acc_freq': 1,
#     'test_acc_freq': 1,
#   }
#
#   copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
#   for i in range(exp_conf['cross_val_num_folds']):
#     print('#' * 60)
#     print(f'Beginning cross_val iter {i}')
#     exp_conf['cross_val_which_fold'] = i
#     run_with_config(**run_config)
#     copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
#     GCDB.merge_local_to_master(
#       master_loc=master_loc,
#       local_loc=local_loc,
#       only_keep_every_nth_epoch_images=3,
#       keep_specific_epoch_images=[i for i in range(0, 20)]
#     )
#
#
# def ec_locals():
#   local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/mlp_locals'
#   master_loc = f'{EXPDIR}/master_gc.sqlite'
#   local_loc = f'{EXPDIR}/local_gc.sqlite'
#   gcdb = setup_gcdb(
#     master_loc=master_loc,
#     local_loc=local_loc,
#     erase_master=False,
#     merge=True,
#     samples_only=True,
#     add_data_from_master=True
#   )
#
#   # remove_node_level_features = []
#   remove_node_level_features = DISTANCE_DEPENDENT_FEATURES
#
#   data_config = get_me_data_config(overwrite_identifiers=False)
#   eig_conf = get_eig_conf(eig_splits=0)
#
#   feature_conf = {
#     'eig_conf': eig_conf,
#     'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
#     'node_features': [],
#     'edge_features': [],
#     'node_labels': ['communities_L1'],
#     'graph_labels': [],
#     'edge_labels': ['communities_L1'],
#   }
#
#   exp_conf = get_exp_conf_clustering()
#   n_clusters = 20
#   exp_conf['n_clusters'] = n_clusters
#
#   model_conf = {
#     'gnn_out_dims': [64] * 5,
#     'conv_fn': SAGEConv,
#     'conv_conf': {'aggregator_type': 'gcn', 'feat_drop': 0, 'bias': True, 'norm': None, 'activation': None},
#
#     'dropout': 0.4,
#     'activation': F.relu,
#     'self_loops': True,
#     'use_batchnorm': True,
#     'apply_input_dropout': False
#   }
#   model_conf['gnn_out_dims'] += [n_clusters]
#
#   run_config = {
#     'gcdb': gcdb,
#     'exp_dir': local_exp_dir,
#
#     'data_method': data_me,
#     'data_config': data_config,
#     'exp_method': exp_supervised_communities,
#     'exp_config': exp_conf,
#     'model_method': model_node_gnn,
#     'model_config': model_conf,
#
#     'LR': 1e-4,
#     'epochs': 100,
#     'lambda_l2': 0.025,
#
#     'feature_config': feature_conf,
#
#     'chkp_freq': 1,
#     'vis_freq': 1,
#     'vis_n_samples': 1,
#     'val_freq': 1,
#     'test_freq': 1,
#     'train_acc_freq': 1,
#     'test_acc_freq': 1,
#   }
#
#   copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
#   for i in range(exp_conf['cross_val_num_folds']):
#     print('#' * 60)
#     print(f'Beginning cross_val iter {i}')
#     exp_conf['cross_val_which_fold'] = i
#     run_with_config(**run_config)
#     copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
#     GCDB.merge_local_to_master(
#       master_loc=master_loc,
#       local_loc=local_loc,
#       only_keep_every_nth_epoch_images=3,
#       keep_specific_epoch_images=[i for i in range(0, 20)]
#     )

def main():
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # mlp_locals()

  feature_conf = {
    'eig_conf': get_eig_conf(eig_splits=0),
    'local_features': [],
    'node_features': [],
    'edge_features': [],
    'node_labels': [],
    'graph_labels': [],
    'edge_labels': [],
  }

  clean_start_name = 'model_comparisons/motif_expand_0'
  clean_start_dir = f'{EXP_CLEAN_STARTS}/{clean_start_name}'
  force_run = True
  if not force_run and (not os.path.exists(clean_start_dir) or input('Overwrite clean start?') == 'y'):
    initialise_clean_start_dir(
      clean_start_name,
      data_method=data_me,
      data_config=get_me_data_config(overwrite_identifiers=True),
      feature_config=feature_conf
    )

  shutil.rmtree(EXPDIR)
  shutil.copytree(src=clean_start_dir, dst=EXPDIR)

  now = datetime.now().strftime('%Y-%m-%d_%H%M%S')
  saved_exp_dir = f'{ALL_SAVED_EXPS}/{now} (incomplete)'
  validate_directory(saved_exp_dir)
  shutil.copy(src=f'{ROOT}/src/exp_scripts/basic_exps/model_comparisons_me.py',
              dst=f'{saved_exp_dir}/model_comparisons_me.py')


  gnn_spectral()
  shutil.copy(src=f'{EXPDIR}/master_gc.sqlite', dst=f'{saved_exp_dir}/master_gc.sqlite')
  with open(f'{saved_exp_dir}/exps_done.txt', 'a') as f:
    f.write('gnn_spectral\n')

  gnn_locals()
  shutil.copy(src=f'{EXPDIR}/master_gc.sqlite', dst=f'{saved_exp_dir}/master_gc.sqlite')
  with open(f'{saved_exp_dir}/exps_done.txt', 'a') as f:
    f.write('gnn_locals\n')

  mlp_spectral()
  shutil.copy(src=f'{EXPDIR}/master_gc.sqlite', dst=f'{saved_exp_dir}/master_gc.sqlite')
  with open(f'{saved_exp_dir}/exps_done.txt', 'a') as f:
    f.write('mlp_spectral\n')

  mlp_locals()
  shutil.copy(src=f'{EXPDIR}/master_gc.sqlite', dst=f'{saved_exp_dir}/master_gc.sqlite')
  with open(f'{saved_exp_dir}/exps_done.txt', 'a') as f:
    f.write('mlp_locals\n')

  shutil.copytree(src=f'{clean_start_dir}/datasets', dst=f'{saved_exp_dir}/datasets')


  completed_dir = f'{ALL_SAVED_EXPS}/{now}'
  shutil.copytree(src=f'{saved_exp_dir}', dst=completed_dir)
  shutil.rmtree(saved_exp_dir)


  # ec_spectral()
  # ec_locals()
  
if __name__ == '__main__':
  main()