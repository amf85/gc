from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES
from util.constants import BASIC_EXPS as EXPDIR
from util.util import validate_directory
validate_directory(EXPDIR)
from exp_scripts.default_setups import (
  setup_gcdb, run_with_config, data_me, data_sbm, data_re, data_hn, data_sm,
  copy_db_loc_to_most_recent_exp_dir, run_sc_with_config, exp_supervised_communities, model_node_gnn)
from sql.db_util import GCDB
from torch.nn import functional as F
from datetime import datetime
from torch_geometric.nn import GraphConv, GCNConv, SAGEConv, GATConv
# from functools import partial


########################################  SHARED MOTIFS  ###########################################

def get_sm_data_config(overwrite_identifiers=False):
  remove_node_level_features = ['betweenness_centrality']
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if
                       f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if
                        f not in remove_graph_level_features]

  eig_splits = 8
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    # 'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_eig_algorithms': ['np_eig'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,

    'per_layer_eigenvectors': [20] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [5] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }

  data_config = {
    'datasets_dir': f'{EXPDIR}/datasets',

    'verbose': 0,

    'init_min_nodes': 10,
    'init_max_nodes': 25,
    'init_randoms': True,
    'init_rings': False,
    'init_cliques': False,
    'init_stars': False,
    'init_lines': False,
    'init_exp_node_edges': 2.5,
    'expansion_prob': 1.0,
    'maintain_motif_degree': True,
    'n_passes': 1,

    'pool_min_nodes': 5,
    'pool_max_nodes': 10,
    'pool_size': 20,
    'pool_randoms': True,
    'pool_rings': True,
    'pool_cliques': False,
    'pool_stars': False,
    'pool_lines': True,
    'pool_exp_node_edges': 4.0,

    # 'train_samples': 50 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    'train_samples': 512,
    'test_samples': 0,

    'train_identifier': 'basic_exps_sm_511',
    'test_identifier': None,
    'overwrite_identifiers': overwrite_identifiers,

    'test_split_changes': {},

    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
    },

    'create_new_splits': False,
  }
  return data_config

def get_exp_conf():
  return {
    'batch_size': 64,
    'cross_val_group_start': datetime.now(),
    'cross_val_num_folds': 10,
    'cross_val_split_method': 'ordered',
    'cross_val_which_fold': None,

    'val_size': 0.15,
    'val_seed': 196759,
    'early_stop_patience': 10,
    'early_stop_on': 'acc',
    'early_stop_direction': 'max',

    'lambda_cxe': None,
    'lambda_cmse': None,
    'lambda_cmae': None,
    'lambda_tce': 0,
    'lambda_cae': 0,

    'lambda_mod': 1.,
    'lambda_silh': 10.,
    'lambda_nll': 1.,
  }

def get_eig_conf(eig_splits=None):
  eig_splits = 6 if eig_splits is None else eig_splits
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    # 'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_eig_algorithms': ['np_eig'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [3] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  return eig_conf


def gnn_spectral():
  local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/gnn_spectral'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True
  )

  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_sm_data_config(overwrite_identifiers=False)
  eig_conf = get_eig_conf()

  feature_conf = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': [],
  }

  exp_conf = get_exp_conf()
  n_clusters = 30
  exp_conf['n_clusters'] = n_clusters

  model_conf = {
    'gnn_out_dims': [256, 256, 256, 256, 256, n_clusters],
    'conv_fn': GraphConv,
    'conv_conf': {'aggr': 'mean', 'bias': True},  # GraphConv
    # 'conv_fn': SAGEConv,
    # 'conv_conf': {'unsqueeze_X': True, 'normalize': False, 'root_weight': True, 'bias': True},  # SAGEConv
    # 'conv_fn': GCNConv,
    # 'conv_conf': {'improved': True, 'cached': False, 'add_self_loops': True, 'normalize': True, 'bias': True},
    # 'conv_fn': GATConv,
    # 'conv_conf': {'unsqueeze_X': True, 'heads': 1, 'concat': True, 'negative_slope': 0.2, 'dropout': 0.4, 'add_self_loops': False, 'bias': True},
    'dropout': 0.4,
    'activation': F.relu,
    'self_loops': True,
    'use_batchnorm': True
  }

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,

    'data_method': data_sm,
    'data_config': data_config,
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    'model_method': model_node_gnn,
    'model_config': model_conf,

    'LR': 1e-4,
    'epochs': 60,
    'lambda_l2': 0.025,

    'feature_config': feature_conf,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 1,
    'test_acc_freq': 1,
  }

  copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
  for i in range(exp_conf['cross_val_num_folds']):
    print('#' * 60)
    print(f'Beginning cross_val iter {i}')
    exp_conf['cross_val_which_fold'] = i
    run_with_config(**run_config)
    copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
    GCDB.merge_local_to_master(
      master_loc=master_loc,
      local_loc=local_loc,
      only_keep_every_nth_epoch_images=3,
      keep_specific_epoch_images=[i for i in range(0, 20)]
    )


def gnn_locals():
  local_exp_dir = f'{EXPDIR}/experiments/model_comparisons/gnn_locals'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True
  )

  remove_node_level_features = ['eccentricity', 'is_periphery', 'is_centre', 'closeness_centrality']

  data_config = get_sm_data_config(overwrite_identifiers=False)
  eig_conf = get_eig_conf(eig_splits=0)

  feature_conf = {
    'eig_conf': eig_conf,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': [],
  }

  exp_conf = get_exp_conf()
  n_clusters = 30
  exp_conf['n_clusters'] = n_clusters

  model_conf = {
    'gnn_out_dims': [64, 64, 64, n_clusters],
    'conv_fn': GraphConv,
    'conv_conf': {'aggr': 'add', 'bias': True},  # GraphConv
    # 'conv_fn': SAGEConv,
    # 'conv_conf': {'unsqueeze_X': True, 'normalize': False, 'root_weight': True, 'bias': True},  # SAGEConv
    # 'conv_fn': GCNConv,
    # 'conv_conf': {'improved': True, 'cached': False, 'add_self_loops': True, 'normalize': True, 'bias': True},
    # 'conv_fn': GATConv,
    # 'conv_conf': {'unsqueeze_X': True, 'heads': 1, 'concat': True, 'negative_slope': 0.2, 'dropout': 0.4, 'add_self_loops': False, 'bias': True},
    'dropout': 0.4,
    'activation': F.relu,
    'self_loops': True,
    'use_batchnorm': True
  }

  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,

    'data_method': data_sm,
    'data_config': data_config,
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    'model_method': model_node_gnn,
    'model_config': model_conf,

    'LR': 1e-3,
    'epochs': 60,
    'lambda_l2': 0.025,

    'feature_config': feature_conf,

    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 1,
    'test_acc_freq': 1,
  }

  copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
  for i in range(exp_conf['cross_val_num_folds']):
    print('#' * 60)
    print(f'Beginning cross_val iter {i}')
    exp_conf['cross_val_which_fold'] = i
    run_with_config(**run_config)
    copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
    GCDB.merge_local_to_master(
      master_loc=master_loc,
      local_loc=local_loc,
      only_keep_every_nth_epoch_images=3,
      keep_specific_epoch_images=[i for i in range(0, 20)]
    )

  
if __name__ == '__main__':
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)

  gnn_spectral()
  # gnn_locals()