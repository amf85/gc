from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES
# from util.constants import ARTIFICIAL_EXPS as EXPDIR
from util.constants import NP_TESTS as EXPDIR
from util.util import validate_directory
validate_directory(EXPDIR)
from exp_scripts.default_setups import (
  setup_gcdb, run_sc_with_config, data_me, data_sbm, data_re, data_hn, data_sm,
  copy_db_loc_to_most_recent_exp_dir, run_sc_with_config)
from sql.db_util import GCDB


#################### MOTIF EXPANDED NETWORKS ##################################

def get_me_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',
    
      'init_g_min_nodes': 10,
      'init_g_max_nodes': 30,
      'init_g_exp_node_edges': 3.5,
      'motif_min_nodes': 4,
      'motif_max_nodes': 12,
      'motif_exp_node_edges': 5.5,
      'motif_pool_size': 3,
      'expansion_prob': 1.0,
      'maintain_motif_degree': True,
      'n_passes': [1],
      
      'train_samples': 1000 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 500 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
    
    'train_identifier': 'sc_me_train_v0' if debug_n_samples_overwrite is None else 'sc_me_debug_train',
    'test_identifier': 'sc_me_test_v1' if debug_n_samples_overwrite is None else 'sc_me_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
  
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
  
    'create_new_splits': False,
  }
  return data_config

def _0_supervised_comparisons_recursive_spectral_me():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/recursive_spectral_me'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True
  )
  
  remove_node_level_features = NODE_LEVEL_FEATURES
  
  data_config = get_me_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)
  eig_splits = 8
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_me,
  
    'n_clusters': 30,
    'LR': 3e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    'lambda_silh': 10.0,
    'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    # 'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

def _1_supervised_comparisons_local_features_me():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/local_features_me'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = []
  
  data_config = get_me_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)
  
  eig_splits = 0
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_me,
    
    'n_clusters': 30,
    'LR': 1e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])
  

########################################## SBMS ################################################

def get_sbm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',
    
      'min_nodes': 100,
      'max_nodes': 400,
      'min_clusters': 2,
      'max_clusters': 15,
      'priors_a': 2,
      'priors_b': 3,
      'intra_connect_min': 0.2,
      'intra_connect_max': 0.35,
      'inter_connect_min': 0.03,
      'inter_connect_max': 0.05,
      
      'train_samples': 64 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 64 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
  
    'train_identifier': 'sc_sbm_train_v0' if debug_n_samples_overwrite is None else 'sc_sbm_debug_train',
    'test_identifier': 'sc_sbm_test_v0' if debug_n_samples_overwrite is None else 'sc_sbm_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
  
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'create_new_splits': False,
  }
  return data_config

def _2_supervised_comparisons_recursive_spectral_sbm():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/recursive_spectral_sbm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)

  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_sbm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 8
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_sbm,
    
    'n_clusters': 15,
    'LR': 1e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

def _3_supervised_comparisons_local_features_sbm():
  exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/local_features_sbm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)

  remove_node_level_features = []

  data_config = get_sbm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 0
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': exp_dir,
    'data_method': data_sbm,
    
    'n_clusters': 15,
    'LR': 1e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])
  

########################################  HIERARCHICAL  ############################################

def get_hn_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',
    
      'motif_sizes': [3, 4, 5, 6, 7],
      'branching_factors': [3, 4, 5, 6, 7, 8],
      'depths': [1, 2],
      
      'train_samples': 1000 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 500 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
  
    'train_identifier': 'hn_sbm_train_v0' if debug_n_samples_overwrite is None else 'hn_sbm_debug_train',
    'test_identifier': 'hn_sbm_test_v0' if debug_n_samples_overwrite is None else 'hn_sbm_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'create_new_splits': False,
  }
  return data_config

def _4_supervised_comparisons_recursive_spectral_hn():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/recursive_spectral_hn'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_hn_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 8
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_hn,
    
    'n_clusters': 12,
    'LR': 1e-3,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.1,
    'lambda_l1': 0.0,
    'lambda_l2': 0.0125,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    'lambda_silh': 10.0,
    'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

def _5_supervised_comparisons_local_features_hn():
  exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/local_features_hn'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = []
  
  data_config = get_hn_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 0
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': exp_dir,
    'data_method': data_hn,
    
    'n_clusters': 12,
    'LR': 3e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])
  

########################################  RANDOM EXPAND  ###########################################

def get_re_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',
    
      'init_g_sizes': [5, 6, 7, 8, 9, 10],
      'motif_min_nodes': 3,
      'motif_max_nodes': 7,
      'n_passes': [1, 2, 3],
      'expansion_prob': 0.35,
      'init_g_sparsity': 0.15,
      'motif_sparsity': 0.85,
      'maintain_motif_degree': True,
      
      'train_samples': 1000 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 500 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
  
    'train_identifier': 're_sbm_train_v0' if debug_n_samples_overwrite is None else 're_sbm_debug_train',
    'test_identifier': 're_sbm_test_v0' if debug_n_samples_overwrite is None else 're_sbm_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'create_new_splits': False,
  }
  return data_config

def _6_supervised_comparisons_recursive_spectral_re():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/recursive_spectral_re'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_re_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 8
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_re,
    
    'n_clusters': 12,
    'LR': 3e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

def _7_supervised_comparisons_local_features_re():
  exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/local_features_re'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = []
  
  data_config = get_re_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 0
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': exp_dir,
    'data_method': data_re,
    
    'n_clusters': 12,
    'LR': 3e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])
  

########################################  SHARED MOTIFS  ###########################################

def get_sm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = []
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 12
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [10] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [10] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',
  
      'init_min_nodes': 5,
      'init_max_nodes': 20,
      'init_randoms': True,
      'init_rings': False,
      'init_cliques': False,
      'init_stars': False,
      'init_lines': False,
      'init_exp_node_edges': 2.5,
      'expansion_prob': 1.0,
      'maintain_motif_degree': True,
      'n_passes': 1,
  
      'pool_min_nodes': 3,
      'pool_max_nodes': 8,
      'pool_size': 15,
      'pool_randoms': True,
      'pool_rings': True,
      'pool_cliques': False,
      'pool_stars': False,
      'pool_lines': False,
      'pool_exp_node_edges': 2.5,
      
      'train_samples': 1000 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 500 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
  
    'train_identifier': 're_sm_train_v0' if debug_n_samples_overwrite is None else 're_sm_debug_train',
    'test_identifier': 're_sm_test_v0' if debug_n_samples_overwrite is None else 're_sm_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'create_new_splits': False,
  }
  return data_config

def _8_supervised_comparisons_recursive_spectral_sm():
  local_exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/recursive_spectral_sm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = NODE_LEVEL_FEATURES

  data_config = get_sm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 8
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_sm,
    
    'n_clusters': 30,
    'LR': 3e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

def _9_supervised_comparisons_local_features_sm():
  exp_dir = f'{EXPDIR}/experiments/supervised_comparisons/local_features_sm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True)
  
  remove_node_level_features = []
  
  data_config = get_sm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None)

  eig_splits = 0
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['single'] * eig_splits,
    'per_layer_vnode_weights': ['sum_edges'] * eig_splits,
    'per_layer_eigenvectors': [2] * eig_splits,
    'per_layer_eigenvalues': [0] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': exp_dir,
    'data_method': data_re,
    
    'n_clusters': 30,
    'LR': 3e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])
  
if __name__ == '__main__':
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  # _0_supervised_comparisons_recursive_spectral_me()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _1_supervised_comparisons_local_features_me()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _2_supervised_comparisons_recursive_spectral_sbm()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _3_supervised_comparisons_local_features_sbm()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _4_supervised_comparisons_recursive_spectral_hn()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _5_supervised_comparisons_local_features_hn()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  # _6_supervised_comparisons_recursive_spectral_re()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _7_supervised_comparisons_local_features_re()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  #
  _8_supervised_comparisons_recursive_spectral_sm()
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _9_supervised_comparisons_local_features_sm()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  