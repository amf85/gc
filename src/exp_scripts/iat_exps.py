from copy import deepcopy
from functools import partial

import torch
import torch.nn.functional as F

from data.gc_data import nx_algorithms, GCSample
from data.hierarch import HNDataset, HNDataSplit, HierarchicalNetwork
from data.random_expand import REDataset, REDataSplit
from data.sbm import SBMDataset, SBMDataSplit
from experiments.opt import GCAdamOpt
from experiments.direct_community_prediction import DirectCommunityPrediction
from experiments.implicit_aux_tgt import ImplicitAuxTgtExp
from models.clustering import ClusteringGNN
from models.implicit_aux import ImplicitAuxTgtGNN
from sql.db_util import GCDB
from util.constants import TESTS
from vis.communities import CommunitiesVisualiser
from torchvision.transforms import Compose

tgt_dev = 'cuda'
# tgt_dev = 'cpu'
device = torch.device('cuda' if tgt_dev == 'cuda' and torch.cuda.is_available() else 'cpu')


def model_2(model_dir,
            gcdb,
            n_spectral_features,
            n_derived_features,
            n_clusters):
  model_params = ImplicitAuxTgtGNN.IATGNNParams(
    dropout=0.25,
    
    X_pre_node_input_dim=1,
    X_pre_use_node_inputs=False,
    X_pre_n_spectral=n_spectral_features,
    X_pre_derived_feat_dim=n_derived_features,
    
    # clustering_layers=[50, 30, 20, 10, n_clusters],
    clustering_layers=None,
    clustering_add_self_loops=True,
    clustering_use_batchnorm=True,
    clustering_activation=F.relu,
    
    # X_post_use_clusters=True,
    X_post_use_clusters=False,
    X_post_use_spectral=True,
    X_post_use_node_inputs=True,
    X_post_use_derived_feats=False,
    
    # intra_layers=[30, 30],
    intra_layers=[30, 30, 30, 30, 2],
    # intra_adj='weighted',
    intra_adj='A',
    intra_softmax=False,
    # intra_add_self_loops=False,
    intra_add_self_loops=True,
    intra_use_batchnorm=True,
    intra_activation=F.relu,
    
    # inter_layers=[30, 30, 2],
    inter_layers=None,
    inter_adj='weighted',
    inter_softmax=False,
    inter_add_self_loops=False,
    inter_use_batchnorm=True,
    inter_activation=F.relu,
    
    output_aggregation='none',
    output_layers=None,
    output_use_batchnorm=True,
    output_activation=F.relu,
    output_softmax=False
  )
  model = ImplicitAuxTgtGNN(model_dir=model_dir, gcdb=gcdb, params=model_params)
  model.to(device)
  return model

def data_spectral_only_hn(datasets_dir, label, n_spectral_vectors, gcdb):
  precompute_features = list(nx_algorithms.keys())
  dataset = HNDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = HNDataSplit.HNSplitParams(
    n_samples=1000,
    motif_sizes=[4, 5, 6],
    branching_factors=[2, 3],
    depths=[1, 2],
    preprocess_spectral='max',
    preprocess_distance=True,
    preprocess_additional_features=precompute_features
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = 300
  # test_split_params.motif_sizes = [3, 4, 5, 6, 7]
  # test_split_params.branching_factors = [2, 3, 4]
  
  feature_list = ['random_binary']
  feature_list += GCSample.get_spectral_feature_list(n_vectors=n_spectral_vectors)
  label_transform = partial(HierarchicalNetwork.add_per_node_max_cluster,
                            label=label)
  sample_transform = partial(GCSample.dgl_transform,
                             feature_list=feature_list,
                             label=label,
                             include_distance_matrices=True)
  transform = Compose([label_transform, sample_transform])

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )

  return dataset, len(feature_list)

def _3_0_per_node_is_max_cluster(gcdb):
  exp_dir = f'{TESTS}/experiments/3/0'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'
  
  n_spectral_vectors = 200
  n_clusters = 5
  label = ['per_node_L1_max_cluster', 'communities_L1']
  
  epochs = 500
  batch_size = 50
  LR = 1e-3
  
  lambda_aux_nll = 0.0
  lambda_nll = 0.003
  lambda_silh = 1.0
  lambda_mod = 30.0
  lambda_tce = 0.0
  lambda_cae = 0.0
  
  dataset, input_feature_dim = data_spectral_only_hn(datasets_dir=datasets_dir,
                                                     n_spectral_vectors=n_spectral_vectors,
                                                     label=label,
                                                     gcdb=gcdb)
  
  model = model_2(model_dir=model_dir,
                  gcdb=gcdb,
                  n_spectral_features=n_spectral_vectors,
                  n_derived_features=0,
                  n_clusters=n_clusters)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = ImplicitAuxTgtExp.IATParams(
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.0,
    batch_size=batch_size,
    n_clusters=n_clusters,
    lambda_aux_nll=lambda_aux_nll,
    lambda_nll=lambda_nll,
    lambda_silh=lambda_silh,
    lambda_mod=lambda_mod,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=n_spectral_vectors,
    derived_node_features={},
    label=label
  )
  
  experiment = ImplicitAuxTgtExp(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device
  )
  
  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=3,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   train_acc_freq=5,
                   test_acc_freq=5)
  
  
def main():
  local_db_dir = f'/opt/project/sql_store/tests'
  master_loc = f'/opt/project/sql_store/gc.sqlite'
  debug_local_loc = f'/opt/project/sql_store/gc2.sqlite'
  gcdb = GCDB()
  gcdb.merge_local_samples_to_master(master_loc=master_loc, local_loc=debug_local_loc, samples_only=True)
  # gcdb.init_master(master_loc=master_loc, verbose=True, fresh=False)

  gcdb.init_local(local_loc=debug_local_loc,
                  fresh=True,
                  verbose=False,
                  add_data_from_master=True,
                  master_loc=master_loc)
  _3_0_per_node_is_max_cluster(gcdb)
  
if __name__ == '__main__':
  main()