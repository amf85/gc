from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES
from util.constants import MSP_RES_EXP_TESTS as EXPDIR
from util.constants import *
from util.util import validate_directory
validate_directory(EXPDIR)
from exp_scripts.default_setups import (
  setup_gcdb,
  run_sc_with_config,
  run_sc_with_config,
  data_mr,
  copy_db_loc_to_most_recent_exp_dir,
)
from sql.db_util import GCDB


#################### MOTIF EXPANDED NETWORKS ##################################

def get_mr_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  precompute_local_features = set(NODE_LEVEL_FEATURES)
  precompute_global_features = set(GRAPH_LEVEL_FEATURES)
  # remove_features = {'clustering_coeff_sq', 'avg_clustering_coeff_sq'}
  remove_features = {}
  precompute_local_features = precompute_local_features.difference(remove_features)
  precompute_global_features = precompute_global_features.difference(remove_features)
  
  data_config = {
    'datasets_dir': f'{EXPDIR}/datasets',
    'label': 'secondary_structure',
    
    'train_identifier': 'mr_train_v0' if debug_n_samples_overwrite is None else 'sc_me_debug_train',
    'test_identifier': 'mr_test_v1' if debug_n_samples_overwrite is None else 'sc_me_debug_test',
    'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,

    
    'train_samples': 50 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    'test_samples': 50 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    
    'raw_res_root': MSP_RES_RAW,
    'res_config_version': '0',
    'selection_method': 'lexicographical',
    'sample_type': 'any',
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
    
    'create_new_splits': False,
    
  }
  return data_config

def msp_res_test():
  local_exp_dir = f'{EXPDIR}/spectral_mr'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=False,
    samples_only=True,
    add_data_from_master=False)
  
  # used_local_features = {k: False for k in set(node_level_features)}
  remove_node_level_features = NODE_LEVEL_FEATURES
  local_features = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  
  
  data_config = get_mr_data_config(overwrite_identifiers=True, debug_n_samples_overwrite=None)
  
  eig_splits = 12
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'per_layer_use_virtual_node': [False] * eig_splits,
    'per_layer_eigenvectors': [4] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_mr,
    'data_config': data_config,
    
    'n_clusters': 10,
    'LR': 1e-4,
    'batch_size': 32,
    'epochs': 60,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    # 'lambda_silh': 10.0,
    # 'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'feature_config': {
      'eig_conf': eig_conf,
      # 'local_features_set_id': '0',
      'local_features': local_features,
      'node_features': ['residue_name']
    },
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_sc_with_config(**run_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])
  
  
if __name__ == '__main__':
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  msp_res_test()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  # _1_supervised_comparisons_local_features_me()
  # copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  