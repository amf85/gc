from copy import deepcopy
from functools import partial

import torch
import torch.nn.functional as F

from data.gc_data import nx_algorithms, GCSample
from data.hierarch import HNDataset, HNDataSplit
from data.random_expand import REDataset, REDataSplit
from data.sbm import SBMDataset, SBMDataSplit
from experiments.opt import GCAdamOpt
from experiments.silhouette_community_prediction import SilhouetteCommunityPrediction
from models.clustering import ClusteringGNN
from sql.db_util import GCDB
from util.constants import TESTS
from vis.communities import CommunitiesVisualiser

tgt_dev = 'cuda'
device = torch.device('cuda' if tgt_dev == 'cuda' and torch.cuda.is_available() else 'cpu')


def model_0(input_feature_dim, n_clusters, model_dir, gcdb):
  model_params = ClusteringGNN.ClusteringGNNParams(
    input_dim=input_feature_dim,
    gnn_out_dims=[60, 60, 60, 60, n_clusters],
    activation=F.relu,
    add_self_loops=True
  )
  model = ClusteringGNN(model_dir=model_dir, params=model_params, gcdb=gcdb)
  model.to(device)
  return model

def data_structure_free_he(datasets_dir, gcdb, label):
  precompute_features = list(nx_algorithms.keys())
  dataset = HNDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = HNDataSplit.HNSplitParams(
    n_samples=1000,
    motif_sizes=[3, 4, 5, 6],
    branching_factors=[2, 3, 4, 5],
    depths=[1, 2],
    preprocess_spectral='max',
    preprocess_distance=True,
    preprocess_additional_features=precompute_features
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = 100
  
  feature_list = precompute_features
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  return dataset, len(feature_list)

def data_structure_free_re(datasets_dir, gcdb, label):
  precompute_features = list(nx_algorithms.keys())
  dataset = REDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = REDataSplit.RESplitParams(
    n_samples=1000,
    init_g_sizes=[5, 6, 7, 8, 9],
    motif_min_nodes=3,
    motif_max_nodes=7,
    n_passes=[1, 2, 3],
    expansion_prob=0.35,
    init_g_sparsity=0.15,
    motif_sparsity=0.85,
    maintain_motif_degree=True,
    preprocess_spectral=200,
    preprocess_distance=True,
    preprocess_additional_features=precompute_features,
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.init_g_sizes = [8, 9, 10]
  test_split_params.n_samples = 100
  
  feature_list = precompute_features
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  return dataset, len(feature_list)


def data_structure_free_sbm(datasets_dir, gcdb, label):
  precompute_features = list(nx_algorithms.keys())
  precompute_features.remove('closeness_centrality')
  precompute_features.remove('betweenness_centrality')
  precompute_features.remove('current_flow_closeness_centrality')
  precompute_features.remove('current_flow_betweenness_centrality')
  precompute_features.remove('approximate_current_flow_betweenness_centrality')
  precompute_features.remove('load_centrality')
  precompute_features.remove('clustering_coeff_sq')
  precompute_features.remove('eccentricity')
  dataset = SBMDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = SBMDataSplit.SBMSplitParams(
    n_samples=1000,
    
    min_nodes=20,
    max_nodes=100,
    min_clusters=2,
    max_clusters=5,
    priors_a=2,
    priors_b=3,
    intra_connect_min=0.2,
    intra_connect_max=0.35,
    inter_connect_min=0.03,
    inter_connect_max=0.05,
    
    preprocess_spectral='max',
    preprocess_distance=True,
    preprocess_additional_features=precompute_features,
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = 100
  
  feature_list = precompute_features
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  return dataset, len(feature_list)

def data_spectral_only_hn(datasets_dir, label, n_spectral_vectors, gcdb):
  precompute_features = list(nx_algorithms.keys())
  dataset = HNDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = HNDataSplit.HNSplitParams(
    n_samples=1000,
    motif_sizes=[3, 4, 5, 6],
    branching_factors=[2, 3, 4, 5],
    depths=[1, 2],
    preprocess_spectral='max',
    preprocess_distance=True,
    preprocess_additional_features=precompute_features
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = 100
  
  feature_list = GCSample.get_spectral_feature_list(n_vectors=n_spectral_vectors)
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  return dataset, len(feature_list)

def data_spectral_only_re(datasets_dir, label, n_spectral_vectors, gcdb):
  precompute_features = list(nx_algorithms.keys())
  dataset = REDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = REDataSplit.RESplitParams(
    n_samples=1000,
    init_g_sizes=[5, 6, 7, 8, 9],
    motif_min_nodes=3,
    motif_max_nodes=7,
    n_passes=[1, 2, 3],
    expansion_prob=0.35,
    init_g_sparsity=0.15,
    motif_sparsity=0.85,
    maintain_motif_degree=True,
    preprocess_spectral=200,
    preprocess_distance=True,
    preprocess_additional_features=precompute_features,
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.init_g_sizes = [8, 9, 10]
  test_split_params.n_samples = 100
  
  feature_list = GCSample.get_spectral_feature_list(n_vectors=n_spectral_vectors)
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  # input_feature_dim += n_spectral_vectors
  return dataset, len(feature_list)


def data_spectral_only_sbm(datasets_dir, gcdb, label, n_spectral_vectors):
  precompute_features = list(nx_algorithms.keys())
  precompute_features.remove('closeness_centrality')
  precompute_features.remove('betweenness_centrality')
  precompute_features.remove('current_flow_closeness_centrality')
  precompute_features.remove('current_flow_betweenness_centrality')
  precompute_features.remove('approximate_current_flow_betweenness_centrality')
  precompute_features.remove('load_centrality')
  precompute_features.remove('clustering_coeff_sq')
  precompute_features.remove('eccentricity')
  dataset = SBMDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = False
  train_split_params = SBMDataSplit.SBMSplitParams(
    n_samples=1000,
    
    min_nodes=20,
    max_nodes=100,
    min_clusters=2,
    max_clusters=5,
    priors_a=2,
    priors_b=3,
    intra_connect_min=0.2,
    intra_connect_max=0.35,
    inter_connect_min=0.03,
    inter_connect_max=0.05,
    
    preprocess_spectral='max',
    preprocess_distance=True,
    preprocess_additional_features=precompute_features,
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = 100
  
  # feature_list = precompute_features
  feature_list = GCSample.get_spectral_feature_list(n_vectors=n_spectral_vectors)
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  return dataset, len(feature_list)

def _0_structure_free_hn(gcdb):
  """
  Loss decreases quite substantially, but ultimately the clusters are primarily arranged by
  node degree, i.e. can see that nodes at the 'boundaries' are all one cluster, as are nodes
  in each particular 'level', all the way back to the centre node, (typically its own cluster).
  
  Left to run it starts to look at first glance like improving, but actually it is just getting
  better at identifying the within-motif locations of nodes. Rather than clustering the whole
  motif together, (e.g. the motif centres at some particular layer might all be assigned to
  one cluster).
  
  Is there a way of quantifying that this type of clustering is undesirable??
  """
  exp_dir = f'{TESTS}/experiments/0'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'
  
  n_clusters = 6
  label = 'communities_L1'
  
  LR = 1e-4
  lambda_tce = 100.0
  lambda_cae = 0.8
  batch_size = 100
  epochs = 500
  
  dataset, input_feature_dim = data_structure_free_he(datasets_dir=datasets_dir,
                                                      label=label,
                                                      gcdb=gcdb)
  
  model = model_0(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SilhouetteCommunityPrediction.SCPParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=0,
    include_binary_node_feature=False,
    label=label,
  )
  experiment = SilhouetteCommunityPrediction(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )

  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=5,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   )


def _1_structure_free_re(gcdb):
  """
  Similar to _0_, the clusters produced are very dependent on node degree. Node close to graph
  boundaries typically clustered together, even when they are quite spatially disparate.
  """
  exp_dir = f'{TESTS}/experiments/1'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'
  
  n_clusters = 12
  label = 'communities_L1'
  
  LR = 1e-4
  lambda_tce = 100.0
  lambda_cae = 15
  batch_size = 100
  epochs = 500
  
  dataset, input_feature_dim = data_structure_free_re(datasets_dir=datasets_dir,
                                                      gcdb=gcdb,
                                                      label=label)
  
  model = model_0(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SilhouetteCommunityPrediction.SCPParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=0,
    include_binary_node_feature=False,
    label=label
  )
  experiment = SilhouetteCommunityPrediction(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )

  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=5,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   )


def _2_spectral_only_hn(gcdb):
  """
  """
  exp_dir = f'{TESTS}/experiments/2'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'
  
  n_spectral_vectors = 200
  n_clusters = 6
  label = 'communities_L1'
  
  LR = 3e-3
  lambda_tce = 10.0
  lambda_cae = 0.1
  batch_size = 100
  epochs = 500
  
  dataset, input_feature_dim = data_spectral_only_hn(datasets_dir=datasets_dir,
                                                     n_spectral_vectors=n_spectral_vectors,
                                                     label=label,
                                                     gcdb=gcdb)
  
  model = model_0(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SilhouetteCommunityPrediction.SCPParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=n_spectral_vectors,
    include_binary_node_feature=False,
    label=label
  )
  experiment = SilhouetteCommunityPrediction(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )

  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=2,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   )


def _3_spectral_only_re(gcdb):
  """
  """
  exp_dir = f'{TESTS}/experiments/3'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'

  n_spectral_vectors = 100
  n_clusters = 12
  label = 'communities_L1'
  
  LR = 1e-2
  lambda_tce = 100.0
  lambda_cae = 0.8
  batch_size = 100
  epochs = 500
  
  dataset, input_feature_dim = data_spectral_only_re(datasets_dir=datasets_dir,
                                                     label=label,
                                                     n_spectral_vectors=n_spectral_vectors,
                                                     gcdb=gcdb)
  
  model = model_0(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb)
  model = model.to(device)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SilhouetteCommunityPrediction.SCPParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=n_spectral_vectors,
    include_binary_node_feature=False,
    label=label
  )
  experiment = SilhouetteCommunityPrediction(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )

  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=5,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   )


def _10_silhouette_structure_free_sbm(gcdb):
  """
  Similar to _0_, the clusters produced are very dependent on node degree. Node close to graph
  boundaries typically clustered together, even when they are quite spatially disparate.
  """
  exp_dir = f'{TESTS}/experiments/10'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'
  
  n_clusters = 6
  label = 'communities_L1'
  
  LR = 1e-2
  lambda_tce = 100.0
  lambda_cae = 0.8
  batch_size = 100
  epochs = 500
  
  dataset, input_feature_dim = data_structure_free_sbm(datasets_dir=datasets_dir,
                                                       gcdb=gcdb,
                                                       label=label)
  
  model = model_0(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SilhouetteCommunityPrediction.SCPParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=0,
    include_binary_node_feature=False,
    label=label
  )
  experiment = SilhouetteCommunityPrediction(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )

  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=5,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   )


def _11_silhouette_spectral_only_sbm(gcdb):
  """
  Similar to _0_, the clusters produced are very dependent on node degree. Node close to graph
  boundaries typically clustered together, even when they are quite spatially disparate.
  """
  exp_dir = f'{TESTS}/experiments/11'
  model_dir = f'{exp_dir}/models'
  datasets_dir = f'{TESTS}/datasets'
  
  n_clusters = 6
  label = 'communities_L1'
  n_spectral_vectors = 200
  
  LR = 1e-3
  lambda_tce = 100.0
  lambda_cae = 0.8
  batch_size = 100
  epochs = 500
  
  dataset, input_feature_dim = data_spectral_only_sbm(datasets_dir=datasets_dir,
                                                      gcdb=gcdb,
                                                      label=label,
                                                      n_spectral_vectors=n_spectral_vectors)
  
  model = model_0(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SilhouetteCommunityPrediction.SCPParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    n_spectral_vectors=n_spectral_vectors,
    include_binary_node_feature=False,
    label=label
  )
  experiment = SilhouetteCommunityPrediction(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )
  
  experiment.start(epochs=epochs,
                   chkp_freq=1,
                   vis_freq=5,
                   vis_n_samples=5,
                   val_freq=1,
                   test_freq=1,
                   )

def main():
  local_db_dir = f'/opt/project/sql_store/tests'
  master_loc = f'/opt/project/sql_store/gc.sqlite'
  debug_local_loc = f'/opt/project/sql_store/gc2.sqlite'
  gcdb = GCDB()
  gcdb.merge_local_samples_to_master(master_loc=master_loc, local_loc=debug_local_loc, samples_only=True)
  # gcdb.init_master(master_loc=master_loc, verbose=True, fresh=False)

  gcdb.init_local(local_loc=debug_local_loc,
                  fresh=True,
                  verbose=False,
                  add_data_from_master=True,
                  master_loc=master_loc)
  # _0_structure_free_hn(gcdb)
  # _1_structure_free_re(gcdb)
  _2_spectral_only_hn(gcdb)
  # _3_spectral_only_re(gcdb)
  # _10_silhouette_structure_free_sbm(gcdb)
  # _11_silhouette_spectral_only_sbm(gcdb)


if __name__ == '__main__':
  main()