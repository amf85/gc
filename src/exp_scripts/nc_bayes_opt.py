from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES
from util.constants import BO_TESTS_SC as EXPDIR
from util.util import validate_directory
validate_directory(EXPDIR)
from exp_scripts.default_setups import *
from sql.db_util import GCDB
import torch.nn.functional as F
from optimisation.gc_bayes_opt import BayesOptVariable
from optimisation.node_clustering_opt import NodeClusteringOpt
from datetime import timedelta

# def run_bayes_opt(opt_config, data_config, run_config):
#
#   opt = BayesOpt()

  
  

########################################  SHARED MOTIFS  ###########################################

def get_sm_data_config(overwrite_identifiers=False, debug_n_samples_overwrite=None):
  remove_node_level_features = ['betweenness_centrality']
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 7
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
  
    'per_layer_eigenvectors': [20] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [5] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  data_config = {
    'data_method_config': {
      'datasets_dir': f'{EXPDIR}/datasets',

      'verbose': 0,
  
      'init_min_nodes': 10,
      'init_max_nodes': 25,
      'init_randoms': True,
      'init_rings': False,
      'init_cliques': False,
      'init_stars': False,
      'init_lines': False,
      'init_exp_node_edges': 2.5,
      'expansion_prob': 0.4,
      'maintain_motif_degree': True,
      'n_passes': 1,
  
      'pool_min_nodes': 5,
      'pool_max_nodes': 10,
      'pool_size': 20,
      'pool_randoms': True,
      'pool_rings': True,
      'pool_cliques': False,
      'pool_stars': False,
      'pool_lines': False,
      'pool_exp_node_edges': 4.0,
      
      'train_samples': 512 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
      'test_samples': 256 if debug_n_samples_overwrite is None else debug_n_samples_overwrite,
    },
  
    'train_identifier': 'bo_edges_sm_train_v0' if debug_n_samples_overwrite is None else 'bo_edges_sm_train_v0',
    'test_identifier': 'bo_edges_sm_test_v0' if debug_n_samples_overwrite is None else 'bo_edges_sm_test_v0',
    # 'overwrite_identifiers': overwrite_identifiers if debug_n_samples_overwrite is None else True,
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
  
    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': True,
      'node_features': ['communities_L1'],
      'node_labels': [],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
    },
    
    # 'create_new_splits': False,
  }
  return data_config

def _0_bayes_opt_test_sm():
  local_exp_dir = f'{EXPDIR}/experiments/bayes_opt/test_sm'
  master_loc = f'{EXPDIR}/master_gc.sqlite'
  local_loc = f'{EXPDIR}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=True,
    add_all_from_master=True,
  )
  
  remove_node_level_features = NODE_LEVEL_FEATURES

  data_conf = get_sm_data_config(overwrite_identifiers=True, debug_n_samples_overwrite=None)

  eig_splits = 7
  eig_conf = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig', 30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,
    'per_layer_eigenvectors': [3] * eig_splits,
    'per_layer_eigenvalues': [1] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }
  
  feature_conf = {
      'eig_conf': eig_conf,
      'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
      'node_features': [],
      'edge_features': [],
      'node_labels': ['communities_L1'],
      'graph_labels': [],
      'edge_labels': [],
  }
  
  exp_conf = {
    'batch_size': 64,
    'val_size': 0.0,

    'n_clusters': 25,

    'lambda_cxe': 0,
    'lambda_cmse': 0,
    'lambda_cmae': 0,
    'lambda_tce': 0,
    'lambda_cae': 0,
    'lambda_l1': 0,

    'lambda_mod': 0,
    'lambda_silh': 0,
    'lambda_nll': 1
  }
  
  model_conf = {
    # 'gnn_out_dims': [128, 128, 128, 128],
    'activation': F.relu,
    'self_loops': True,
    'use_batchnorm': True
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': local_exp_dir,
    'data_method': data_sm,
    
    'exp_method': exp_supervised_communities,
    'exp_config': exp_conf,
    
    'model_method': model_node_gnn,
    'model_config': model_conf,

    'feature_config': feature_conf,
  }

  opt_conf = {
    'identifier': 'nc_opt_r3_0',
    'opt_dir': f'{local_exp_dir}',
    'variables': {
      'gnn_layers': BayesOptVariable(distribution=list(range(2, 7)), scale='linear'),
      'gnn_dim': BayesOptVariable(distribution=[32, 64, 128, 256, 512],
                                  scale='linear'),
      'LR': BayesOptVariable(distribution=(3e-4, 3e-1), scale='logarithmic'),
      'gnn_dropout': BayesOptVariable(distribution=(0.0, 0.6), scale='linear'),
      'lambda_L2': BayesOptVariable(distribution=(1e-3, 1e-1), scale='logarithmic',
                                    zero_thresh=1e-2),
      # 'lambda_silh': BayesOptVariable(distribution=(1e-5, 1e2), scale='logarithmic',
      #                                 zero_thresh=1e-4),
      # 'lambda_mod': BayesOptVariable(distribution=(1e-5, 1e2), scale='logarithmic',
      #                                zero_thresh=1e-4),
      # 'lambda_silh': BayesOptVariable(distribution=(0, 0), scale='logarithmic',
      #                                 zero_thresh=1e-4),
      # 'lambda_mod': BayesOptVariable(distribution=(0, 0), scale='logarithmic',
      #                                zero_thresh=1e-4),
      # 'lambda_nll': BayesOptVariable(distribution=(1e-5, 1e0), scale='logarithmic',
      #                                zero_thresh=1e-4),
      # todo: lambda_cae, lambda_tce
    },
    'random_steps': 5,
    'bayes_steps': 5000,
    'eval_repeats': 2,
    'time_limit': timedelta(hours=30),
    'eval_time_limit': timedelta(minutes=10),
    'eval_epoch_limit': 40,
    'early_stop_after_stalled': 10,
  }

  opt = NodeClusteringOpt(
    gcdb=gcdb,
    run_conf=run_config,
    data_conf=data_conf,
    **opt_conf)


  opt.save_info()
  best_so_far = opt.get_top_N(n=20, display_in_console=True)


  opt.run_optimiser()
  # run_with_config(**run_config, **data_config)
  GCDB.merge_local_to_master(master_loc=master_loc,
                             local_loc=local_loc,
                             only_keep_every_nth_epoch_images=3,
                             keep_specific_epoch_images=[i for i in range(0, 20)])

  
if __name__ == '__main__':
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  _0_bayes_opt_test_sm()
  copy_db_loc_to_most_recent_exp_dir(f'{EXPDIR}/local_gc.sqlite', original=True)
  
  