import numpy as np
from copy import deepcopy
from functools import partial
import os
import shutil

import torch
import torch.nn.functional as F

from data.gc_data import NODE_LEVEL_FEATURES, GCSample
from data.motif_expand import MEDataset, MEDataSplit
from data.sbm import SBMDataset, SBMDataSplit, StochasticBlockModel
from data.random_expand import REDataset, REDataSplit
from data.hierarch import HNDataset, HNDataSplit
from data.msp_residue import MSPResidue, MRDataset, MRDataSplit
from data.shared_motifs import SMDataset, SMDataSplit
from data.enzymes import EnzymesDataset, EnzymesDataSplit
from data.dd import DDDataset, DDDataSplit
from experiments.opt import GCAdamOpt
from experiments.supervised_communities import SupervisedCommunities
from experiments.supervised_edge_communities import SupervisedEdgeCommunities
from experiments.supervised_graph_classify import SupervisedGraphClassify
from models.clustering import ClusteringGNN
from models.gnn_classify import GNNClassify
from models.edge_clustering import EdgeClusteringGNN
from models.edge_cluster_classify import EdgeClusterClassify
from models.mlp_clustering import ClusteringMLP
from util.constants import *
from vis.communities import CommunitiesVisualiser
from vis.edge_motifs import EdgeMotifsVisualiser
from vis.edge_scores import EdgeScoresVisualiser
from sql.db_util import GCDB
from util.util import validate_directory
from experiments.gc_experiment import GCExperiment

tgt_dev = 'cuda'
device = torch.device('cuda' if tgt_dev == 'cuda' and torch.cuda.is_available() else 'cpu')



def model_node_gnn(
  gcdb,
  model_dir,
  input_dim,
  gnn_out_dims,
  conv_fn,
  conv_conf,
  dropout,
  activation,
  self_loops,
  use_batchnorm,
  apply_input_dropout,
):
  params = ClusteringGNN.ClusteringGNNParams(
    input_dim=input_dim,
    gnn_out_dims=gnn_out_dims,
    conv_fn=conv_fn,
    conv_conf=conv_conf,
    activation=activation,
    self_loops=self_loops,
    use_batchnorm=use_batchnorm,
    dropout=dropout,
    apply_input_dropout=apply_input_dropout,
  )
  model = ClusteringGNN(model_dir=model_dir, params=params, gcdb=gcdb)
  model.to(device)
  return model

def model_node_mlp(
  gcdb,
  model_dir,
  input_dim,
  apply_input_dropout,

  layer_out_dims,
  dropout,
  activation,
  use_batchnorm,
):
  params = ClusteringMLP.ClusteringMLPParams(
    input_dim=input_dim,
    apply_input_dropout=apply_input_dropout,
    layer_out_dims=layer_out_dims,
    activation=activation,
    use_batchnorm=use_batchnorm,
    dropout=dropout,
  )
  model = ClusteringMLP(model_dir=model_dir, params=params, gcdb=gcdb)
  model.to(device)
  return model

def model_gnn_classify(
  gcdb,
  model_dir,
  input_dim,
  gnn_out_dims,
  mlp_out_dims,
  conv_fn,
  conv_conf,
  dropout_gnn,
  dropout_mlp,
  activation,
  self_loops,
  use_batchnorm_gnn,
  use_batchnorm_mlp,
  output_aggr,
):
  params = GNNClassify.GNNClassifyParams(
    input_feature_dim=input_dim,
    gnn_out_dims=gnn_out_dims,
    mlp_out_dims=mlp_out_dims,

    conv_fn=conv_fn,
    conv_conf=conv_conf,
    activation=activation,
    add_self_loops=self_loops,
    use_batchnorm_gnn=use_batchnorm_gnn,
    use_batchnorm_mlp=use_batchnorm_mlp,
    dropout_gnn=dropout_gnn,
    dropout_mlp=dropout_mlp,
    output_aggr=output_aggr
  )
  model = GNNClassify(model_dir=model_dir, params=params, gcdb=gcdb)
  model.to(device)
  return model

def model_edge_cluster_classify(
  gcdb,
  model_dir,
  input_dim,

  add_self_loops,
  edge_gnn_out_dims,
  edge_gnn_conv_fn,
  edge_gnn_conv_conf,

  edge_pred_out_dims,
  classify_from_hidden_representation,

  clsf_gnn_out_dims,
  clsf_gnn_conv_fn,
  clsf_gnn_conv_conf,

  output_aggr,
  clsf_mlp_out_dims,

  batchnorm_edge_gnn,
  batchnorm_edge_pred,
  batchnorm_clsf_gnn,
  batchnorm_clsf_mlp,

  activation_edge_gnn,
  activation_edge_pred,
  activation_edge_pred_score,
  activation_clsf_gnn,
  activation_clsf_mlp,

  dropout_edge_gnn,
  dropout_edge_pred,
  dropout_clsf_gnn,
  dropout_clsf_mlp,

  apply_input_dropout,

):
  params = EdgeClusterClassify.EdgeClusterClassifyParams(
    input_feature_dim=input_dim,
    add_self_loops=add_self_loops,
    edge_gnn_out_dims=edge_gnn_out_dims,
    edge_gnn_conv_fn=edge_gnn_conv_fn,
    edge_gnn_conv_conf=edge_gnn_conv_conf,
    edge_pred_out_dims=edge_pred_out_dims,
    classify_from_hidden_representation=classify_from_hidden_representation,
    clsf_gnn_out_dims=clsf_gnn_out_dims,
    clsf_gnn_conv_fn=clsf_gnn_conv_fn,
    clsf_gnn_conv_conf=clsf_gnn_conv_conf,
    output_aggr=output_aggr,
    clsf_mlp_out_dims=clsf_mlp_out_dims,
    batchnorm_edge_gnn=batchnorm_edge_gnn,
    batchnorm_edge_pred=batchnorm_edge_pred,
    batchnorm_clsf_gnn=batchnorm_clsf_gnn,
    batchnorm_clsf_mlp=batchnorm_clsf_mlp,
    activation_edge_gnn=activation_edge_gnn,
    activation_edge_pred=activation_edge_pred,
    activation_edge_pred_score=activation_edge_pred_score,
    activation_clsf_gnn=activation_clsf_gnn,
    activation_clsf_mlp=activation_clsf_mlp,
    dropout_edge_gnn=dropout_edge_gnn,
    dropout_edge_pred=dropout_edge_pred,
    dropout_clsf_gnn=dropout_clsf_gnn,
    dropout_clsf_mlp=dropout_clsf_mlp,
    apply_input_dropout=apply_input_dropout,
  )
  model = EdgeClusterClassify(model_dir=model_dir, params=params, gcdb=gcdb)
  model.to(device)
  return model

def model_edge_gnn(
  gcdb,
  model_dir,
  input_dim,
  apply_input_dropout,
  add_self_loops,

  gnn_out_dims,
  gnn_conv_fn,
  gnn_conv_conf,

  edge_pred_out_dims,

  batchnorm_gnn,
  batchnorm_edge_pred,
  activation_gnn,
  activation_edge_pred,
  dropout_gnn,
  dropout_edge_pred,
):
  params = EdgeClusteringGNN.EdgeClusteringGNNParams(
    input_dim=input_dim,
    apply_input_dropout=apply_input_dropout,
    add_self_loops=add_self_loops,

    gnn_out_dims=gnn_out_dims,
    gnn_conv_fn=gnn_conv_fn,
    gnn_conv_conf=gnn_conv_conf,

    edge_pred_out_dims=edge_pred_out_dims,

    batchnorm_gnn=batchnorm_gnn,
    batchnorm_edge_pred=batchnorm_edge_pred,
    activation_gnn=activation_gnn,
    activation_edge_pred=activation_edge_pred,
    dropout_gnn=dropout_gnn,
    dropout_edge_pred=dropout_edge_pred,
  )
  model = EdgeClusteringGNN(gcdb=gcdb, model_dir=model_dir, params=params)
  model.to(device)
  return model

def mlp_model_0(input_feature_dim, n_clusters, model_dir, gcdb, dropout):
  model_params = ClusteringMLP.ClusteringMLPParams(
    input_dim=input_feature_dim,
    layer_out_dims=[128, 64, n_clusters],
    # gnn_out_dims=[40, 40, 40, 40, n_clusters],
    activation=F.relu,
    # add_self_loops=True,
    use_batchnorm=True,
    dropout=dropout,
  )
  model = ClusteringMLP(model_dir=model_dir, params=model_params, gcdb=gcdb)
  model.to(device)
  return model



def exp_supervised_edge_communities(
  model,
  optimiser,
  dataset,
  feature_config,
  batch_size,
  val_size,

  cross_val_group_start=None,
  cross_val_num_folds=None,
  cross_val_which_fold=None,
  cross_val_split_method=None,
  cross_val_seed=None,

  scale_eig_features=False,
  scale_local_features=False,
  scale_node_features=True,

  val_seed=None,
  early_stop_patience=None,
  early_stop_on=None,
  early_stop_direction=None,

  lambda_nll=0,

  train_epoch_method='train_epoch',
  visualiser='communities',
):
  exp_params = SupervisedEdgeCommunities.SECParams(
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=val_size,
    batch_size=batch_size,

    cross_val_group_start=cross_val_group_start,
    cross_val_num_folds=cross_val_num_folds,
    cross_val_which_fold=cross_val_which_fold,
    cross_val_split_method=cross_val_split_method,
    cross_val_seed=cross_val_seed,

    scale_eig_features=scale_eig_features,
    scale_local_features=scale_local_features,
    scale_node_features=scale_node_features,

    val_seed=val_seed,
    early_stop_patience=early_stop_patience,
    early_stop_on=early_stop_on,
    early_stop_direction=early_stop_direction,

    lambda_nll=lambda_nll,
    **feature_config
  )

  if visualiser == 'communities':
    visualiser = CommunitiesVisualiser
  elif visualiser == 'edge_scores':
    visualiser = EdgeScoresVisualiser
  elif visualiser == 'edge_motifs':
    visualiser = EdgeMotifsVisualiser
  else:
    raise NotImplementedError()

  return exp_params, SupervisedEdgeCommunities, visualiser




def exp_supervised_communities(model,
                               optimiser,
                               dataset,
                               feature_config,
                               batch_size,
                               val_size,
                               n_clusters,

                               cross_val_group_start=None,
                               cross_val_num_folds=None,
                               cross_val_which_fold=None,
                               cross_val_split_method=None,
                               cross_val_seed=None,

                               scale_eig_features=False,
                               scale_local_features=False,
                               scale_node_features=True,

                               val_seed=None,
                               early_stop_patience=None,
                               early_stop_on=None,
                               early_stop_direction=None,

                               lambda_cxe=0,
                               lambda_cmse=0,
                               lambda_cmae=0,
                               lambda_silh=0,
                               lambda_mod=0,
                               lambda_tce=0,
                               lambda_cae=0,
                               lambda_nll=0,
                               lambda_l1=0,
                               lambda_l2=0,

                               train_epoch_method='train_epoch',
                               visualiser='communities',

                               ):
  exp_params = SupervisedCommunities.SCParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=val_size,
    n_clusters=n_clusters,

    cross_val_group_start=cross_val_group_start,
    cross_val_num_folds=cross_val_num_folds,
    cross_val_which_fold=cross_val_which_fold,
    cross_val_split_method=cross_val_split_method,
    cross_val_seed=cross_val_seed,

    scale_eig_features=scale_eig_features,
    scale_local_features=scale_local_features,
    scale_node_features=scale_node_features,

    val_seed=val_seed,
    early_stop_patience=early_stop_patience,
    early_stop_on=early_stop_on,
    early_stop_direction=early_stop_direction,

    lambda_cxe=lambda_cxe,
    lambda_cmse=lambda_cmse,
    lambda_cmae=lambda_cmae,
    lambda_silh=lambda_silh,
    lambda_mod=lambda_mod,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    lambda_nll=lambda_nll,
    # lambda_l1=lambda_l1,
    # lambda_l2=lambda_l2,

    # train_epoch_method=train_epoch_method,

    **feature_config,
  )

  if visualiser == 'communities':
    visualiser = CommunitiesVisualiser
  elif visualiser == 'edge_scores':
    visualiser = EdgeScoresVisualiser
  elif visualiser == 'edge_motifs':
    visualiser = EdgeMotifsVisualiser
  else:
    raise NotImplementedError()

  return exp_params, SupervisedCommunities, visualiser




def exp_sgc(model,
            optimiser,
            dataset,
            feature_config,
            batch_size,
            val_size,
            n_clusters=None,

            cross_val_group_start=None,
            cross_val_num_folds=None,
            cross_val_which_fold=None,
            cross_val_split_method=None,
            cross_val_seed=None,

            scale_eig_features=False,
            scale_local_features=False,
            scale_node_features=True,

            val_seed=None,
            early_stop_patience=None,
            early_stop_on=None,
            early_stop_direction=None,

            # lambda_cxe=0,
            # lambda_cmse=0,
            # lambda_cmae=0,
            lambda_silh=0,
            lambda_mod=0,
            lambda_tce=0,
            lambda_cae=0,
            lambda_nll=0,
            lambda_l1=0,
            lambda_l2=0,

            train_epoch_method='train_epoch',
            visualiser='communities',
            ):
  exp_params = SupervisedGraphClassify.SGCParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=val_size,
    n_clusters=n_clusters,

    cross_val_group_start=cross_val_group_start,
    cross_val_num_folds=cross_val_num_folds,
    cross_val_which_fold=cross_val_which_fold,
    cross_val_split_method=cross_val_split_method,
    cross_val_seed=cross_val_seed,

    scale_eig_features=scale_eig_features,
    scale_local_features=scale_local_features,
    scale_node_features=scale_node_features,

    val_seed=val_seed,
    early_stop_patience=early_stop_patience,
    early_stop_on=early_stop_on,
    early_stop_direction=early_stop_direction,

    # lambda_cxe=lambda_cxe,
    # lambda_cmse=lambda_cmse,
    # lambda_cmae=lambda_cmae,
    lambda_silh=lambda_silh,
    lambda_mod=lambda_mod,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    lambda_nll=lambda_nll,
    # lambda_l1=lambda_l1,
    # lambda_l2=lambda_l2,

    train_epoch_method=train_epoch_method,

    **feature_config,
  )

  if visualiser == 'communities':
    visualiser = CommunitiesVisualiser
  elif visualiser == 'edge_scores':
    visualiser = EdgeScoresVisualiser
  elif visualiser == 'edge_motifs':
    visualiser = EdgeMotifsVisualiser
  else:
    raise NotImplementedError()

  return exp_params, SupervisedGraphClassify, visualiser


def data_me(datasets_dir,
            gcdb,
            train_samples,
            test_samples,
            test_split_changes,
            transform,
            preprocess_config,
            feature_config,
            create_new_splits,
            overwrite_identifiers,
            train_identifier,
            test_identifier,
            
            init_g_min_nodes,
            init_g_max_nodes,
            init_g_exp_node_edges,
            motif_min_nodes,
            motif_max_nodes,
            motif_exp_node_edges,
            motif_pool_size,
            expansion_prob,
            maintain_motif_degree,
            n_passes,

            verbose=1,
            
            ):
  dataset = MEDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = MEDataSplit.MESplitParams(
    n_samples=train_samples,
    
    init_g_min_nodes=init_g_min_nodes,
    init_g_max_nodes=init_g_max_nodes,
    init_g_exp_node_edges=init_g_exp_node_edges,
    motif_min_nodes=motif_min_nodes,
    motif_max_nodes=motif_max_nodes,
    motif_exp_node_edges=motif_exp_node_edges,
    motif_pool_size=motif_pool_size,
    expansion_prob=expansion_prob,
    maintain_motif_degree=maintain_motif_degree,
    n_passes=n_passes,
  )

  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  return dataset, train_split_params


def data_enz(datasets_dir,
             gcdb,
             train_samples,
             test_samples,
             test_split_changes,
             transform,
             preprocess_config,
             feature_config,
             create_new_splits,
             overwrite_identifiers,
             train_identifier,
             test_identifier,

             verbose=1,

             ):
  dataset = EnzymesDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = EnzymesDataSplit.EnzymesSplitParams(
    n_samples=train_samples,
  )

  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  return dataset, train_split_params


def data_dd(datasets_dir,
             gcdb,
             train_samples,
             test_samples,
             test_split_changes,
             transform,
             preprocess_config,
             feature_config,
             create_new_splits,
             overwrite_identifiers,
             train_identifier,
             test_identifier,

             verbose=1,

             ):
  dataset = DDDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = DDDataSplit.DDSplitParams(
    n_samples=train_samples,
  )

  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  return dataset, train_split_params


def data_sm(datasets_dir,
            gcdb,
            train_samples,
            test_samples,
            test_split_changes,
            transform,
            preprocess_config,
            feature_config,
            create_new_splits,
            overwrite_identifiers,
            train_identifier,
            test_identifier,
            
            init_min_nodes,
            init_max_nodes,
            init_randoms,
            init_rings,
            init_cliques,
            init_stars,
            init_lines,
            init_exp_node_edges,
            expansion_prob,
            maintain_motif_degree,
            n_passes,
            
            pool_min_nodes,
            pool_max_nodes,
            pool_size,
            pool_randoms,
            pool_rings,
            pool_cliques,
            pool_stars,
            pool_lines,
            pool_exp_node_edges,

            verbose=1,
            
            ):
  dataset = SMDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = SMDataSplit.SMSplitParams(
    n_samples=train_samples,
  
    init_min_nodes=init_min_nodes,
    init_max_nodes=init_max_nodes,
    init_randoms=init_randoms,
    init_rings=init_rings,
    init_cliques=init_cliques,
    init_stars=init_stars,
    init_lines=init_lines,
    init_exp_node_edges=init_exp_node_edges,
    expansion_prob=expansion_prob,
    maintain_motif_degree=maintain_motif_degree,
    n_passes=n_passes,
    
    pool_min_nodes=pool_min_nodes,
    pool_max_nodes=pool_max_nodes,
    pool_size=pool_size,
    pool_randoms=pool_randoms,
    pool_rings=pool_rings,
    pool_cliques=pool_cliques,
    pool_stars=pool_stars,
    pool_lines=pool_lines,
    pool_exp_node_edges=pool_exp_node_edges,
  )

  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  return dataset, train_split_params


def data_mr(datasets_dir,
            gcdb,
            train_samples,
            test_samples,
            
            raw_res_root,
            res_config_version,
            selection_method,
            sample_type,

            verbose=1,
            ):
  dataset = MRDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = MRDataSplit.MRSplitParams(
    n_samples=train_samples,
    
    raw_res_root=raw_res_root,
    res_config_version=res_config_version,
    selection_method=selection_method,
    sample_type=sample_type,
  )
  return dataset, train_split_params
  
  # test_split_params = deepcopy(train_split_params)
  # test_split_params.n_samples = test_samples
  # for k, v in test_split_changes.items():
  #   test_split_params.__dict__[k] = v
  #
  # transform = partial(MSPResidue.dgl_transform,
  #                     features=feature_config,
  #                     label=label,
  #                     include_distance_matrices=True)
  #
  # train_split = dataset.add_split(
  #   split_name='train',
  #   split_params=train_split_params,
  #   exclude_sample_ids=[],
  #   transform=transform,
  #   create=create_new_splits,
  #   named_identifier=train_identifier,
  #   overwrite_identifier=overwrite_identifiers,
  # )
  # test_split = dataset.add_split(
  #   split_name='test',
  #   split_params=test_split_params,
  #   exclude_sample_ids=train_split.sample_ids,
  #   transform=transform,
  #   create=create_new_splits,
  #   named_identifier=test_identifier,
  #   overwrite_identifier=overwrite_identifiers,
  # )
  #
  # input_dim = MSPResidue.get_features_input_dim(feature_config=feature_config)
  #
  # return dataset, input_dim


def data_re(datasets_dir,
            gcdb,
            train_samples,
            test_samples,

            init_g_sizes,
            motif_min_nodes,
            motif_max_nodes,
            n_passes,
            expansion_prob,
            init_g_sparsity,
            motif_sparsity,
            maintain_motif_degree,

            verbose=1,
            ):
  dataset = REDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = REDataSplit.RESplitParams(
    n_samples=train_samples,
    
    init_g_sizes=init_g_sizes,
    motif_min_nodes=motif_min_nodes,
    motif_max_nodes=motif_max_nodes,
    n_passes=n_passes,
    expansion_prob=expansion_prob,
    init_g_sparsity=init_g_sparsity,
    motif_sparsity=motif_sparsity,
    maintain_motif_degree=maintain_motif_degree,
  )
  return dataset, train_split_params


def data_sbm(datasets_dir,
             gcdb,
             train_samples,
             test_samples,
             test_split_changes,
             transform,
             preprocess_config,
             feature_config,
             create_new_splits,
             overwrite_identifiers,
             train_identifier,
             test_identifier,

             min_nodes,
             max_nodes,
             min_clusters,
             max_clusters,
             priors_a,
             priors_b,
             intra_connect_min,
             intra_connect_max,
             inter_connect_min,
             inter_connect_max,

             verbose=1,
             ):
  dataset = SBMDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = SBMDataSplit.SBMSplitParams(
    n_samples=train_samples,
    
    min_nodes=min_nodes,
    max_nodes=max_nodes,
    min_clusters=min_clusters,
    max_clusters=max_clusters,
    priors_a=priors_a,
    priors_b=priors_b,
    intra_connect_min=intra_connect_min,
    intra_connect_max=intra_connect_max,
    inter_connect_min=inter_connect_min,
    inter_connect_max=inter_connect_max,
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  return dataset, train_split_params


def data_hn(datasets_dir,
            gcdb,
            train_samples,
            test_samples,
            test_split_changes,
            transform,
            preprocess_config,
            feature_config,
            create_new_splits,
            overwrite_identifiers,
            train_identifier,
            test_identifier,

            motif_sizes,
            branching_factors,
            depths,

            verbose=1,
            ):
  dataset = HNDataset(dataset_dir=datasets_dir, gcdb=gcdb, verbose=verbose)
  train_split_params = HNDataSplit.HNSplitParams(
    n_samples=train_samples,
    
    motif_sizes=motif_sizes,
    branching_factors=branching_factors,
    depths=depths,
  )

  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  return dataset, train_split_params


def run_sc_with_config(gcdb,
                       exp_dir,
                       data_method,
                       data_method_config,

                       test_split_changes,
                       preprocess_config,
                       create_new_splits,
                       train_identifier,
                       test_identifier,
                       overwrite_identifiers,

                       # label,
                       feature_config,
                       n_clusters=5,
                       LR=1e-3,
                       batch_size=32,
                       dropout=0.0,
                       lambda_l1=0.0,
                       lambda_l2=0.0,
                       lambda_cxe=0,
                       lambda_cmse=0,
                       lambda_cmae=0,
                       lambda_silh=0,
                       lambda_mod=0,
                       lambda_tce=0,
                       lambda_cae=0,
                       lambda_nll=0,
                       epochs=500,
                       chkp_freq=1,
                       vis_freq=3,
                       vis_n_samples=5,
                       val_freq=1,
                       test_freq=1,
                       train_acc_freq=5,
                       test_acc_freq=5):
  with open(LAST_EXP_DIR_LOC, 'w') as f:
    f.write(exp_dir)
  model_dir = f'{exp_dir}/models'
  
  dataset, train_split_params = data_method(gcdb=gcdb, **data_method_config)
  
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = data_method_config['test_samples']
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v

  transform = partial(GCSample.dgl_transform,
                      features=feature_config,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=train_identifier,
    overwrite_identifier=overwrite_identifiers,
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    preprocess_eig=preprocess_config['eig_conf'],
    preprocess_local=preprocess_config['local_features'],
    preprocess_global=preprocess_config['global_features'],
    preprocess_distance=preprocess_config['preprocess_distance'],
    feature_conf=feature_config,
    transform=transform,
    create=create_new_splits,
    named_identifier=test_identifier,
    overwrite_identifier=overwrite_identifiers,
  )

  input_dim = GCSample.get_features_input_dim(feature_config=feature_config)
  
  # model = model_4(input_feature_dim=input_dim,
  #                 n_clusters=n_clusters,
  #                 model_dir=model_dir,
  #                 gcdb=gcdb,
  #                 dropout=dropout)
  
  
  model = mlp_model_0(input_feature_dim=input_dim,
                      n_clusters=n_clusters,
                      model_dir=model_dir,
                      gcdb=gcdb,
                      dropout=dropout)

  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR, weight_decay=lambda_l2)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)

  exp_params = SupervisedCommunities.SCParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
  
    lambda_cxe=lambda_cxe,
    lambda_cmse=lambda_cmse,
    lambda_cmae=lambda_cmae,
    lambda_silh=lambda_silh,
    lambda_mod=lambda_mod,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    lambda_nll=lambda_nll,
  
    node_features=feature_config['local_features'],
    exp_label=feature_config['node_labels'],
  )

  experiment = SupervisedCommunities(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )

  experiment.start(epochs=epochs,
                   chkp_freq=chkp_freq,
                   vis_freq=vis_freq,
                   vis_n_samples=vis_n_samples,
                   val_freq=val_freq,
                   test_freq=test_freq,
                   train_acc_freq=train_acc_freq,
                   test_acc_freq=test_acc_freq,
                   )


def run_with_config(gcdb,
                    exp_dir,
                    
                    data_method,
                    data_config,
                    exp_method,
                    exp_config,
                    model_method,
                    model_config,

                    # test_split_changes,
                    # preprocess_config,
                    # create_new_splits,
                    # train_identifier,
                    # test_identifier,
                    # overwrite_identifiers,

                    feature_config,
                    # n_clusters=5,
                    LR=1e-3,
                    # dropout=0.0,
                    # lambda_l1=0.0,
                    lambda_l2=0.0,
                    epochs=500,
                    chkp_freq=1,
                    vis_freq=3,
                    vis_n_samples=5,
                    val_freq=1,
                    test_freq=1,
                    train_acc_freq=5,
                    test_acc_freq=5):
  with open(LAST_EXP_DIR_LOC, 'w') as f:
    f.write(exp_dir)
  model_dir = f'{exp_dir}/models'

  transform = partial(GCSample.dgl_transform,
                      features=feature_config,
                      include_distance_matrices=True)
  
  dataset, train_split_params = data_method(
    gcdb=gcdb,
    transform=transform,
    feature_config=feature_config,

    **data_config)
  


  input_dim = GCSample.get_features_input_dim(feature_config)
  model = model_method(gcdb=gcdb, model_dir=model_dir, input_dim=input_dim, **model_config)
  model.to(device)

  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR, weight_decay=lambda_l2)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)

  exp_params, exp_class, vis_class = exp_method(
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    feature_config=feature_config,
    **exp_config)
  
  experiment = exp_class(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=vis_class(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device
  )

  experiment.start(epochs=epochs,
                   chkp_freq=chkp_freq,
                   vis_freq=vis_freq,
                   vis_n_samples=vis_n_samples,
                   val_freq=val_freq,
                   test_freq=test_freq,
                   train_acc_freq=train_acc_freq,
                   test_acc_freq=test_acc_freq,
                   )

def copy_db_loc_to_most_recent_exp_dir(db_loc, original=False):
  if os.path.exists(db_loc):
    if os.path.exists(LAST_EXP_DIR_LOC):
      with open(LAST_EXP_DIR_LOC, 'r') as f:
        last_exp_dir = f.read()
      fname = f'{last_exp_dir}/db'
      if original:
        fname += '/original'
      else:
        fname += '/modified'
      validate_directory(fname)
      shutil.copy(db_loc, f'{fname}/local_gc.sqlite')

def initialise_clean_start_dir(
  clean_start_name,
  data_method,
  data_config,
  feature_config,
):
  clean_start_dir = f'{EXP_CLEAN_STARTS}/{clean_start_name}'
  if os.path.exists(clean_start_dir):
    confirm = input(f'Directory "{clean_start_dir}" exists, type y to overwrite:')
    if confirm != 'y':
      print('Exiting...')
      exit(0)
    shutil.rmtree(clean_start_dir)
    validate_directory(clean_start_dir)

  validate_directory(clean_start_dir)
  master_loc = f'{EXP_CLEAN_STARTS}/{clean_start_name}/master_gc.sqlite'
  local_loc = f'{EXP_CLEAN_STARTS}/{clean_start_name}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=True,
    merge=False,
    samples_only=False,
    add_data_from_master=False,
    add_all_from_master=False
  )

  data_config['datasets_dir'] = f'{EXP_CLEAN_STARTS}/{clean_start_name}/datasets'
  data_config['overwrite_identifiers'] = True
  transform = partial(GCSample.dgl_transform,
                      features=feature_config,
                      include_distance_matrices=True,
                      )
  dataset, train_split_params = data_method(
    gcdb=gcdb,
    transform=transform,
    feature_config=feature_config,
    **data_config
  )

  gcdb.merge_local_to_master(
    master_loc=master_loc,
    local_loc=local_loc,
    only_keep_every_nth_epoch_images=1,
    keep_specific_epoch_images=[],
  )




def setup_gcdb(master_loc,
               local_loc,
               erase_master,
               merge,
               samples_only,
               add_data_from_master,
               add_all_from_master=False,
               use_master_as_local=False,
               ):
  # local_db_dir = f'/opt/project/sql_store/tests'
  # master_loc = f'/opt/project/sql_store/gc.sqlite'
  # debug_local_loc = f'/opt/project/sql_store/gc2.sqlite'
  if not use_master_as_local:
    copy_db_loc_to_most_recent_exp_dir(local_loc, original=True)
  else:
    copy_db_loc_to_most_recent_exp_dir(master_loc, original=False)
  gcdb = GCDB()
  if erase_master:
    erase_master = input('To confirm erase master type "y":') == 'y'
  gcdb.init_master(master_loc=master_loc, verbose=False, fresh=erase_master)
  if not use_master_as_local and merge and not erase_master and os.path.exists(local_loc) and os.path.exists(master_loc):
    gcdb.merge_local_to_master(master_loc=master_loc,
                               local_loc=local_loc,
                               only_keep_every_nth_epoch_images=3,
                               keep_specific_epoch_images=[i for i in range(0, 21)])
  if use_master_as_local:
    copy_db_loc_to_most_recent_exp_dir(master_loc, original=False)
  else:
    copy_db_loc_to_most_recent_exp_dir(local_loc, original=False)
  gcdb.init_local(local_loc=local_loc,
                  fresh=True,
                  copy_most_recent=False,
                  verbose=False,
                  add_data_from_master=add_data_from_master,
                  add_all_from_master=add_all_from_master,
                  master_loc=master_loc,
                  use_master_as_local=use_master_as_local,
                  )
  return gcdb