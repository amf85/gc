from copy import deepcopy
from functools import partial

import torch
import torch.nn.functional as F

from data.gc_data import NODE_LEVEL_FEATURES, GRAPH_LEVEL_FEATURES, GCSample
from data.hierarch import HNDataset, HNDataSplit
from data.random_expand import REDataset, REDataSplit
from data.sbm import SBMDataset, SBMDataSplit
from data.motif_expand import MEDataset, MEDataSplit
from experiments.opt import GCAdamOpt
# from experiments.silhouette_community_prediction import SilhouetteCommunityPrediction
# from experiments.measured_unsupervised import MeasuredUnsupervised
from experiments.supervised_communities import SupervisedCommunities
from models.clustering import ClusteringGNN
from sql.db_util import GCDB
from util.constants import TESTS
from vis.communities import CommunitiesVisualiser
from util.constants import LAST_EXP_DIR_LOC

tgt_dev = 'cuda'
device = torch.device('cuda' if tgt_dev == 'cuda' and torch.cuda.is_available() else 'cpu')


def model_4(input_feature_dim, n_clusters, model_dir, gcdb, dropout):
  model_params = ClusteringGNN.ClusteringGNNParams(
    input_dim=input_feature_dim,
    gnn_out_dims=[256, 256, 256, 256, 256, n_clusters],
    # gnn_out_dims=[40, 40, 40, 40, n_clusters],
    activation=F.relu,
    add_self_loops=True,
    use_batchnorm=True,
    dropout=dropout,
  )
  model = ClusteringGNN(model_dir=model_dir, params=model_params, gcdb=gcdb)
  model.to(device)
  return model


def data_me(datasets_dir,
            gcdb,
            label,
            n_spectral_vectors,
            create_new_splits,
            train_samples,
            test_samples,
            init_g_min_nodes,
            init_g_max_nodes,
            init_g_exp_node_edges,
            motif_min_nodes,
            motif_max_nodes,
            motif_exp_node_edges,
            motif_pool_size,
            expansion_prob,
            maintain_motif_degree,
            n_passes,
            preprocess_spectral,
            preprocess_distance,
            # precompute_features,
            precompute_local_features,
            precompute_global_features,
            used_local_features,
            preprocess_top_down_methods,
            preprocess_top_down_n_splits,
            preprocess_top_down_vectors_per_split,
            preprocess_top_down_assignments,
            used_top_down_methods,
            used_top_down_n_splits,
            used_top_down_vectors_per_split,
            used_top_down_assignments,
            test_split_changes):
  dataset = MEDataset(dataset_dir=datasets_dir, gcdb=gcdb)
  create_new_splits = create_new_splits
  train_split_params = MEDataSplit.MESplitParams(
    n_samples=train_samples,
    
    init_g_min_nodes=init_g_min_nodes,
    init_g_max_nodes=init_g_max_nodes,
    init_g_exp_node_edges=init_g_exp_node_edges,
    motif_min_nodes=motif_min_nodes,
    motif_max_nodes=motif_max_nodes,
    motif_exp_node_edges=motif_exp_node_edges,
    motif_pool_size=motif_pool_size,
    expansion_prob=expansion_prob,
    maintain_motif_degree=maintain_motif_degree,
    n_passes=n_passes,
    
    preprocess_spectral=preprocess_spectral,
    preprocess_distance=preprocess_distance,
    # preprocess_additional_features=precompute_features,
    preprocess_local_features=precompute_local_features,
    preprocess_global_features=precompute_global_features,
    
    # preprocess_top_down_ncut='ncut' in preprocess_top_down_methods,
    # preprocess_top_down_median='median' in preprocess_top_down_methods,
    # preprocess_top_down_sign='sign' in preprocess_top_down_methods,
    preprocess_top_down_methods=preprocess_top_down_methods,
    top_down_n_splits=preprocess_top_down_n_splits,
    top_down_vectors_per_split=preprocess_top_down_vectors_per_split,
    top_down_assignments=preprocess_top_down_assignments,
    **{
      'used_n_spectral_vectors': n_spectral_vectors,
      'used_distance_matrix': True,
      'used_local_features': used_local_features,
      'used_top_down_methods': used_top_down_methods,
      'used_top_down_n_splits': used_top_down_n_splits,
      'used_top_down_vectors_per_split': used_top_down_vectors_per_split,
      'used_top_down_assignments': used_top_down_assignments,
    }
  )
  test_split_params = deepcopy(train_split_params)
  test_split_params.n_samples = test_samples
  for k, v in test_split_changes.items():
    test_split_params.__dict__[k] = v
  # test_split_params.init_g_exp_node_edges = 1.
  # test_split_params.expansion_prob = 0.75
  
  feature_list = GCSample.get_spectral_feature_list(n_spectral_vectors)
  feature_list += GCSample.get_top_down_feature_list(split_methods=used_top_down_methods,
                                                     n_splits=used_top_down_n_splits,
                                                     vectors_per_split=used_top_down_vectors_per_split,
                                                     include_assignments=used_top_down_assignments)
  feature_list += [f for f in NODE_LEVEL_FEATURES if f in used_local_features]
  
  transform = partial(GCSample.dgl_transform,
                      feature_list=feature_list,
                      label=label,
                      include_distance_matrices=True)
  
  train_split = dataset.add_split(
    split_name='train',
    split_params=train_split_params,
    exclude_sample_ids=[],
    transform=transform,
    create=create_new_splits
  )
  test_split = dataset.add_split(
    split_name='test',
    split_params=test_split_params,
    exclude_sample_ids=train_split.sample_ids,
    transform=transform,
    create=create_new_splits
  )
  
  return dataset, len(feature_list)


def run_with_config(gcdb,
                    exp_dir,
                    data_config,
                    label,
                    # used_additional_features,
                    used_local_features,
                    n_spectral_vectors,
                    used_top_down_methods,
                    used_top_down_n_splits,
                    used_top_down_vectors_per_split,
                    used_top_down_assignments,
                    n_clusters,
                    LR=1e-3,
                    batch_size=50,
                    dropout=0.0,
                    lambda_l1=0.0,
                    lambda_l2=0.0,
                    lambda_cxe=0,
                    lambda_cmse=0,
                    lambda_cmae=0,
                    lambda_silh=0,
                    lambda_mod=0,
                    lambda_tce=0,
                    lambda_cae=0,
                    lambda_nll=0,
                    epochs=500,
                    chkp_freq=1,
                    vis_freq=3,
                    vis_n_samples=5,
                    val_freq=1,
                    test_freq=1,
                    train_acc_freq=5,
                    test_acc_freq=5):
  with open(LAST_EXP_DIR_LOC, 'w') as f:
    f.write(exp_dir)
  model_dir = f'{exp_dir}/models'
  dataset, input_feature_dim = data_me(
    label=label,
    gcdb=gcdb,
    n_spectral_vectors=n_spectral_vectors,
    used_top_down_methods=used_top_down_methods,
    used_top_down_n_splits=used_top_down_n_splits,
    used_top_down_vectors_per_split=used_top_down_vectors_per_split,
    used_top_down_assignments=used_top_down_assignments,
    **data_config
  )
  
  model = model_4(input_feature_dim=input_feature_dim,
                  n_clusters=n_clusters,
                  model_dir=model_dir,
                  gcdb=gcdb,
                  dropout=dropout)
  
  optimiser_params = GCAdamOpt.AdamOptParams(LR=LR, weight_decay=lambda_l2)
  optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)
  
  exp_params = SupervisedCommunities.SCParams(
    batch_size=batch_size,
    model=model,
    optimiser=optimiser,
    dataset=dataset,
    val_size=0.1,
    n_clusters=n_clusters,
    
    lambda_cxe=lambda_cxe,
    lambda_cmse=lambda_cmse,
    lambda_cmae=lambda_cmae,
    lambda_silh=lambda_silh,
    lambda_mod=lambda_mod,
    lambda_tce=lambda_tce,
    lambda_cae=lambda_cae,
    lambda_nll=lambda_nll,
    
    n_spectral_vectors=n_spectral_vectors,
    node_features=used_local_features,
    exp_label=label,
  )
  experiment = SupervisedCommunities(
    experiment_dir=exp_dir,
    params=exp_params,
    visualiser=CommunitiesVisualiser(vis_dir=f'{exp_dir}/vis'),
    gcdb=gcdb,
    device=device,
  )
  
  experiment.start(epochs=epochs,
                   chkp_freq=chkp_freq,
                   vis_freq=vis_freq,
                   vis_n_samples=vis_n_samples,
                   val_freq=val_freq,
                   test_freq=test_freq,
                   train_acc_freq=train_acc_freq,
                   test_acc_freq=test_acc_freq,
                   )


def _5_0_spectral_only_me(gcdb):
  """
  """
  
  precompute_local_features = set(NODE_LEVEL_FEATURES)
  precompute_global_features = set(GRAPH_LEVEL_FEATURES)
  # remove_features = {'clustering_coeff_sq', 'avg_clustering_coeff_sq'}
  remove_features = {}
  precompute_local_features = precompute_local_features.difference(remove_features)
  precompute_global_features = precompute_global_features.difference(remove_features)
  
  used_local_features = {k: False for k in precompute_local_features}
  # _global_features
  
  data_config = {
    'datasets_dir': f'{TESTS}/datasets',
    
    'train_samples': 100,
    'test_samples': 100,
    
    'init_g_min_nodes': 15,
    'init_g_max_nodes': 30,
    'init_g_exp_node_edges': 3.5,
    'motif_min_nodes': 4,
    'motif_max_nodes': 12,
    'motif_exp_node_edges': 5.5,
    'motif_pool_size': 3,
    'expansion_prob': 1.0,
    'maintain_motif_degree': True,
    'n_passes': [1],
    
    'preprocess_spectral': 'max',
    'preprocess_distance': True,
    # 'precompute_features': precompute_features,
    'precompute_local_features': precompute_local_features,
    'precompute_global_features': precompute_global_features,
    'used_local_features': used_local_features,
    
    'test_split_changes': {
      # 'init_g_exp_node_edges': 2.5,
      # 'expansion_prob': 0.75
    },
    
    'create_new_splits': False,
    
    'preprocess_top_down_methods': ['median', 'sign', 'ncut'],
    'preprocess_top_down_n_splits': 12,
    'preprocess_top_down_vectors_per_split': 10,
    'preprocess_top_down_assignments': True,
  }
  
  run_config = {
    'gcdb': gcdb,
    'exp_dir': f'{TESTS}/experiments/5/0',
    'data_config': data_config,
    'label': 'communities_L1',
    
    'n_clusters': 30,
    'LR': 1e-4,
    'batch_size': 32,
    'epochs': 500,
    'dropout': 0.25,
    'lambda_l1': 0.0,
    'lambda_l2': 0.025,
    
    # 'lambda_cxe': 10.0,
    # 'lambda_cmse': -100.0,
    # 'lambda_cmae': -100.0,
    'lambda_silh': 10.0,
    'lambda_mod': 1.0,
    # 'lambda_tce': 0.0,  # encourages fewer clusters
    # 'lambda_cae': 0.1,  # encourages increased per-node confidence
    'lambda_nll': 1.0,
    
    'n_spectral_vectors': 10,
    'used_local_features': used_local_features,
    'used_top_down_methods': [],
    'used_top_down_n_splits': 10,
    'used_top_down_vectors_per_split': 4,
    'used_top_down_assignments': True,
    
    
    'chkp_freq': 1,
    'vis_freq': 1,
    'vis_n_samples': 1,
    'val_freq': 1,
    'test_freq': 1,
    'train_acc_freq': 5,
    'test_acc_freq': 5,
  }
  
  run_with_config(**run_config)


def main():
  local_db_dir = f'/opt/project/sql_store/tests'
  master_loc = f'/opt/project/sql_store/gc.sqlite'
  debug_local_loc = f'/opt/project/sql_store/gc2.sqlite'
  gcdb = GCDB()
  erase_master = False
  if erase_master:
    erase_master = input('To confirm erase master type "y"') == 'y'
  gcdb.init_master(master_loc=master_loc, verbose=True, fresh=erase_master)
  # merge = False
  merge = True
  samples_only = True
  if merge:
    gcdb.merge_local_samples_to_master(master_loc=master_loc,
                                       local_loc=debug_local_loc,
                                       samples_only=samples_only,
                                       keep_train_eg_confs=3,
                                       keep_train_eg_hmaxs=float('inf'),
                                       keep_train_eg_labels=float('inf'),
                                       keep_test_eg_confs=3,
                                       keep_test_eg_hmaxs=float('inf'),
                                       keep_test_eg_labels=float('inf'))
  # gcdb.init_master(master_loc=master_loc, verbose=True, fresh=False)
  
  gcdb.init_local(local_loc=debug_local_loc,
                  fresh=True,
                  verbose=False,
                  add_data_from_master=False,
                  master_loc=master_loc)
  
  _5_0_spectral_only_me(gcdb)


if __name__ == '__main__':
  main()
