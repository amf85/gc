from sqlalchemy import select, and_, or_
from sql.sql_base import *
import numpy as np
import pandas as pd
import scipy as sp
import pickle
from scipy import stats

from util.constants import TESTS, ARTIFICIAL_EXPS
from util.util import validate_directory
from sql.db_util import GCDB
from data.gc_data import GRAPH_LEVEL_FEATURES, NODE_LEVEL_FEATURES, GCDataSplit
from pprint import pprint, PrettyPrinter
import json
from analysis.analysis_util import json_conv

class DCAnalysis(object):
  stats = {'mean': np.mean, 'median': np.median, 'min': np.min, 'max': np.max, 'std': np.std,
           'skew': sp.stats.skew, 'kurtosis': sp.stats.kurtosis}
  
  def __init__(self, anaylsis_dir, db_locs: dict):
    '''
    
    :param db_locs: pass a dict with db_id + location
    '''
    self.analysis_dir = validate_directory(anaylsis_dir)
    self.db_locs = db_locs
    self.sessionmakers = {}
    for db_id, db_loc in db_locs.items():
      self.sessionmakers[db_id] = GCDB.get_sessionmaker(db_loc, verbose=False)
    
  def analyse_datasets(self, db_id):
    exp_data_breakdown = {}
    exps = self.get_experiments(db_id)
    cols = ['exp_id', 'split', 'split_id', 'n_samples']
    cols += [f'{feature}_{stat}' for feature in GRAPH_LEVEL_FEATURES for stat in self.stats.keys()]
    df = pd.DataFrame(columns=cols)
    for gc_exp, exp in exps:
      exp_data_breakdown[exp.id] = {}
      if 'train_split_id' in exp.__dict__.keys():
        train_split = self.get_datasplit(db_id, exp.train_split_id)
        breakdown = self.split_breakdown(train_split)
        exp_data_breakdown[exp.id]['train'] = breakdown
        row = [exp.id, 'train', exp.train_split_id, len(train_split.samples)]
        row += [breakdown[feature][stat]
                for feature in GRAPH_LEVEL_FEATURES for stat in self.stats.keys()]
        df.loc[len(df)] = row
      if 'val_split_id' in exp.__dict__.keys():
        val_split = self.get_datasplit(db_id, exp.val_split_id)
        breakdown = self.split_breakdown(val_split)
        exp_data_breakdown[exp.id]['val'] = breakdown
        row = [exp.id, 'val', exp.train_split_id, len(val_split.samples)]
        row += [breakdown[feature][stat]
                for feature in GRAPH_LEVEL_FEATURES for stat in self.stats.keys()]
        df.loc[len(df)] = row
      if 'test_split_id' in exp.__dict__.keys():
        test_split = self.get_datasplit(db_id, exp.test_split_id)
        breakdown = self.split_breakdown(test_split)
        exp_data_breakdown[exp.id]['test'] = breakdown
        row = [exp.id, 'test', exp.train_split_id, len(test_split.samples)]
        row += [breakdown[feature][stat]
                for feature in GRAPH_LEVEL_FEATURES for stat in self.stats.keys()]
        df.loc[len(df)] = row
    print(json.dumps(exp_data_breakdown, indent=4, sort_keys=False, default=json_conv))
    
    with open(f'{self.analysis_dir}/dataset_analysis.json', 'w') as f:
      f.write(json.dumps(exp_data_breakdown, indent=4, sort_keys=False, default=json_conv))
    df.to_csv(f'{self.analysis_dir}/dataset_analysis.csv',
              header=True, index=True, index_label='index')
    with open(f'{self.analysis_dir}/dataset_analysis_py.pkl', 'wb') as f:
      pickle.dump((df, exp_data_breakdown), f)
    
  @staticmethod
  def split_breakdown(split) -> dict:
    breakdown = {}
    for feature in GRAPH_LEVEL_FEATURES:
      stats = {}
      data = np.array([sample.__dict__[feature] for sample in split.samples])
      stats['mean'] = np.mean(data)
      stats['max'] = np.max(data)
      stats['min'] = np.min(data)
      stats['median'] = np.median(data)
      stats['std'] = np.std(data)
      stats['skew'] = sp.stats.skew(data)
      stats['kurtosis'] = sp.stats.kurtosis(data)
      breakdown[feature] = stats
    return breakdown
    
  def get_experiments(self, db_id):
    sess = self.sessionmakers[db_id]()
    select_exps = select(GCExperimentBase)
    sess.execute(select_exps)
    gc_exps = [row[0] for row in sess.execute(select_exps).all()]
    def recover_exp_stmt(gc_exp_base):
      subclass = class_to_base[gc_exp_base.experiment_class]
      return select(subclass).where(subclass.id == gc_exp_base.id)
    exps = [(exp, sess.execute(recover_exp_stmt(exp)).first()[0]) for exp in gc_exps]
    return exps
    
  def get_datasplit(self, db_id, split_id):
    sess = self.sessionmakers[db_id]()
    select_datasplit = select(GCDataSplitBase).where(GCDataSplitBase.id == split_id)
    datasplit = sess.execute(select_datasplit).first()[0]
    return datasplit

def analyse_all_splits_in_db(db_loc):
  session = GCDB.get_sessionmaker(db_loc, verbose=True)()
  datasplit_rows = session.execute(select(GCDataSplitBase)).all()
  ids = [dsr[0].id for dsr in datasplit_rows]
  for id in ids:
    analyse_split_by_id(split_id=id, db_loc=db_loc)
  

def analyse_split_by_id(split_id, db_loc):
  session = GCDB.get_sessionmaker(db_loc, verbose=True)()
  GCDataSplit.push_analysis_to_db(session, split_id)
  
def correlate_sample_features(sample_class, db_loc, output_dir):
  validate_directory(output_dir)
  session = GCDB.get_sessionmaker(db_loc, verbose=True)()
  class_fields = [f for f in sample_class.__dict__.keys()
                         if not f.startswith('_') and f != 'id']
  generic_fields = GRAPH_LEVEL_FEATURES + ['n_nodes', 'n_edges']
  class_select_fields = [sample_class.__dict__[f] for f in class_fields]
  generic_select_fields = [GCSampleBase.__dict__[f] for f in generic_fields]
  samples_df = pd.DataFrame.from_records(
    session.execute(
      select(tuple(class_select_fields + generic_select_fields))
        .join(sample_class, sample_class.id==GCSampleBase.id)).all(),
    columns=class_fields+generic_fields)
  samples_df.corr().to_csv(f'{output_dir}/{sample_class.__tablename__}.csv')



if __name__ == '__main__':
  art_exps_local_loc = f'{ARTIFICIAL_EXPS}/local_gc.sqlite'
  test_db_loc = f'{TESTS}/experiments/6/0/gc.sqlite'
  correlate_sample_features(sample_class=MotifExpandedNetworkBase,
                            db_loc=art_exps_local_loc,
                            output_dir=f'{ARTIFICIAL_EXPS}/sample_feature_correlations')
  # analyse_all_splits_in_db(art_exps_local_loc)
  # analyse_split_by_id(split_id=21, db_loc=art_exps_local_loc)
  
  # dca = DCAnalysis(anaylsis_dir=f'{TESTS}/experiments/6/0/analysis/data', db_locs={'db': test_db_loc})
  # dca.analyse_datasets('db')