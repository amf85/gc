from datetime import timedelta
import numpy as np
from sqlalchemy import select
from sql.sql_base import *

def get_experiments(session):
  select_exps = select(GCExperimentBase)
  session.execute(select_exps)
  gc_exps = [row[0] for row in session.execute(select_exps).all()]
  
  def recover_exp_stmt(gc_exp_base):
    subclass = class_to_base[gc_exp_base.experiment_class]
    return select(subclass).where(subclass.id == gc_exp_base.id)
  
  exps = [(exp, session.execute(recover_exp_stmt(exp)).first()[0]) for exp in gc_exps]
  return exps

def get_epochs_for_exp(session, gc_exp):
  exp_id = gc_exp.id
  class_matched_epochs = [
    e[0] for e in session.execute(
      select(GCEpochBase)
        .where(GCEpochBase.experiment_class == gc_exp.experiment_class)).all()]
  epoch_class = tablenames_to_base[class_matched_epochs[0].table_name]
  exp_epochs = session.execute(select(epoch_class).where(epoch_class.experiment_id == exp_id)).all()
  return exp_epochs

def json_conv(obj):
  if isinstance(obj, np.int64):
    return int(obj)
  if isinstance(obj, timedelta):
    return str(obj)