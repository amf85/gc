from util.constants import *
from exp_scripts.default_setups import *
from sql.db_util import GCDB
from data.gc_data import GRAPH_LEVEL_FEATURES, NODE_LEVEL_FEATURES
from torch_geometric.datasets import TUDataset

DIST_ROOT = f'{OUTPUT}/dist_analysis'


def dists_ME():
  local_exp_dir = validate_directory(f'{DIST_ROOT}/motif_expand')
  master_loc = f'{DIST_ROOT}/master_gc.sqlite'
  local_loc = f'{DIST_ROOT}/local_gc.sqlite'
  gcdb = setup_gcdb(
    master_loc=master_loc,
    local_loc=local_loc,
    erase_master=False,
    merge=True,
    samples_only=True,
    add_data_from_master=False
  )

  remove_node_level_features = ['betweenness_centrality', 'is_periphery', 'is_centre', ]
  preprocess_locals = [f for f in NODE_LEVEL_FEATURES if f not in remove_node_level_features]
  remove_graph_level_features = []
  preprocess_globals = [f for f in GRAPH_LEVEL_FEATURES if f not in remove_graph_level_features]

  eig_splits = 0
  eig_preprocess = {
    'per_layer_adjacency_variants': ['laplacian'] * eig_splits,
    'per_layer_eig_algorithms': ['sp_eigsh'] * eig_splits,
    'per_layer_split_methods': ['median'] * eig_splits,
    'eig_fail_progression':
      {5: 'sp_eigs', 10: 'sp_eigh', 15: 'sp_eig', 20: 'np_eigh', 25: 'np_eig',
       30: 'fail'},
    'per_layer_vnodes': ['no_vnode'] * eig_splits,
    'per_layer_vnode_weights': ['unweighted'] * eig_splits,

    'per_layer_eigenvectors': [20] + [10] * (eig_splits - 1),
    'per_layer_eigenvalues': [5] * eig_splits,
    'per_layer_assignments': [True] * eig_splits,
    'per_layer_use_gpu': [False] * eig_splits,
    'per_layer_tols': [0.] * eig_splits
  }

  feature_conf = {
    'eig_conf': eig_preprocess,
    'local_features': set(NODE_LEVEL_FEATURES).difference(remove_node_level_features),
    'node_features': [],
    'edge_features': [],
    'node_labels': ['communities_L1'],
    'graph_labels': [],
    'edge_labels': [],
  }

  base_data_config = {
    'datasets_dir': f'{local_exp_dir}/datasets',

    'verbose': 0,

    'init_g_min_nodes': 10,
    'init_g_max_nodes': 25,
    'init_g_exp_node_edges': 1.5,
    'motif_min_nodes': 4,
    'motif_max_nodes': 12,
    'motif_exp_node_edges': 3.5,
    'motif_pool_size': 3,
    'expansion_prob': 1.0,
    'maintain_motif_degree': True,
    'n_passes': [1],

    'train_samples': 1000,
    'test_samples': 0,

    'train_identifier': None,
    'test_identifier': None,
    'overwrite_identifiers':False,

    'test_split_changes': {},

    'preprocess_config': {
      'eig_conf': eig_preprocess,
      'local_features': preprocess_locals,
      'global_features': preprocess_globals,
      'preprocess_distance': False,
      'node_features': ['communities_L1'],
      'node_labels': [],
      'graph_labels': [],
      'edge_labels': ['communities_L1'],
    },

    'create_new_splits': False,

    'transform': None,
    'feature_config': feature_conf,
  }

  dataset, train_split_params = data_me(gcdb=gcdb, **base_data_config)


if __name__ == '__main__':
  e = TUDataset(root='./PROTEINS', name='PROTEINS', use_node_attr=True)
  dists_ME()
