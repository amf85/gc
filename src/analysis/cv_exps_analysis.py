from sqlalchemy import select, and_, or_
from sql.sql_base import *
import numpy as np
import pandas as pd
import scipy as sp
import pickle
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns

from util.constants import *
from util.util import validate_directory
from sql.db_util import GCDB
from data.gc_data import GRAPH_LEVEL_FEATURES, NODE_LEVEL_FEATURES
from pprint import pprint, PrettyPrinter
import json
from analysis.analysis_util import get_experiments, get_epochs_for_exp, json_conv

from analysis.analysis_util import get_experiments

def get_session(db_loc):
  return GCDB.get_sessionmaker(db_loc, verbose=False)()

def get_most_recent_n_cv_exps(db_loc, n=1):
  session = get_session(db_loc)
  recent_exp_cv_groups = []
  group_starts_done = []
  all_exps = session.execute(select(GCExperimentBase).order_by(GCExperimentBase.id.desc())).all()
  i = 0
  while len(recent_exp_cv_groups) < n and i < len(all_exps):
    gc_exp = all_exps[i][0]
    exp_class = GCDB.exp_names_tables[gc_exp.table_name][0]
    exp = session.execute(select(exp_class).where(exp_class.id == gc_exp.id)).first()[0]
    cv_group_start = exp.cross_val_group_start
    if cv_group_start in group_starts_done:
      continue
    group_starts_done.append(cv_group_start)
    group_exps = session.execute(select(exp_class).where(exp_class.cross_val_group_start == cv_group_start)).all()
    group_exps = [ge[0] for ge in group_exps]
    recent_exp_cv_groups.append(group_exps)
    i += 1
  return recent_exp_cv_groups



  
if __name__ == '__main__':
  # exp_loc = f'{TESTS}/experiments/6/0'
  db_loc = f'{OUTPUT}/active_db/master_gc.sqlite'
  get_most_recent_n_cv_exps(db_loc=db_loc, n=1)