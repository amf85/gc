from sqlalchemy import select, and_, or_
from sql.sql_base import *
import numpy as np
import pandas as pd
import scipy as sp
import pickle
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns

from util.constants import TESTS
from util.util import validate_directory
from sql.db_util import GCDB
from data.gc_data import GRAPH_LEVEL_FEATURES, NODE_LEVEL_FEATURES
from pprint import pprint, PrettyPrinter
import json
from analysis.analysis_util import get_experiments, get_epochs_for_exp, json_conv

from analysis.analysis_util import get_experiments

class RunAnalysis(object):
  def __init__(self, analysis_dir, db_locs: dict):
    self.analysis_dir = validate_directory(analysis_dir)
    self.db_locs = db_locs
    self.sessionmakers = {}
    for db_id, db_loc in db_locs.items():
      self.sessionmakers[db_id] = GCDB.get_sessionmaker(db_loc, verbose=True)
  
  def analyse_most_recent_run(self, db_id):
    sess = self.sessionmakers[db_id]()
    exps = get_experiments(sess)
    latest_exp = sorted(exps, key=lambda x: x[0].id, reverse=True)[0]
    gc_exp, class_exp = latest_exp[0], latest_exp[1]
    
    epoch_rows = get_epochs_for_exp(sess, gc_exp)
    epochs_df = self.get_epochs_data(epoch_rows, max_epoch_num=100, custom_metrics=['test_nll'])
    summary = self.summarise_single_run(epochs_df)
    summary_str = json.dumps(summary, indent=4, sort_keys=False, default=json_conv)
    with open(f'{self.analysis_dir}/summary.json', 'w') as f:
      f.write(summary_str)
    self.plot_single_epochs_df_data(epochs_df, plots=[('train_acc', 'test_acc')])
    print('test')
    
  def summarise_single_run(self, epochs_df, max_epoch_num=None):
    summary = {}
    if max_epoch_num:
      epochs_df = epochs_df[:max_epoch_num+1]
    summary['total_epochs'] = epochs_df['epoch_num'].max()
    summary['elapsed_time'] = epochs_df['elapsed_time'].max()
    summary['elapsed_perf_time'] = epochs_df['elapsed_perf_time'].max()
    summary['mean_epoch_time'] = epochs_df['elapsed_time'].diff().mean()
    summary['mean_epoch_perf_time'] = epochs_df['elapsed_perf_time'].diff().mean()
    summary['peak_train_acc'] = epochs_df['train_acc'].max()
    summary['peak_train_acc_epoch'] = epochs_df['train_acc'].argmax()
    summary['peak_test_acc'] = epochs_df['test_acc'].max()
    summary['peak_test_acc_epoch'] = epochs_df['test_acc'].argmax()
    summary['peak_train_loss'] = epochs_df['train_loss'].min()
    summary['peak_train_loss_epoch'] = epochs_df['train_loss'].argmin()
    summary['peak_test_loss'] = epochs_df['test_loss'].min()
    summary['peak_test_loss_epoch'] = epochs_df['test_loss'].argmin()
    summary['mean_train_acc_abs_diff'] = epochs_df['train_acc'].diff().abs().mean()
    summary['mean_train_loss_abs_diff'] = epochs_df['train_loss'].diff().abs().mean()
    summary['mean_test_acc_abs_diff'] = epochs_df['test_acc'].diff().abs().mean()
    summary['mean_test_loss_abs_diff'] = epochs_df['test_loss'].diff().abs().mean()
    return summary
    
    
    
    
    
  
  def plot_single_epochs_df_data(self, epochs_df, plots):
    plots_dir = validate_directory(f'{self.analysis_dir}/plots')
    for plot_metrics in plots:
      x = epochs_df['epoch_num']
      ys = []
      for series_name in plot_metrics:
        y = epochs_df[series_name]
        sns.lineplot(x=x, y=y, legend='full')
      print('test')
      plt.savefig(f'{plots_dir}/test.jpg')
  
  @staticmethod
  def get_epochs_data(epoch_rows, max_epoch_num=None, custom_metrics=None):
    df_cols = ['epoch_num', 'elapsed_time', 'elapsed_perf_time',
               'train_acc', 'train_loss', 'test_acc', 'test_loss']
    if custom_metrics is not None:
      df_cols += custom_metrics
    df = pd.DataFrame(columns=df_cols)
    for epoch_row in epoch_rows:
      epoch = epoch_row[0]
      if max_epoch_num is not None and epoch.epoch_num > max_epoch_num:
        break
      row = [epoch.__dict__[col] for col in df_cols]
      df.loc[len(df)] = row
    return df
    
  
if __name__ == '__main__':
  exp_loc = f'{TESTS}/experiments/6/0'
  db_loc = f'{exp_loc}/db/original/gc.sqlite'
  ra = RunAnalysis(analysis_dir=f'{exp_loc}/analysis/runs', db_locs={'db': db_loc})
  ra.analyse_most_recent_run('db')