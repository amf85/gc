import pygraphviz as pgv
import scipy as sp
import scipy.sparse
import seaborn as sns


class HierarchicalNetVisualiser(object):
  def __init__(self):
    super(HierarchicalNetVisualiser, self).__init__()
  
  def visualise_net(self, A, out_path, show_node_info=False, node_features=None, node_colours=None):
    palette = sns.color_palette('Set2')
    g = pgv.AGraph(strict=False, directed=False)
    color = palette[2]
    for n in range(len(A)):
      g.add_node(n)
      node = g.get_node(n)
      if node_colours is not None:
        color = palette[node_colours[n]]
      color_str = '#%02x%02x%02x' % (int(color[0] * 255), int(color[1] * 255), int(color[2] * 255))
      node.attr['fillcolor'] = color_str
      node.attr['color'] = color_str
      if show_node_info:
        node.attr['label'] = node_features[n]
        node.attr['label'] = f'{n}'
        node.attr['style'] = 'filled'
        # node.attr['shape'] = 'point'
        node.attr['width'] = 0.1
        node.attr['height'] = 0.1
        node.attr['fontsize'] = 8
      else:
        node.attr['label'] = ''
        node.attr['style'] = 'filled'
        node.attr['shape'] = 'point'
        node.attr['width'] = 0.1
        node.attr['height'] = 0.1
    
    edges = sp.sparse.coo_matrix(A)
    for e in range(edges.getnnz()):
      i, j = edges.row[e], edges.col[e]
      if i <= j:
        g.add_edge(edges.row[e], edges.col[e])
        edge = g.get_edge(edges.row[e], edges.col[e])
        edge.attr['weight'] = 0.01
        edge.attr['penwidth'] = 0.2
    
    layouts = ['neato', 'dot', 'twopi', 'circo', 'fdp', 'nop']
    g.layout(layouts[0])
    g.draw(out_path)
