import pygraphviz as pgv
import seaborn as sns
import numpy as np

from util.util import validate_directory, rgba_to_rgb

BG_COL = (1.0, 1.0, 1.0)

palette_12 = [
  (141, 211, 199),
  (255, 255, 179),
  (190, 186, 218),
  (251, 128, 114),
  (128, 177, 211),
  (253, 180, 98),
  (179, 222, 105),
  (252, 205, 229),
  (217, 217, 217),
  (188, 128, 189),
  (204, 235, 197),
  (255, 237, 111)
]
palette_12 = [tuple([c / 255 for c in p]) for p in palette_12]


class CommunitiesVisualiser(object):
  def __init__(self, vis_dir):
    super(CommunitiesVisualiser, self).__init__()
    self.vis_dir = validate_directory(vis_dir)
  
  def visualise(self,
                A_edge,
                labels,
                confidences=None,
                filename='temp.jpg',
                n_colours=None,
                np_seed=None):
    use_numbers = False
    if n_colours is not None:
      if n_colours > 12:
        # palette = sns.color_palette('husl', n_colours)
        palette = palette_12
        use_numbers = True
      elif n_colours > 8:
        palette = palette_12
      else:
        palette = sns.color_palette("Set2")
    else:
      palette = sns.color_palette("Set2")
    if np_seed is not None:
      np.random.seed(np_seed)
    palette = [palette[i] for i in np.random.permutation(len(palette))]
    g = pgv.AGraph(strict=False, directed=False)
    alpha = 1.0
    for n in range(len(labels)):
      g.add_node(n)
      node = g.get_node(n)
      node.attr['community'] = labels[n]
      color = palette[labels[n] % len(palette)]
      if confidences is not None:
        alpha = confidences[n]
      color = rgba_to_rgb(foreground=color, background=BG_COL, alpha=alpha)
      color_str = '#%02x%02x%02x' % (int(color[0] * 255), int(color[1] * 255), int(color[2] * 255))
      node.attr['fillcolor'] = color_str
      node.attr['color'] = color_str
      node.attr['style'] = 'filled'
      node.attr['width'] = 0.25
      node.attr['height'] = 0.25
      
      if use_numbers:
        node.attr['shape'] = 'circle'
        node.attr['label'] = f'{labels[n]}'
        node.attr['fontsize'] = 14
        node.attr['fontname'] = 'Helvetica'
        node.attr['fixedsize'] = True
        node.attr['width'] = 0.4
        node.attr['height'] = 0.4
      else:
        node.attr['shape'] = 'point'
        node.attr['label'] = ''
    
    for e in range(A_edge.shape[1]):
      i, j = A_edge[0, e], A_edge[1, e]
      if i <= j:
        g.add_edge(i, j)
        edge = g.get_edge(i, j)
        edge.attr['weight'] = 0.1
        edge.attr['penwidth'] = 0.2
    
    g.layout('neato')
    g.draw(f'{self.vis_dir}/{filename}')
