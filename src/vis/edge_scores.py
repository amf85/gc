import pygraphviz as pgv
import seaborn as sns
import numpy as np
import scipy.sparse as sps

from util.util import validate_directory, rgba_to_rgb

BG_COL = (1.0, 1.0, 1.0)

palette_12 = [
  (141, 211, 199),
  (255, 255, 179),
  (190, 186, 218),
  (251, 128, 114),
  (128, 177, 211),
  (253, 180, 98),
  (179, 222, 105),
  (252, 205, 229),
  (217, 217, 217),
  (188, 128, 189),
  (204, 235, 197),
  (255, 237, 111)
]
palette_12 = [tuple([c / 255 for c in p]) for p in palette_12]

missing_col = sns.color_palette('husl', 8)[5]
wrong_col = sns.color_palette('hls', 8)[0]

class EdgeScoresVisualiser(object):
  def __init__(self, vis_dir):
    super(EdgeScoresVisualiser, self).__init__()
    self.vis_dir = validate_directory(vis_dir)
  
  def visualise(self,

                coo_edges,
                n_nodes,
                edge_scores,

                # A_edge,
                # preds=None,
                # labels=None,
                # confidences=None,
                filename='temp.jpg',
                # n_colours=None,
                np_seed=None):
    g = pgv.AGraph(strict=False, directed=False)
    alpha = 1.0
    for n in range(n_nodes):
      g.add_node(n)
      node = g.get_node(n)
      node.attr['style'] = 'filled'
      node.attr['width'] = 0.25
      node.attr['height'] = 0.25
      node.attr['label'] = ''


    for e_idx in range(edge_scores.shape[0]):
      i, j = coo_edges[0][e_idx], coo_edges[1][e_idx]
      # if i <= j:
      if i < j:
        g.add_edge(i, j)
      # if g.has_edge(j, i)
        edge = g.get_edge(i, j)
        e2_idx = [k for k in range(len(coo_edges[0])) if coo_edges[0][k] == j and coo_edges[1][k] == i][0]

        mean_score = 0.5 * (edge_scores[e_idx] + edge_scores[e2_idx])


        edge.attr['weight'] = 0.1
        edge.attr['penwidth'] = 8. * (mean_score**2) + 0.2

    
    g.layout('neato')
    g.draw(f'{self.vis_dir}/{filename}')
