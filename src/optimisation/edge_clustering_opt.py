# from bayes_opt import BayesianOptimization
from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from bayes_opt.util import UtilityFunction, acq_max, ensure_rng
import math
from util.util import GCTime, validate_directory, printProgressBar
from datetime import timedelta, datetime
import numpy as np
from sqlalchemy import select, and_
from sql.sql_base import *
import json
import pandas as pd
from exp_scripts.default_setups import *
from optimisation.gc_bayes_opt import BayesOpt, BayesOptVariable, BayesOptDatapoint



class EdgeClusteringOpt(BayesOpt):
  
  def __init__(self,
               gcdb,
               identifier,
               opt_dir,
               # matching_exps_method,
               # target_function,
               variables,
               random_steps,
               bayes_steps,
               time_limit,
               eval_repeats,
               eval_time_limit,
               eval_epoch_limit,
               early_stop_after_stalled,
               run_conf,
               data_conf
               ):
    super(EdgeClusteringOpt, self).__init__(
      gcdb=gcdb,
      identifier=identifier,
      opt_dir=opt_dir,
      variables=variables,
      random_steps=random_steps,
      bayes_steps=bayes_steps,
      time_limit=time_limit,
      eval_repeats=eval_repeats,
      eval_time_limit=eval_time_limit,
      eval_epoch_limit=eval_epoch_limit,
      early_stop_after_stalled=early_stop_after_stalled,
      run_conf=run_conf,
      data_conf=data_conf)
    # self.gcdb = gcdb
    # self.identifier = identifier
    # self.opt_dir = validate_directory(opt_dir)
    # # self.matching_exps_method = matching_exps_method
    # # self.target_function = target_function
    # self.variables = variables
    # self.random_steps = random_steps
    # self.bayes_steps = bayes_steps
    # self.time_limit = time_limit
    # self.eval_repeats = eval_repeats
    # self.eval_time_limit = eval_time_limit
    # self.eval_epoch_limit = eval_epoch_limit
    # self.early_stop_after_stalled = early_stop_after_stalled
    # self.run_conf = run_conf
    # self.data_conf = data_conf

  def target_fn(
          self,
          # iter,
          eval_repeats,
          eval_time_limit,
          eval_epoch_limit,
          early_stop_after_stalled,
          #
          gcdb=None,
          exp_dir=None,

          data_method=None,
          data_method_config=None,
          exp_method=None,
          exp_config=None,
          model_method=None,
          model_config=None,
          test_split_changes=None,
          preprocess_config=None,
          train_identifier=None,
          test_identifier=None,

          feature_config=None,
          gnn_layers=None,
          mlp_layers=None,
          gnn_dim=None,
          mlp_dim_0=None,
          mlp_dim_reduction=None,
          LR=None,
          gnn_dropout=None,
          mlp_dropout=None,
          lambda_L2=None,
          data_config=None,
          run_config=None,
          *args,
          **kwargs,

  ):
    super(EdgeClusteringOpt, self).target_fn(eval_repeats, eval_time_limit, eval_epoch_limit, early_stop_after_stalled)

    accs = []
    for r in range(eval_repeats):

      model_dir = f'{exp_dir}/models'

      dataset, train_split_params = data_method(gcdb=gcdb, **data_method_config)

      test_split_params = deepcopy(train_split_params)
      test_split_params.n_samples = data_method_config['test_samples']
      for k, v in test_split_changes.items():
        test_split_params.__dict__[k] = v

      transform = partial(GCSample.dgl_transform,
                          features=feature_config,
                          include_distance_matrices=True)

      train_split = dataset.add_split(
        split_name='train',
        split_params=train_split_params,
        exclude_sample_ids=[],
        preprocess_eig=preprocess_config['eig_conf'],
        preprocess_local=preprocess_config['local_features'],
        preprocess_global=preprocess_config['global_features'],
        preprocess_distance=preprocess_config['preprocess_distance'],
        feature_conf=feature_config,
        transform=transform,
        create=False,
        named_identifier=train_identifier,
        overwrite_identifier=False,
      )
      test_split = dataset.add_split(
        split_name='test',
        split_params=test_split_params,
        exclude_sample_ids=train_split.sample_ids,
        preprocess_eig=preprocess_config['eig_conf'],
        preprocess_local=preprocess_config['local_features'],
        preprocess_global=preprocess_config['global_features'],
        preprocess_distance=preprocess_config['preprocess_distance'],
        feature_conf=feature_config,
        transform=transform,
        create=False,
        named_identifier=test_identifier,
        overwrite_identifier=False,
      )

      input_dim = GCSample.get_features_input_dim(feature_config)

      model_config['gnn_out_dims'] = [gnn_dim] * gnn_layers
      mlp_out_dims = [mlp_dim_0]
      for L in range(mlp_layers - 1):
        if mlp_dim_reduction == 'halve':
          mlp_out_dims += [mlp_out_dims[-1] / 2]
        elif mlp_dim_reduction == 'same':
          mlp_out_dims += [mlp_out_dims[-1]]
        else:
          raise NotImplementedError()
      mlp_out_dims += [2]
      model_config['mlp_out_dims'] = list(map(int, mlp_out_dims))
      model_config['dropout_gnn'] = gnn_dropout
      model_config['dropout_mlp'] = mlp_dropout

      model = model_method(gcdb=gcdb, model_dir=model_dir, input_dim=input_dim, **model_config)
      model.to(device)

      optimiser_params = GCAdamOpt.AdamOptParams(LR=LR, weight_decay=lambda_L2)
      optimiser = GCAdamOpt(params=model.parameters(), opt_params=optimiser_params, gcdb=gcdb)

      exp_params, exp_class, vis_class = exp_method(
        model=model,
        optimiser=optimiser,
        dataset=dataset,
        feature_config=feature_config,
        **exp_config)

      experiment: SupervisedEdgeCommunities = exp_class(
        experiment_dir=exp_dir,
        params=exp_params,
        visualiser=vis_class(vis_dir=f'{exp_dir}/vis'),
        gcdb=gcdb,
        device=device
      )

      train_batches, test_batches = experiment.prepare_data()

      train_1s, train_n_edges, test_1s, test_n_edges = 0, 0, 0, 0
      for _, edge_labels in train_batches:
        train_1s += torch.sum(edge_labels)
        train_n_edges += edge_labels.size()[0]
      for _, edge_labels in test_batches:
        test_1s += torch.sum(edge_labels)
        test_n_edges += edge_labels.size()[0]

      print(f'Eval {r + 1}/{eval_repeats}')
      print(f'Experiment id: {experiment.id}')
      print(f'Test naive baseline = {(test_1s / test_n_edges).item()}    '
            f'({test_1s.item()}/{test_n_edges})')

      timer = GCTime()
      timer.start_timer('eval')
      peak_acc = -np.inf
      epochs_without_improvement = 0
      for epoch_num in range(eval_epoch_limit):
        epoch = experiment.train_epoch(
          epoch_num=epoch_num,
          train_batches=train_batches,
          test_batches=test_batches,
          test_freq=1,
          vis_freq=0,
          vis_n_samples=0,
        )
        if epoch.test_acc > peak_acc:
          peak_acc = epoch.test_acc
          epochs_without_improvement = 0
        else:
          epochs_without_improvement += 1

        epoch.elapsed_perf_time = timedelta(seconds=timer.elapsed_perf_time('eval'))
        epoch.elapsed_time = timedelta(seconds=timer.elapsed_time('eval'))

        session = gcdb.local_sess()
        epoch.write_to_db(session=session)

        printProgressBar(
          iteration=epoch_num + 1,
          total=eval_epoch_limit,
          prefix=f'Run progress:',
          suffix=f' -- Epoch {epoch_num + 1}/{eval_epoch_limit} | '
                 f'Last epoch: {round(epoch.test_acc, 4)} | '
                 f'Best epoch: {round(peak_acc, 4)} (Epoch {epoch_num + 1 - epochs_without_improvement}, {str(epoch.elapsed_time)})',
          length=50,
          decimals=2
        )

        if epochs_without_improvement > early_stop_after_stalled:
          print('\n')
          break

        if timer.elapsed_time('eval', as_timedelta=True) > eval_time_limit:
          print('\n')
          break

      accs.append(peak_acc)
    return np.mean(accs)

  def matching_fn(
          self,
          session,
          data_conf,
          model_conf,
          exp_conf,
          feature_conf,
          variables,
          eval_time_limit,
          eval_epoch_limit,
          early_stop_after_stalled,
          constrain_param_range=True,
          return_exp_ids=False,
          raw=False,
  ):
    find_train_split = select(GCDataSplitBase).where(
      GCDataSplitBase.name == data_conf['train_identifier'])
    find_test_split = select(GCDataSplitBase).where(
      GCDataSplitBase.name == data_conf['test_identifier'])
    matching_train_splits = session.execute(find_train_split).all()
    assert len(matching_train_splits) < 2, \
      f'found duplicate splits {data_conf["train_identifier"]}'
    matching_test_splits = session.execute(find_test_split).all()
    assert len(matching_test_splits) < 2, \
      f'found duplicate splits {data_conf["test_identifier"]}'
    if len(matching_train_splits) == 0 or len(matching_test_splits) == 0:
      return []
    train_split_id = matching_train_splits[0][0].id
    test_split_id = matching_test_splits[0][0].id

    find_model = select(EdgeClusteringGNNBase).where(and_(
      EdgeClusteringGNNBase.activation == model_conf['activation'].__name__,
      EdgeClusteringGNNBase.self_loops == model_conf['add_self_loops'],
      EdgeClusteringGNNBase.batchnorm_gnn == model_conf['batchnorm_gnn'],
      EdgeClusteringGNNBase.batchnorm_mlp == model_conf['batchnorm_mlp']
    ))

    models = session.execute(find_model).all()
    if len(models) == 0:
      return []

    matching_model_ids = []
    for model_row in models:
      model: EdgeClusteringGNNBase = model_row[0]

      gnn_out_dims_split = list(map(int, model.gnn_out_dims.split(','))) \
        if len(model.gnn_out_dims) > 0 else []
      if len(gnn_out_dims_split) > 0 and \
              any([d != gnn_out_dims_split[0] for d in gnn_out_dims_split]):
        continue

      mlp_out_dims_split = list(map(int, model.mlp_out_dims.split(',')))[:-1]
      # yuck
      if len(mlp_out_dims_split) > 1:
        if not (all([d == mlp_out_dims_split[0] for d in mlp_out_dims_split]) or
                all([d2 == d1 / 2 for d1, d2 in
                     zip(mlp_out_dims_split[:-1], mlp_out_dims_split[1:])])):
          continue

      if constrain_param_range:
        if len(gnn_out_dims_split) > 0 and \
                gnn_out_dims_split[0] not in variables['gnn_dim'].distribution:
          continue
        if len(gnn_out_dims_split) not in variables['gnn_layers'].distribution:
          continue
        if mlp_out_dims_split[0] not in variables['mlp_dim_0'].distribution:
          continue
        if len(mlp_out_dims_split) not in variables['mlp_layers'].distribution:
          continue
        if model.dropout_gnn < variables['gnn_dropout'].distribution[0] or \
                model.dropout_gnn > variables['gnn_dropout'].distribution[1]:
          continue
        if model.dropout_mlp < variables['mlp_dropout'].distribution[0] or \
                model.dropout_mlp > variables['mlp_dropout'].distribution[1]:
          continue
        matching_model_ids.append(model.id)
      else:
        matching_model_ids.append(model.id)

    find_optimisers = select(GCAdamOptBase)
    optimisers = session.execute(find_optimisers).all()
    if constrain_param_range:
      lr_pbound = variables['LR'].get_pbounds()
      wd_pbound = variables['lambda_L2'].get_pbounds()
      lr_cvrt = variables['LR'].value_to_sample_point
      wd_cvrt = variables['lambda_L2'].value_to_sample_point
      matching_opt_ids = []
      for opt_row in optimisers:
        optimiser: GCAdamOptBase = opt_row[0]
        # lr_min = variables['LR'].zero_thresh
        # if lr_min
        LR = lr_cvrt(optimiser.LR)
        if LR < lr_pbound[0] or LR > lr_pbound[1]:
          continue
        wd = wd_cvrt(optimiser.weight_decay)
        if wd < wd_pbound[0] or wd > wd_pbound[1]:
          continue

        # if optimiser.LR < variables['LR'].distribution[0] or \
        #   optimiser.LR > variables['LR'].distribution[1]:
        #   continue
        # if optimiser.weight_decay < variables['lambda_L2'].distribution[0] or \
        #   optimiser.weight_decay > variables['lambda_L2'].distribution[1]:
        #   continue
        matching_opt_ids.append(optimiser.id)
    else:
      matching_opt_ids = [opt_row[0].id for opt_row in optimisers]

    local_features = ','.join(map(str, sorted(list(feature_conf['local_features']))))
    node_features = ','.join(map(str, sorted(list(feature_conf['node_features']))))
    edge_features = ','.join(map(str, sorted(list(feature_conf['edge_features']))))
    node_labels = ','.join(map(str, sorted(list(feature_conf['node_labels']))))
    graph_labels = ','.join(map(str, sorted(list(feature_conf['graph_labels']))))
    edge_labels = ','.join(map(str, sorted(list(feature_conf['edge_labels']))))
    eig_conf = json.dumps(feature_conf['eig_conf'], indent=2, sort_keys=False)

    find_exps = select(SupervisedEdgeCommunitiesBase).where(and_(
      SupervisedEdgeCommunitiesBase.model_id.in_(matching_model_ids),
      SupervisedEdgeCommunitiesBase.optimiser_id.in_(matching_opt_ids),
      SupervisedEdgeCommunitiesBase.train_split_id == train_split_id,
      SupervisedEdgeCommunitiesBase.test_split_id == test_split_id,
      SupervisedEdgeCommunitiesBase.val_size == exp_conf['val_size'],
      SupervisedEdgeCommunitiesBase.batch_size == exp_conf['batch_size'],
      SupervisedEdgeCommunitiesBase.lambda_nll == exp_conf['lambda_nll'],
      SupervisedEdgeCommunitiesBase.eig_conf == eig_conf,
      SupervisedEdgeCommunitiesBase.local_features == local_features,
      SupervisedEdgeCommunitiesBase.node_features == node_features,
      SupervisedEdgeCommunitiesBase.edge_features == edge_features,
      SupervisedEdgeCommunitiesBase.node_labels == node_labels,
      SupervisedEdgeCommunitiesBase.graph_labels == graph_labels,
      SupervisedEdgeCommunitiesBase.edge_labels == edge_labels
    ))

    matching_exps = session.execute(find_exps).all()

    exp_targets = []
    exp_data_points = []
    exp_ids = []

    for exp_row in matching_exps:
      exp: SupervisedEdgeCommunitiesBase = exp_row[0]
      find_epochs = select(SECEpochBase).where(
        SECEpochBase.experiment_id == exp.id
      ).order_by(SECEpochBase.epoch_num)
      epoch_rows = session.execute(find_epochs).all()
      if len(epoch_rows) < 1:
        continue
      elapsed = epoch_rows[-1][0].elapsed_time
      if elapsed > eval_time_limit or len(epoch_rows) >= eval_epoch_limit:
        exp_targets.append(np.max([er[0].test_acc for er in epoch_rows]))
      else:
        epochs_since_improved = []
        peak_test_acc = -np.inf
        for epoch_row in epoch_rows:
          epoch: SECEpochBase = epoch_row[0]
          acc = epoch.test_acc
          if acc > peak_test_acc:
            peak_test_acc = acc
            epochs_since_improved.append(0)
          else:
            epochs_since_improved.append(epochs_since_improved[-1] + 1)

        relevant_epochs_since_improved = epochs_since_improved[:eval_epoch_limit]
        if max(relevant_epochs_since_improved) >= early_stop_after_stalled:
          exp_targets.append(np.max([er[0].test_acc for er in epoch_rows[:eval_epoch_limit]]))
        else:
          continue

      model: EdgeClusteringGNNBase = session.execute(select(EdgeClusteringGNNBase).where(
        EdgeClusteringGNNBase.id == exp.model_id)).first()[0]
      adam_opt: GCAdamOptBase = session.execute(select(GCAdamOptBase).where(
        GCAdamOptBase.id == exp.optimiser_id)).first()[0]
      gnn_out_dims = list(map(int, model.gnn_out_dims.split(','))) if \
        len(model.gnn_out_dims) > 0 else []
      n_gnn_layers = len(gnn_out_dims)
      gnn_dim = gnn_out_dims[0] if len(gnn_out_dims) > 0 \
        else np.random.choice(variables['gnn_dim'].distribution)
      mlp_out_dims = list(map(int, model.mlp_out_dims.split(',')))[:-1]
      n_mlp_layers = len(mlp_out_dims)
      mlp_dim_0 = mlp_out_dims[0]
      if n_mlp_layers > 1:
        if mlp_out_dims[0] == mlp_out_dims[1]:
          mlp_reduction = 'same'
        else:
          mlp_reduction = 'halve'
      else:
        seed = abs(hash((model.dropout_mlp, model.dropout_gnn, adam_opt.LR, adam_opt.weight_decay)))
        while seed >= (2 ** 32 - 1):
          seed = int(seed / 2)
        np.random.seed(seed)
        mlp_reduction = np.random.choice(['same', 'halve'])

      LR = adam_opt.LR
      lambda_L2 = adam_opt.weight_decay
      gnn_dropout = model.dropout_gnn
      mlp_dropout = model.dropout_mlp
      data_point = {
        'gnn_layers': n_gnn_layers,
        'mlp_layers': n_mlp_layers,
        'gnn_dim': gnn_dim,
        'mlp_dim_0': mlp_dim_0,
        'mlp_dim_reduction': mlp_reduction,
        'LR': LR,
        'gnn_dropout': gnn_dropout,
        'mlp_dropout': mlp_dropout,
        'lambda_L2': lambda_L2
      }
      if raw:
        exp_data_points.append(data_point)
      else:
        exp_data_points.append({k: variables[k].value_to_sample_point(v)
                                for k, v in data_point.items()})
      exp_ids.append(exp.id)

    ret = list(zip(exp_targets, exp_data_points))
    if return_exp_ids:
      ret = (ret, exp_ids)
    return ret
