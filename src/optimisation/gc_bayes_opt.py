# from bayes_opt import BayesianOptimization
from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from bayes_opt.util import UtilityFunction, acq_max, ensure_rng
import math
from util.util import GCTime, validate_directory, printProgressBar
from datetime import timedelta, datetime
import numpy as np
from sqlalchemy import select, and_
from sql.sql_base import *
import json
import pandas as pd
from exp_scripts.default_setups import *
from abc import abstractmethod
from typing import Any

class BayesOpt(object):
  
  def __init__(self,
               gcdb,
               identifier,
               opt_dir,
               # matching_exps_method,
               # target_function,
               variables,
               random_steps,
               bayes_steps,
               time_limit,
               eval_repeats,
               eval_time_limit,
               eval_epoch_limit,
               early_stop_after_stalled,
               run_conf,
               data_conf
               ):
    super(BayesOpt, self).__init__()
    self.gcdb = gcdb
    self.identifier = identifier
    self.opt_dir = validate_directory(opt_dir)
    # self.matching_exps_method = matching_exps_method
    # self.target_function = target_function
    self.variables = variables
    self.random_steps = random_steps
    self.bayes_steps = bayes_steps
    self.time_limit = time_limit
    self.eval_repeats = eval_repeats
    self.eval_time_limit = eval_time_limit
    self.eval_epoch_limit = eval_epoch_limit
    self.early_stop_after_stalled = early_stop_after_stalled
    self.run_conf = run_conf
    self.data_conf = data_conf

  @abstractmethod
  def target_fn(self,
                eval_repeats,
                eval_time_limit,
                eval_epoch_limit,
                early_stop_after_stalled,
                *args: Any,
                **kwargs: Any
                ):
    ...

  @abstractmethod
  def matching_fn(self,
                  session,
                  data_conf,
                  model_conf,
                  exp_conf,
                  feature_conf,
                  variables,
                  eval_time_limit,
                  eval_epoch_limit,
                  early_stop_after_stalled,
                  constrain_param_range=True,
                  return_exp_ids=False,
                  raw=False
                  ):
    ...

  def save_info(self):
    associated_exps = self.get_top_N(n=0)
    if len(associated_exps) == 0:
      return

    info = {
      'opt_identifier': self.identifier,
      'train_identifier': self.data_conf['train_identifier'],
      'test_identifier': self.data_conf['test_identifier'],
      'opt_class': self.__class__.__name__,
      'eval_repeats': self.eval_repeats,
      'eval_time_limit': self.eval_time_limit.seconds/  60,
      'eval_epoch_limit': self.eval_epoch_limit,
      'early_stop_after_stalled': self.early_stop_after_stalled,
      'variable_distributions': self.variables,
      'n_exps': len(associated_exps),
      'best_score': associated_exps[0][0],
      'mean_top_10': np.mean([ae[0] for ae in associated_exps[:10]])
    }

    jsons_dir = validate_directory(f'{self.opt_dir}/runs')
    save_id = datetime.now().strftime(f'%Y_%M_%d %H:%m:%S ({len(associated_exps)} exps)')
    with open(f'{jsons_dir}/{save_id}.json', 'w') as f:
      f.write(json.dumps(info, indent=4, default=lambda x: str(x) if isinstance(x, BayesOptVariable) else x))

  def get_top_N(self, n=1, display_in_console=False):
    existing_data = self.matching_fn(
      session=self.gcdb.local_sess(),
      data_conf=self.data_conf,
      model_conf=self.run_conf['model_config'],
      exp_conf=self.run_conf['exp_config'],
      feature_conf=self.run_conf['feature_config'],
      eval_time_limit=self.eval_time_limit,
      eval_epoch_limit=self.eval_epoch_limit,
      early_stop_after_stalled=self.early_stop_after_stalled,
      variables=self.variables,
      raw=False)

    used_data = [(t, BayesOptDatapoint({k: self.variables[k].get_value(v) for k, v in dp.items()})) for t, dp in existing_data]
    unique_used_data_points = {}

    for t, dp in used_data:
      if dp in unique_used_data_points.keys():
        unique_used_data_points[dp].append(t)
      else:
        unique_used_data_points[dp] = [t]

    existing_data = [(np.mean(e[1]), {k: self.variables[k].value_to_sample_point(v) for k, v in e[0].items()}) for e in unique_used_data_points.items()]

    ordered = sorted(existing_data, key=lambda e: e[0])[-n:]
    if display_in_console:
      for entry in ordered:
        print('\nParams:')
        display_params = {k: self.variables[k].get_value(v) for k, v in entry[1].items()}
        print(json.dumps(display_params, indent=4))
        print(f'Peak test acc: {entry[0]}')
    return ordered[::-1]


  def make_optimiser(self):
    optimiser = BayesianOptimization(
      f=self.target_fn,
      pbounds={k: v.get_pbounds() for k, v in self.variables.items()},
      random_state=None,
    )
    logger = JSONLogger(path=validate_directory(f'{self.opt_dir}/bayes_opt_logs'))
    optimiser.subscribe(Events.OPTIMIZATION_STEP, logger)
    return optimiser

  def run_optimiser(self):

    existing_data = self.matching_fn(
      session=self.gcdb.local_sess(),
      data_conf=self.data_conf,
      model_conf=self.run_conf['model_config'],
      exp_conf=self.run_conf['exp_config'],
      feature_conf=self.run_conf['feature_config'],
      eval_time_limit=self.eval_time_limit,
      eval_epoch_limit=self.eval_epoch_limit,
      early_stop_after_stalled=self.early_stop_after_stalled,
      variables=self.variables)

    used_data = [(t, BayesOptDatapoint({k: self.variables[k].get_value(v) for k, v in dp.items()})) for t, dp in
                 existing_data]
    unique_used_data_points = {}

    for t, dp in used_data:
      if dp in unique_used_data_points.keys():
        unique_used_data_points[dp].append(t)
      else:
        unique_used_data_points[dp] = [t]

    existing_data = [(np.mean(e[1]), {k: self.variables[k].value_to_sample_point(v) for k, v in e[0].items()}) for e in
                     unique_used_data_points.items()]

    optimiser = self.make_optimiser()

    timer = GCTime()
    timer.start_timer('optimiser')

    # default settings
    acq = 'ucb'
    kappa = 2.576
    kappa_decay = 1
    kappa_decay_delay = 0
    xi = 0.0

    util = UtilityFunction(kind=acq,
                           kappa=kappa,
                           xi=xi,
                           kappa_decay=kappa_decay,
                           kappa_decay_delay=kappa_decay_delay)
    optimiser._prime_subscriptions()
    init_probs_points = [optimiser.suggest(util) for i in range(self.random_steps)]

    for target, data_point in existing_data:
      optimiser.register(params=data_point, target=target)
      # if len(existing_data) == 1:  # ugh
      #   optimiser.register(params={k: v + 1e-8 for k, v in data_point.items()}, target=target+1e-9)

    optimiser.dispatch(Events.OPTIMIZATION_START)

    # self.run_conf.update(values)
    r = len(existing_data)
    print(f'Found {r} existing data points.')
    print(f'Beginning random search.')
    for probe_point in init_probs_points:
      r += 1
      values = {k: self.variables[k].get_value(v) for k, v in probe_point.items()}
      # self.run_conf.update(values)
      print(f'Evaluating run {r} (random sample): '
            f'(elapsed time: {timer.elapsed_time("optimiser", as_timedelta=True)})')
      print(json.dumps(values, indent=4, sort_keys=False))
      f_p = self.target_fn(
        eval_repeats=self.eval_repeats,
        eval_time_limit=self.eval_time_limit,
        eval_epoch_limit=self.eval_epoch_limit,
        early_stop_after_stalled=self.early_stop_after_stalled,
        **self.data_conf,
        **self.run_conf,
        # **self.run_conf
        **values
      )
      print(f'Final peak accuracy: {f_p}')
      print('#' * 60)
      print('\n')
      optimiser.register(params=probe_point, target=f_p)

    i = 0
    print('Beginning Bayesian optimisation.')
    while i < self.bayes_steps and \
      timer.elapsed_time('optimiser', as_timedelta=True) < self.time_limit:

      r += 1
      probe_point = optimiser.suggest(util)
      values = {k: self.variables[k].get_value(v) for k, v in probe_point.items()}
      print(f'Evaluating run {r}: '
            f'(elapsed time: {timer.elapsed_time("optimiser", as_timedelta=True)})')
      print(json.dumps(values, indent=4, sort_keys=False))
      f_p = self.target_fn(
        eval_repeats=self.eval_repeats,
        eval_time_limit=self.eval_time_limit,
        eval_epoch_limit=self.eval_epoch_limit,
        early_stop_after_stalled=self.early_stop_after_stalled,
        # **self.run_conf
        **self.data_conf,
        **self.run_conf,
        **values
      )
      optimiser.register(params=probe_point, target=f_p)
      print(f'Final peak accuracy: {f_p}')
      print('#' * 60)
      print('\n')
      i+=1

    optimiser.dispatch(Events.OPTIMIZATION_END)




      # """Mazimize your function"""
      # optimiser._prime_subscriptions()
      # optimiser.dispatch(Events.OPTIMIZATION_START)
      # optimiser._prime_queue(self.random_steps)

      # optimiser.set_gp_params(**gp_params)
      # iteration = 0
      # while (not optimiser._queue.empty or iteration < self.bayes_steps) and \
      #   timer.elapsed_time('optimiser') < timedelta(minutes=self.time_limit_minutes):
      #   try:
      #     x_probe = next(optimiser._queue)
      #   except StopIteration:
      #     util.update_params()
      #     x_probe = optimiser.suggest(util)
      #     iteration += 1
      #
      #   optimiser.probe(x_probe, lazy=False)
      #
      #   if optimiser._bounds_transformer:
      #     optimiser.set_bounds(
      #       optimiser._bounds_transformer.transform(optimiser._space))
      #
      # optimiser.dispatch(Events.OPTIMIZATION_END)


    # optimiser.maximize(init_points=self.random_steps, n_iter=self.bayes_steps)


  # def read_exps(self):
  #   # o = BayesianOptimization()
  #   pass

class BayesOptDatapoint(dict):

  def __eq__(self, other):

    return self.items() == other.items()

  def __hash__(self):

    return hash((tuple(self.keys()), tuple(self.values())))

class BayesOptVariable(object):

  eps = 1e-6

  def __init__(self, distribution, scale='linear', zero_thresh=0, invert=False):
    super(BayesOptVariable, self).__init__()
    assert scale in ['linear', 'logarithmic']
    self.distribution = distribution
    self.scale = scale
    self.zero_thresh = zero_thresh
    self.invert = invert

  def __str__(self):
    if isinstance(self.distribution, list):
      dist = '[' + ','.join(map(str, self.distribution)) + ']'
    else:
      dist = self.distribution
    return json.dumps({
      'distribution': dist,
      'scale': self.scale,
      'zero_thresh': self.zero_thresh,
      'eps': self.eps,
      'invert': self.invert},
      indent=4
    )

  def get_pbounds(self):
    if isinstance(self.distribution, list) and self.scale == 'linear':
      return 0., 1.-self.eps,
    elif isinstance(self.distribution, list) and self.scale == 'logarithmic':
      raise NotImplementedError()
    elif isinstance(self.distribution, tuple) and self.scale == 'linear':
      return self.distribution
    elif isinstance(self.distribution, tuple) and self.scale == 'logarithmic':
      if 0 in self.distribution or \
        (self.distribution[0] < 0 and self.distribution[1] > 0) or \
        (self.distribution[0] > self.distribution[1]):
        raise ValueError()
      return math.log(self.distribution[0]), math.log(self.distribution[1]),
    else:
      return NotImplementedError()

  def get_value(self, sample_point):
    if isinstance(self.distribution, list) and self.scale == 'linear':
      idx = math.floor(sample_point * len(self.distribution))
      return self.distribution[idx]
    elif isinstance(self.distribution, list) and self.scale == 'logarithmic':
      raise NotImplementedError()
    elif isinstance(self.distribution, tuple) and self.scale == 'linear':
      return sample_point
    elif isinstance(self.distribution, tuple) and self.scale == 'logarithmic':
      value = math.pow(math.e, sample_point)
      if value < self.zero_thresh:
        value = 0.
      return value

  def value_to_sample_point(self, value, centre_discretes=True):
    if isinstance(self.distribution, list) and self.scale == 'linear':
      idx = self.distribution.index(value)
      sample_point = np.random.rand()
      if centre_discretes:
        sample_point *= 1e-6  # stabilising
        sample_point += 0.5

      sample_point /= len(self.distribution)
      sample_point += idx/len(self.distribution)
      return sample_point
    elif isinstance(self.distribution, list) and self.scale == 'logarithmic':
      raise NotImplementedError()
    elif isinstance(self.distribution, tuple) and self.scale == 'linear':
      return value
    elif isinstance(self.distribution, tuple) and self.scale == 'logarithmic':

      if value > 0:
        return math.log(value)
      elif value < 0:
        raise NotImplementedError()
      else:
        sample_point = np.random.rand()

        sample_point *= (self.zero_thresh - self.distribution[0])
        sample_point += self.distribution[0]
        sample_point = math.log(sample_point)

        # sample_point /= (math.log(self.distribution[0]) - math.log(self.zero_thresh))
        # sample_point += (math.log(self.distribution[0]))

        # sample_point *= (math.log(self.zero_thresh - self.distribution[0]))
        # sample_point += math.log(self.distribution[0])
        return sample_point

