from abc import abstractmethod

from sql.sql_base import (
  GCEpochBase,
  DCPEpochBase,
  SCPEpochBase,
  MUEpochBase,
  IATEpochBase,
  SCEpochBase,
  SECEpochBase,
  SGCEpochBase
)


class GCEpoch(object):
  @property
  @abstractmethod
  def db_table(self):
    ...
  
  @property
  def epochs_table(self):
    return 'epochs'
  
  def __init__(self,
               experiment):
    super(GCEpoch, self).__init__()
    self.id = None
    self.experiment = experiment
  
  @abstractmethod
  def write_to_db(self, session):
    base = GCEpochBase(
      experiment_class=self.experiment.experiment_class,
      table_name=self.db_table,
      fresh=True,
    )
    session.add(base)
    session.flush()
    self.id = base.id
    session.commit()


class DCPEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_direct_community_prediction'
  
  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(DCPEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num
    self.train_loss = 0
    self.train_raw = 0
    self.train_accuracy = 0
    self.train_tce = 0
    self.train_cae = 0
    self.train_silhouette = 0
    self.val_loss = 0
    self.val_raw = 0
    self.val_accuracy = 0
    self.val_tce = 0
    self.val_cae = 0
    self.val_silhouette = 0
    self.test_loss = 0
    self.test_raw = 0
    self.test_accuracy = 0
    self.test_tce = 0
    self.test_cae = 0
    self.test_silhouette = 0
    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0
    self.train_example = None
    self.test_example = None
  
  def write_to_db(self, session):
    super().write_to_db(session)
    base = DCPEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,
      train_loss=self.train_loss,
      train_raw=self.train_raw,
      train_accuracy=self.train_accuracy,
      train_tce=self.train_tce,
      train_cae=self.train_cae,
      train_silhouette=self.train_silhouette,
      val_loss=self.val_loss,
      val_raw=self.val_raw,
      val_accuracy=self.val_accuracy,
      val_tce=self.val_tce,
      val_cae=self.val_cae,
      val_silhouette=self.val_silhouette,
      test_loss=self.test_loss,
      test_raw=self.test_raw,
      test_accuracy=self.test_accuracy,
      test_tce=self.test_tce,
      test_cae=self.test_cae,
      test_silhouette=self.test_silhouette,
      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,
      train_example=self.train_example,
      test_example=self.test_example
    )
    session.add(base)
    session.commit()


class SCPEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_silhouette_community_prediction'
  
  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(SCPEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num
    self.train_loss = 0
    self.train_raw = 0
    self.train_tce = 0
    self.train_cae = 0
    self.val_loss = 0
    self.val_raw = 0
    self.val_tce = 0
    self.val_cae = 0
    self.test_loss = 0
    self.test_raw = 0
    self.test_tce = 0
    self.test_cae = 0
    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0
    self.train_example = None
    self.test_example = None
  
  def write_to_db(self, session):
    super().write_to_db(session)
    base = SCPEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,
      train_loss=self.train_loss,
      train_raw=self.train_raw,
      train_tce=self.train_tce,
      train_cae=self.train_cae,
      val_loss=self.val_loss,
      val_raw=self.val_raw,
      val_tce=self.val_tce,
      val_cae=self.val_cae,
      test_loss=self.test_loss,
      test_raw=self.test_raw,
      test_tce=self.test_tce,
      test_cae=self.test_cae,
      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,
      train_example=self.train_example,
      test_example=self.test_example
    )
    session.add(base)
    session.commit()


class MUEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_measured_unsupervised'
  
  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(MUEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num

    self.train_loss = 0
    self.train_silh = 0
    self.train_mod = 0
    self.train_tce = 0
    self.train_cae = 0
    self.train_acc = 0
    self.train_nll = 0
    self.test_loss = 0
    self.test_silh = 0
    self.test_mod = 0
    self.test_tce = 0
    self.test_cae = 0
    self.test_acc = 0
    self.test_nll = 0
    
    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0
    
    self.train_eg_conf = None
    self.train_eg_hmax = None
    self.train_eg_labels = None
    self.test_eg_conf = None
    self.test_eg_hmax = None
    self.test_eg_labels = None
  
  def write_to_db(self, session):
    super().write_to_db(session)
    base = MUEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,
  
      train_loss = self.train_loss,
      train_silh = self.train_silh,
      train_mod = self.train_mod,
      train_tce = self.train_tce,
      train_cae = self.train_cae,
      train_acc = self.train_acc,
      train_nll = self.train_nll,
      test_loss = self.test_loss,
      test_silh = self.test_silh,
      test_mod = self.test_mod,
      test_tce = self.test_tce,
      test_cae = self.test_cae,
      test_acc = self.test_acc,
      test_nll = self.test_nll,
      
      
      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,
  
      train_eg_conf = self.train_eg_conf,
      train_eg_hmax = self.train_eg_hmax,
      train_eg_labels = self.train_eg_labels,
      test_eg_conf = self.test_eg_conf,
      test_eg_hmax = self.test_eg_hmax,
      test_eg_labels =self.test_eg_labels
    )
    session.add(base)
    session.commit()


class SCEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_supervised_communities'
  
  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(SCEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num

    self.train_loss = 0
    self.train_acc = 0
    self.train_edge_acc = 0
    self.train_cxe = 0
    self.train_cmse = 0
    self.train_cmae = 0
    self.train_silh = 0
    self.train_mod = 0
    self.train_tce = 0
    self.train_cae = 0
    self.train_nll = 0

    self.val_loss = 0
    self.val_acc = 0
    self.val_edge_acc = 0
    self.val_cxe = 0
    self.val_cmse = 0
    self.val_cmae = 0
    self.val_silh = 0
    self.val_mod = 0
    self.val_tce = 0
    self.val_cae = 0
    self.val_nll = 0

    self.test_loss = 0
    self.test_acc = 0
    self.test_edge_acc = 0
    self.test_cxe = 0
    self.test_cmse = 0
    self.test_cmae = 0
    self.test_silh = 0
    self.test_mod = 0
    self.test_tce = 0
    self.test_cae = 0
    self.test_nll = 0
    
    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0
    
    self.train_eg_conf = None
    self.train_eg_hmax = None
    self.train_eg_labels = None
    self.test_eg_conf = None
    self.test_eg_hmax = None
    self.test_eg_labels = None
  
  def write_to_db(self, session):
    super().write_to_db(session)
    base = SCEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,
  
      train_loss = self.train_loss,
      train_acc = self.train_acc,
      train_edge_acc = self.train_edge_acc,
      train_cxe = self.train_cxe,
      train_cmse = self.train_cmse,
      train_cmae = self.train_cmae,
      train_silh = self.train_silh,
      train_mod = self.train_mod,
      train_tce = self.train_tce,
      train_cae = self.train_cae,
      train_nll = self.train_nll,
  
      val_loss = self.val_loss,
      val_acc = self.val_acc,
      val_edge_acc = self.val_edge_acc,
      val_cxe = self.val_cxe,
      val_cmse = self.val_cmse,
      val_cmae = self.val_cmae,
      val_silh = self.val_silh,
      val_mod = self.val_mod,
      val_tce = self.val_tce,
      val_cae = self.val_cae,
      val_nll = self.val_nll,

      test_loss = self.test_loss,
      test_acc = self.test_acc,
      test_edge_acc = self.test_edge_acc,
      test_cxe = self.test_cxe,
      test_cmse = self.test_cmse,
      test_cmae = self.test_cmae,
      test_silh = self.test_silh,
      test_mod = self.test_mod,
      test_tce = self.test_tce,
      test_cae = self.test_cae,
      test_nll = self.test_nll,
      
      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,
  
      train_eg_conf = self.train_eg_conf,
      train_eg_hmax = self.train_eg_hmax,
      train_eg_labels = self.train_eg_labels,
      test_eg_conf = self.test_eg_conf,
      test_eg_hmax = self.test_eg_hmax,
      test_eg_labels =self.test_eg_labels
    )
    session.add(base)
    session.commit()
    session.commit()


class SGCEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_supervised_graph_classify'

  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(SGCEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num

    self.train_loss = 0
    self.train_acc = 0
    # self.train_cxe = 0
    # self.train_cmse = 0
    # self.train_cmae = 0
    self.train_silh = 0
    self.train_mod = 0
    self.train_tce = 0
    self.train_cae = 0
    self.train_nll = 0

    self.val_loss = 0
    self.val_acc = 0
    # self.val_cxe = 0
    # self.val_cmse = 0
    # self.val_cmae = 0
    self.val_silh = 0
    self.val_mod = 0
    self.val_tce = 0
    self.val_cae = 0
    self.val_nll = 0

    self.test_loss = 0
    self.test_acc = 0
    # self.test_cxe = 0
    # self.test_cmse = 0
    # self.test_cmae = 0
    self.test_silh = 0
    self.test_mod = 0
    self.test_tce = 0
    self.test_cae = 0
    self.test_nll = 0

    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0

    self.train_eg_conf = None
    self.train_eg_hmax = None
    self.train_eg_labels = None
    self.test_eg_conf = None
    self.test_eg_hmax = None
    self.test_eg_labels = None

  def write_to_db(self, session):
    super().write_to_db(session)
    base = SGCEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,

      train_loss = self.train_loss,
      train_acc = self.train_acc,
      # train_cxe = self.train_cxe,
      # train_cmse = self.train_cmse,
      # train_cmae = self.train_cmae,
      train_silh = self.train_silh,
      train_mod = self.train_mod,
      train_tce = self.train_tce,
      train_cae = self.train_cae,
      train_nll = self.train_nll,

      val_loss = self.val_loss,
      val_acc = self.val_acc,
      # val_cxe = self.val_cxe,
      # val_cmse = self.val_cmse,
      # val_cmae = self.val_cmae,
      val_silh = self.val_silh,
      val_mod = self.val_mod,
      val_tce = self.val_tce,
      val_cae = self.val_cae,
      val_nll = self.val_nll,

      test_loss = self.test_loss,
      test_acc = self.test_acc,
      # test_cxe = self.test_cxe,
      # test_cmse = self.test_cmse,
      # test_cmae = self.test_cmae,
      test_silh = self.test_silh,
      test_mod = self.test_mod,
      test_tce = self.test_tce,
      test_cae = self.test_cae,
      test_nll = self.test_nll,

      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,

      train_eg_conf = self.train_eg_conf,
      train_eg_hmax = self.train_eg_hmax,
      train_eg_labels = self.train_eg_labels,
      test_eg_conf = self.test_eg_conf,
      test_eg_hmax = self.test_eg_hmax,
      test_eg_labels =self.test_eg_labels
    )
    session.add(base)
    session.commit()
    session.commit()


class SECEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_supervised_edge_communities'
  
  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(SECEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num

    self.train_loss = 0
    self.train_edge_acc = 0
    self.train_nll = 0

    self.val_loss = 0
    self.val_edge_acc = 0
    self.val_nll = 0

    self.test_loss = 0
    self.test_edge_acc = 0
    self.test_nll = 0
    
    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0
    
    self.train_eg_conf = None
    self.train_eg_hmax = None
    self.train_eg_labels = None
    self.test_eg_conf = None
    self.test_eg_hmax = None
    self.test_eg_labels = None
  
  def write_to_db(self, session):
    super().write_to_db(session)
    base = SECEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,
  
      train_loss = self.train_loss,
      train_edge_acc = self.train_edge_acc,
      train_nll = self.train_nll,

      val_loss = self.val_loss,
      val_edge_acc = self.val_edge_acc,
      val_nll = self.val_nll,
  
      test_loss = self.test_loss,
      test_edge_acc = self.test_edge_acc,
      test_nll = self.test_nll,
      
      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,
  
      train_eg_conf = self.train_eg_conf,
      train_eg_hmax = self.train_eg_hmax,
      train_eg_labels = self.train_eg_labels,
      test_eg_conf = self.test_eg_conf,
      test_eg_hmax = self.test_eg_hmax,
      test_eg_labels =self.test_eg_labels
    )
    session.add(base)
    session.commit()



class IATEpoch(GCEpoch):
  @property
  def db_table(self):
    return 'epoch_implicit_aux_tgt'
  
  def __init__(self,
               experiment,
               epoch_num,
               ):
    super(IATEpoch, self).__init__(experiment)
    self.epoch_num = epoch_num

    self.train_loss = 0
    self.train_acc = 0
    self.train_nll = 0
    self.train_aux_acc = 0
    self.train_aux_nll = 0
    self.train_silh = 0
    self.train_mod = 0
    self.train_tce = 0
    self.train_cae = 0
    
    self.test_loss = 0
    self.test_acc = 0
    self.test_nll = 0
    self.test_aux_acc = 0
    self.test_aux_nll = 0
    self.test_silh = 0
    self.test_mod = 0
    self.test_tce = 0
    self.test_cae = 0
    
    self.epoch_perf_time = 0
    self.epoch_time = 0
    self.elapsed_perf_time = 0
    self.elapsed_time = 0
    
    self.train_eg_conf = None
    self.train_eg_hmax = None
    self.train_eg_labels = None
    self.test_eg_conf = None
    self.test_eg_hmax = None
    self.test_eg_labels = None
  
  def write_to_db(self, session):
    super().write_to_db(session)
    base = IATEpochBase(
      id=self.id,
      experiment_id=self.experiment.id,
      epoch_num=self.epoch_num,
  
      train_loss=self.train_loss,
      train_acc=self.train_acc,
      train_nll=self.train_nll,
      train_aux_acc=self.train_aux_acc,
      train_aux_nll=self.train_aux_nll,
      train_silh=self.train_silh,
      train_mod=self.train_mod,
      train_tce=self.train_tce,
      train_cae=self.train_cae,
      
      test_loss=self.test_loss,
      test_acc=self.test_acc,
      test_nll=self.test_nll,
      test_aux_acc=self.test_aux_acc,
      test_aux_nll=self.test_aux_nll,
      test_silh=self.test_silh,
      test_mod=self.test_mod,
      test_tce=self.test_tce,
      test_cae=self.test_cae,
      
      epoch_perf_time=self.epoch_perf_time,
      epoch_time=self.epoch_time,
      elapsed_perf_time=self.elapsed_perf_time,
      elapsed_time=self.elapsed_time,
  
      train_eg_conf = self.train_eg_conf,
      train_eg_hmax = self.train_eg_hmax,
      train_eg_labels = self.train_eg_labels,
      test_eg_conf = self.test_eg_conf,
      test_eg_hmax = self.test_eg_hmax,
      test_eg_labels =self.test_eg_labels
    )
    session.add(base)
    session.commit()
