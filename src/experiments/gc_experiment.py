from abc import ABC, abstractmethod
from typing import Any

import torch

from data.gc_data import GCDataset
from models.gc_model import GCModel
from experiments.opt import GCOptimiser
from sql.sql_base import GCExperimentBase
from util.util import GCTime


class GCExperiment(ABC):
  @property
  @abstractmethod
  def db_table(self):
    ...
  
  @property
  @abstractmethod
  def experiment_class(self):
    ...
  
  @property
  def experiments_table(self):
    return 'experiments'
  
  class ExperimentParams(object):
    def __init__(self,
                 model: GCModel,
                 optimiser: GCOptimiser,
                 dataset: GCDataset):
      super().__init__()
      self.model = model
      self.optimiser = optimiser
      self.dataset = dataset
      self.opt_class = optimiser.opt_class
      self.LR = optimiser.opt_params.LR
  
  def __init__(self,
               gcdb,
               experiment_dir,
               params: ExperimentParams,
               visualiser=None,
               device=torch.device('cpu'),
               ):
    super(GCExperiment, self).__init__()
    self.gcdb = gcdb
    self.timer = GCTime()
    self.experiment_dir = experiment_dir
    self.params = params
    self.visualiser = visualiser
    self.device = device
    self.cpu = torch.device('cpu')
    self.id = GCExperiment.add_sql_entry(self)
  
  @abstractmethod
  def start(self, *args: Any):
    ...
  
  @abstractmethod
  def resume(self, *args: Any):
    ...
  
  @abstractmethod
  def add_sql_entry(self):
    base = GCExperimentBase(
      experiment_class=self.experiment_class,
      table_name=self.db_table,
      fresh=True,
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.flush()
    id = base.id
    session.commit()
    return id
