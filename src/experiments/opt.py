from abc import ABC, abstractmethod

import torch

from misc.gc_abc import GCParams
from sql.sql_base import GCOptimiserBase, GCAdamOptBase


class GCOptimiser(object):
  @property
  @abstractmethod
  def db_table(self):
    ...
  
  @property
  def optimisers_table(self):
    return 'optimisers'
  
  @property
  @abstractmethod
  def opt_class(self):
    ...
  
  class OptParams(GCParams, ABC):
    def __init__(self, LR, **kwargs):
      super(GCOptimiser.OptParams, self).__init__(LR=LR, **kwargs)
      self.LR = LR
  
  def __init__(self, opt_params, gcdb):
    super(GCOptimiser, self).__init__()
    self.gcdb = gcdb
    self.opt_params = opt_params
    self.id = self.add_optimisers_sql_entry()
    self.add_sql_entry()
  
  @abstractmethod
  def add_sql_entry(self):
    ...
  
  def add_optimisers_sql_entry(self):
    base = GCOptimiserBase(
      optimiser_class=self.opt_class,
      table_name=self.db_table,
      fresh=True,
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.flush()
    id = base.id
    session.commit()
    return id


class GCAdamOpt(torch.optim.Adam, GCOptimiser):
  @property
  def db_table(self):
    return 'opt_adam'
  
  @property
  def opt_class(self):
    return 'Adam'
  
  class AdamOptParams(GCOptimiser.OptParams):
    def __init__(self,
                 LR,
                 beta_1=0.9,
                 beta_2=0.999,
                 eps=1e-8,
                 weight_decay=0,
                 amsgrad=False):
      super(GCAdamOpt.AdamOptParams, self).__init__(LR=LR, beta_1=beta_1, beta_2=beta_2, eps=eps,
                                                    weight_decay=weight_decay, amsgrad=amsgrad)
  
  def __init__(self, params, gcdb, opt_params: AdamOptParams):
    torch.optim.Adam.__init__(self=self,
                              params=params,
                              lr=opt_params.LR,
                              betas=(opt_params.beta_1, opt_params.beta_2),
                              eps=opt_params.eps,
                              weight_decay=opt_params.weight_decay,
                              amsgrad=opt_params.amsgrad)
    GCOptimiser.__init__(self=self, opt_params=opt_params, gcdb=gcdb)
  
  def add_sql_entry(self):
    base = GCAdamOptBase(
      id=self.id,
      optimiser_class=self.opt_class,
      LR=self.opt_params.LR,
      beta_1=self.opt_params.beta_1,
      beta_2=self.opt_params.beta_2,
      eps=self.opt_params.eps,
      weight_decay=self.opt_params.weight_decay,
      amsgrad=self.opt_params.amsgrad
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.commit()
