import itertools
from datetime import timedelta

import dgl
import numpy as np
import torch
import torch_geometric
from prettytable import PrettyTable
from torch.utils.data import DataLoader, random_split
from torch_geometric.utils import to_dense_adj

from data.gc_data import GCSample
from experiments.epochs import IATEpoch
from experiments.gc_experiment import GCExperiment
from sql.sql_base import ImplicitAuxTgtBase
from experiments.losses import silhouette_loss, cluster_assignment_entropy, total_cluster_entropy, \
  modularity_loss, direct_community_loss, aux_tgt_loss


class ImplicitAuxTgtExp(GCExperiment):
  @property
  def db_table(self):
    return 'exp_implicit_aux_tgt'
  
  @property
  def experiment_class(self):
    return 'ImplicitAuxTgt'
  
  class IATParams(GCExperiment.ExperimentParams):
    def __init__(self,
                 model,
                 optimiser,
                 dataset,
                 val_size,
                 batch_size,
                 n_clusters,
                 lambda_aux_nll,
                 lambda_nll,
                 lambda_silh,
                 lambda_mod,
                 lambda_tce,
                 lambda_cae,
                 n_spectral_vectors,
                 derived_node_features,
                 label,
                 ):
      super(ImplicitAuxTgtExp.IATParams, self).__init__(model=model,
                                                        optimiser=optimiser,
                                                        dataset=dataset)
      self.val_size = val_size
      self.batch_size = batch_size
      self.n_clusters = n_clusters
      self.lambda_aux_nll = lambda_aux_nll
      self.lambda_nll = lambda_nll
      self.lambda_silh = lambda_silh
      self.lambda_mod = lambda_mod
      self.lambda_tce = lambda_tce
      self.lambda_cae = lambda_cae
      self.n_spectral_vectors = n_spectral_vectors
      self.derived_node_features = derived_node_features
      self.label = label
  
  def __init__(self,
               gcdb,
               experiment_dir,
               params: IATParams,
               visualiser=None,
               device=torch.device('cpu'),
               ):
    super(ImplicitAuxTgtExp, self).__init__(experiment_dir=experiment_dir,
                                            params=params,
                                            visualiser=visualiser,
                                            gcdb=gcdb,
                                            device=device,
                                            )
    self.add_sql_entry()
  
  def add_sql_entry(self):
    base = ImplicitAuxTgtBase(
      id=self.id,
      model_id=self.params.model.id,
      optimiser_id=self.params.optimiser.id,
      train_split_id=self.params.dataset.splits['train'].id,
      test_split_id=self.params.dataset.splits['test'].id,
      val_size=self.params.val_size,
      batch_size=self.params.batch_size,
      n_clusters=self.params.n_clusters,
      
      lambda_aux_nll=self.params.lambda_aux_nll,
      lambda_nll=self.params.lambda_nll,
      lambda_silh=self.params.lambda_silh,
      lambda_mod=self.params.lambda_mod,
      lambda_tce=self.params.lambda_tce,
      lambda_cae=self.params.lambda_cae,
      
      task_label=self.params.label[0],
      aux_label=self.params.label[1],
      
      n_spectral_vectors=self.params.n_spectral_vectors,
      degree_centrality=self.params.derived_node_features.get('degree_centrality', False),
      closeness_centrality=self.params.derived_node_features.get('closeness_centrality', False),
      betweenness_centrality=self.params.derived_node_features.get('betweenness_centrality', False),
      current_flow_closeness_centrality=self.params.derived_node_features.get('current_flow_closeness_centrality', False),
      current_flow_betweenness_centrality=self.params.derived_node_features.get('current_flow_betweenness_centrality', False),
      approximate_current_flow_betweenness_centrality=self.params.derived_node_features.get('approximate_current_flow_betweenness_centrality', False),
      load_centrality=self.params.derived_node_features.get('load_centrality', False),
      average_neighbor_degree=self.params.derived_node_features.get('average_neighbor_degree', False),
      triangles=self.params.derived_node_features.get('triangles', False),
      clustering_coeff_tri=self.params.derived_node_features.get('clustering_coeff_tri', False),
      clustering_coeff_sq=self.params.derived_node_features.get('clustering_coeff_sq', False),
      eccentricity=self.params.derived_node_features.get('eccentricity', False)
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.commit()
  
  def start(self,
            epochs,
            chkp_freq=1,
            vis_freq=float('inf'),
            vis_n_samples=10,
            val_freq=1,
            test_freq=1,
            train_acc_freq=1,
            test_acc_freq=1,
            ):
    train_batches, test_batches = self.prepare_data()
    self.train_model(train_batches=train_batches,
                     test_batches=test_batches,
                     epochs=epochs,
                     chkp_freq=chkp_freq,
                     test_freq=test_freq,
                     train_acc_freq=train_acc_freq,
                     test_acc_freq=test_acc_freq,
                     vis_freq=vis_freq,
                     vis_n_samples=vis_n_samples,
                     )
  
  def resume(self, checkpoint, chkp_freq, epochs, **kwargs):
    raise NotImplementedError()
  
  def prepare_data(self):
    train_batches = DataLoader(dataset=self.params.dataset.splits['train'],
                               collate_fn=GCSample.collate_fn(
                                 include_labels=True,
                                 include_distance_matrices=True),
                               batch_size=self.params.batch_size,
                               shuffle=True,
                               prefetch_factor=2)
    test_batches = DataLoader(dataset=self.params.dataset.splits['test'],
                              batch_size=self.params.batch_size,
                              collate_fn=GCSample.collate_fn(
                                include_labels=True,
                                include_distance_matrices=True),
                              shuffle=True,
                              prefetch_factor=2)
    return train_batches, test_batches,
  
  def visualise_batch(self,
                      batch_edges,
                      batch_labels,
                      batch_confidences=None,
                      batch_node_boundaries=None,
                      batch_edge_boundaries=None,
                      samples_to_visualise=None,
                      filename='temp.jpg',
                      np_seed=None):
    batch_edges = batch_edges.detach().to(self.cpu)
    batch_labels = batch_labels.detach().to(self.cpu)
    batch_confidences = batch_confidences.detach().to(
      self.cpu) if batch_confidences is not None else None
    
    node_starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                             torch.cumsum(batch_node_boundaries, dim=0)[:-1]), dim=0)
    edge_starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                             torch.cumsum(batch_edge_boundaries, dim=0)[:-1]), dim=0)
    edges = torch.zeros(size=(2, 0))
    labels = torch.zeros(size=(0,))
    
    confidences = None if batch_confidences is None else torch.zeros(size=(0,))
    for s_idx in samples_to_visualise:
      s_edges = batch_edges[:, edge_starts[s_idx]:
                               edge_starts[s_idx] + batch_edge_boundaries[s_idx]]
      s_labels = batch_labels[node_starts[s_idx]:
                              node_starts[s_idx] + batch_node_boundaries[s_idx]]
      if batch_confidences is not None:
        s_confidences = batch_confidences[node_starts[s_idx]:
                                          node_starts[s_idx] + batch_node_boundaries[s_idx]]
        confidences = torch.cat((confidences, s_confidences), dim=0)
      
      edges = torch.cat((edges, s_edges), dim=1)
      labels = torch.cat((labels, s_labels), dim=0)
    
    if batch_confidences is not None:
      confidences = confidences.detach().numpy()
    communities = labels.numpy().astype(np.int32)
    self.visualiser.edge_visualise(A_edge=edges.numpy().astype(np.int32),
                                   labels=communities,
                                   confidences=confidences,
                                   filename=filename,
                                   n_colours=max(communities) + 1,
                                   np_seed=np_seed)
  
  
  def train_model(self,
                  train_batches,
                  test_batches,
                  epochs,
                  chkp_freq=1,
                  test_freq=1,
                  train_acc_freq=1,
                  test_acc_freq=1,
                  vis_freq=float('inf'),
                  vis_n_samples=10,
                  ):
    self.timer.start_timer('start')
    t = PrettyTable()
    t.field_names = [
      'Epoch',
      'Train loss', 'Train acc', 'Train NLL', 'Train aux acc', 'Train aux nll',
      'Train silh', 'Train mod', 'Train TCE', 'Train CAE',
      'Test loss', 'Test acc', 'Test NLL', 'Test aux acc', 'Test aux NLL',
      'Test silh', 'Test mod', 'Test TCE', 'Test CAE',
      'Elapsed']
    table_head_printed = False
    
    for e in range(epochs):
      vis_train, vis_test = e % vis_freq == 0, e % vis_freq == 0
      self.timer.start_timer('epoch')
      self.params.optimiser.zero_grad()
      epoch = IATEpoch(experiment=self, epoch_num=e)
      
      correct, aux_correct, total = torch.tensor(0), torch.tensor(0), torch.tensor(0)
      for samples, labels, distance_matrices in train_batches:
        samples = samples.to(self.device)
        objective = labels[0].to(self.device)
        aux_tgts = labels[1].to(self.device)
        X = samples.ndata['X']
        A_edge = samples.edges()
        A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
        A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
        if len(X.size()) < 2:
          X = torch.unsqueeze(X, dim=1).float()
        preds, S = self.params.model(X, A_edge, A_attr)
        
        sample_sizes = torch.tensor(
          [s.number_of_nodes() for s in dgl.unbatch(samples)]
        ).to(self.device)
        distance_matrices = [dm.to(self.device) for dm in distance_matrices]

        if self.params.lambda_aux_nll == 0 and e % train_acc_freq != 0:
          aux_nll, aux_n_correct = torch.tensor(0), torch.tensor(0)
        else:
          aux_nll, aux_n_correct = direct_community_loss(preds=S,
                                                         labels=aux_tgts,
                                                         sample_sizes=sample_sizes,
                                                         n_clusters=self.params.n_clusters,
                                                         softmax_preds=True)
        
        nll, n_correct = aux_tgt_loss(preds=preds, S=S, labels=objective, softmax_S=True)
        S = torch.softmax(S, dim=1)
        silh = silhouette_loss(preds=S,
                               sample_sizes=sample_sizes,
                               distance_matrices=distance_matrices,
                               device=self.device)
        mod = modularity_loss(preds=S,
                              edge_matrix=to_dense_adj(
                                edge_index=A_edge, edge_attr=A_attr).squeeze(),
                              device=self.device)
        tce = total_cluster_entropy(S)
        cae = total_cluster_entropy(S)
        
        loss = torch.tensor(0).float().to(self.device)
        loss += (nll * self.params.lambda_nll)
        loss += (aux_nll * self.params.lambda_aux_nll)
        loss += (silh * self.params.lambda_silh)
        loss += (mod * self.params.lambda_mod)
        tce += (tce * self.params.lambda_tce)
        cae += (cae * self.params.lambda_cae)
        
        epoch.train_loss += loss.item()
        epoch.train_nll += nll.item()
        epoch.train_aux_nll += aux_nll.item()
        epoch.train_silh += silh.item()
        epoch.train_mod += mod.item()
        epoch.train_tce += tce.item()
        epoch.train_cae += cae.item()
        
        loss.backward()
        self.params.optimiser.step()
        self.params.optimiser.zero_grad()
        
        correct += n_correct.item()
        aux_correct += aux_n_correct.item()
        total += X.size()[0]
        
        if vis_train:
          A_edge = A_edge.cpu()
          sample_sizes = sample_sizes.cpu()
          n_sample_edges = torch.tensor([s.number_of_edges() for s in dgl.unbatch(samples)]).cpu()
          batch_confidences, batch_preds = torch.max(S, dim=1)
          np_seed = np.random.randint(99999999)
          self.visualise_batch(batch_edges=A_edge,
                               batch_labels=batch_preds,
                               batch_confidences=batch_confidences,
                               batch_node_boundaries=sample_sizes,
                               batch_edge_boundaries=n_sample_edges,
                               samples_to_visualise=np.arange(vis_n_samples),
                               filename='temp_conf.jpg',
                               np_seed=np_seed)
          self.visualise_batch(batch_edges=A_edge,
                               batch_labels=batch_preds,
                               batch_node_boundaries=sample_sizes,
                               batch_edge_boundaries=n_sample_edges,
                               samples_to_visualise=np.arange(vis_n_samples),
                               filename='temp_hmax.jpg',
                               np_seed=np_seed)
          self.visualise_batch(batch_edges=A_edge,
                               batch_labels=aux_tgts,
                               batch_node_boundaries=sample_sizes,
                               batch_edge_boundaries=n_sample_edges,
                               samples_to_visualise=np.arange(vis_n_samples),
                               filename='temp_labels.jpg',
                               np_seed=np_seed)
          with open(f'{self.visualiser.vis_dir}/temp_conf.jpg', 'rb') as f:
            epoch.train_eg_conf = f.read()
          with open(f'{self.visualiser.vis_dir}/temp_hmax.jpg', 'rb') as f:
            epoch.train_eg_hmax = f.read()
          with open(f'{self.visualiser.vis_dir}/temp_labels.jpg', 'rb') as f:
            epoch.train_eg_labels = f.read()
          vis_train = False
      
      epoch.train_acc = (correct / total).item()
      epoch.train_aux_acc = (aux_correct / total).item()
      
      row = [e + 1,
             round(epoch.train_loss, 3),
             round(epoch.train_acc, 4),
             round(epoch.train_nll, 3),
             round(epoch.train_aux_acc, 4),
             round(epoch.train_aux_nll, 3),
             round(epoch.train_silh, 3),
             round(epoch.train_mod, 3),
             round(epoch.train_tce, 3),
             round(epoch.train_cae, 3),
             ]
      
      if e % test_freq == 0:
        self.params.model.eval()
        eval_device = torch.device('cpu')
        correct, aux_correct, total = torch.tensor(0), torch.tensor(0), torch.tensor(0)
        for samples, labels, distance_matrices in test_batches:
          samples = samples.to(self.device)
          objective = labels[0].to(eval_device)
          aux_tgts = labels[1].to(eval_device)
          X = samples.ndata['X']
          A_edge = samples.edges()
          A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
          A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
          if len(X.size()) < 2:
            X = torch.unsqueeze(X, dim=1).float()
          preds, S = self.params.model(X, A_edge, A_attr)
    
          sample_sizes = torch.tensor(
            [s.number_of_nodes() for s in dgl.unbatch(samples)]
          ).to(eval_device)
          distance_matrices = [dm.to(eval_device) for dm in distance_matrices]
          
          preds = preds.to(eval_device)
          S = S.to(eval_device)
          if self.params.lambda_aux_nll == 0 and e % train_acc_freq != 0:
            aux_nll, aux_n_correct = torch.tensor(0), torch.tensor(0)
          else:
            aux_nll, aux_n_correct = direct_community_loss(preds=S,
                                                           labels=aux_tgts,
                                                           sample_sizes=sample_sizes,
                                                           n_clusters=self.params.n_clusters,
                                                           softmax_preds=True)

          nll, n_correct = aux_tgt_loss(preds=preds, S=S, labels=objective, softmax_S=True)
          S = torch.softmax(S, dim=1)

          silh = silhouette_loss(preds=S,
                                 sample_sizes=sample_sizes,
                                 distance_matrices=distance_matrices,
                                 device=eval_device)
          mod = modularity_loss(preds=S,
                                edge_matrix=to_dense_adj(
                                  edge_index=A_edge, edge_attr=A_attr).squeeze().to(eval_device),
                                device=eval_device)
          tce = total_cluster_entropy(S)
          cae = total_cluster_entropy(S)
    
          loss = torch.tensor(0).float().to(eval_device)
          loss += (nll * self.params.lambda_nll)
          loss += (aux_nll * self.params.lambda_aux_nll)
          loss += (silh * self.params.lambda_silh)
          loss += (mod * self.params.lambda_mod)
          tce += (tce * self.params.lambda_tce)
          cae += (cae * self.params.lambda_cae)
    
          epoch.test_loss += loss.item()
          epoch.test_nll += nll.item()
          epoch.test_aux_nll += aux_nll.item()
          epoch.test_silh += silh.item()
          epoch.test_mod += mod.item()
          epoch.test_tce += tce.item()
          epoch.test_cae += cae.item()

          correct += n_correct.item()
          aux_correct += aux_n_correct.item()
          total += X.size()[0]
    
    
          if vis_test:
            A_edge = A_edge.cpu()
            sample_sizes = sample_sizes.cpu()
            n_sample_edges = torch.tensor([s.number_of_edges() for s in dgl.unbatch(samples)]).cpu()
            batch_confidences, batch_preds = torch.max(S, dim=1)
            np_seed = np.random.randint(99999999)
            self.visualise_batch(batch_edges=A_edge,
                                 batch_labels=batch_preds,
                                 batch_confidences=batch_confidences,
                                 batch_node_boundaries=sample_sizes,
                                 batch_edge_boundaries=n_sample_edges,
                                 samples_to_visualise=np.arange(vis_n_samples),
                                 filename='temp_conf.jpg',
                                 np_seed=np_seed)
            self.visualise_batch(batch_edges=A_edge,
                                 batch_labels=batch_preds,
                                 batch_node_boundaries=sample_sizes,
                                 batch_edge_boundaries=n_sample_edges,
                                 samples_to_visualise=np.arange(vis_n_samples),
                                 filename='temp_hmax.jpg',
                                 np_seed=np_seed)
            self.visualise_batch(batch_edges=A_edge,
                                 batch_labels=aux_tgts,
                                 batch_node_boundaries=sample_sizes,
                                 batch_edge_boundaries=n_sample_edges,
                                 samples_to_visualise=np.arange(vis_n_samples),
                                 filename='temp_labels.jpg',
                                 np_seed=np_seed)
            with open(f'{self.visualiser.vis_dir}/temp_conf.jpg', 'rb') as f:
              epoch.test_eg_conf = f.read()
            with open(f'{self.visualiser.vis_dir}/temp_hmax.jpg', 'rb') as f:
              epoch.test_eg_hmax = f.read()
            with open(f'{self.visualiser.vis_dir}/temp_labels.jpg', 'rb') as f:
              epoch.test_eg_labels = f.read()
            vis_test = False

        epoch.test_acc = (correct / total).item()
        epoch.test_aux_acc = (aux_correct / total).item()
        
        row += [round(epoch.test_loss, 3),
                round(epoch.test_acc, 4),
                round(epoch.test_nll, 3),
                round(epoch.test_aux_acc, 4),
                round(epoch.test_aux_nll, 3),
                round(epoch.test_silh, 3),
                round(epoch.test_mod, 3),
                round(epoch.test_tce, 3),
                round(epoch.test_cae, 3),
                ]
      else:
        row += ['---']*9
      self.params.model.train()
      
      epoch.epoch_perf_time = timedelta(seconds=self.timer.elapsed_perf_time('epoch'))
      epoch.epoch_time = timedelta(seconds=self.timer.elapsed_time('epoch'))
      epoch.elapsed_perf_time = timedelta(seconds=self.timer.elapsed_perf_time('start'))
      epoch.elapsed_time = timedelta(seconds=self.timer.elapsed_time('start'))
      row += [str(epoch.elapsed_time)]
      t.add_row(row)
      if not table_head_printed:
        print('\n'.join(t.get_string().splitlines()[:3]))
        table_head_printed = True
      print('\n'.join(t.get_string().splitlines()[-2:]))
      session = self.gcdb.local_sess()
      epoch.write_to_db(session=session)
