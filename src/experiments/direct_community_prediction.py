import itertools
from datetime import timedelta

import numpy as np
import torch
import dgl
import torch_geometric
from prettytable import PrettyTable
from torch.utils.data import DataLoader, random_split
from sqlalchemy_imageattach.context import store_context

from data.gc_data import GCSample
from experiments.epochs import DCPEpoch
from experiments.gc_experiment import GCExperiment
from sql.sql_base import DirectCommunityPredictionBase
from experiments.losses import direct_community_loss, total_cluster_entropy, cluster_assignment_entropy, silhouette_loss


class DirectCommunityPrediction(GCExperiment):
  @property
  def db_table(self):
    return 'exp_direct_community_prediction'
  
  @property
  def experiment_class(self):
    return 'DirectCommunityPrediction'
  
  class DCPParams(GCExperiment.ExperimentParams):
    def __init__(self,
                 model,
                 optimiser,
                 dataset,
                 val_size,
                 batch_size,
                 n_clusters,
                 lambda_tce,
                 lambda_cae,
                 n_spectral_vectors):
      super(DirectCommunityPrediction.DCPParams, self).__init__(model=model,
                                                                optimiser=optimiser,
                                                                dataset=dataset)
      self.val_size = val_size
      self.batch_size = batch_size
      self.n_clusters = n_clusters
      self.lambda_tce = lambda_tce
      self.lambda_cae = lambda_cae
      self.n_spectral_vectors = n_spectral_vectors
  
  def __init__(self,
               gcdb,
               experiment_dir,
               params: DCPParams,
               visualiser=None,
               device=torch.device('cpu'),
               ):
    super(DirectCommunityPrediction, self).__init__(experiment_dir=experiment_dir,
                                                    params=params,
                                                    visualiser=visualiser,
                                                    gcdb=gcdb,
                                                    device=device,
                                                    )
  
  def add_sql_entry(self):
    base = DirectCommunityPredictionBase(
      id=self.id,
      model_id=self.params.model.id,
      optimiser_id=self.params.optimiser.id,
      train_split_id=self.params.dataset.splits['train'].id,
      test_split_id=self.params.dataset.splits['test'].id,
      val_size=self.params.val_size,
      batch_size=self.params.batch_size,
      n_clusters=self.params.n_clusters,
      lambda_tce=self.params.lambda_tce,
      lambda_cae=self.params.lambda_cae,
      n_spectral_vectors=self.params.n_spectral_vectors
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.commit()
  
  def start(self,
            epochs,
            chkp_freq=1,
            vis_freq=float('inf'),
            vis_n_samples=10,
            val_freq=1,
            test_freq=1,
            ):
    train_batches, val_batches, test_batches = self.prepare_data()
    self.train_model(train_batches=train_batches,
                     val_batches=val_batches,
                     test_batches=test_batches,
                     epochs=epochs,
                     chkp_freq=chkp_freq,
                     val_freq=val_freq,
                     test_freq=test_freq,
                     vis_freq=vis_freq,
                     vis_n_samples=vis_n_samples,
                     )
  
  def resume(self, checkpoint, chkp_freq, epochs, **kwargs):
    raise NotImplementedError()
  
  def prepare_data(self):
    val_size = int(len(self.params.dataset.splits['train']) * self.params.val_size)
    train_size = len(self.params.dataset.splits['train']) - val_size
    train_subset, val_subset = random_split(dataset=self.params.dataset.splits['train'],
                                            lengths=[train_size, val_size])
    
    train_batches = DataLoader(dataset=train_subset,
                               collate_fn=GCSample.collate_fn(
                                 include_labels=True,
                                 include_distance_matrices=True),
                               batch_size=self.params.batch_size,
                               shuffle=True,
                               prefetch_factor=2)
    val_batches = DataLoader(dataset=val_subset,
                             batch_size=self.params.batch_size,
                             collate_fn=GCSample.collate_fn(
                               include_labels=True,
                               include_distance_matrices=True),
                             shuffle=True,
                             prefetch_factor=2)
    test_batches = DataLoader(dataset=self.params.dataset.splits['test'],
                              batch_size=self.params.batch_size,
                              collate_fn=GCSample.collate_fn(
                                include_labels=True,
                                include_distance_matrices=True),
                              shuffle=True,
                              prefetch_factor=2)
    return train_batches, val_batches, test_batches,
  
  def visualise_batch(self,
                      batch_edges,
                      batch_labels,
                      batch_confidences=None,
                      n_sample_nodes=None,
                      n_sample_edges=None,
                      samples_to_visualise=None,
                      filename='temp.jpg'):
    node_starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                             torch.cumsum(n_sample_nodes, dim=0)[:-1]), dim=0)
    edge_starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                             torch.cumsum(n_sample_edges, dim=0)[:-1]), dim=0)
    edges = torch.zeros(size=(2, 0))
    labels = torch.zeros(size=(0,))
    if batch_confidences is not None:
      confidences = torch.zeros(size=(0,))
    for s_idx in samples_to_visualise:
      s_edges = batch_edges[:, edge_starts[s_idx]:
                               edge_starts[s_idx] + n_sample_edges[s_idx]]
      s_labels = batch_labels[node_starts[s_idx]:
                              node_starts[s_idx] + n_sample_nodes[s_idx]]
      if batch_confidences is not None:
        s_confidences = batch_confidences[node_starts[s_idx]:
                                          node_starts[s_idx] + n_sample_nodes[s_idx]]
        confidences = torch.cat((confidences, s_confidences), dim=0)
      
      edges = torch.cat((edges, s_edges), dim=1)
      labels = torch.cat((labels, s_labels), dim=0)
    
    communities = labels.numpy().astype(np.int32)
    if batch_confidences is not None:
      confidences = confidences.detach().numpy()
    else:
      confidences = None
    self.visualiser.edge_visualise(A_edge=edges.numpy().astype(np.int32),
                                   labels=communities,
                                   confidences=confidences,
                                   filename=filename,
                                   n_colours=max(communities) + 1)
  
  def train_model(self,
                  train_batches,
                  val_batches,
                  test_batches,
                  epochs,
                  chkp_freq=1,
                  val_freq=1,
                  test_freq=1,
                  vis_freq=float('inf'),
                  vis_n_samples=10,
                  ):
    self.timer.start_timer('start')
    t = PrettyTable()
    t.field_names = ['Epoch',
                     'Train accuracy', 'Train loss', 'Train raw', 'Train tce', 'Train cae', 'Train silh',
                     'Val accuracy', 'Val loss',
                     'Test accuracy', 'Test loss', 'Test silh',
                     'Elapsed', 'Perf']
    table_head_printed = False
    eval_device = torch.device('cpu')
    for e in range(epochs):
      vis_train, vis_test = e % vis_freq == 0, e % vis_freq == 0
      self.timer.start_timer('epoch')
      self.params.optimiser.zero_grad()
      epoch = DCPEpoch(experiment=self, epoch_num=e)
      correct, total = torch.tensor(0), torch.tensor(0)
      for samples, labels, distance_matrices in train_batches:
        samples, labels = samples.to(self.device), labels.to(self.device)
        # distance_matrices = [dm.to(self.device) for dm in distance_matrices]
        X = samples.ndata['X']
        A_edge = samples.edges()
        A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
        A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
        if len(X.size()) < 2:
          X = torch.unsqueeze(X, dim=1).float()
        preds, penalties = self.params.model(X, A_edge, A_attr)
        sample_sizes = torch.tensor([s.number_of_nodes() for s in dgl.unbatch(samples)])
        loss = torch.tensor(0).float().to(self.device)
        
        raw_loss, batch_correct = direct_community_loss(preds, labels,
                                                        sample_sizes=sample_sizes,
                                                        n_clusters=self.params.n_clusters,
                                                        )
                                                        # device=self.device)
        epoch.train_raw += raw_loss.item()
        loss += raw_loss
        tce = total_cluster_entropy(preds)
        epoch.train_tce += tce.item()
        loss += (tce * self.params.lambda_tce)
        cae = cluster_assignment_entropy(preds)
        epoch.train_cae += cae.item()
        loss += (cae * self.params.lambda_cae)

        epoch.train_loss += float(loss.item())
        loss.backward()
        self.params.optimiser.step()
        self.params.optimiser.zero_grad()
        correct += batch_correct
        total += preds.size()[0]
        
        epoch.train_silhouette = silhouette_loss(preds=preds.cpu(),
                                                 sample_sizes=sample_sizes.cpu(),
                                                 distance_matrices=distance_matrices,
                                                 device=eval_device).item()
        
        if vis_train:
          n_sample_edges = torch.tensor([s.number_of_edges() for s in dgl.unbatch(samples)])
          batch_confidences, batch_preds = torch.max(preds, dim=1)
          self.visualise_batch(batch_edges=A_edge.cpu(),
                               batch_labels=batch_preds.cpu(),
                               batch_confidences=batch_confidences.cpu(),
                               n_sample_nodes=sample_sizes.cpu(),
                               n_sample_edges=n_sample_edges.cpu(),
                               samples_to_visualise=np.arange(vis_n_samples),
                               filename='temp_preds.jpg')
          with open(f'{self.visualiser.vis_dir}/temp_preds.jpg', 'rb') as f:
            epoch.train_example = f.read()
          vis_train = False
        
      epoch.train_accuracy = float(correct.item() / total.item())
      
      row = [e + 1,
             round(epoch.train_accuracy, 4),
             round(epoch.train_loss, 4),
             round(epoch.train_raw, 4),
             round(epoch.train_tce, 4),
             round(epoch.train_cae, 4),
             round(epoch.train_silhouette, 4)
             ]
      
      if e % val_freq == 0:
        correct, total = torch.tensor(0), torch.tensor(0)
        for samples, labels, distance_matrices in val_batches:
          samples = samples.to(self.device)
          # distance_matrices = [dm.to(self.device) for dm in distance_matrices]
          X = samples.ndata['X']
          A_edge = samples.edges()
          A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
          A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
          if len(X.size()) < 2:
            X = torch.unsqueeze(X, dim=1).float()
          preds, penalties = self.params.model(X, A_edge, A_attr)
          
          preds = preds.cpu()
          labels = labels.cpu()
          sample_sizes = torch.tensor([s.number_of_nodes() for s in dgl.unbatch(samples)]).cpu()
          loss = torch.tensor(0).float().cpu()
          
          raw_loss, batch_correct = direct_community_loss(preds, labels,
                                                          sample_sizes=sample_sizes,
                                                          n_clusters=self.params.n_clusters)
          epoch.val_raw += raw_loss.item()
          loss += raw_loss
          tce = total_cluster_entropy(preds)
          epoch.val_tce += tce.item()
          loss += (tce * self.params.lambda_tce)
          cae = cluster_assignment_entropy(preds)
          epoch.val_cae += cae.item()
          loss += (cae * self.params.lambda_cae)

          epoch.val_loss += float(loss.item())
          correct += batch_correct
          total += preds.size()[0]
          
          epoch.val_silhouette = silhouette_loss(preds=preds.cpu(),
                                                 sample_sizes=sample_sizes.cpu(),
                                                 distance_matrices=distance_matrices)
          
        epoch.val_accuracy = float(correct.item() / total.item())
        row += [round(epoch.val_accuracy, 4), round(epoch.val_loss, 4)]
      else:
        row += ['---', '---']
      
      if e % test_freq == 0:
        correct, total = torch.tensor(0), torch.tensor(0)
        for samples, labels, distance_matrices in test_batches:
          samples = samples.to(self.device)
          # distance_matrices = [dm.to(self.device) for dm in distance_matrices]
          self.timer.start_timer('test_model')
          X = samples.ndata['X']
          A_edge = samples.edges()
          A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
          A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
          if len(X.size()) < 2:
            X = torch.unsqueeze(X, dim=1).float()
          preds, penalties = self.params.model(X, A_edge, A_attr)
          
          preds = preds.cpu()
          sample_sizes = torch.tensor([s.number_of_nodes() for s in dgl.unbatch(samples)]).cpu()
          loss = torch.tensor(0).float().cpu()

          raw_loss, batch_correct = direct_community_loss(preds, labels,
                                                          sample_sizes=sample_sizes,
                                                          n_clusters=self.params.n_clusters,
                                                          )
          epoch.test_raw += raw_loss.item()
          loss += raw_loss
          tce = total_cluster_entropy(preds)
          epoch.test_tce += tce.item()
          loss += (tce * self.params.lambda_tce)
          cae = cluster_assignment_entropy(preds)
          epoch.test_cae += cae.item()
          loss += (cae * self.params.lambda_cae)

          epoch.test_loss += float(loss.item())
          correct += batch_correct
          total += preds.size()[0]
          
          epoch.test_silhouette = silhouette_loss(preds=preds.cpu(),
                                                  sample_sizes=sample_sizes.cpu(),
                                                  distance_matrices=distance_matrices).item()

          if vis_test:
            n_sample_edges = torch.tensor([s.number_of_edges() for s in dgl.unbatch(samples)])
            batch_confidences, batch_preds = torch.max(preds, dim=1)
            self.visualise_batch(batch_edges=A_edge.cpu(),
                                 batch_labels=batch_preds.cpu(),
                                 batch_confidences=batch_confidences.cpu(),
                                 n_sample_nodes=sample_sizes.cpu(),
                                 n_sample_edges=n_sample_edges.cpu(),
                                 samples_to_visualise=np.arange(vis_n_samples),
                                 filename='temp_preds.jpg')
            with open(f'{self.visualiser.vis_dir}/temp_preds.jpg', 'rb') as f:
              epoch.test_example = f.read()
            vis_test = False
          
        epoch.test_accuracy = float(correct.item() / total.item())
        row += [round(epoch.test_accuracy, 4),
                round(epoch.test_loss, 4),
                round(epoch.test_silhouette, 4),
                ]
      else:
        row += ['---', '---']
      
      epoch.epoch_perf_time = timedelta(seconds=self.timer.elapsed_perf_time('epoch'))
      epoch.epoch_time = timedelta(seconds=self.timer.elapsed_time('epoch'))
      epoch.elapsed_perf_time = timedelta(seconds=self.timer.elapsed_perf_time('start'))
      epoch.elapsed_time = timedelta(seconds=self.timer.elapsed_time('start'))
      row += [str(epoch.elapsed_time),
              str(epoch.elapsed_perf_time)]
      t.add_row(row)
      if not table_head_printed:
        print('\n'.join(t.get_string().splitlines()[:3]))
        table_head_printed = True
      print('\n'.join(t.get_string().splitlines()[-2:]))
      session = self.gcdb.local_sess()
      epoch.write_to_db(session=session)
  
  # def fowlkes_mallow_loss_fn(self, preds, labels, penalties=None):
  #   pred_values = torch.argmax(preds, dim=1)
  #   assignment_edges = torch.stack((pred_values, labels), dim=0)
  #   contingency_edges, contingency_counts = torch.unique(assignment_edges, dim=1,
  #                                                        return_counts=True)
  #   M = torch_geometric.utils.to_dense_adj(edge_index=contingency_edges,
  #                                          edge_attr=contingency_counts,
  #                                          max_num_nodes=self.params.n_clusters).squeeze(dim=0)
  #   Tk = torch.matmul(M, M) - self.params.n_clusters
  #   M2 = M * M
  #   Tk = torch.sum(M2) - self.params.n_clusters
  #   row_sum = torch.sum(M, dim=0)
  #   col_sum = torch.sum(M, dim=1)
  #   Pk = torch.sum(row_sum * row_sum) - self.params.n_clusters
  #   Qk = torch.sum(col_sum * col_sum) - self.params.n_clusters
  #   Bk = Tk / torch.sqrt((Pk * Qk).float())
  #   return 1 - Bk, Bk
  #
  # def loss_fn(self, preds, labels):
  #   loss = np.inf
  #   correct = 0
  #   permutations = itertools.permutations(np.arange(self.params.n_clusters))
  #   for perm in permutations:
  #     p_labels = np.choose(labels, perm)
  #     t_labels = torch.tensor(p_labels)
  #     likelihoods = torch.gather(preds, dim=1, index=torch.unsqueeze(t_labels, 1))
  #     # likelihoods = torch.gather(predictions, dim=1, index=t_labels)
  #     log_likelihoods = torch.log(likelihoods + 1e-8)
  #     nll = -log_likelihoods.sum()
  #     if nll < loss:
  #       loss = nll
  #       correct = torch.sum(t_labels == torch.max(preds, dim=1)[1])
  #   return loss, correct, preds.size()[0]
  #
  # def total_cluster_entropy(self, preds):
  #   # sum_probs is also an indicator of relative cluster sizes
  #   sum_probs = torch.sum(preds, dim=0)
  #   p = sum_probs / preds.size()[0]
  #   entropy = -torch.sum(torch.log2(p) * p)
  #   return entropy
  #
  # def cluster_assignment_entropy(self, preds):
  #   log_probs = torch.log2(preds)
  #   node_entropys = -torch.sum(log_probs * preds, dim=1)
  #   mean_entropy = torch.mean(node_entropys)
  #   return mean_entropy