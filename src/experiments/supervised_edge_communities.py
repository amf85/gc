import itertools
from datetime import timedelta

import json
import dgl
import numpy as np
import torch
import torch_geometric
import torch.nn.functional as F
from prettytable import PrettyTable
from torch.utils.data import DataLoader, random_split
from torch_geometric.utils import to_dense_adj
from torchvision.transforms import transforms
from time import strftime, gmtime
import pickle
from sklearn.model_selection import KFold, train_test_split
from sklearn.preprocessing import StandardScaler

from data.gc_data import GCSample, GCKFolds
from experiments.epochs import SECEpoch
from experiments.gc_experiment import GCExperiment
from sql.sql_base import SupervisedEdgeCommunitiesBase
from experiments.losses import (
  silhouette_loss,
  cluster_assignment_entropy,
  total_cluster_entropy,
  modularity_loss,
  direct_community_loss,
  communities_loss,
  communities_acc,
  permutation_nll,
  nll_edges,
)


class SupervisedEdgeCommunities(GCExperiment):
  @property
  def db_table(self):
    return 'exp_supervised_edge_communities'
  
  @property
  def experiment_class(self):
    return 'SupervisedEdgeCommunities'
  
  class SECParams(GCExperiment.ExperimentParams):
    def __init__(self,
                 model,
                 optimiser,
                 dataset,
                 val_size,
                 batch_size,
                 
                 lambda_nll,

                 cross_val_group_start=None,
                 cross_val_num_folds=None,
                 cross_val_which_fold=None,
                 cross_val_split_method=None,
                 cross_val_seed=None,

                 scale_eig_features=False,
                 scale_local_features=False,
                 scale_node_features=True,

                 val_seed=None,
                 early_stop_patience=None,
                 early_stop_on=None,
                 early_stop_direction=None,
                 
                 eig_conf=None,
                 local_features=None,
                 node_features=None,
                 edge_features=None,
                 
                 node_labels=None,
                 graph_labels=None,
                 edge_labels=None,

                 train_epoch_method='train_epoch',
                 ):
      super(SupervisedEdgeCommunities.SECParams, self).__init__(model=model,
                                                                optimiser=optimiser,
                                                                dataset=dataset)
      self.val_size = val_size
      self.batch_size = batch_size
      self.lambda_nll = lambda_nll

      self.cross_val_group_start = cross_val_group_start
      self.cross_val_num_folds = cross_val_num_folds
      self.cross_val_which_fold = cross_val_which_fold
      self.cross_val_split_method = cross_val_split_method
      self.cross_val_seed = cross_val_seed

      self.scale_eig_features = scale_eig_features
      self.scale_local_features = scale_local_features
      self.scale_node_features = scale_node_features

      self.val_seed = val_seed
      self.early_stop_patience = early_stop_patience
      self.early_stop_on = early_stop_on
      self.early_stop_direction = early_stop_direction
      
      self.eig_conf = eig_conf
      self.local_features = local_features
      self.node_features = node_features
      self.edge_features = edge_features
      
      self.node_labels = node_labels
      self.graph_labels = graph_labels
      self.edge_labels = edge_labels

      self.train_epoch_method = train_epoch_method

  
  def __init__(self,
               gcdb,
               experiment_dir,
               params: SECParams,
               visualiser=None,
               device=torch.device('cpu'),
               ):
    super(SupervisedEdgeCommunities, self).__init__(experiment_dir=experiment_dir,
                                                    params=params,
                                                    visualiser=visualiser,
                                                    gcdb=gcdb,
                                                    device=device,
                                                    )
    self.add_sql_entry()
    if params.train_epoch_method == 'train_epoch':
      self.train_epoch_method = self.train_epoch
    elif params.train_epoch_method == 'train_epoch_with_edge_vis':
      raise NotImplementedError()
  
  def add_sql_entry(self):
    local_features = ','.join(map(str, sorted(list(self.params.local_features))))
    node_features = ','.join(map(str, sorted(list(self.params.node_features))))
    edge_features = ','.join(map(str, sorted(list(self.params.edge_features))))
    node_labels = ','.join(map(str, sorted(list(self.params.node_labels))))
    graph_labels = ','.join(map(str, sorted(list(self.params.graph_labels))))
    edge_labels = ','.join(map(str, sorted(list(self.params.edge_labels))))
    eig_conf = json.dumps(self.params.eig_conf, indent=2, sort_keys=False)
    
    base = SupervisedEdgeCommunitiesBase(
      id=self.id,
      model_id=self.params.model.id,
      optimiser_id=self.params.optimiser.id,
      train_split_id=self.params.dataset.splits['train'].id,
      test_split_id=self.params.dataset.splits['test'].id,
      val_size=self.params.val_size,
      batch_size=self.params.batch_size,
      
      lambda_nll=self.params.lambda_nll,

      cross_val_group_start=self.params.cross_val_group_start,
      cross_val_num_folds=self.params.cross_val_num_folds,
      cross_val_which_fold=self.params.cross_val_which_fold,
      cross_val_split_method=self.params.cross_val_split_method,
      cross_val_seed=self.params.cross_val_seed,

      val_seed=self.params.val_seed,
      early_stop_patience=self.params.early_stop_patience,
      early_stop_on=self.params.early_stop_on,
      
      eig_conf=eig_conf,
      local_features=local_features,
      node_features=node_features,
      edge_features=edge_features,
      
      node_labels=node_labels,
      graph_labels=graph_labels,
      edge_labels=edge_labels,
    )
    session = self.gcdb.local_sess()
    session.add(base)
    session.commit()
  
  def start(self,
            epochs,
            chkp_freq=1,
            vis_freq=float('inf'),
            vis_n_samples=5,
            val_freq=1,
            test_freq=1,
            train_acc_freq=1,
            test_acc_freq=1,
            ):
    train_batches, val_batches, test_batches = self.prepare_data()
    self.train_model(
      train_batches=train_batches,
      val_batches=val_batches,
      test_batches=test_batches,
      epochs=epochs,
      chkp_freq=chkp_freq,
      test_freq=test_freq,
      train_acc_freq=train_acc_freq,
      test_acc_freq=test_acc_freq,
      vis_freq=vis_freq,
      vis_n_samples=vis_n_samples,
    )
  
  def resume(self, checkpoint, chkp_freq, epochs, **kwargs):
    raise NotImplementedError()
  
  def prepare_data(self):

    if self.params.cross_val_num_folds is not None:
      if self.params.cross_val_split_method == 'random':
        np.random.seed(self.params.cross_val_seed)
        kf = KFold(n_splits=self.params.cross_val_num_folds, shuffle=True)
      elif self.params.cross_val_split_method == 'ordered':
        kf = KFold(n_splits=self.params.cross_val_num_folds)
      else:
        raise NotImplementedError()

      folds = list(kf.split(self.params.dataset.splits['train'].sample_ids))
      train_indices, test_indices = folds[self.params.cross_val_which_fold]
      test_source = self.params.dataset.splits['train']

    else:
      train_indices = np.arange(len(self.params.dataset.splits['train']))
      test_indices = np.arange(len(self.params.dataset.splits['test']))
      test_source = self.params.dataset.splits['test']

    if self.params.val_size > 0:

      train_indices, val_indices = train_test_split(
        train_indices,
        test_size=self.params.val_size,
        shuffle=True,
        random_state=self.params.val_seed
      )


    else:
      val_indices = None

    train_sample_ids = [self.params.dataset.splits['train'].sample_ids[i] for i in train_indices]
    train_file_index = [self.params.dataset.splits['train'].file_index[i] for i in train_indices]
    test_sample_ids = [test_source.sample_ids[i] for i in test_indices]
    test_file_index = [test_source.file_index[i] for i in test_indices]

    n_eig_features = sum([sum(self.params.eig_conf['per_layer_eigenvectors']),
                          sum(self.params.eig_conf['per_layer_eigenvalues']),
                          sum(self.params.eig_conf['per_layer_assignments'])])
    n_local_features = len(self.params.local_features)
    n_node_features = len(self.params.node_features)

    eig_feature_scaler = StandardScaler()
    local_feature_scaler = StandardScaler()
    node_feature_scaler = StandardScaler()
    dgl_transform = self.params.dataset.splits['train'].transform

    for train_file in train_file_index:
      sample: GCSample = pickle.load(open(train_file, 'rb'))
      dgl_graph, node_labels, edge_labels, distance_matrix = dgl_transform(sample)
      dgl_eig_features = dgl_graph.ndata['X'][:, :n_eig_features]
      dgl_local_features = dgl_graph.ndata['X'][:, n_eig_features: n_eig_features + n_local_features]
      dgl_node_features = dgl_graph.ndata['X'][:, -n_node_features:]
      if n_eig_features > 0:
        eig_feature_scaler.partial_fit(dgl_eig_features)
      if n_local_features > 0:
        local_feature_scaler.partial_fit(dgl_local_features)
      if n_node_features > 0:
        node_feature_scaler.partial_fit(dgl_node_features)

    def feature_scaling(receive):
      dgl_graph = receive[0]
      if self.params.scale_eig_features and n_eig_features > 0:
        dgl_eig_features = dgl_graph.ndata['X'][:, :n_eig_features]
        transformed_eig_features = eig_feature_scaler.transform(dgl_eig_features)
        dgl_graph.ndata['X'][:, :n_eig_features] = torch.tensor(transformed_eig_features)
      if self.params.scale_local_features and n_local_features > 0:
        dgl_local_features = dgl_graph.ndata['X'][:, n_eig_features: n_eig_features + n_local_features]
        transformed_local_features = local_feature_scaler.transform(dgl_local_features)
        dgl_graph.ndata['X'][:, n_eig_features: n_eig_features + n_local_features] = \
          torch.tensor(transformed_local_features)
      if self.params.scale_node_features and n_node_features > 0:
        dgl_node_features = dgl_graph.ndata['X'][:, -n_node_features:]
        transformed_node_features = node_feature_scaler.transform(dgl_node_features)
        dgl_graph.ndata['X'][:, -n_node_features:] = torch.tensor(transformed_node_features)
      send = [dgl_graph] + receive[1:]
      return send

    transform = transforms.Compose([dgl_transform, feature_scaling])

    train_dataset = GCKFolds(sample_ids=train_sample_ids,
                             file_index=train_file_index,
                             transform=transform)
    test_dataset = GCKFolds(sample_ids=test_sample_ids,
                            file_index=test_file_index,
                            transform=transform)

    train_batches = DataLoader(dataset=train_dataset,
                               collate_fn=GCSample.collate_fn(
                                 include_labels=True,
                                 include_edge_labels=True,
                                 include_distance_matrices=False),
                               batch_size=self.params.batch_size,
                               shuffle=True,
                               prefetch_factor=2,
                               drop_last=False)
    test_batches = DataLoader(dataset=test_dataset,
                              batch_size=self.params.batch_size,
                              collate_fn=GCSample.collate_fn(
                                include_labels=True,
                                include_edge_labels=True,
                                include_distance_matrices=False),
                              shuffle=True,
                              prefetch_factor=2,
                              drop_last=False)
    ret = [train_batches]
    if val_indices is not None:

      val_sample_ids = [self.params.dataset.splits['train'].sample_ids[i] for i in val_indices]
      val_file_index = [self.params.dataset.splits['train'].file_index[i] for i in val_indices]
      val_dataset = GCKFolds(sample_ids=val_sample_ids,
                             file_index=val_file_index,
                             transform=transform)

      val_batches = DataLoader(dataset=val_dataset,
                               batch_size=self.params.batch_size,
                               collate_fn=GCSample.collate_fn(
                                 include_labels=True,
                                 include_edge_labels=True,
                                 include_distance_matrices=False),
                               shuffle=True,
                               prefetch_factor=2,
                               drop_last=False)
      ret.append(val_batches)
    else:
      ret.append(None)
    ret.append(test_batches)
    return ret
  
  def visualise_batch(self,
                      batch_edges,
                      batch_preds=None,
                      batch_confidences=None,
                      batch_labels=None,
                      batch_node_boundaries=None,
                      batch_edge_boundaries=None,
                      samples_to_visualise=None,
                      filename='temp.jpg',
                      np_seed=None):
    
    # return
    batch_edges = batch_edges.detach().to(self.cpu)
    batch_preds = batch_preds.detach().to(self.cpu) if batch_preds is not None else None
    batch_confidences = batch_confidences.detach().to(
      self.cpu) if batch_confidences is not None else None
    batch_labels = batch_labels.detach().to(
      self.cpu) if batch_labels is not None else None

    node_starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                             torch.cumsum(batch_node_boundaries, dim=0)[:-1]), dim=0)
    edge_starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                             torch.cumsum(batch_edge_boundaries, dim=0)[:-1]), dim=0)
    edges = torch.zeros(size=(2, 0))
    preds = torch.zeros(size=(0,)) if batch_preds is not None else None
    labels = torch.zeros(size=(0,))

    confidences = None if batch_confidences is None else torch.zeros(size=(0,))
    for s_idx in samples_to_visualise:
      s_edges = batch_edges[:, edge_starts[s_idx]:
                               edge_starts[s_idx] + batch_edge_boundaries[s_idx]]
      edges = torch.cat((edges, s_edges), dim=1)

      if batch_preds is not None:
        s_preds = batch_preds[node_starts[s_idx]:
                                node_starts[s_idx] + batch_edge_boundaries[s_idx]]
        preds = torch.cat((preds, s_preds), dim=0)

      
      if batch_confidences is not None:
        s_confidences = batch_confidences[edge_starts[s_idx]:
                                          edge_starts[s_idx] + batch_edge_boundaries[s_idx]]
        confidences = torch.cat((confidences, s_confidences), dim=0)
      if batch_labels is not None:
        s_labels = batch_labels[edge_starts[s_idx]:
                                edge_starts[s_idx] + batch_edge_boundaries[s_idx]]
        labels = torch.cat((labels, s_labels), dim=0)

    if batch_preds is not None:
      preds = preds.detach().numpy().astype(np.int32)
    if batch_confidences is not None:
      confidences = confidences.detach().numpy()
    if batch_labels is not None:
      labels = labels.detach().numpy()
    self.visualiser.visualise(A_edge=edges.numpy().astype(np.int32),
                                   preds=preds,
                                   labels=labels,
                                   confidences=confidences,
                                   filename=filename,
                                   # n_colours=max(communities) + 1,
                                   np_seed=np_seed)
  
  
  def train_model(self,
                  train_batches,
                  val_batches,
                  test_batches,
                  epochs,
                  chkp_freq=1,
                  test_freq=1,
                  train_acc_freq=1,
                  test_acc_freq=1,
                  vis_freq=float('inf'),
                  vis_n_samples=10,
                  ):
    self.timer.start_timer('start')

    loss_variants = ('nll',)
    def get_loss_variant_headers(phase):
      headers = []
      for name in loss_variants:
        if self.params.__dict__[f'lambda_{name}'] is not None:
          headers.append(f'{phase} {name}')
      return headers

    field_names = ['Epoch', 'Train loss', 'Train edge acc']
    field_names += get_loss_variant_headers('Train')
    if val_batches is not None:
      field_names += ['Val loss', 'Val edge acc']
      field_names += get_loss_variant_headers('Val')
    field_names += ['Test loss', 'Test edge acc']
    field_names += get_loss_variant_headers('Test')
    field_names += ['Elapsed', 'System']

    t = PrettyTable()
    t.field_names = field_names
    table_head_printed = False


    train_1s, train_n_edges, val_1s, val_n_edges, test_1s, test_n_edges = 0, 0, 0, 0, 0, 0
    for _, _, edge_labels in train_batches:
      train_1s += torch.sum(edge_labels)
      train_n_edges += edge_labels.size()[0]
    for _, _, edge_labels in val_batches:
      val_1s += torch.sum(edge_labels)
      val_n_edges += edge_labels.size()[0]
    for _, _, edge_labels in test_batches:
      test_1s += torch.sum(edge_labels)
      test_n_edges += edge_labels.size()[0]
    print(f'Train naive baseline = {(train_1s / train_n_edges).item()}    '
          f'({train_1s.item()}/{train_n_edges})')
    print(f'Val naive baseline = {(val_1s / val_n_edges).item()}    '
          f'({val_1s.item()}/{val_n_edges})')
    print(f'Test naive baseline = {(test_1s / test_n_edges).item()}    '
          f'({test_1s.item()}/{test_n_edges})')

    def get_str_variant_vals(phase, variants=loss_variants):
      vals = []
      for name in variants:
        if name in ('loss', 'edge_acc') or self.params.__dict__[f'lambda_{name}'] is not None:
          vals.append('%s'%float('%5.4g' % epoch.__dict__[f'{phase}_{name}']))
      return vals
    all_loss_names = ['loss', 'edge_acc'] + list(loss_variants)

    early_stop_counter = 0
    early_stop_best = np.inf
    early_stop_test_acc = None
    early_stopped = False
    if self.params.early_stop_direction == 'max' or self.params.early_stop_direction == 'maxeq':
      early_stop_best = -np.inf

    for epoch_num in range(epochs):
      epoch = self.train_epoch_method(epoch_num=epoch_num,
                                      train_batches=train_batches,
                                      val_batches=val_batches,
                                      test_batches=test_batches,
                                      test_freq=test_freq,
                                      vis_freq=vis_freq,
                                      vis_n_samples=vis_n_samples)
      row = [epoch_num + 1]
      row += get_str_variant_vals('train', variants=all_loss_names)
      if val_batches is not None:
        row += get_str_variant_vals('val', variants=all_loss_names)
      if epoch_num % test_freq == 0:
        row += get_str_variant_vals('test', variants=all_loss_names)
      else:
        row += ['---'] * len(get_str_variant_vals('test'))

      epoch.elapsed_perf_time = timedelta(seconds=self.timer.elapsed_perf_time('start'))
      epoch.elapsed_time = timedelta(seconds=self.timer.elapsed_time('start'))
      session = self.gcdb.local_sess()
      epoch.write_to_db(session=session)

      row += [str(epoch.elapsed_time)]
      row += [strftime('%H:%M:%S', gmtime())]
      t.add_row(row)
      if not table_head_printed:
        print('\n'.join(t.get_string().splitlines()[:3]))
        table_head_printed = True
      print('\n'.join(t.get_string().splitlines()[-2:]))

      if self.params.early_stop_on is not None:
        early_stop_metric = epoch.__dict__[f'val_{self.params.early_stop_on}']
        if ((self.params.early_stop_direction == 'min' and early_stop_metric < early_stop_best)
          or (self.params.early_stop_direction == 'max' and early_stop_metric > early_stop_best)
          or (self.params.early_stop_direction == 'mineq' and early_stop_metric <= early_stop_best)
          or (self.params.early_stop_direction == 'maxeq' and early_stop_metric >= early_stop_best)):
          early_stop_best = early_stop_metric
          early_stop_test_acc = epoch.__dict__[f'test_edge_acc']
          early_stop_counter = 0
        else:
          early_stop_counter += 1
        if early_stop_counter > self.params.early_stop_patience:
          print(f'No improvement in val_{self.params.early_stop_on} after waiting '
                f'{early_stop_counter - 1} epochs triggered early termination.')
          print(f'Early stop best: {early_stop_best}')
          print(f'Associated test acc: {early_stop_test_acc}')
          early_stopped = True
          break

    if not early_stopped:
      print(f'Early stop best: {early_stop_best}')
      print(f'Associated test acc: {early_stop_test_acc}')


  def train_epoch(self,
                  epoch_num,
                  train_batches,
                  val_batches,
                  test_batches,
                  test_freq,
                  vis_freq,
                  vis_n_samples
                  ):
    self.params.model.train()
    vis_train, vis_test = [vis_freq > 0 and epoch_num % vis_freq == 0] * 2
    self.timer.start_timer('epoch')
    self.params.optimiser.zero_grad()
    epoch = SECEpoch(experiment=self, epoch_num=epoch_num)

    correct, total = 0, 0
    for samples, node_labels, edge_labels in train_batches:
      samples = samples.to(self.device)
      edge_labels = edge_labels.long().squeeze().to(self.device)
      X = samples.ndata['X']
      A_edge = samples.edges()
      A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
      A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
      if len(X.size()) < 2:
        X = torch.unsqueeze(X, dim=1).float()
      preds, _ = self.params.model.forward(samples, softmax_output=False)
      sample_sizes = torch.tensor(
        [s.number_of_nodes() for s in dgl.unbatch(samples)]
      ).to(self.device)

      preds = F.softmax(preds, dim=1)

      nll, n_correct = nll_edges(preds, edge_labels, return_n_correct=True)

      loss = torch.tensor(0).float().to(self.device)
      loss += (nll * self.params.lambda_nll)

      epoch.train_loss += loss.item()
      epoch.train_nll += nll.item()
      correct += n_correct
      total += preds.size()[0]

      loss.backward()
      self.params.optimiser.step()
      self.params.optimiser.zero_grad()

      # vis_train = False
      if vis_train:
        A_edge = A_edge.cpu()
        sample_sizes = sample_sizes.cpu()
        n_sample_edges = torch.tensor(
          [s.number_of_edges() for s in dgl.unbatch(samples)]).cpu()
        batch_confidences, batch_preds = torch.max(preds.detach(), dim=1)
        np_seed = np.random.randint(99999999)
        self.visualise_batch(batch_edges=A_edge,
                             batch_preds=batch_preds,
                             batch_labels=edge_labels,
                             batch_confidences=batch_confidences,
                             batch_node_boundaries=sample_sizes,
                             batch_edge_boundaries=n_sample_edges,
                             samples_to_visualise=np.arange(vis_n_samples),
                             filename='temp_conf.jpg',
                             np_seed=np_seed)
        # self.visualise_batch(batch_edges=A_edge,
        #                      batch_preds=batch_preds,
        #                      batch_labels=edge_labels,
        #                      batch_node_boundaries=sample_sizes,
        #                      batch_edge_boundaries=n_sample_edges,
        #                      samples_to_visualise=np.arange(vis_n_samples),
        #                      filename='temp_hmax.jpg',
        #                      np_seed=np_seed)
        self.visualise_batch(batch_edges=A_edge,
                             batch_preds=edge_labels,
                             # batch_labels=torch.tensor(optimal_labels),
                             batch_node_boundaries=sample_sizes,
                             batch_edge_boundaries=n_sample_edges,
                             samples_to_visualise=np.arange(vis_n_samples),
                             filename='temp_labels.jpg',
                             np_seed=np_seed)
        with open(f'{self.visualiser.vis_dir}/temp_conf.jpg', 'rb') as f:
          epoch.train_eg_conf = f.read()
        # with open(f'{self.visualiser.vis_dir}/temp_hmax.jpg', 'rb') as f:
        #   epoch.train_eg_hmax = f.read()
        with open(f'{self.visualiser.vis_dir}/temp_labels.jpg', 'rb') as f:
          epoch.train_eg_labels = f.read()
        vis_train = False

    epoch.train_edge_acc += (correct / total).item()



    if val_batches is not None:
      self.params.model.eval()
      eval_device = torch.device('cpu')
      correct, total = 0, 0
      for samples, node_labels, edge_labels in val_batches:
        samples = samples.to(self.device)
        edge_labels = edge_labels.long().squeeze()
        X = samples.ndata['X']
        A_edge = samples.edges()
        A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
        A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
        if len(X.size()) < 2:
          X = torch.unsqueeze(X, dim=1).float()
        preds, _ = self.params.model.forward(samples, softmax_output=False)

        sample_sizes = torch.tensor(
          [s.number_of_nodes() for s in dgl.unbatch(samples)]
        ).to(eval_device)

        preds = preds.to(eval_device)
        preds = F.softmax(preds, dim=1)

        nll, n_correct = nll_edges(preds, edge_labels, return_n_correct=True)

        loss = torch.tensor(0).float().to(self.device)
        loss += (nll * self.params.lambda_nll)

        epoch.val_loss += loss.item()
        epoch.val_nll += nll.item()
        correct += n_correct
        total += preds.size()[0]
      epoch.val_edge_acc = (correct / total).item()


    if epoch_num % test_freq == 0:
      self.params.model.eval()
      eval_device = torch.device('cpu')
      correct, total = 0, 0
      for samples, node_labels, edge_labels in test_batches:
        samples = samples.to(self.device)
        edge_labels = edge_labels.long().squeeze()
        X = samples.ndata['X']
        A_edge = samples.edges()
        A_edge = torch.stack((A_edge[1], A_edge[0]), dim=0).long()
        A_attr = torch.ones(size=(A_edge.size()[1],)).float().to(self.device)
        if len(X.size()) < 2:
          X = torch.unsqueeze(X, dim=1).float()
        preds, _ = self.params.model.forward(samples, softmax_output=False)

        sample_sizes = torch.tensor(
          [s.number_of_nodes() for s in dgl.unbatch(samples)]
        ).to(eval_device)

        preds = preds.to(eval_device)
        preds = F.softmax(preds, dim=1)

        nll, n_correct = nll_edges(preds, edge_labels, return_n_correct=True)

        loss = torch.tensor(0).float().to(self.device)
        loss += (nll * self.params.lambda_nll)

        epoch.test_loss += loss.item()
        epoch.test_nll += nll.item()
        correct += n_correct
        total += preds.size()[0]

        # vis_test=False
        if vis_test:
          A_edge = A_edge.cpu()
          sample_sizes = sample_sizes.cpu()
          n_sample_edges = torch.tensor(
            [s.number_of_edges() for s in dgl.unbatch(samples)]).cpu()
          batch_confidences, batch_preds = torch.max(preds.detach(), dim=1)
          np_seed = np.random.randint(99999999)
          self.visualise_batch(batch_edges=A_edge,
                               batch_preds=batch_preds,
                               batch_labels=edge_labels,
                               batch_confidences=batch_confidences,
                               batch_node_boundaries=sample_sizes,
                               batch_edge_boundaries=n_sample_edges,
                               samples_to_visualise=np.arange(vis_n_samples),
                               filename='temp_conf.jpg',
                               np_seed=np_seed)
          # self.visualise_batch(batch_edges=A_edge,
          #                      batch_preds=batch_preds,
          #                      batch_node_boundaries=sample_sizes,
          #                      batch_edge_boundaries=n_sample_edges,
          #                      samples_to_visualise=np.arange(vis_n_samples),
          #                      filename='temp_hmax.jpg',
          #                      np_seed=np_seed)
          self.visualise_batch(batch_edges=A_edge,
                               batch_preds=None,
                               batch_labels=edge_labels,
                               # batch_labels=torch.tensor(optimal_labels),
                               batch_node_boundaries=sample_sizes,
                               batch_edge_boundaries=n_sample_edges,
                               samples_to_visualise=np.arange(vis_n_samples),
                               filename='temp_labels.jpg',
                               np_seed=np_seed)
          with open(f'{self.visualiser.vis_dir}/temp_conf.jpg', 'rb') as f:
            epoch.test_eg_conf = f.read()
          # with open(f'{self.visualiser.vis_dir}/temp_hmax.jpg', 'rb') as f:
          #   epoch.test_eg_hmax = f.read()
          with open(f'{self.visualiser.vis_dir}/temp_labels.jpg', 'rb') as f:
            epoch.test_eg_labels = f.read()
          vis_test = False

      epoch.test_edge_acc = (correct / total).item()

      self.params.model.train()

    epoch.epoch_perf_time = timedelta(seconds=self.timer.elapsed_perf_time('epoch'))
    epoch.epoch_time = timedelta(seconds=self.timer.elapsed_time('epoch'))


    return epoch























