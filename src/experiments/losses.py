import torch
import itertools
import numpy as np
import torch_geometric
import torch.nn.functional as F
from scipy.optimize import linear_sum_assignment



def nll_edges(preds,
              labels,
              return_n_correct=True):
  ret = []
  nll = F.binary_cross_entropy_with_logits(preds, F.one_hot(labels, num_classes=2).float())
  ret.append(nll)
  if return_n_correct:
    n_correct = torch.sum(torch.max(preds, dim=1)[1] == labels)
    # n_correct = torch.sum(labels)
    ret.append(n_correct)
  return ret
  


def communities_loss(preds,
                     labels,
                     cxe=True,
                     cmse=True,
                     cmae=True,
                     normalise=False,
                     softmax_preds=False):
  # t = []
  if softmax_preds:
    preds = F.softmax(preds, dim=1)
  
  # todo: debug
  # preds = F.one_hot(torch.argmax(preds, dim=1), num_classes=preds.size()[1]).float()
  
  L = F.one_hot(labels.long(), num_classes=preds.size()[1]).float()
  LLT = torch.matmul(L, L.T)
  PPT = torch.matmul(preds, preds.T)
  if cxe:
    cxe = LLT * torch.log(PPT + 1e-9) + (1 - LLT) * torch.log(1 - PPT + 1e-9)
    if normalise:
      cxe = torch.mean(cxe)
      # t.append(torch.mean(cxe))
    else:
      cxe = torch.sum(cxe)
      # t.append(torch.sum(cxe))
  else:
    cxe = None
  if cmse:
    cmse = torch.square(LLT - PPT)
    if normalise:
      cmse = torch.mean(cmse)
      # t.append(torch.mean(cmse))
    else:
      cmse = torch.sum(cmse)
      # t.append(torch.sum(cmse))
  else:
    cmse = None
  if cmae:
    cmae = torch.abs(LLT - PPT)
    if normalise:
      cmae = torch.mean(cmae)
      # t.append(torch.mean(cmae))
    else:
      cmae = torch.sum(cmae)
      # t.append(torch.sum(cmae))
  else:
    cmae = None
  return cxe, cmse, cmae
  
  
def communities_acc(preds,
                    labels,
                    sample_sizes,
                    return_n_correct=True,
                    return_optimal_label_permutation=True):
  """
  
  :param preds: should be detached numpy
  :param labels: also numpy
  :param sample_sizes: again, offline numpy
  :return: acc, n_correct
  """
  starts = np.concatenate([np.zeros(shape=(1,)),
                           np.cumsum(sample_sizes, axis=0)], axis=0).astype(int)
  n_correct = 0
  updated_labels = np.empty(shape=labels.shape[0]).astype(int)
  for start, size in zip(starts, sample_sizes):
    assignment_matrix = preds[start:start + size, :]
    sample_labels = labels[start: start + size]
    assignment_partitions = [np.argwhere(np.argmax(assignment_matrix, axis=1) == i)
                             for i in range(preds.shape[1])]
    label_partitions = [np.argwhere(sample_labels == i) for i in range(preds.shape[1])]
    f = np.vectorize(lambda i, j: np.intersect1d(
      label_partitions[i], assignment_partitions[j]).size)
    cost = np.fromfunction(f, shape=(preds.shape[1], preds.shape[1]), dtype=int)
    row_ind, col_ind = linear_sum_assignment(cost, maximize=True)
    if return_optimal_label_permutation:
      permuted_labels = col_ind[sample_labels]
      updated_labels[start:start + size] = permuted_labels
    if return_n_correct:
      n_correct += cost[row_ind, col_ind].sum()
      
  t = []
  if return_n_correct:
    t.append(n_correct)
  if return_optimal_label_permutation:
    t.append(updated_labels)
  return t
  


def modularity_loss(preds, edge_matrix, device=torch.device('cpu'), normalise=False):
  M = torch.matmul(preds.T, torch.matmul(edge_matrix, preds))
  M_signed = ((torch.eye(M.size()[0]).to(device) * 2) - 1) * M
  
  e = torch.sum(torch.relu(M_signed), dim=0)
  a = torch.sum(torch.relu(-M_signed), dim=0)
  mx = torch.max(e, a)
  
  loss = 0.5 + (e - a)/(2*mx)
  if normalise:
    return -torch.mean(loss)
  else:
    return -torch.sum(loss)
  
  # return -torch.mean(((e - a) / mx) - 1)
  

def silhouette_loss(preds, sample_sizes, distance_matrices, device=torch.device('cpu'), normalise=False):
  # loss = torch.tensor(0, dtype=torch.float).to(device)
  loss = []
  starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long).to(device),
                      torch.cumsum(sample_sizes, dim=0)[:-1]), dim=0)
  for start, size, distance_matrix in zip(starts, sample_sizes, distance_matrices):
    assignment_matrix = preds[start:start + size]
    distance_matrix = distance_matrix.float()
    node_cluster_distances = torch.matmul(distance_matrix, assignment_matrix)
    
    a = torch.sum(assignment_matrix * node_cluster_distances, dim=1)
    
    inverse_assignments = torch.ones_like(assignment_matrix) - assignment_matrix
    weighted_inter_cluster_distances = inverse_assignments * node_cluster_distances
    b = torch.mean(weighted_inter_cluster_distances, dim=1)
    
    m = torch.max(torch.cat((torch.unsqueeze(a, dim=1),
                             torch.unsqueeze(b, dim=1)), dim=1), dim=1).values
    # todo: transform from [-1, 1] to [0, 1], rather than [0, 2]
    # s = torch.mean(1 + ((a - b) / m), dim=0)
    if normalise:
      loss.append(torch.mean(0.5 + ((a - b) / (2*m)), dim=0))
    else:
      loss.append(torch.sum(0.5 + ((a - b) / (2*m)), dim=0))
    # loss += s
  if normalise:
    return torch.mean(torch.stack(loss))
  else:
    return torch.sum(torch.stack(loss))

def permutation_nll(preds,
                    permuted_labels,
                    softmax_preds=False,
                    return_n_correct=False,
                    device=torch.device('cpu'),
                    normalise=False,
                    ):
  if softmax_preds:
    preds = F.softmax(preds, dim=1)
  labels = torch.tensor(permuted_labels).to(device)
  likelihoods = torch.gather(preds, dim=1, index=torch.unsqueeze(labels, dim=1))
  # log_likelihoods = likelihoods * torch.log(likelihoods + 1e-9)
  log_likelihoods = torch.log(likelihoods + 1e-9)
  if normalise:
    nll = -log_likelihoods.mean()
  else:
    nll = -log_likelihoods.sum()
  t = [nll]
  if return_n_correct:
    n_correct = torch.sum(labels == torch.max(preds, dim=1)[1])
    t.append(n_correct)
  return t if len(t) > 1 else t[0]


def nll_graph(preds,
              labels,
              return_n_correct=True,
              normalise=False,
              ):
  # ret = []
  likelihoods = torch.gather(preds, dim=1, index=torch.unsqueeze(labels, dim=1))
  log_likelihoods = torch.log(likelihoods + 1e-9)
  if normalise:
    nll = -log_likelihoods.mean()
  else:
    nll = -log_likelihoods.sum()
  t = [nll]
  if return_n_correct:
    n_correct = torch.sum(torch.max(preds, dim=1)[1] == labels)
    t.append(n_correct)
  return t if len(t) > 1 else t[0]


# def permutation_catXE(preds,
#                       permuted_labels,
#                       return_n_correct=False,)


def direct_community_loss(preds, labels, sample_sizes, n_clusters, softmax_preds=False):#, device=torch.device('cpu')):
  loss = torch.tensor(0).float()
  correct = torch.tensor(0)
  sample_sizes = sample_sizes.cpu()
  starts = torch.cat((torch.zeros(size=(1,), dtype=torch.long),
                      torch.cumsum(sample_sizes, dim=0)[:-1]), dim=0)
  if softmax_preds:
    preds = torch.softmax(preds, dim=1)
  preds = preds.cpu()
  labels = labels.cpu()
  for start, size in zip(starts, sample_sizes):
    s_loss = torch.tensor(float('inf'))#.to(device)
    s_correct = 0
    assignmnent_matrix = preds[start: start+size]
    
    permutations = itertools.permutations(np.arange(n_clusters))
    s_labels = labels[start: start+size]
    for p in permutations:
      p_labels = np.choose(s_labels, p)
      # todo: moving this tensor to gpu for each permutation is potential bottleneck
      t_labels = torch.tensor(p_labels)#.detach().to(device)
      likelihoods = torch.gather(assignmnent_matrix,
                                 dim=1,
                                 index=torch.unsqueeze(t_labels, dim=1))
      log_likelihoods = torch.log(likelihoods + 1e-8)
      nll = -log_likelihoods.sum()
      if nll < s_loss:
        s_loss = nll
        s_correct = torch.sum(t_labels == torch.max(assignmnent_matrix, dim=1)[1])
    loss += s_loss
    correct += s_correct
  
  return loss, correct


def total_cluster_entropy(preds, normalise=False):
  # sum_probs is also an indicator of relative cluster sizes
  sum_probs = torch.sum(preds, dim=0)
  p = sum_probs / preds.size()[0]
  if normalise:
    entropy = -torch.mean(torch.log2(p)*p)
  else:
    entropy = -torch.sum(torch.log2(p) * p)
  return entropy

def cluster_assignment_entropy(preds, normalise=True):
  log_probs = torch.log2(preds + 1e-8)
  node_entropys = -torch.sum(log_probs * preds, dim=1)
  if normalise:
    entropy = torch.mean(node_entropys)
  else:
    entropy = torch.sum(node_entropys)
  return entropy


def fowlkes_mallow_loss_fn(preds, labels, n_clusters):
  '''
  Can't make this differentiable, but could potentially be used as a metric
  '''
  pred_values = torch.argmax(preds, dim=1)
  assignment_edges = torch.stack((pred_values, labels), dim=0)
  contingency_edges, contingency_counts = torch.unique(assignment_edges, dim=1,
                                                       return_counts=True)
  M = torch_geometric.utils.to_dense_adj(edge_index=contingency_edges,
                                         edge_attr=contingency_counts,
                                         max_num_nodes=n_clusters).squeeze(dim=0)
  Tk = torch.matmul(M, M) - n_clusters
  M2 = M * M
  Tk = torch.sum(M2) - n_clusters
  row_sum = torch.sum(M, dim=0)
  col_sum = torch.sum(M, dim=1)
  Pk = torch.sum(row_sum * row_sum) - n_clusters
  Qk = torch.sum(col_sum * col_sum) - n_clusters
  Bk = Tk / torch.sqrt((Pk * Qk).float())
  return 1 - Bk, Bk

def aux_tgt_loss(preds, S, labels, softmax_S=True):
  processed_preds = torch.matmul(S, preds)
  if softmax_S:
    processed_preds = torch.softmax(processed_preds, dim=1)
  loss = torch.nn.NLLLoss(reduction='sum')(processed_preds, labels.long())
  n_correct = torch.sum(torch.argmax(processed_preds, dim=1) == labels)
  return loss, n_correct