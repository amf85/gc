import json
import pickle

import dgl
import numpy as np
import scipy as sp
import scipy.sparse
import torch

from util import util


class HierarchicalNetwork(object):
  def __init__(self, init_A, init_nodes, depth, branch_factor):
    super(HierarchicalNetwork, self).__init__()
    self.init_A = init_A
    self.init_nodes = init_nodes
    self.depth = depth
    self.branch_factor = branch_factor
    self.A = self.init_A
    self.nodes = self.init_nodes
    self.node_info = np.random.randint(low=0, high=2, size=(len(self.nodes)))
    self.node_mode_info = np.random.choice([-1, 1], size=(len(self.nodes)))
    self.modulo_label = np.sum(self.node_info[self.init_nodes == 'c']) % 2
    self.c_node_info_sum = np.sum(self.node_info[self.init_nodes == 'c'])
    self.all_node_info_sum = np.sum(self.node_info)
    self.node_l1_communities = np.array([0] * len(self.init_nodes))
    self.max_cluster_sum = np.sum(self.node_info)
    self.hierarchical_mode = np.clip(np.sum(self.node_mode_info), a_min=-1, a_max=1)
    for i in range(depth):
      node_tile = self.nodes.copy()
      level_node_info = [np.random.randint(0, 2, size=(len(node_tile))) for i in
                         range(branch_factor)]
      level_node_mode_info = [np.random.choice([-1, 1], size=len(node_tile)) for i in
                              range(branch_factor)]
      self.hierarchical_mode = np.clip(np.sum([self.hierarchical_mode] +
                                              [np.clip(np.sum(info), -1, 1)
                                               for info in level_node_mode_info]),
                                       -1, 1)
      self.all_node_info_sum += np.sum([np.sum(info) for info in level_node_info])
      self.max_cluster_sum = max(self.max_cluster_sum, max(np.sum(level_node_info, axis=1)))
      self.modulo_label = np.sum([self.modulo_label] +
                                 [motif[node_tile == 'c'][0] for motif in level_node_info]) % 2
      self.c_node_info_sum = np.sum([self.c_node_info_sum] +
                                    [motif[node_tile == 'c'][0] for motif in level_node_info])
      l_communities = [[i + max(self.node_l1_communities)] * len(level_node_info[i]) for i in
                       range(len(level_node_info))]
      self.node_l1_communities = np.concatenate([self.node_l1_communities,
                                                 np.concatenate([
                                                   [i + max(self.node_l1_communities) + 1] * len(
                                                     level_node_info[i])
                                                   for i in range(len(level_node_info))])])
      self.node_info = np.concatenate([self.node_info, np.concatenate(level_node_info)])
      self.nodes[self.nodes == 'p'] = 'o'
      node_tile[node_tile == 'c'] = 'o'
      nodes = np.tile(node_tile, (1, self.branch_factor))
      nodes = np.concatenate([self.nodes, nodes.squeeze()], axis=0)
      empty = np.zeros(self.A.shape)
      eye = np.eye(self.branch_factor + 1)
      A = np.bmat([[self.A if int(e) == 1 else empty for e in row] for row in eye])
      p_indices = np.argwhere(nodes == 'p')
      c_indices = np.argwhere(nodes == 'c')
      for p_idx in p_indices:
        for c_idx in c_indices:
          A[p_idx, c_idx] = 1
          A[c_idx, p_idx] = 1
      self.A = A
      self.nodes = nodes
    
    # self.graph_label = self.modulo_label
    # self.graph_label = self.branch_factor
    # self.graph_label = self.depth
    # self.graph_label = len(self.init_nodes)
    self.graph_label = self.c_node_info_sum
    # self.graph_label = self.node_l1_communities
    # self.graph_label = self.max_cluster_sum
    # self.graph_label = self.hierarchical_mode
    # self.graph_label = self.all_node_info_sum
  
  def dgl_representation(self):
    X = torch.tensor(self.node_info)
    coo_A = sp.sparse.coo_matrix(self.A)
    g = dgl.graph((torch.tensor(coo_A.col).long(), torch.tensor(coo_A.row).long()),
                  num_nodes=X.shape[0])
    g.ndata['X'] = X
    return g


def is_connected(A):
  connected = set()
  queue = set()
  queue.add(0)
  while len(queue) > 0:
    cur = queue.pop()
    neighbours = np.argwhere(A[cur] == True)
    for neighbour in neighbours:
      if neighbour[0] not in connected:
        queue.add(neighbour[0])
    connected.add(cur)
    if len(connected) == A.shape[0]:
      return True
  return False


def generate_motif(n_nodes):
  gotone = False
  while not gotone:
    A = np.random.randint(0, 2, size=(n_nodes, n_nodes))
    for i in range(n_nodes):
      A[i, i] = 0
    A = np.tril(A) + np.tril(A).T
    if is_connected(A):
      gotone = True
  X = ['c'] + ['p'] * (n_nodes - 1)
  return np.array(X), A


def create_dataset(n_train_samples, n_test_samples, params, directory):
  util.validate_directory(directory)
  set_params = {
    'train_samples': n_train_samples,
    'test_samples': n_test_samples,
    'generator_params': params
  }
  json.dump(set_params, open(f'{directory}/dataset_params.json', 'w'), indent=2)
  motif_sizes = params['motif_sizes']
  depths = params['depths']
  branching_factors = params['branching_factors']
  
  def create_and_save(n_samples, filename):
    samples = []
    for i in range(n_samples):
      motif_size = np.random.choice(motif_sizes)
      depth = np.random.choice(depths)
      branching_factor = np.random.choice(branching_factors)
      X, A = generate_motif(motif_size)
      samples.append(HierarchicalNetwork(A, X, depth, branching_factor))
      if i % 1000 == 0:
        print(f'Generating {filename}, done {i}/{n_samples}')
    pickle.dump(samples, open(f'{directory}/{filename}', 'wb'))
    return samples
  
  train_samples = create_and_save(n_train_samples, 'train.pkl')
  test_samples = create_and_save(n_test_samples, 'test.pkl')
  return train_samples, n_test_samples
