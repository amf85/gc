import numpy as np

n_nodes = 30
cluster_prior = [0.2, 0.8]
connectivity = [[0.6, 0.1],
                [0.1, 0.3]]


class SBM(object):
  def __init__(self, n_nodes, cluster_prior, connectivity):
    super(SBM, self).__init__()
    self.n_nodes = n_nodes
    self.cluster_prior = cluster_prior
    self.connectivity = connectivity
  
  def generate(self):
    X = np.random.choice(a=len(self.cluster_prior), size=self.n_nodes, p=self.cluster_prior).astype(
      int)
    adj_probs = [[self.connectivity[i][j] for j in X] for i in X]
    for i in range(len(adj_probs)):
      adj_probs[i][i] = 0
    A = np.vectorize(lambda p: 1 if np.random.rand() < p else 0)(adj_probs)
    A = np.tril(A) + np.tril(A).T
    return X, A


def test():
  sbm = SBM(n_nodes=n_nodes, cluster_prior=cluster_prior, connectivity=connectivity)
  X, A = sbm.generate()
  pass


if __name__ == '__main__':
  test()
