import dgl.backend
import numpy as np
from dgl import DGLGraph
from scipy import sparse


class SpectralClusterer(object):
  def __init__(self):
    super(SpectralClusterer, self).__init__()
  
  def compute_laplacian_eigenvectors(self, graph: DGLGraph):
    k = graph.number_of_nodes() - 2
    A = graph.adjacency_matrix_scipy(return_edge_ids=False).astype(float)
    N = sparse.diags(dgl.backend.asnumpy(graph.in_degrees()).clip(1), dtype=float)
    L = N - A
    
    eigenvalues, eigenvectors = sparse.linalg.eigs(L, k=k, which='SR', tol=5e-1)
    eigenvectors = eigenvectors[:, eigenvalues.argsort()]
    return np.real(eigenvectors)
  
  def create_node_features(self):
    pass
