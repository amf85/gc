import logging

logging.basicConfig(level="INFO")
from graphein.protein.config import ProteinGraphConfig
from graphein.protein.graphs import construct_graph
import networkx as nx
import numpy as np
import atom3d.datasets as da
import pandas as pd
from torch.utils.data import Dataset, DataLoader
# import second_test as st
import os
import torch
from torch import nn
# from torch_geometric.nn import GCNConv
from torch.nn import BatchNorm1d, Linear
import torch.nn.functional as F
import torch_geometric.data as gdata
import torch_geometric.utils as gutil
from scipy.sparse import coo_matrix
import dgl

dgl.backend.set_default_backend('pytorch')
# import dgl.data as d
from dgl.nn.pytorch import GraphConv
import time

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def get_unique_chain_ids(residue_dataset):
  chain_ids = np.zeros(shape=(0,))
  chain_id_counts = {}
  num_chains_counts = {}
  i = 0
  for entry in residue_dataset:
    print('test')
    entry_chain_ids = np.unique(np.concatenate([
      np.unique([r['chain_id'] for r in entry['wild_type'].nodes.values()]),
      np.unique([r['chain_id'] for r in entry['mutated'].nodes.values()])
    ]))
    wt_counts = pd.Series([i['chain_id'] for i in entry['wild_type'].nodes.values()]).value_counts()
    # chain_id_counts = {k: v if k not in chain_id_counts.keys() else v + chain_id_counts[k]
    #                    for k, v in wt_counts.items()}
    for k, v in wt_counts.items():
      if k in chain_id_counts.keys():
        chain_id_counts[k] += v
      else:
        chain_id_counts[k] = v
    num_chains = len(wt_counts)
    if num_chains in num_chains_counts.keys():
      num_chains_counts[num_chains] += 1
    else:
      num_chains_counts[num_chains] = 1
    
    mt_counts = pd.Series([i['chain_id'] for i in entry['mutated'].nodes.values()]).value_counts()
    for k, v in mt_counts.items():
      if k in chain_id_counts.keys():
        chain_id_counts[k] += v
      else:
        chain_id_counts[k] = v
    
    num_chains = len(mt_counts)
    if num_chains in num_chains_counts.keys():
      num_chains_counts[num_chains] += 1
    else:
      num_chains_counts[num_chains] = 1
    # chain_id_counts = {k: v if k not in chain_id_counts.keys() else v + chain_id_counts[k]
    #                    for k, v in mt_counts.items()}
    chain_ids = np.unique(np.concatenate([chain_ids, entry_chain_ids]))
    print(f'Done {i}/{len(residue_dataset)}')
    i += 1
  pd.Series(chain_ids).to_csv('../../output/tests/chain_ids.csv', header=['chain_id'],
                              index_label=['index'])
  pd.Series(chain_id_counts).to_csv('../../output/tests/chain_id_counts.csv', header=['count'],
                                    index=True,
                                    index_label=['chain_id'])
  pd.Series(num_chains_counts).to_csv('../../output/tests/num_chain_counts.csv', header=['count'],
                                      index=True,
                                      index_label=['num_chains'])


def get_unique_residues():
  dataset = da.load_dataset('../../data/new_msp/raw/MSP/data/', 'lmdb')
  residues = np.zeros(shape=(0,))
  residue_counts = {}
  num_residue_counts = {}
  i = 0
  for entry in dataset:
    entry_residues = np.unique(np.concatenate([
      np.unique(next(iter(dataset))['mutated_atoms']['resname']),
      np.unique(next(iter(dataset))['original_atoms']['resname'])
    ]))
    wt_counts = entry['original_atoms']['resname'].value_counts()
    # residue_counts = {k: v if k not in residue_counts.keys() else v + residue_counts[k]
    #                   for k, v in wt_counts.items()}
    for k, v in wt_counts.items():
      if k in residue_counts.keys():
        residue_counts[k] += v
      else:
        residue_counts[k] = v
    num_residues = len(wt_counts)
    if num_residues in num_residue_counts.keys():
      num_residue_counts[num_residues] += 1
    else:
      num_residue_counts[num_residues] = 1
    
    mt_counts = entry['mutated_atoms']['resname'].value_counts()
    # residue_counts = {k: v if k not in residue_counts.keys() else v + residue_counts[k]
    #                   for k, v in mt_counts.items()}
    for k, v in mt_counts.items():
      if k in residue_counts.keys():
        residue_counts[k] += v
      else:
        residue_counts[k] = v
    
    num_residues = len(mt_counts)
    if num_residues in num_residue_counts.keys():
      num_residue_counts[num_residues] += 1
    else:
      num_residue_counts[num_residues] = 1
    
    residues = np.unique(np.concatenate([residues, entry_residues]))
    print(f'done {i}/{len(dataset)}')
    i += 1
  pd.Series(residue_counts).to_csv('../../output/tests/res_count.csv', header=['count'], index=True,
                                   index_label='residue')
  pd.Series(num_residue_counts).to_csv('../../output/tests/num_residue_counts.csv',
                                       header=['count'], index=True,
                                       index_label='num_residues')
  pd.Series(residues).to_csv('../../output/tests/res.csv', header=['resname'],
                             index_label=['index'])


# class ResidueGraphDglDataset():


class ResidueGraphDataset(Dataset):
  def __init__(self, residue_root, transform=None):
    self.residue_root = residue_root
    self.csv = pd.read_csv(f'{self.residue_root}/labels.csv')
    self.res_ids = {k: v for v, k in
                    list(pd.read_csv(f'{self.residue_root}/residue_names.csv').values)}
    self.transform = transform
  
  def __len__(self):
    return len(self.csv)
  
  def __getitem__(self, idx):
    if torch.is_tensor(idx):
      idx = idx.tolist()
    
    wt_res = nx.read_gpickle(
      f'{self.residue_root}/wild_types/{self.csv.iloc[idx]["wild_type_file"]}')
    mt_res = nx.read_gpickle(f'{self.residue_root}/mutated/{self.csv.iloc[idx]["mutated_file"]}')
    
    sample = {'wild_type': wt_res, 'mutated': mt_res, 'label': self.csv.iloc[idx]['label']}
    
    if self.transform:
      sample = self.transform(sample)
    
    return sample
  
  def nxgraph_to_tensors(self, nxgraph, label=None):
    node_features = pd.DataFrame(nxgraph.nodes().values())
    chain_indices = {k: v for v, k in enumerate(node_features['chain_id'].value_counts().keys())}
    node_features = node_features.replace({'chain_id': chain_indices})
    chain_id = np.eye(8)[np.array(node_features['chain_id'])]
    node_features = node_features.replace({'residue_name': self.res_ids})
    res_ids = np.eye(len(self.res_ids))[np.array(node_features['residue_name'])]
    coords = np.stack(node_features['coords'])
    bf = np.expand_dims(node_features['b_factor'], 1)
    x = np.concatenate([chain_id, res_ids, coords, bf], axis=1)
    adj = nx.adjacency_matrix(nxgraph).todense()
    gutil.dense_to_sparse(torch.tensor(nx.adjacency_matrix(nxgraph).todense()))
    
    x = torch.from_numpy(x).float()
    coo_adj = coo_matrix(nx.adjacency_matrix(nxgraph))
    edge_index = torch.from_numpy(
      np.concatenate([np.expand_dims(coo_adj.col, 0), np.expand_dims(coo_adj.row, 0)])) \
      .type(torch.LongTensor)
    # .float()
    edge_attr = torch.from_numpy(np.expand_dims(coo_adj.data, axis=1))  # .float()
    
    data = gdata.Data(x=x, edge_index=edge_index, edge_attr=edge_attr)
    if label is not None:
      data.y = torch.tensor(label)
  
  def nxgraph_to_dglgraph(self, nxgraph, label=None):
    node_features = pd.DataFrame(nxgraph.nodes().values())
    chain_indices = {k: v for v, k in enumerate(node_features['chain_id'].value_counts().keys())}
    node_features = node_features.replace({'chain_id': chain_indices})
    chain_id = np.eye(8)[np.array(node_features['chain_id'])]
    node_features = node_features.replace({'residue_name': self.res_ids})
    res_ids = np.eye(len(self.res_ids))[np.array(node_features['residue_name'])]
    coords = np.stack(node_features['coords'])
    bf = np.expand_dims(node_features['b_factor'], 1)
    x = np.concatenate([chain_id, res_ids, coords, bf], axis=1)
    
    g = dgl.DGLGraph()
    x = torch.from_numpy(x).float()
    g.add_nodes(num=x.shape[0], data={'x': x})
    g.add_edges(torch.tensor(coo_matrix(nx.adjacency_matrix(nxgraph)).col).type(torch.int64),
                torch.tensor(coo_matrix(nx.adjacency_matrix(nxgraph)).row).type(torch.int64))
    # add self loops
    g.add_edges(g.nodes(), g.nodes())
    
    return g
    # coo_adj = coo_matrix(nx.adjacency_matrix(nxgraph))
    # edge_index = torch.from_numpy(np.concatenate([np.expand_dims(coo_adj.col, 0), np.expand_dims(coo_adj.row, 0)]))\
    #     .type(torch.LongTensor)
    #     # .float()
    # edge_attr = torch.from_numpy(np.expand_dims(coo_adj.data, axis=1))#.float()
    #
    #
    # data = gdata.Data(x=x, edge_index=edge_index, edge_attr=edge_attr)
    # if label is not None:
    #     data.y = torch.tensor(label)
    #
    # return data
  
  def collate_fn(self, sample_list):
    labels = [sample['label'] for sample in sample_list]
    wild_types = [sample['wild_type'] for sample in sample_list]
    wildtype_tensors = [self.nxgraph_to_dglgraph(w) for w in wild_types]
    mutants = [sample['mutated'] for sample in sample_list]
    mutant_tensors = [self.nxgraph_to_dglgraph(m) for m in mutants]
    
    return dgl.batch(wildtype_tensors), \
           dgl.batch(mutant_tensors), \
           torch.tensor(labels)
  
  def collate_fn_old(self, sample_list):
    labels = [sample['label'] for sample in sample_list]
    # labels = torch.tensor(labels)
    
    wild_types = [sample['wild_type'] for sample in sample_list]
    wildtype_tensors = [self.nxgraph_to_tensors(w) for w in wild_types]
    # wildtype_x = [w[0] for w in wildtype_tensors]
    # wildtype_x = torch.block_diag(*wildtype_x)
    # wildtype_adj = [w[1] for w in wildtype_tensors]
    # wildtype_adj = torch.block_diag(*wildtype_adj)
    wildtype_batch = gdata.Batch.from_data_list(wildtype_tensors)
    
    mutants = [sample['mutated'] for sample in sample_list]
    mutant_tensors = [self.nxgraph_to_tensors(m, l) for m, l in zip(mutants, labels)]
    # mutant_x = [m[0] for m in mutant_tensors]
    # mutant_adj = [m[1] for m in mutant_tensors]
    # mutant_x = torch.block_diag(*mutant_x)
    # mutant_adj = torch.block_diag(*mutant_adj)
    mutant_batch = gdata.Batch.from_data_list(mutant_tensors)
    
    return (wildtype_batch, mutant_batch)


def validate_dir(dir):
  if not os.path.exists(dir):
    os.makedirs(dir)
  return dir


def create_residue_graph_dataset(pdb_root, residue_root):
  params = {"granularity": "CA"}
  config = ProteinGraphConfig(**params)
  mutated_res_dir = validate_dir(f'{residue_root}/mutated')
  wild_type_res_dir = validate_dir(f'{residue_root}/wild_types')
  pdb_labels_csv = pd.read_csv(f'{pdb_root}/labels.csv')
  res_labels_csv = f'{residue_root}/labels.csv'
  if os.path.exists(res_labels_csv):
    df = pd.read_csv(res_labels_csv)
  else:
    df = pd.DataFrame(columns=list(pdb_labels_csv.columns))
  for entry in pdb_labels_csv.values:
    index = entry[0]
    id = entry[1]
    wt_pdb_fname = entry[2]
    mt_pdb_fname = entry[3]
    label = entry[4]
    wt_res_fname = f'{wt_pdb_fname[:-4]}.gpkl'
    mt_res_fname = f'{mt_pdb_fname[:-4]}.gpkl'
    
    if wt_res_fname not in df['wild_type_file'].unique():
      original_res = construct_graph(config=config,
                                     pdb_path=f'{pdb_root}/wild_types/{wt_pdb_fname}')
      nx.write_gpickle(original_res, f'{wild_type_res_dir}/{wt_res_fname}')
    if mt_res_fname not in df['mutated_file'].unique():
      mutated_res = construct_graph(config=config, pdb_path=f'{pdb_root}/mutated/{mt_pdb_fname}')
      nx.write_gpickle(mutated_res, f'{mutated_res_dir}/{mt_res_fname}')
    
    row = [index, id, wt_res_fname, mt_res_fname, label]
    df.loc[index] = row
    print(f'Done {len(df)}/{len(pdb_labels_csv.values)}')
    df.to_csv(f'{res_labels_csv}', index=False, header=True)


def nx_residue_graph_to_tensors(nx_graph: nx.Graph):
  node_list = nx_graph.nodes()
  print('test')
  # get_unique_residues()


class GCN(nn.Module):
  def __init__(self, inp_dim, out_dims):
    super(GCN, self).__init__()
    self.gcn_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    dimensions = [inp_dim] + out_dims
    for i in range(len(out_dims)):
      # self.gcn_layers.append(gnn.GCNConv(dimensions[i], dimensions[i+1]))
      self.gcn_layers.append(GraphConv(dimensions[i], dimensions[i + 1]))
      self.batchnorms.append(BatchNorm1d(dimensions[i + 1]))
    self.fc1 = Linear(dimensions[-1] * 2, int(dimensions[-1] / 2))
    self.fc1bn = BatchNorm1d(int(dimensions[-1] / 2))
    self.fc2 = Linear(int(dimensions[-1] / 2), int(dimensions[-1] / 4))
    self.fc2bn = BatchNorm1d(int(dimensions[-1] / 4))
    self.fcout = Linear(int(dimensions[-1] / 4), 2)
  
  def forward(self, wild, mut):
    # wt_x, wt_edge_index, wt_edge_attr = wild.x, wild.edge_index, wild.edge_attr
    # mt_x, mt_edge_index, mt_edge_attr = mut.x, mut.edge_index, mut.edge_attr
    
    w_feat = wild.ndata['x']
    m_feat = mut.ndata['x']
    
    # wt_edge_index = wt_edge_index.type(torch.I)
    # wt_x = wt_x.type(torch.DoubleTensor)
    
    for i in range(len(self.gcn_layers)):
      w_feat = self.gcn_layers[i](wild, w_feat)  # , wt_edge_attr)
      m_feat = self.gcn_layers[i](mut, m_feat)  # , mt_edge_attr)
      # w_feat = self.gcn_layers[i](w_feat, wild)#, wt_edge_attr)
      # m_feat = self.gcn_layers[i](m_feat, mut)#, mt_edge_attr)
      w_feat = F.relu(w_feat)
      m_feat = F.relu(m_feat)
      w_feat = self.batchnorms[i](w_feat)
      m_feat = self.batchnorms[i](m_feat)
      wild.ndata['x'] = w_feat
      mut.ndata['x'] = m_feat
    
    # wt_x = scatter.scatter_mean(mt_x, mut.batch)
    w_feat = dgl.mean_nodes(wild, 'x')
    m_feat = dgl.mean_nodes(mut, 'x')
    x = torch.cat((w_feat, m_feat), dim=1)
    x = self.fc1(x)
    x = F.relu(x)
    x = self.fc1bn(x)
    x = self.fc2(x)
    x = F.relu(x)
    x = self.fc2bn(x)
    x = self.fcout(x)
    x = F.softmax(x, dim=1)
    return x


# def test_loop():
#     model = GCN()

def test():
  batch_size = 25
  batch_report_freq = 20
  epochs = 100
  LR = 1e-5
  res_dataset = ResidueGraphDataset(f'../data/res_graphs', transform=None)
  dataloader = DataLoader(res_dataset, batch_size=batch_size,
                          collate_fn=res_dataset.collate_fn,
                          shuffle=True, prefetch_factor=2)
  # model = GCN(32, [64, 128, 256, 512, 1024, 2048])
  # model = GCN(32, [64, 128, 256, 512, 1024, 2048])
  model = GCN(32, [128] * 64)
  model = GCN(32, [2048, 4096])
  model = model.float()
  model.to(device)
  optimiser = torch.optim.Adam(model.parameters(), lr=LR)
  loss_fn = nn.NLLLoss()
  
  for epoch in range(epochs):
    epoch_loss = 0
    epoch_correct = 0
    epoch_total = 0
    report_loss = 0
    report_correct = 0
    report_total = 0
    start = time.time()
    
    for i, data in enumerate(dataloader, 0):
      wilds, muts, labels = data
      wilds, muts, labels = wilds.to(device), muts.to(device), labels.to(device)
      optimiser.zero_grad()
      outputs = model(wilds, muts)
      loss = loss_fn(outputs, labels)
      loss.backward()
      optimiser.step()
      
      logits, predicted = torch.max(outputs.data, dim=1)
      batch_correct = (predicted == labels).sum().item()
      epoch_correct += batch_correct
      epoch_total += len(labels)
      epoch_loss += loss.item()
      report_loss += loss.item()
      report_correct += batch_correct
      report_total += len(labels)
      
      if i % batch_report_freq == batch_report_freq - 1:
        # print(f'Batch {i+1}/{len(dataloader)}: accuracy = {round(batch_correct / len(labels), 4)}%; loss = {round(loss.item(), 4)}')
        print(
          f'Batch {i + 1}/{len(dataloader)}. Last {batch_report_freq} batches: accuracy = {round(report_correct / report_total, 4)}%; loss = {round(report_loss, 4)}')
        report_correct = 0
        report_total = 0
        report_loss = 0
    
    print(f'#####################################################')
    print(
      f'Epoch {epoch + 1}: accuracy = {round(epoch_correct / epoch_total, 4)}%; loss = {round(epoch_loss, 4)}')
    print(f'Time = {time.time() - start}')
    # print(f'Last predicted: {predicted}')
    print(f'Predicted more stable = {predicted.sum().item() / len(labels)}')
    # print(f'Last outputs: {logits}')
    print(f'Mean confidence: {logits.sum().item() / len(labels)}')
  print('test')


if __name__ == '__main__':
  # get_unique_residues()
  # csv = pd.read_csv('../data/res_graphs/labels.csv')
  # create_residue_graph_dataset(f'../data/new_pdb', '../data/res_graphs')
  # res_dataset = ResidueGraphDataset(f'../data/res_graphs', transform=None)
  # get_unique_chain_ids(res_dataset)
  test()
