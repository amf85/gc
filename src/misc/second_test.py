import os

import atom3d.datasets as da
import atom3d.util.formats as a3fmt
import pandas as pd
from Bio.PDB import PDBParser
from Bio.PDB.DSSP import DSSP
from Bio.PDB.PDBIO import PDBIO

# from graphein.protein.config import

MSP_LOC = '../data/new_msp/raw/MSP/data'


def validate_dir(dir):
  if not os.path.exists(dir):
    os.makedirs(dir)
  return dir


def a3d_dataset_to_pdb(out_dir):
  file = '{}/'.format(MSP_LOC)
  dataset = da.load_dataset(file, 'lmdb')
  
  wt_dir = validate_dir("%s/%s" % (out_dir, 'wild_types'))
  mt_dir = validate_dir("%s/%s" % (out_dir, 'mutated'))
  labels_csv = f'{out_dir}/labels.csv'
  headers = ['index', 'id', 'wild_type_file', 'mutated_file', 'label']
  if os.path.exists(labels_csv):
    df = pd.read_csv(labels_csv)
  else:
    df = pd.DataFrame(columns=headers)
  io = PDBIO()
  for entry in dataset:
    id = entry['id']
    if id in df['id'].unique():
      continue
    columns = entry['original_atoms']['columns']
    mutated_fname = entry['mutated_atoms']['data'][0][0]
    wild_type_fname = entry['original_atoms']['data'][0][0]
    wild_type = pd.DataFrame(entry['original_atoms']['data'], columns=columns)
    mutated = pd.DataFrame(entry['mutated_atoms']['data'], columns=columns)
    if wild_type_fname not in df['wild_type_file'].unique():
      wt_bp = a3fmt.df_to_bp(wild_type)
      io.set_structure(wt_bp)
      io.save("{}/{}".format(wt_dir, wild_type_fname))
    if mutated_fname not in df['mutated_file'].unique():
      mt_bp = a3fmt.df_to_bp(mutated)
      io.set_structure(mt_bp)
      io.save("{}/{}".format(mt_dir, mutated_fname))
    
    label = entry['label']
    row = [len(df), id, wild_type_fname, mutated_fname, label]
    df.loc[len(df)] = row
    print(f'Done {len(df)}/{len(dataset)}')
    
    df.to_csv(f'{out_dir}/labels.csv', index=False, header=True)


def pdb_to_dssp(pdb_dir):
  labels = pd.read_csv(f'{pdb_dir}/labels.csv')
  mutated_dir = f'{pdb_dir}/mutated'
  wild_type_dir = f'{pdb_dir}/wild_types'
  p = PDBParser()
  for idx, row in labels.iterrows():
    mutated_file = row.mutated_file
    structure = p.get_structure(mutated_file[:-4], f'{mutated_dir}/{mutated_file}')
    model = structure[0]
    dssp = DSSP(model, f'{mutated_dir}/{mutated_file}')
    print('test')


def test():
  new_pdb_dir = '../../data/new_pdb'
  a3d_dataset_to_pdb(new_pdb_dir)
  pdb_to_dssp(new_pdb_dir)
  print('test')


if __name__ == '__main__':
  test()
