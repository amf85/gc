from abc import abstractmethod


class GCParams(object):
  def __init__(self, **kwargs):
    super(GCParams, self).__init__()
    self.__dict__.update(kwargs)
  
  @abstractmethod
  def sql_sanitise(self):
    raise NotImplementedError()
