import json
import os
import pickle
from time import process_time

import atom3d.datasets.datasets as da
import atom3d.util.graph as gr
import numpy as np
# from atom3d.models.gnn import GCN
# from atom3dsrc import gnn
# from atom3d.models.ff import FeedForward
# from atom3dsrc import ff
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import BatchNorm1d, Linear
from torch_geometric.nn import GCNConv

LEARN_RATE = 1e-4
EPOCHS = 5
FEAT_DIM = 128
OUT_DIM = 1

MSP_LOC = '/data/atom3d/msp/mutation_prediction/'


def prepare_interactions():
  dataset = da.load_dataset(MSP_LOC + 'raw/', 'pdb')
  get_dataset_entry_metadata(dataset)
  graphs = []
  print(f'Dataset size = {len(dataset)}')
  labels = pd.read_csv((MSP_LOC + 'raw/mutated/data.csv'), header=None)
  
  ordered_labels, ordered_wild_types = get_ordered_names(dataset, labels)
  
  pd.DataFrame(ordered_labels).to_csv('../../data/msp/ordered_labels.csv', header=False,
                                      index=False)
  pd.DataFrame(ordered_wild_types).to_csv('../../data/msp/ordered_wild_types.csv', header=False,
                                          index=False)
  # pair_names = [(i.name[:i.name.rfind('_')], i.name[i.name.rfind('_'):][1:-4]) for i in dataset._file_list]
  # with_labels = [labels.loc[(labels[0] == pair[0]) & (labels[1] == pair[1])].values for pair in pair_names]
  
  prepare_mutant(dataset, ordered_labels, 0)
  
  # wild_type_names = labels[0].unique()
  
  # for interaction in dataset[:100]:
  #     node_feats, edges, edge_weights, node_pos = gr.prot_df_to_graph(interaction, edge_dist_cutoff=4.5)
  #     graphs.append((node_feats, edges, edge_weights, node_pos))
  
  print('test')


def get_dataset_entry_metadata(dataset):
  ids = []
  filepaths = []
  atoms = []
  for i in range(len(dataset)):
    entry = dataset[i]
    ids.append(entry['id'])
    filepaths.append(entry['file_path'])
    atoms.append(entry['atoms'])
    print(i)
  csv = pd.DataFrame({'id': ids, 'file_path': filepaths})
  csv.to_csv('../data/msp/dataset_entry_metadata.csv')
  df = pd.DataFrame({'id': ids, 'file_path': filepaths, 'atoms': atoms})
  df.to_pickle('../data/msp/dataset_df.pkl')


def make_entry_metadata_dict():
  entry_metadata = pd.read_csv('../../data/msp/dataset_entry_metadata.csv')
  entry_metadata_dict = {entry_metadata['id'].values[i][:-4]: {'idx': i,
                                                               'file_name':
                                                                 entry_metadata['id'].values[i],
                                                               'file_path':
                                                                 entry_metadata['file_path'].values[
                                                                   i]}
                         for i in range(len(entry_metadata))}
  pickle.dump(entry_metadata_dict, open('../../data/msp/dataset_entry_metadata.pkl', 'wb'))
  
  print('test')


def get_ordered_names(dataset, labels):
  ordered_labels = []
  ordered_wild_types = []
  for ppath in dataset._file_list:
    if ppath.parent.named_identifier == 'mutated':
      sep_idx = ppath.named_identifier.rfind('_')
      wild_name = ppath.named_identifier[:sep_idx]
      mut_name = ppath.named_identifier[sep_idx:][1:-4]
      label = labels.loc[(labels[0] == wild_name) & (labels[1] == mut_name)].values[0, 2]
      ordered_labels.append((wild_name, mut_name, label))
    elif ppath.parent.named_identifier == 'original':
      ordered_wild_types.append(ppath.named_identifier[:-4])
  return ordered_labels, ordered_wild_types


def prepare_mutant(dataset, ordered_labels, idx):
  atoms = dataset[idx]['atoms']
  node_feats, edges, edge_weights, node_pos = gr.prot_df_to_graph(atoms, edge_dist_cutoff=4.5)
  
  return node_feats, edges


class GCN(nn.Module):
  
  def __init__(self, inp_dim, out_dims):
    super(GCN, self).__init__()
    self.gcn_layers = nn.ModuleList()
    self.batchnorms = nn.ModuleList()
    # self.testlayer = GCNConv(inp_dim, out_dims[-1])
    dimensions = [inp_dim] + out_dims
    for i in range(len(dimensions) - 1):
      self.gcn_layers.append(GCNConv(dimensions[i], dimensions[i + 1]))
      self.batchnorms.append(BatchNorm1d(dimensions[i + 1]))
    self.fc1 = Linear(dimensions[-1] * 2, int(dimensions[-1] / 2))
    self.fc1bn = BatchNorm1d(int(dimensions[-1] / 2))
    self.fc2 = Linear(int(dimensions[-1] / 2), 2)
  
  def forward(self, g1, g2):
    x1, edge_index_1, edge_weight_1 = g1[0], g1[1], g1[2]
    x2, edge_index_2, edge_weight_2 = g2[0], g2[1], g2[2]
    for i in range(len(self.gcn_layers)):
      x1 = self.gcn_layers[i](x1, edge_index_1, edge_weight_1)
      x2 = self.gcn_layers[i](x2, edge_index_2, edge_weight_2)
      x1 = F.relu(x1)
      x2 = F.relu(x2)
      # x1 = self.batchnorms[i](x1)
      # x2 = self.batchnorms[i](x2)
    
    x1 = torch.mean(x1, dim=0)
    x2 = torch.mean(x2, dim=0)
    x = torch.cat((x1, x2), dim=0)
    x = self.fc1(x)
    x = F.relu(x)
    # x = self.fc1bn(x)
    x = self.fc2(x)
    x = F.softmax(x, dim=0)
    
    return x


def make_tensor_set_from_df(dataset, entry_metadata):
  complexes = {}
  i = 0
  shard_metadata = {}
  shard_metadata['filenames'] = []
  shard_metadata['shards'] = 0
  shard_metadata['expected_samples'] = len(dataset)
  shard_metadata['total_samples'] = 0
  shard = 0
  os.makedirs('../../data/msp/complexes_shards/')
  for key, value in entry_metadata.items():
    idx = value['idx']
    atoms = dataset[idx]['atoms']
    nodes, edges, weights, pos = gr.prot_df_to_graph(atoms, edge_dist_cutoff=4.5)
    cmplx = {'idx': idx, 'file_name': value['file_name'], 'file_path': value['file_path'],
             'nodes': nodes, 'edges': edges, 'weights': weights}
    complexes[key] = cmplx
    print(i)
    i += 1
    shard_metadata['total_samples'] += 1
    if i % 100 == 0:
      pickle.dump(complexes, open(f'../data/msp/complexes_shards/complex_{shard}.pkl', 'wb'))
      shard_metadata['shards'] += 1
      shard_metadata['filenames'].append(f'complex_{shard}.pkl')
      json.dump(shard_metadata, open('../../data/msp/complexes_shards/metadata.json', 'w'))
      shard += 1
      complexes = {}
  
  if i % 100 != 0:
    # shard += 1
    shard_metadata['shards'] += 1
    shard_metadata['filenames'].append(f'complex_{shard}.pkl')
    pickle.dump(complexes, open(f'../data/msp/complexes_shards/complex_{shard}.pkl', 'wb'))
    json.dump(shard_metadata, open('../../data/msp/complexes_shards/metadata.json', 'w'))


def recover_dataset_from_shards():
  shard_metadata = json.load(open('../../data/msp/complexes_shards/metadata.json', 'r'))
  complexes = {}
  for filename in shard_metadata['filenames']:
    if filename == 'complex_45.pkl':
      continue
    shard = pickle.load(open(f'../data/msp/complexes_shards/{filename}', 'rb'))
    for key, value in shard.items():
      complexes[key] = value
  return complexes


def test_loop():
  # dataset = da.load_dataset(MSP_LOC + 'raw/', 'pdb')
  ordered_labels = pd.read_csv('../../data/msp/ordered_labels.csv')
  # ordered_wild_types = pd.read_csv('../data/msp/ordered_wild_types.csv')
  # csv_metadata = pd.read_csv('../data/msp/dataset_entry_metadata.csv')
  # entry_metadata = pickle.load(open('../data/msp/dataset_entry_metadata.pkl', 'rb'))
  # make_tensor_set_from_df(dataset, entry_metadata)
  print('loading dataset...')
  start = process_time()
  # dataset_df = pickle.load(open('../data/msp/dataset_df.pkl', 'rb'))
  complexes = recover_dataset_from_shards()
  print(f'done in {process_time() - start}s')
  LEARN_RATE = 1e-4
  
  model = GCN(18, [40, 80, 160, 160, 160])
  # params = model.parameters()
  optimiser = torch.optim.Adam(model.parameters(), lr=LEARN_RATE)
  loss_fn = nn.NLLLoss()
  
  # train_lbls = ordered_labels[:3000]
  train_size = 3000
  EPOCHS = 100000
  crude_accs = {}
  b = 0
  start = process_time()
  for e in range(1, EPOCHS):
    crude_right = 0
    crude_wrong = 0
    idx = np.random.randint(train_size)
    mut_label = ordered_labels.values[idx]
    wild_name = mut_label[0]
    mut_name = wild_name + '_' + mut_label[1]
    # wild_idx = entry_metadata[wild_name]['idx']
    # mut_idx = entry_metadata[mut_name]['idx']
    # wild_atoms = dataset[wild_idx]['atoms']
    # mut_atoms = dataset[mut_idx]['atoms']
    wild = complexes[wild_name]
    mut = complexes[mut_name]
    y = mut_label[2]
    
    wild_nodes, wild_edges, wild_weights = wild['nodes'], wild['edges'], wild['weights']
    mut_nodes, mut_edges, mut_weights = mut['nodes'], mut['edges'], mut['weights']
    
    g1 = (wild_nodes, wild_edges, wild_weights)
    g2 = (mut_nodes, mut_edges, mut_weights)
    
    out = model(g1, g2)
    # out = model(g1, g2).cuda()
    loss = loss_fn(torch.unsqueeze(out, dim=0), torch.tensor([y]))
    # loss = loss_fn(torch.unsqueeze(out, dim=0), torch.tensor([y]).cuda())
    loss.backward()
    optimiser.step()
    
    # print(torch.argmax(out)==y)
    
    if torch.argmax(out) == y:
      print(f'Sample {e}: Correct')
      crude_right += 1
    else:
      print(f'Sample {e}: Wrong')
      crude_wrong += 1
    
    if e % 100 == 0:
      b += 1
      print(process_time() - start)
      start = process_time()
      crude_accs[b] = (crude_right / (crude_right + crude_wrong))
      crude_right = 0
      crude_wrong = 0
      json.dump(crude_accs, open('../../output/tests/crude_accs.json', 'w'))
    
    # print('test')
    
    # wild_atoms = dataset[len(ordered_labels) + ]


if __name__ == '__main__':
  dataset = da.load_dataset(MSP_LOC + 'split/', 'lmdb')
  
  # testfile = MSP_LOC + 'split/pairs_train_0002_40.h5'
  # # testfile = MSP_LOC + 'sharded/pairs_0002_40.h5'
  # # testfile = MSP_LOC + 'split/pairs_train_meta_40.h5'
  # with h5py.File(testfile, 'r') as f:
  #     keys = []
  #     for key in f.keys():
  #         print(key)
  #         keys.append(key)
  #     print(f.keys())
  #     d1 = list(f[list(f.keys())[0]])
  #     d2 = list(f[list(f.keys())[1]])
  #     d11 = f.get('axis0')
  #     print('test')
  
  # make_entry_metadata_dict()
  test_loop()
  
  # prepare_interactions()
  #
  # # dataset = da.load_dataset('/data/atom3d/msp/mutation_prediction/', 'pdb', transform=tr.graph_transform)
  # dataset = da.load_dataset('/data/atom3d/msp/mutation_prediction/', 'pdb')
  # dataloader = DataLoader(dataset, batch_size=5, shuffle=True)
  #
  # for batch in dataloader:
  #     graph = batch['atoms']
  #     in_dim = graph.num_features
  #     break
  #
  # feat_model = gnn.GCN(in_dim, FEAT_DIM)
  # out_model = ff.FeedForward(FEAT_DIM, [64], OUT_DIM)
  #
  # params = [x for x in feat_model.parameters()] + [x for x in out_model.parameters()]
  # optimizer = torch.optim.Adam(params, lr=LEARN_RATE)
  # criterion = nn.BCEWithLogitsLoss()
  #
  # for epoch in range(EPOCHS):
  #     for batch in dataloader:
  #         labels = batch['label'].float()
  #         graph = batch['atoms']
  #         feats = feat_model(graph.x, graph.edge_index, graph.edge_attr, graph.batch)
  #         out = out_model(feats)
  #         loss = criterion(out.view(-1), labels)
  #         loss.backward()
  #         optimizer.step()
  #     print('Epoch {}: train loss {}'.format(epoch, loss))
