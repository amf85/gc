import pickle
from abc import ABC, abstractmethod
from functools import partial
from queue import Queue
from typing import Any
import os
from pathlib import Path
import json

import dgl
import networkx as nx
import numpy as np
import scipy as sp
import scipy.stats
import scipy.sparse as sps
import torch
from better_abc import abstract_attribute
from sqlalchemy import or_, select
from torch.utils.data import Dataset

from sql.sql_base import GCDataSplitBase, GCSampleBase, DataSplitAnalysisBase
from util.util import validate_directory, printProgressBar
# from analysis.dataset_composition import DCAnalysis
from util.constants import *

NODE_LEVEL_FEATURES = [
  'degree_centrality',
  'closeness_centrality',
  'betweenness_centrality',
  'clustering_coeff',
  'clustering_coeff_sq',
  'average_neighbour_degree',
  'eccentricity',
  'is_periphery',
  'is_centre',
]
DISTANCE_DEPENDENT_FEATURES = [
    'closeness_centrality',
    'eccentricity',
    'is_periphery',
    'is_centre',
    'radius',
    'diameter',
    'periphery_coeff'
  ]
GRAPH_LEVEL_FEATURES = [
  'n_nodes',
  'n_edges',
  'density',
  'transitivity',
  'transitivity_sq',
  'mean_eccentricity',
  'diameter',
  'radius',
  'periphery_coeff',
  'centre_coeff',
  'degree_assortativity',
  # 'avg_clustering_coeff',  # aka transitivity
  # 'avg_clustering_coeff_sq',
]
stats = {'mean': np.mean, 'median': np.median, 'min': np.min, 'max': np.max, 'std': np.std,
           'skew': sp.stats.skew, 'kurtosis': sp.stats.kurtosis}


class GCKFolds(Dataset):
  def __init__(self, sample_ids, file_index, transform=None):
    self.sample_ids = sample_ids
    self.file_index = file_index
    self.transform = transform

  def __len__(self):
    return len(self.sample_ids)

  def __getitem__(self, index):
    if torch.is_tensor(index):
      index = index.tolist()
    sample = pickle.load(open(f'{self.file_index[index]}', 'rb'))
    if self.transform:
      sample = self.transform(sample)
    return sample



class GCDataSplit(Dataset):
  @property
  def datasplits_table(self):
    return 'datasplits'
  
  @property
  @abstractmethod
  def db_table(self):
    ...
  
  @property
  @abstractmethod
  def split_class(self):
    ...
  
  class GCSplitParams(object):
    def __init__(self,
                 n_samples,
                 preprocess_spectral=0,
                 preprocess_distance=False,
                 preprocess_local_features=None,
                 preprocess_global_features=None,
                 preprocess_top_down_methods=None,
                 # top_down_n_splits=0,
                 # top_down_vectors_per_split=0,
                 # top_down_assignments=False,
                 
                 # used_n_spectral_vectors=0,
                 # used_distance_matrix=True,
                 # used_local_features=None,
                 # used_top_down_methods=None,
                 # used_top_down_n_splits=0,
                 # used_top_down_vectors_per_split=0,
                 # used_top_down_assignments=False,
                 ):
      super(GCDataSplit.GCSplitParams).__init__()
      self.n_samples = n_samples
      self.preprocess_spectral = preprocess_spectral
      self.preprocess_distance = preprocess_distance
      self.preprocess_local_features = preprocess_local_features
      self.preprocess_global_features = preprocess_global_features
      self.preprocess_top_down_methods = preprocess_top_down_methods
      # self.top_down_n_splits = top_down_n_splits
      # self.top_down_vectors_per_split = top_down_vectors_per_split
      # self.top_down_assignments = top_down_assignments
      #
      # self.used_n_spectral_vectors = used_n_spectral_vectors
      # self.used_distance_matrix = used_distance_matrix
      # self.used_local_features = used_local_features
      # self.used_top_down_methods = used_top_down_methods
      # self.used_top_down_n_splits = used_top_down_n_splits
      # self.used_top_down_vectors_per_split = used_top_down_vectors_per_split
      # self.used_top_down_assignments = used_top_down_assignments
  
  def __init__(self,
               dataset,
               params: GCSplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=False,
               named_identifier=None,
               overwrite_identifier=False,
               verbose=1,
               ):
    super(GCDataSplit, self).__init__()
    self.dataset = dataset
    self.named_identifier = named_identifier
    self.overwrite_identifier = overwrite_identifier
    self.params = params
    self.exclude_sample_ids = exclude_sample_ids
    self.split = split
    self.transform = transform
    self.split_dir = validate_directory(f'{dataset.dataset_dir}/{self.split_class}')
    
    ## the following is what happens if you code past your bedtime
    if named_identifier is not None:
      session = self.dataset.gcdb.local_sess()
      matching = session.execute(select(GCDataSplitBase)
                                 .where(GCDataSplitBase.name==named_identifier)).all()
      assert len(matching) < 2, f'found multiple data splits with identifier "{named_identifier}"'

      if overwrite_identifier:
        inp = input(f'Type "y" to confirm overwrite of data split {named_identifier}, "n" to use existing:')
        if inp == "y":
          overwrite_identifier = True
        elif inp == "n":
          overwrite_identifier = False
        else:
          exit(0)

      if len(matching) == 1 and overwrite_identifier:
        # overwrite_identifier = input(f'Type "y" to confirm overwrite of data split {named_identifier}:') == 'y'
        # if not overwrite_identifier:
        #   exit(0)
        ds = matching[0][0]
        ds.name = None
        session.commit()
        self.id = self.add_datasplits_sql_entry()
        self.add_sql_entry()
        self.file_index, self.sample_ids = self.add_samples(
          params=params, create_new=create, preprocess_local=preprocess_local,
          preprocess_global=preprocess_global, preprocess_eig=preprocess_eig,
          preprocess_distance=preprocess_distance, feature_conf=feature_conf)
      elif len(matching) == 1:
        ds = matching[0][0]
        self.id = ds.id
        self.sample_ids = [s.id for s in ds.samples]
        self.file_index = [s.file for s in ds.samples]
        class_specific_split_rows = session.execute(
          f'select * from {ds.table_name} where id = {ds.id}').all()
        if len(class_specific_split_rows) == 0 or class_specific_split_rows[0].n_samples != len(self.file_index):
          print(f'Corrupt data split with name "{named_identifier}, '
                f'Overwriting with received parameters.')
          ds.name = None
          session.commit()
          self.id = self.add_datasplits_sql_entry()
          self.add_sql_entry()
          self.file_index, self.sample_ids = self.add_samples(
            params=params, create_new=create, preprocess_local=preprocess_local,
            preprocess_global=preprocess_global, preprocess_eig=preprocess_eig,
            preprocess_distance=preprocess_distance, feature_conf=feature_conf)
        else:
          i = 0
          for sample in ds.samples:
            sample_pkl: GCSample = pickle.load(open(sample.file, 'rb'))
            if preprocess_distance and not sample_pkl.preprocessed_distance:
              sample_pkl.preprocess_distance_matrix(recompute=False)
            if any(f not in sample_pkl.preprocessed_global_features for f in preprocess_global) \
              or any(f not in sample_pkl.preprocessed_local_features for f in preprocess_local):
              sample_pkl.preprocess_graph_features(
                local_feature_names=preprocess_local, global_feature_names=preprocess_global)
            if not sample_pkl.verify_eig_conf_present(preprocess_eig):
              sample_pkl.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)

            if any(f not in sample_pkl.preprocessed_local_features for f in
                   feature_conf['local_features']):
              sample_pkl.preprocess_graph_features(
                local_feature_names=feature_conf['local_features'], global_feature_names=[])
            if not sample_pkl.verify_eig_conf_present(feature_conf['eig_conf']):
              sample_pkl.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'],
                                              force_recompute=False)
            
            i += 1
            if verbose < 1:
              printProgressBar(
                iteration=i,
                total=len(ds.samples),
                prefix=f'Verifying preprocessing for samples from split "{named_identifier}"',
                suffix=f'({i}/{len(ds.samples)})',
                length=50,
                decimals=2)
      else:
        self.id = self.add_datasplits_sql_entry()
        self.add_sql_entry()
        self.file_index, self.sample_ids = self.add_samples(
          params=params, create_new=create, preprocess_local=preprocess_local,
          preprocess_global=preprocess_global, preprocess_eig=preprocess_eig,
          preprocess_distance=preprocess_distance, feature_conf=feature_conf)
    else:
      self.id = self.add_datasplits_sql_entry()
      self.add_sql_entry()
      self.file_index, self.sample_ids = self.add_samples(
        params=params, create_new=create, preprocess_local=preprocess_local,
        preprocess_global=preprocess_global, preprocess_eig=preprocess_eig,
        preprocess_distance=preprocess_distance, feature_conf=feature_conf)
    
    self.push_analysis_to_db(session=self.dataset.gcdb.local_sess(), split_id=self.id)
    
  
  def __len__(self):
    return self.params.n_samples
  
  def __getitem__(self, index):
    if torch.is_tensor(index):
      index = index.tolist()
    sample = pickle.load(open(f'{self.file_index[index]}', 'rb'))
    if self.transform:
      sample = self.transform(sample)
    return sample
  
  def class_specific_setup(self, *args: Any):
    pass
  
  @staticmethod
  def validate_data_directories(root_dir):
    validate_directory(f'{root_dir}/{SAMPLES_SFX}')
    validate_directory(f'{root_dir}/{ADJ_INDEX_SFX}')
    validate_directory(f'{root_dir}/{ADJ_ATTR_SFX}')
    validate_directory(f'{root_dir}/{EIGS_SFX}')
    validate_directory(f'{root_dir}/{GLOBAL_FEATS_SFX}')
    validate_directory(f'{root_dir}/{LOCAL_FEATS_SFX}')
    validate_directory(f'{root_dir}/{DISTANCE_MAT_SFX}')
    validate_directory(f'{root_dir}/{PREDECESSORS_MAT_SFX}')
    validate_directory(f'{root_dir}/{INFO_SFX}')
    validate_directory(f'{root_dir}/{NODE_LBL_SFX}')
    validate_directory(f'{root_dir}/{GRAPH_LBL_SFX}')
    validate_directory(f'{root_dir}/{EDGE_LBL_SFX}')
    validate_directory(f'{root_dir}/{NODE_ATTR_SFX}')
    validate_directory(f'{root_dir}/{NODE_ATTR_IDX}')

  def add_datasplits_sql_entry(self):
    base = GCDataSplitBase(datasplit_class=self.split_class, table_name=self.db_table, fresh=True)
    if self.named_identifier is not None:
      base.name = self.named_identifier
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.flush()
    id = base.id
    session.commit()
    return id
  
  
  @staticmethod
  def push_analysis_to_db(session, split_id):
    base_gcsplit_rows = session.execute(
      select(GCDataSplitBase).where(GCDataSplitBase.id == split_id)).all()
    assert len(base_gcsplit_rows) < 2, f'found duplicate split ids for {split_id}'
    assert len(base_gcsplit_rows) > 0, f'could not find split by id supplied={split_id}'
    base_gcsplit_row = base_gcsplit_rows[0]
    base_gcsplit = base_gcsplit_row[0]
    
    if len(base_gcsplit.samples) > 0:
      file = base_gcsplit.samples[0].file
      root = file[:file.index(f'/{SAMPLES_SFX}/{base_gcsplit.samples[0].id}.pkl')]
      global_features_index = json.load(open(f'{root}/{INFO_SFX}/{GLOBAL_FEATS_IDX}.json', 'r'))
      
    else:
      return
    
    existing_analysis_base_rows = session.execute(
      select(DataSplitAnalysisBase).where(DataSplitAnalysisBase.id == split_id)).all()
    assert len(base_gcsplit_rows) < 2, f'found duplicate datasplitanalysisbase ids for {split_id}'
    if len(existing_analysis_base_rows) > 0:
      analysis_base = existing_analysis_base_rows[0][0]
    else:
      analysis_base = DataSplitAnalysisBase()
      analysis_base.id = split_id
    for feature in GRAPH_LEVEL_FEATURES:
      data = []
      for sample in base_gcsplit.samples:
        global_features = np.load(f'{root}/{GLOBAL_FEATS_SFX}/{sample.id}.npy')
        data.append(global_features[global_features_index[feature]])
      
      data = np.array(data)
      for stat_name, stat_method in stats.items():
        analysis_base.__dict__[f'{feature}_{stat_name}'] = stat_method(data)
    
    session.add(analysis_base)
    session.commit()
    
    
  
    
  
  @abstractmethod
  def add_samples(self,
                  params: GCSplitParams,
                  create_new: bool,
                  preprocess_eig=None,
                  preprocess_local=None,
                  preprocess_global=None,
                  preprocess_distance=False,
                  feature_conf=None,
                  ):
    ...
  
  @abstractmethod
  def add_sql_entry(self):
    ...
  
  @staticmethod
  @abstractmethod
  def collate_fn(sample_list, **kwargs):
    ...


class GCDataset(ABC):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(GCDataset, self).__init__()
    self.dataset_dir = dataset_dir
    self.gcdb = gcdb
    self.verbose = verbose
    self.split_params = {}
    self.splits = {}
  
  @abstractmethod
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ) -> GCDataSplit:
    ...


def is_connected(A, reverse=False):
  connected = set()
  q = Queue()
  q.put(0)
  while not q.empty():
    cur = q.get()
    neighbours = np.argwhere(A[cur])
    for neighbour in neighbours:
      if neighbour[1 if reverse else 0] not in connected:
        q.put(neighbour[1 if reverse else 0])
    connected.add(cur)
    if len(connected) == A.shape[0]:
      return True
  return False


def generate_random_graph(n_nodes, sparsity=0.5, exp_node_edges=None, ensure_connected=True):
  valid = False
  A = None
  if exp_node_edges is not None:
    sparsity = 1 - (exp_node_edges) / (2*(n_nodes - 1))
  f = np.vectorize(lambda x: 1 if x > sparsity else 0)
  # rejection sampling
  while not valid:
    A = np.random.rand(n_nodes, n_nodes)
    A = f(A)
    for i in range(n_nodes):
      A[i, i] = 0
    A = np.tril(A) + np.tril(A).T
    if ensure_connected and is_connected(A):
      valid = True
  return A.astype(np.int32)


class GCSample(object):
  db_table: str
  samples_table = 'samples'
  
  
  def __init__(self,
               session,
               sample_dir=None,
               ):
    super(GCSample, self).__init__()
    self.sample_dir = sample_dir
    self.components = {}
    
    self.preprocessed_distance = False
    self.preprocessed_global_features = []
    self.preprocessed_local_features = []
    self.preprocessed_eigen_features = {}

    self.id = self.add_general_sample_sql_entry(session)
    self.filename = f'{self.sample_dir}/{SAMPLES_SFX}/{self.id}.pkl'
    pickle.dump(self, open(f'{self.filename}', 'wb'))
    self.add_class_specific_sql_entry(session)
    
    self.generate()
    
  @staticmethod
  def get_features_input_dim(feature_config):
    dim = len(feature_config['local_features'])
    dim += len(feature_config['node_features'])
    eig_conf = feature_config['eig_conf']
    dim += sum(eig_conf['per_layer_eigenvectors'])
    dim += sum(eig_conf['per_layer_eigenvalues'])
    dim += sum(eig_conf['per_layer_assignments'])
    return dim
  
  
  def add_general_sample_sql_entry(self, session):
    base = GCSampleBase(
      sample_table=self.db_table,
      datasplits=[],
      file=f'temp',
      fresh=True,
    )
    
    session.add(base)
    session.flush()
    samples_dir = f'{self.sample_dir}/{SAMPLES_SFX}'
    if os.path.exists(f'{samples_dir}/{base.id}.pkl'):
      new_id = max([int(f.stem) for f in Path(samples_dir).glob('*.pkl')]) + 1
      base.id = new_id
    base.file = f'{samples_dir}/{base.id}.pkl'
    id = base.id
    session.commit()
    return id

  @abstractmethod
  def add_class_specific_sql_entry(self, *args: Any):
    ...

  @abstractmethod
  def generate(self):
    ...

  @staticmethod
  def permute_nodes(adj, node_labels_dict=None, edge_labels_dict=None):
    ret = []
    perm = np.random.permutation(adj.shape[0])
    P = np.eye(adj.shape[0])[:, perm]
    adj = P.T @ adj @ P
    ret.append(adj)
    if node_labels_dict is not None:
      for k, v in node_labels_dict.items():
        node_labels_dict[k] = node_labels_dict[k][perm]
      ret.append(node_labels_dict)
    if edge_labels_dict is not None:
      # todo: pretty sure can just permute the rows and cols of a sparse matrix separately
      raise NotImplementedError()
    return ret
  
  def save_adjacency_matrix_component(self, A, long_index=False):
    coo_A = sp.sparse.coo_matrix(A)
    index_type = np.int64 if long_index else np.int32
    edge_index = np.vstack([coo_A.row, coo_A.col]).astype(index_type)
    edge_attr = coo_A.data.astype(np.float32)
    
    edge_index_fname = f'{self.sample_dir}/{ADJ_INDEX_SFX}/{self.id}.npy'
    edge_attr_fname = f'{self.sample_dir}/{ADJ_ATTR_SFX}/{self.id}.npy'
    
    np.save(arr=edge_index, file=edge_index_fname)
    np.save(arr=edge_attr, file=edge_attr_fname)
    
    self.components['edge_index'] = edge_index_fname
    self.components['edge_attr'] = edge_attr_fname
    pickle.dump(self, open(self.filename, 'wb'))
    
  def save_graph_labels(self, graph_labels_dict):
    graph_label_inf_fname = f'{self.sample_dir}/{INFO_SFX}/{GRAPH_LBL_IDX}.json'
    if os.path.exists(graph_label_inf_fname):
      graph_label_info = json.load(open(graph_label_inf_fname, 'r'))
    else:
      graph_label_info = {}
    for k in graph_labels_dict:
      if k not in graph_label_info.keys():
        graph_label_info[k] = len(graph_label_info)
    json.dump(graph_label_info, open(graph_label_inf_fname, 'w'))
    
    graph_labels_fname = f'{self.sample_dir}/{GRAPH_LBL_SFX}/{self.id}.npy'
    if os.path.exists(graph_labels_fname):
      graph_labels = np.load(graph_labels_fname)
      if graph_labels.shape[0] < len(graph_label_info):
        graph_labels = np.concatenate([
          graph_labels,
          np.zeros(len(graph_label_info) - graph_labels.shape[0], dtype=np.float32)
        ], axis=0)
    else:
      graph_labels = np.zeros(len(graph_label_info), dtype=np.float32)
      
    for k, v in graph_labels_dict.items():
      graph_labels[graph_label_info[k]] = v
    assert graph_labels.shape == (len(graph_label_info),)
    np.save(arr=graph_labels, file=graph_labels_fname)
    self.components['graph_labels'] = graph_labels_fname
    pickle.dump(self, open(self.filename, 'wb'))
    
  def save_node_labels(self, node_labels_dict):
    num_nodes = list(node_labels_dict.values())[0].shape[0]
    
    node_label_inf_fname = f'{self.sample_dir}/{INFO_SFX}/{NODE_LBL_IDX}.json'
    if os.path.exists(node_label_inf_fname):
      node_label_info = json.load(open(node_label_inf_fname, 'r'))
    else:
      node_label_info = {}
    for k in node_labels_dict:
      if k not in node_label_info.keys():
        node_label_info[k] = len(node_label_info)
    json.dump(node_label_info, open(node_label_inf_fname, 'w'))
    
    node_labels_fname = f'{self.sample_dir}/{NODE_LBL_SFX}/{self.id}.npy'
    if os.path.exists(node_labels_fname):
      node_labels = np.load(node_labels_fname)
      if node_labels.shape[1] < len(node_label_info):
        node_labels = np.concatenate([
          node_labels,
          np.zeros((num_nodes, len(node_label_info) - node_labels.shape[1]), dtype=np.float32)
        ], axis=1)
    else:
      node_labels = np.zeros(shape=(num_nodes, len(node_label_info)), dtype=np.float32)
    
    for k, v in node_labels_dict.items():
      node_labels[:, node_label_info[k]] = v
    assert node_labels.shape == (num_nodes, len(node_label_info))
    np.save(arr=node_labels, file=node_labels_fname)
    self.components['node_labels'] = node_labels_fname
    pickle.dump(self, open(self.filename, 'wb'))

  def save_node_attrs(self, node_attrs_dict):
    num_nodes = list(node_attrs_dict.values())[0].shape[0]

    node_attrs_inf_fname = f'{self.sample_dir}/{INFO_SFX}/{NODE_ATTR_IDX}.json'
    if os.path.exists(node_attrs_inf_fname):
      node_attrs_info = json.load(open(node_attrs_inf_fname, 'r'))
    else:
      node_attrs_info = {}
    for k in node_attrs_dict:
      if k not in node_attrs_info.keys():
        node_attrs_info[k] = len(node_attrs_info)
    json.dump(node_attrs_info, open(node_attrs_inf_fname, 'w'))

    node_attrs_fname = f'{self.sample_dir}/{NODE_ATTR_SFX}/{self.id}.npy'
    if os.path.exists(node_attrs_fname):
      node_attrs = np.load(node_attrs_fname)
      if node_attrs.shape[1] < len(node_attrs_info):
        node_attrs = np.concatenate([
          node_attrs,
          np.zeros((num_nodes, len(node_attrs_info)), dtype=np.float32)
        ], axis=1)
    else:
      node_attrs = np.zeros((num_nodes, len(node_attrs_info)), dtype=np.float32)

    for k, v in node_attrs_dict.items():
      node_attrs[:, node_attrs_info[k]] = v
    assert node_attrs.shape == (num_nodes, len(node_attrs_info))
    np.save(arr=node_attrs, file=node_attrs_fname)
    self.components['node_attrs'] = node_attrs_fname
    pickle.dump(self, open(self.filename, 'wb'))
    
  def save_edge_labels(self, edge_labels_dict):
    num_edges = list(edge_labels_dict.values())[0].shape[0]
    
    edge_label_inf_fname = f'{self.sample_dir}/{INFO_SFX}/{EDGE_LBL_IDX}.json'
    if os.path.exists(edge_label_inf_fname):
      edge_label_info = json.load(open(edge_label_inf_fname, 'r'))
    else:
      edge_label_info = {}
    for k in edge_labels_dict:
      if k not in edge_label_info.keys():
        edge_label_info[k] = len(edge_label_info)
    json.dump(edge_label_info, open(edge_label_inf_fname, 'w'))
    
    edge_labels_fname = f'{self.sample_dir}/{EDGE_LBL_SFX}/{self.id}.npy'
    if os.path.exists(edge_labels_fname):
      edge_labels = np.load(edge_labels_fname)
      if edge_labels.shape[1] < len(edge_label_info):
        edge_labels = np.concatenate([
          edge_labels,
          np.zeros((num_edges, len(edge_label_info) - edge_labels.shape[1]), dtype=np.float32)
        ], axis=1)
    else:
      edge_labels = np.zeros(shape=(num_edges, len(edge_label_info)), dtype=np.float32)
    
    for k, v in edge_labels_dict.items():
      edge_labels[:, edge_label_info[k]] = v
    assert edge_labels.shape == (num_edges, len(edge_label_info))
    np.save(arr=edge_labels, file=edge_labels_fname)
    self.components['edge_labels'] = edge_labels_fname
    pickle.dump(self, open(self.filename, 'wb'))

  def communities_to_lp_labels(self, communities_label):
    node_label_inf_fname = f'{self.sample_dir}/{INFO_SFX}/{NODE_LBL_IDX}.json'
    assert os.path.exists(node_label_inf_fname), 'no node label indicies info exists'
    node_label_inf = json.load(open(node_label_inf_fname, 'r'))
    cmty_key = node_label_inf[communities_label]
    
    node_labels_fname = f'{self.sample_dir}/{NODE_LBL_SFX}/{self.id}.npy'
    assert os.path.exists(node_labels_fname), f'no node labels exist for sample id={self.id}'
    all_node_labels = np.load(node_labels_fname)
    node_labels = all_node_labels[:, cmty_key]
    
    A = self.load_adjacency(dense=False)
    # if not is_connected(A.todense(), reverse=True):
    #   print('test')
    unique_cmtys = np.unique(node_labels)
    for cmty_lbl in unique_cmtys:
      pA = np.array(np.where(node_labels == cmty_lbl))[0]
      pB = np.array(np.where(node_labels != cmty_lbl))[0]
      if len(pA) > 0 and len(pB) > 0:
      # todo: this is a bit hacky... and totally unsuitable for weighted edges
        A[np.ix_(pA, pB)] *= 2
        A[np.ix_(pB, pA)] *= 2
      # else:
      #   print('test')
    
    A[np.where(A.toarray() >= 2)] = 0
    self.save_edge_labels({communities_label: A.data})
    
    
    
      


  def verify_eig_conf_present(self, eig_conf):
    per_layer_adjacency_variants = eig_conf['per_layer_adjacency_variants']
    per_layer_eig_algorithms = eig_conf['per_layer_eig_algorithms']
    per_layer_split_methods = eig_conf['per_layer_split_methods']
    per_layer_vnodes = eig_conf['per_layer_vnodes']
    per_layer_vnode_weights = eig_conf['per_layer_vnode_weights']
    per_layer_eigenvectors = eig_conf['per_layer_eigenvectors']
    per_layer_eigenvalues = eig_conf['per_layer_eigenvalues']
    per_layer_assignments = eig_conf['per_layer_assignments']
    
    current_conf = self.preprocessed_eigen_features
    
    for adj_var, eig_alg, split_method, n_eig_vecs, n_eig_vals, \
      use_assignments, vnode, vnode_weight \
      in zip(per_layer_adjacency_variants, per_layer_eig_algorithms, per_layer_split_methods,
             per_layer_eigenvectors, per_layer_eigenvalues, per_layer_assignments,
             per_layer_vnodes, per_layer_vnode_weights):
      if use_assignments and not current_conf.get('assignments', False):
        return False
      
      if adj_var not in current_conf.keys():
        return False
      current_conf = current_conf[adj_var]
      if eig_alg not in current_conf.keys():
        return False
      current_conf = current_conf[eig_alg]
      if vnode not in current_conf.keys():
        return False
      current_conf = current_conf[vnode]
      if vnode_weight not in current_conf.keys():
        return False
      current_conf = current_conf[vnode_weight]
      
      conf_eigenfeatures = current_conf.get('n_eigenfeatures', 0)
      if conf_eigenfeatures < n_eig_vecs or conf_eigenfeatures < n_eig_vals:
        return False
      
      # if vnode, vnode_weight and not current_conf.get('used_virtual_node', False):
      #   return False
      if split_method not in current_conf.keys():
        return False
      current_conf = current_conf[split_method]
    
    return True

  ##################################################################################################
  ##########################################  DATA PREP  ###########################################
  ##################################################################################################
  
  def dgl_transform(self,
                    features=None,
                    include_distance_matrices=False):
    eig_conf = features['eig_conf']
    local_features = features['local_features']
    node_features = features['node_features']

    per_layer_adjacency_variants = eig_conf['per_layer_adjacency_variants']
    per_layer_eig_algorithms = eig_conf['per_layer_eig_algorithms']
    per_layer_split_methods = eig_conf['per_layer_split_methods']
    per_layer_vnode = eig_conf['per_layer_vnodes']
    per_layer_vnode_weights = eig_conf['per_layer_vnode_weights']
    per_layer_eigenvectors = eig_conf['per_layer_eigenvectors']
    per_layer_eigenvalues = eig_conf['per_layer_eigenvalues']
    per_layer_assignments = eig_conf['per_layer_assignments']
    
    feature_matrix = None
    
    current_dir = f'{self.sample_dir}/{EIGS_SFX}'
    for adj_var, eig_alg, split_method, n_eig_vecs, n_eig_vals, use_assignments, \
        vnode, vnode_weight \
      in zip(per_layer_adjacency_variants, per_layer_eig_algorithms, per_layer_split_methods,
             per_layer_eigenvectors, per_layer_eigenvalues, per_layer_assignments,
             per_layer_vnode, per_layer_vnode_weights):
      current_dir = f'{current_dir}/{adj_var}/{eig_alg}/{vnode}/{vnode_weight}'
      
      eig_vals = None
      if n_eig_vals > 0:
        eig_vals = np.load(f'{current_dir}/eigenvalues/{self.id}.npy')
        if feature_matrix is None:
          feature_matrix = eig_vals[:, :n_eig_vals]
        else:
          feature_matrix = np.concatenate([feature_matrix, eig_vals[:,:n_eig_vals]], axis=1)
      
      eig_vecs = None
      if n_eig_vecs > 0:
        eig_vecs = np.load(f'{current_dir}/eigenvectors/{self.id}.npy')
        if feature_matrix is None:
          feature_matrix = eig_vecs[:, :n_eig_vecs]
        else:
          feature_matrix = np.concatenate([feature_matrix, eig_vecs[:, :n_eig_vecs]], axis=1)
          
      if use_assignments:  # todo: pretty sure can do this in a single call inside final directory
        if eig_vals is None:
          if eig_vecs is None:
            eig_arr = np.load(f'{current_dir}/eigenvectors/{self.id}.npy')
          else:
            eig_arr = eig_vecs
        else:
          eig_arr = eig_vals
        current_dir = f'{current_dir}/{split_method}'
        assignments = np.unpackbits(
          np.load(f'{current_dir}/assignments/{self.id}.npy'),
          axis=0, count=eig_arr.shape[0])
        if feature_matrix is None:
          feature_matrix = assignments[:, -1:]
        else:
          feature_matrix = np.concatenate([feature_matrix, assignments[:, -1:]], axis=1)
      else:
        current_dir = f'{current_dir}/{split_method}'
      
    if len(local_features) > 0:
      local_features_index = json.load(
        open(f'{self.sample_dir}/{INFO_SFX}/{LOCAL_FEATS_IDX}.json', 'r'))
      local_features_data = np.load(f'{self.sample_dir}/{LOCAL_FEATS_SFX}/{self.id}.npy')
      req_features = local_features_data[:, [local_features_index[f] for f in local_features]]
      if feature_matrix is None:
        feature_matrix = req_features
      else:
        feature_matrix = np.concatenate([feature_matrix, req_features], axis=1)
        
    if len(node_features) > 0:
      node_features_index = json.load(open(f'{self.sample_dir}/{INFO_SFX}/{NODE_ATTR_IDX}.json', 'r'))
      node_features_data = np.load(f'{self.sample_dir}/{NODE_ATTR_SFX}/{self.id}.npy')
      req_features = node_features_data[:, [node_features_index[lbl] for lbl in node_features]]
      if feature_matrix is None:
        feature_matrix = req_features
      else:
        feature_matrix = np.concatenate([feature_matrix, req_features], axis=1)
      # ret.append(torch.tensor(node_features).float())
    
    adj_csr = self.load_adjacency(dense=False)
    adj_coo = sp.sparse.coo_matrix(adj_csr)
    g = dgl.graph((torch.tensor(adj_coo.col).long(), torch.tensor(adj_coo.row).long()), num_nodes=adj_coo.shape[0])
    if feature_matrix is not None:
      g.ndata['X'] = torch.tensor(feature_matrix).float()
    ret = [g]
    
    node_labels = features['node_labels']
    edge_labels = features['edge_labels']
    graph_labels = features['graph_labels']
    if len(node_labels) > 0:
      node_label_index = json.load(open(f'{self.sample_dir}/{INFO_SFX}/{NODE_LBL_IDX}.json', 'r'))
      node_label_data = np.load(f'{self.sample_dir}/{NODE_LBL_SFX}/{self.id}.npy')
      node_labels = node_label_data[:, [node_label_index[lbl] for lbl in node_labels]]
      ret.append(torch.tensor(node_labels).float())
    if len(edge_labels) > 0:
      edge_label_index = json.load(open(f'{self.sample_dir}/{INFO_SFX}/{EDGE_LBL_IDX}.json', 'r'))
      edge_label_data = np.load(f'{self.sample_dir}/{EDGE_LBL_SFX}/{self.id}.npy')
      edge_labels = edge_label_data[:, [edge_label_index[lbl] for lbl in edge_labels]]
      ret.append(torch.tensor(edge_labels).float())
    if len(graph_labels) > 0:
      graph_label_index = json.load(open(f'{self.sample_dir}/{INFO_SFX}/{GRAPH_LBL_IDX}.json', 'r'))
      graph_label_data = np.load(f'{self.sample_dir}/{GRAPH_LBL_SFX}/{self.id}.npy')
      graph_labels = graph_label_data[[graph_label_index[lbl] for lbl in graph_labels]]
      ret.append(torch.tensor(graph_labels).float())
    
    if include_distance_matrices:
      distance_mat = np.load(f'{self.sample_dir}/{DISTANCE_MAT_SFX}/{self.id}.npy')
      ret.append(torch.tensor(distance_mat).float())
    
    return ret

  @staticmethod
  def collate_fn(include_labels=False, include_edge_labels=False, include_distance_matrices=False):
    return partial(GCSample._internal_collate_fn,
                   include_labels=include_labels,
                   include_edge_labels=include_edge_labels,
                   include_distance_matrices=include_distance_matrices)

  @staticmethod
  def _internal_collate_fn(sample_list, **kwargs):
    include_labels = kwargs['include_labels']
    include_edge_labels = kwargs.get('include_edge_labels', False)
    include_distance_matrices = kwargs['include_distance_matrices']
    collated = []
    graphs = [sample[0] for sample in sample_list]
    collated.append(dgl.batch(graphs))
    if include_labels:
      if type(sample_list[0][1]) is list:
        labels = []
        for i in range(len(sample_list[0][1])):
          labels.append(torch.cat([sample[1][i] for sample in sample_list]))
        collated.append(labels)
      else:
        collated.append(torch.cat([sample[1] for sample in sample_list], dim=0))
    if include_edge_labels:
      if type(sample_list[0][2]) is list:
        labels = []
        for i in range(len(sample_list[0][2])):
          labels.append(torch.cat([sample[2][i] for sample in sample_list]))
        collated.append(labels)
      else:
        collated.append(torch.cat([sample[2] for sample in sample_list], dim=0))
    if include_distance_matrices:
      collated.append([sample[-1] for sample in sample_list])
    return collated
  
  
  ##################################################################################################
  #######################################  PREPROCESSING  ##########################################
  ##################################################################################################

  def preprocess_distance_matrix(self, recompute=False):
  
    distance_matrix_fname = f'{self.sample_dir}/{DISTANCE_MAT_SFX}/{self.id}.npy'
    predecessors_matrix_fname = f'{self.sample_dir}/{PREDECESSORS_MAT_SFX}/{self.id}.npy'
    if os.path.exists(distance_matrix_fname) and os.path.exists(predecessors_matrix_fname) \
        and not recompute:
      assert self.components['distance_matrix'] == distance_matrix_fname
      assert self.components['predecessors_matrix'] == predecessors_matrix_fname
      return
    
    adj_csr = self.load_adjacency(dense=False)
    unweighted = all(adj_csr.data == 1)
  
    distance_matrix, predecessors = sp.sparse.csgraph.floyd_warshall(
      adj_csr, directed=False, unweighted=unweighted, return_predecessors=True)
    distance_matrix = distance_matrix.astype(np.float32)
    predecessors = predecessors.astype(np.int32)
  
    np.save(arr=predecessors, file=predecessors_matrix_fname)
    np.save(arr=distance_matrix, file=distance_matrix_fname)
    self.components['predecessors_matrix'] = predecessors_matrix_fname
    self.components['distance_matrix'] = distance_matrix_fname
    self.preprocessed_distance = True
    pickle.dump(self, open(self.filename, 'wb'))
    
  def load_adjacency(self, dense=False):
    edge_index = np.load(f'{self.sample_dir}/{ADJ_INDEX_SFX}/{self.id}.npy')
    edge_attr = np.load(f'{self.sample_dir}/{ADJ_ATTR_SFX}/{self.id}.npy')
    if hasattr(self, 'num_nodes'):
      adj_csr = sp.sparse.csr_matrix((edge_attr, (edge_index[0], edge_index[1])),
                                     shape=(self.num_nodes, self.num_nodes))
    else:
      adj_csr = sp.sparse.csr_matrix((edge_attr, (edge_index[0], edge_index[1])))
    if dense:
      return adj_csr.toarray()
    else:
      return adj_csr
  
  def preprocess_graph_features(self, local_feature_names, global_feature_names, recompute=False):
    g = nx.from_numpy_array(self.load_adjacency(dense=True))
    n_nodes = g.number_of_nodes()
    n_edges = g.number_of_edges()
    
    local_feature_index_fname = f'{self.sample_dir}/{INFO_SFX}/{LOCAL_FEATS_IDX}.json'
    global_feature_index_fname = f'{self.sample_dir}/{INFO_SFX}/{GLOBAL_FEATS_IDX}.json'
    if os.path.exists(local_feature_index_fname):
      local_feature_index = json.load(open(local_feature_index_fname, 'r'))
    else:
      local_feature_index = {}
    for k in local_feature_names:
      if k not in local_feature_index.keys():
        local_feature_index[k] = len(local_feature_index)
    json.dump(local_feature_index, open(local_feature_index_fname, 'w'))
    if os.path.exists(global_feature_index_fname):
      global_feature_index = json.load(open(global_feature_index_fname, 'r'))
    else:
      global_feature_index = {}
    for k in global_feature_names:
      if k not in global_feature_index.keys():
        global_feature_index[k] = len(global_feature_index)
    json.dump(global_feature_index, open(global_feature_index_fname, 'w'))
    
    local_features_fname = f'{self.sample_dir}/{LOCAL_FEATS_SFX}/{self.id}.npy'
    global_features_fname = f'{self.sample_dir}/{GLOBAL_FEATS_SFX}/{self.id}.npy'
    if os.path.exists(local_features_fname):
      local_features = np.load(local_features_fname)
      if local_features.shape[1] < len(local_feature_index):
        local_features = np.concatenate([
          local_features,
          np.zeros((n_nodes, len(local_feature_index) - local_features.shape[1]),
                   dtype=np.float32)
        ], axis=1)
    else:
      local_features = np.zeros((n_nodes, len(local_feature_index)))
    if os.path.exists(global_features_fname):
      global_features = np.load(global_features_fname)
      if global_features.shape[0] < len(global_feature_index):
        global_features = np.concatenate([
          global_features,
          np.zeros((len(global_feature_index) - global_features.shape[0],), dtype=np.float32)
        ], axis=0)
    else:
      global_features = np.zeros((len(global_feature_index),), dtype=np.float32)
      
    distance_mat_fname = f'{self.sample_dir}/{DISTANCE_MAT_SFX}/{self.id}.npy'
    if os.path.exists(distance_mat_fname):
      distance_mat = np.load(distance_mat_fname)
    else:
      distance_mat = None
    
    def req_global(name):
      return name in global_feature_names and \
             (recompute or (name not in self.preprocessed_global_features))
    def req_local(name):
      return name in local_feature_names and \
             (recompute or (name not in self.preprocessed_local_features))
  
    if req_global('n_nodes'):
      global_features[global_feature_index['n_nodes']] = n_nodes
      self.preprocessed_global_features.append('n_nodes')
    if req_global('n_edges'):
      global_features[global_feature_index['n_edges']] = n_edges
      self.preprocessed_global_features.append('n_edges')
    if req_global('density'):
      global_features[global_feature_index['density']] = (n_edges*2) / (n_nodes * (n_nodes - 1))
      self.preprocessed_global_features.append('density')
    
    if req_local('degree_centrality'):
      degree_centrality = np.array([d[1] / n_nodes for d in g.degree])
      local_features[:, local_feature_index['degree_centrality']] = degree_centrality
      self.preprocessed_local_features.append('degree_centrality')
    
    if any([req_local('clustering_coeff'), req_global('transitivity')]):
      triangles = np.array([t[2] for t in nx.algorithms.cluster._triangles_and_degree_iter(g)])
      connected_triplets = np.array([d[1] * (d[1] - 1) for d in g.degree])
      clustering_coeff = np.divide(triangles, connected_triplets,
                                   out=np.zeros_like(triangles).astype(np.float),
                                   where=triangles != 0)
      # occasionally get nodes with no connected triplets (single edge nodes)
      # so se clustering coeff for these to 0
      clustering_coeff[np.isnan(clustering_coeff)] = 0
      
      if req_local('clustering_coeff'):
        local_features[:, local_feature_index['clustering_coeff']] = clustering_coeff
        self.preprocessed_local_features.append('clustering_coeff')
      if req_global('transitivity'):
        global_features[global_feature_index['transitivity']] = np.mean(clustering_coeff)
        self.preprocessed_global_features.append('transitivity')
    
    if any([req_local('clustering_coeff_sq'), req_global('transitivity_sq')]):
      clustering_coeff_sq = np.array(list(nx.algorithms.cluster.square_clustering(g).values()))
      
      if req_local('clustering_coeff_sq'):
        local_features[:, local_feature_index['clustering_coeff_sq']] = clustering_coeff_sq
        self.preprocessed_local_features.append('clustering_coeff_sq')
      if req_global('transitivity_sq'):
        global_features[global_feature_index['transitivity_sq']] = np.mean(clustering_coeff_sq)
        self.preprocessed_global_features.append('transitivity_sq')
        
    if req_local('average_neighbour_degree'):
      avg_neighbour_degree = np.array(list(
        nx.algorithms.assortativity.average_neighbor_degree(g).values()))
      local_features[:, local_feature_index['average_neighbour_degree']] = avg_neighbour_degree
      self.preprocessed_local_features.append('average_neighbour_degree')
      
    if req_local('closeness_centrality') and distance_mat is not None:
      sum_distances = np.sum(distance_mat, axis=0)
      closeness_centrality = (n_nodes - 1) / sum_distances
      local_features[:, local_feature_index['closeness_centrality']] = closeness_centrality
      self.preprocessed_local_features.append('closeness_centrality')
    
    if any([req_local('eccentricity'), req_local('is_periphery'), req_local('is_centre'),
            req_global('radius'), req_global('diameter'), req_global('periphery_coeff')]) \
        and distance_mat is not None:
      eccentricity = np.max(distance_mat, axis=0)
      radius = np.min(eccentricity)
      diameter = np.max(eccentricity)
      is_periphery = (eccentricity == diameter)
      is_centre = (eccentricity == radius)
      if req_global('radius'):
        global_features[global_feature_index['radius']] = radius
        self.preprocessed_global_features.append('radius')
      if req_global('diameter'):
        global_features[global_feature_index['diameter']] = diameter
        self.preprocessed_global_features.append('diameter')
      if req_global('periphery_coeff'):
        global_features[global_feature_index['periphery_coeff']] = np.sum(is_periphery) / n_nodes
        self.preprocessed_global_features.append('periphery_coeff')
      if req_global('centre_coeff'):
        global_features[global_feature_index['centre_coeff']] = np.sum(is_centre) / n_nodes
        self.preprocessed_global_features.append('centre_coeff')
      if req_global('mean_eccentricity'):
        global_features[global_feature_index['mean_eccentricity']] = np.mean(eccentricity)
        self.preprocessed_global_features.append('mean_eccentricity')
      if req_local('eccentricity'):
        local_features[:, local_feature_index['eccentricity']] = eccentricity
        self.preprocessed_local_features.append('eccentricity')
      if req_local('is_periphery'):
        local_features[:, local_feature_index['is_periphery']] = is_periphery
        self.preprocessed_local_features.append('is_periphery')
      if req_local('is_centre'):
        local_features[:, local_feature_index['is_centre']] = is_centre
        self.preprocessed_local_features.append('is_centre')

    if req_global('degree_assortativity'):
      degree_assortativity = nx.algorithms.assortativity.degree_pearson_correlation_coefficient(g)
      global_features[global_feature_index['degree_assortativity']] = degree_assortativity
      self.preprocessed_global_features.append('degree_assortativity')
      
    np.save(arr=global_features, file=global_features_fname)
    np.save(arr=local_features, file=local_features_fname)
    self.components['global_features'] = global_features_fname
    self.components['local_features'] = local_features_fname
    
    pickle.dump(self, open(self.filename, 'wb'))
  
  
  def preprocess_eigenfeatures(self, eig_config, force_recompute=False):
    per_layer_adjacency_variants = eig_config['per_layer_adjacency_variants']
    per_layer_eig_algorithms = eig_config['per_layer_eig_algorithms']
    per_layer_split_methods = eig_config['per_layer_split_methods']
    eig_fail_progression = eig_config['eig_fail_progression']
    per_layer_vnodes = eig_config['per_layer_vnodes']
    per_layer_vnode_weights = eig_config['per_layer_vnode_weights']
    per_layer_eigenvectors = eig_config['per_layer_eigenvectors']
    per_layer_eigenvalues = eig_config['per_layer_eigenvalues']
    per_layer_assignments = eig_config['per_layer_assignments']
    per_layer_use_gpu = eig_config['per_layer_use_gpu']
    per_layer_tols = eig_config['per_layer_tols']
    per_layer_eigenfeatures = [max(val, vec) for val, vec in
                               zip(per_layer_eigenvalues, per_layer_eigenvectors)]
    start_dir = validate_directory(f'{self.sample_dir}/{EIGS_SFX}')
    adj_csr = self.load_adjacency(dense=False)
    init_assignments_dir = validate_directory(f'{start_dir}/assignments')
    init_assignments_fname = f'{init_assignments_dir}/{self.id}.npy'
    if not os.path.exists(init_assignments_fname):
      init_assignments = np.zeros((adj_csr.shape[0], 1), dtype=np.bool)
      np.save(arr=np.packbits(init_assignments, axis=0), file=init_assignments_fname)
    
    current_dir = start_dir
    current_conf = self.preprocessed_eigen_features
    def traverse_conf(key):
      if key not in current_conf.keys():
        current_conf[key] = {}
      return current_conf[key]
    
    for (adj_var, eig_alg, split_method, n_eigenfeatures,
      use_assignments, use_gpu, tol, vnode, vnode_weight) in \
      zip(per_layer_adjacency_variants, per_layer_eig_algorithms, per_layer_split_methods,
          per_layer_eigenfeatures, per_layer_assignments, per_layer_use_gpu,
          per_layer_tols, per_layer_vnodes, per_layer_vnode_weights):
      decomposed = False
      fail_counter = 0
      use_eig_alg = eig_alg
      while not decomposed:
        try:
          decomposed = self.execute_single_decomposition(
            adj_csr=adj_csr,
            start_dir=current_dir,
            adjacency_variant=adj_var,
            intended_eig_algorithm=eig_alg,
            use_eig_algorithm=use_eig_alg,
            n_eigenfeatures=n_eigenfeatures,
            split_method=split_method,
            vnode=vnode,
            vnode_weight=vnode_weight,
            gpu=use_gpu,
            tol=tol,
            force_recompute=force_recompute)
        except Exception as e:
          fail_counter += 1
          eig_fail_key = max(
            [k for k in eig_fail_progression.keys() if fail_counter > k], default=0)
          use_eig_alg = eig_fail_progression.get(eig_fail_key, eig_alg)
          if use_eig_alg == 'fail':
            print('Total failure to find algorithms.')
            return False
          print(f'{e}\n(Attempted {fail_counter} times, last used "{eig_alg}". '
                f'Retrying with "{use_eig_alg}"...)')
      
      current_dir = f'{current_dir}/{adj_var}/{eig_alg}/{vnode}/{vnode_weight}/{split_method}'
      
      current_conf['assignments'] = True
      current_conf = traverse_conf(adj_var)
      current_conf = traverse_conf(eig_alg)
      current_conf = traverse_conf(vnode)
      current_conf = traverse_conf(vnode_weight)
      current_conf['n_eigenfeatures'] = n_eigenfeatures
      current_conf['fail_count'] = fail_counter
      current_conf['used_alg'] = use_eig_alg
      current_conf = traverse_conf(split_method)
    
    self.components['eigenfeatures'] = True
    # self.preprocessed_eigen_features = current_conf
    pickle.dump(self, open(self.filename, 'wb'))
    
    
  
  def execute_single_decomposition(self,
                                   adj_csr,
                                   start_dir,
                                   adjacency_variant,
                                   intended_eig_algorithm,
                                   use_eig_algorithm,
                                   n_eigenfeatures,
                                   split_method,
                                   vnode=False,
                                   vnode_weight='unweighted',
                                   gpu=False,
                                   tol=None,
                                   force_recompute=False,
                                   ):
    assert adjacency_variant in ('adjacency', 'laplacian', 'normalised_laplacian')
    assert use_eig_algorithm in ('pyt_eig',
                                 'pyt_symeig',
                                 'pyt_lobpcg',
                                 'np_eig',
                                 'np_eigh',
                                 'sp_eig',
                                 'sp_eigh',
                                 'sp_eigs',
                                 'sp_eigsh')
    assert split_method in ('median', 'sign', 'ncut')
    assert vnode in ['no_vnode', 'single', 'per_partition', 'if_disconnected']
    assert vnode_weight in ['unweighted', 'sum_edges']
    
    eig_algorithm_dir = validate_directory(
      f'{start_dir}/{adjacency_variant}/{intended_eig_algorithm}/{vnode}/{vnode_weight}')
    eigenvectors_dir = validate_directory(f'{eig_algorithm_dir}/eigenvectors')
    eigenvectors_fname = f'{eigenvectors_dir}/{self.id}.npy'
    eigenvalues_dir = validate_directory(f'{eig_algorithm_dir}/eigenvalues')
    eigenvalues_fname = f'{eigenvalues_dir}/{self.id}.npy'
    
    compute_eigenfeatures = True
    compute_assignments = True
    eigenvectors, eigenvalues = None, None
    if os.path.exists(eigenvectors_fname) and os.path.exists(eigenvalues_fname) \
        and not force_recompute:
      eigenvectors = np.load(eigenvectors_fname)
      eigenvalues = np.load(eigenvalues_fname)
      if eigenvectors.shape[1] >= n_eigenfeatures and eigenvalues.shape[1] >= n_eigenfeatures \
          and not force_recompute:
        compute_eigenfeatures = False
    
    split_method_dir = validate_directory(f'{eig_algorithm_dir}/{split_method}')
    tgt_assignments_dir = validate_directory(f'{split_method_dir}/assignments')
    
    if os.path.exists(f'{tgt_assignments_dir}/{self.id}.npy') and not force_recompute:
      compute_assignments = False
    
    if not compute_assignments and not compute_eigenfeatures:
      return True
    
    source_assignments_dir = validate_directory(f'{start_dir}/assignments')
    source_assignments_fname = f'{source_assignments_dir}/{self.id}.npy'
    
    source_assignments = np.unpackbits(np.load(source_assignments_fname),
                                       axis=0, count=adj_csr.shape[0])
    if compute_assignments:
      assignment_col = np.zeros((adj_csr.shape[0], 1), dtype=np.bool)
    else:
      assignment_col = None
    
    if compute_eigenfeatures:
      eigenvectors = np.zeros((adj_csr.shape[0], n_eigenfeatures), dtype=np.float32)
      eigenvalues = np.zeros((adj_csr.shape[0], n_eigenfeatures), dtype=np.float32)
    
    unique_partitions = np.unique(source_assignments, axis=0)
    for partition in unique_partitions:
      partition_idxs = np.where((source_assignments == partition).all(axis=1))[0]
      if len(partition_idxs) < 2:
        continue

      partition_adj = adj_csr[np.ix_(partition_idxs, partition_idxs)]
    
      if compute_eigenfeatures:
        n_vnodes = 0
        if vnode == 'no_vnode':
          pass
        elif vnode == 'single':
          other_idxs = np.where(~np.isin(np.arange(adj_csr.shape[0]), partition_idxs))[0]
          cross_partition = adj_csr[np.ix_(partition_idxs, other_idxs)]
          vnode_edges = np.sum(cross_partition, axis=1)
          if vnode_weight == 'unweighted':
            vnode_edges = vnode_edges.clip(max=1)
          elif vnode_weight == 'sum_edges':
            pass
          else:
            raise NotImplementedError()
          
          if np.count_nonzero(vnode_edges) > 0:
            n_vnodes = 1
            partition_adj = np.pad(partition_adj.toarray(),
                                   pad_width=((0, 1), (0, 1)),
                                   mode='constant',
                                   constant_values=0)
            partition_adj[:-1, -1:] = vnode_edges
            partition_adj[-1:, :-1] = vnode_edges.T
            partition_adj = sps.csr_matrix(partition_adj)
        elif vnode == 'per_partition':
          raise NotImplementedError()
        
        sorted_partition_eig_vals, sorted_partition_eig_vecs = self.compute_eigenfeatures(
          adj=partition_adj,
          adjacency_variant=adjacency_variant,
          eig_algorithm=use_eig_algorithm,
          n_eigenfeatures=n_eigenfeatures + 1,  # will strip first feature after
          gpu=gpu,
          tol=tol)
        
        sorted_partition_eig_vals = sorted_partition_eig_vals[1:]
        sorted_partition_eig_vecs = sorted_partition_eig_vecs[:, 1:]
        
        if n_vnodes > 0:
          sorted_partition_eig_vecs = sorted_partition_eig_vecs[:-n_vnodes, :]
          partition_adj = partition_adj[:-n_vnodes, :-n_vnodes]
        else:
          eigenvalues[partition_idxs, :] = sorted_partition_eig_vals
          eigenvectors[partition_idxs, :] = sorted_partition_eig_vecs
      else:
        sorted_partition_eig_vecs = eigenvectors[partition_idxs, :]
      
      if compute_assignments:
        fiedler_vec = sorted_partition_eig_vecs[:, 0]
        idxs_1, idxs_2 = GCSample.split_on_fiedler(fiedler_vec, split_method, A=partition_adj)
        partition_assignments = np.zeros((partition_adj.shape[0], 1))
        partition_assignments[idxs_2] = 1
        assignment_col[partition_idxs] = partition_assignments
    
    if compute_eigenfeatures:
      np.save(arr=eigenvalues, file=eigenvalues_fname)
      np.save(arr=eigenvectors, file=eigenvectors_fname)
    if compute_assignments:
      assignments = np.concatenate([source_assignments, assignment_col], axis=1)
      np.save(arr=np.packbits(assignments, axis=0), file=f'{tgt_assignments_dir}/{self.id}.npy')
    
    return True
      
    
  def compute_eigenfeatures(self,
                            adj,
                            adjacency_variant,
                            eig_algorithm,
                            n_eigenfeatures,
                            gpu=False,
                            tol=0.,
                            ):
    if adj.shape[0] < n_eigenfeatures:
      spare_vecs = n_eigenfeatures - adj.shape[0]
      n_eigenfeatures = adj.shape[0]
    else:
      spare_vecs = 0
  
    AV = adj
    if adjacency_variant == 'laplacian':
      AV = sp.sparse.csgraph.laplacian(adj, normed=False)
    elif adjacency_variant == 'normalised_laplacian':
      AV = sp.sparse.csgraph.laplacian(adj, normed=True)
  
    if eig_algorithm.startswith('pyt'):
      indices = np.vstack([AV.row, AV.col])
      values = AV.data
      size = AV.shape
      AV = torch.sparse_coo_tensor(indices=indices, values=values, size=size)
      AV = AV.to(torch.device('cuda')) if gpu else AV
  
    if eig_algorithm == 'pyt_eig':
      eig_vals, eig_vecs = torch.eig(AV, eigenvectors=True)
    elif eig_algorithm == 'pyt_symeig':
      eig_vals, eig_vecs = torch.symeig(AV, eigenvectors=True)
    elif eig_algorithm == 'pyt_lobpcg':
      eig_vals, eig_vecs = torch.lobpcg(
        AV, k=n_eigenfeatures, largest=False, tol=tol, method='ortho')
  
    elif eig_algorithm == 'np_eig':
      AV = AV.toarray()
      eig_vals, eig_vecs = np.linalg.eig(AV)
    elif eig_algorithm == 'np_eigh':
      AV = AV.toarray()
      eig_vals, eig_vecs = np.linalg.eigh(AV)
  
    elif eig_algorithm == 'sp_eig':
      AV = AV.toarray()
      eig_vals, eig_vecs = sp.linalg.eig(AV)
    elif eig_algorithm == 'sp_eigh':
      AV = AV.toarray()
      eig_vals, eig_vecs = sp.linalg.eigh(
        AV, eigvals_only=False, subset_by_index=[0, n_eigenfeatures - 1], driver='evr')
    elif eig_algorithm == 'sp_eigs':
      if n_eigenfeatures >= AV.shape[0] - 1:
        AV = AV.toarray()
        eig_vals, eig_vecs = sp.linalg.eig(AV)
      else:
        eig_vals, eig_vecs = sp.sparse.linalg.eigs(
          AV, k=n_eigenfeatures, which='SR', tol=tol, return_eigenvectors=True)
                                   # or SM, as eigs should be real anyway
    elif eig_algorithm == 'sp_eigsh':
      if n_eigenfeatures >= AV.shape[0]:
        AV = AV.toarray()
        eig_vals, eig_vecs = sp.linalg.eigh(
          AV, eigvals_only=False, subset_by_index=[0, n_eigenfeatures - 1], driver='evr')
      else:
        eig_vals, eig_vecs = sp.sparse.linalg.eigsh(
          AV, k=n_eigenfeatures, which='SM', tol=tol, return_eigenvectors=True)
    else:
      raise ValueError()
  
    if eig_algorithm.startswith('pyt'):
      if gpu:
        eig_vals, eig_vecs = eig_vals.cpu(), eig_vecs.cpu()
      eig_vals, eig_vecs = eig_vals.numpy(), eig_vecs.numpy()
  
    eig_val_order = np.argsort(np.abs(eig_vals))
    ordered_vals = eig_vals[eig_val_order]
    ordered_vecs = eig_vecs[:, eig_val_order]
  
    if spare_vecs > 0:
      ordered_vecs = np.concatenate(
        [ordered_vecs, np.zeros(shape=(ordered_vecs.shape[0], spare_vecs))], axis=1)
      ordered_vals = np.concatenate([ordered_vals, np.zeros(spare_vecs)])
  
    return ordered_vals[:n_eigenfeatures + spare_vecs], ordered_vecs[:, :n_eigenfeatures + spare_vecs]

  @staticmethod
  def split_on_fiedler(fiedler_vec, split_method, A=None, D=None, L=None):
    '''
    Get indices of nodes according to partitioning method
    :param fiedler_vec: 1 dimensional vector
    :param split_method:
      "ncut" should search for normalized cut,
      "median" results in roughly evenly sized splits,
      "sign" just cuts based on whether vector val is +ve/-ve
    :param A: adjacency matrix, only needed for ncut
    :return: two numpy column vectors of indices for each partition
    '''
    if split_method == 'ncut':
      return GCSample.ncut_search(fiedler_vec=fiedler_vec, A=A, D=D, L=L)
    elif split_method == 'median':
      node_order = np.argsort(fiedler_vec)
      idxs_1 = node_order[:int(len(node_order) / 2)][:, None]
      idxs_2 = node_order[int(len(node_order) / 2):][:, None]
    elif split_method == 'sign':
      idxs_1 = np.where(fiedler_vec < 0)[0][:, None]
      idxs_2 = np.where(fiedler_vec >= 0)[0][:, None]
    else:
      raise ValueError()
  
    return idxs_1, idxs_2

  @staticmethod
  def ncut_search(fiedler_vec, A=None, D=None, L=None, sparse=False):
    if D is None or L is None:
      L, D = sp.sparse.csgraph.laplacian(A, return_diag=True)
      indices = np.arange(D.shape[0])
      # todo: modify for sparse multiplication
      D = sp.sparse.csr_matrix((D, (indices, indices)))  # .toarray()
      sparse = True
      # L = L.toarray()
    node_order = np.argsort(fiedler_vec)
    best_split_point = 1
    min_ncut = np.inf
    for i in range(1, len(node_order)):
      x = np.zeros(shape=(len(node_order), 1))
      x[:i, 0] = 1
      if sparse:
        x = sp.sparse.csr_matrix(x)
        x_inv = x
        x_inv.data = 1 - x_inv.data
        dA = x.T.dot(D.dot(x)).toarray()[0, 0]
        dB = (x_inv).T.dot(D.dot(x_inv)).toarray()[0, 0]
        cutAB = x.T.dot(L.dot(x)).toarray()[0, 0]
      else:
        dA = np.matmul(x.T, np.matmul(D, x))
        dB = np.matmul((1 - x).T, np.matmul(D, (1 - x)))
        cutAB = np.matmul(x.T, np.matmul(L, x))
      if dA > 0 and dB > 0:
        _4_ncut = (cutAB / dA) + (cutAB / dB)
      else:
        _4_ncut = np.inf
      if _4_ncut < min_ncut:
        min_ncut = _4_ncut
        best_split_point = i
    idxs_1 = node_order[:best_split_point, None]
    idxs_2 = node_order[best_split_point:, None]
    return idxs_1, idxs_2

  