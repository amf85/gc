import numpy as np
from sqlalchemy import select, and_, or_
import json
import pickle
from numbers import Number
import os

import scipy.sparse as sps

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample
from sql.sql_base import GCSampleBase, GCDataSplitBase, SMMotifBase, SMPoolBase, SMDataSplitBase, SharedMotifsNetworkBase
from util.util import printProgressBar, validate_directory
from util.constants import *


class SMDataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_sm'
  
  @property
  def split_class(self):
    return 'SharedMotifsNetwork'
  
  class SMSplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,
                 
                 init_min_nodes,
                 init_max_nodes,
                 init_randoms,
                 init_rings,
                 init_cliques,
                 init_stars,
                 init_lines,
                 init_exp_node_edges,
                 expansion_prob,
                 maintain_motif_degree,
                 n_passes,
                 
                 pool_min_nodes,
                 pool_max_nodes,
                 pool_size,
                 pool_randoms,
                 pool_rings,
                 pool_cliques,
                 pool_stars,
                 pool_lines,
                 pool_exp_node_edges,
    
                 **kwargs,
                 ):
      super(SMDataSplit.SMSplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )

      self.init_min_nodes = init_min_nodes
      self.init_max_nodes = init_max_nodes
      self.init_randoms = init_randoms
      self.init_rings = init_rings
      self.init_cliques = init_cliques
      self.init_stars = init_stars
      self.init_lines = init_lines
      self.init_exp_node_edges = init_exp_node_edges
      self.maintain_motif_degree = maintain_motif_degree
      self.expansion_prob = expansion_prob
      self.maintain_motif_degree = maintain_motif_degree
      self.n_passes = n_passes
      
      self.pool_min_nodes = pool_min_nodes
      self.pool_max_nodes = pool_max_nodes
      self.pool_size = pool_size
      self.pool_randoms = pool_randoms
      self.pool_rings = pool_rings
      self.pool_cliques = pool_cliques
      self.pool_stars = pool_stars
      self.pool_lines = pool_lines
      self.pool_exp_node_edges = pool_exp_node_edges
  
  def __init__(self,
               dataset,
               params: SMSplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    
    self.pool_id = None

    super(SMDataSplit, self).__init__(
      dataset=dataset,
      params=params,
      exclude_sample_ids=exclude_sample_ids,
      split=split,
      preprocess_eig=preprocess_eig,
      preprocess_local=preprocess_local,
      preprocess_global=preprocess_global,
      preprocess_distance=preprocess_distance,
      feature_conf=feature_conf,
      transform=transform,
      create=create,
      named_identifier=named_identifier,
      overwrite_identifier=overwrite_identifier,
    )
    
  def validate_class_directories(self):
    # validate_directory(f'{self.split_dir}/{SM_MOTIFS_SFX}')
    # validate_directory(f'{self.split_dir}/{SM_MOTIFS_SFX}/{SAMPLES_SFX}')
    self.validate_data_directories(root_dir=f'{self.split_dir}/{SM_MOTIFS_SFX}')
    validate_directory(f'{self.split_dir}/{SM_POOLS_SFX}')
  
  def add_samples(self,
                  params: SMSplitParams,
                  preprocess_eig=None,
                  preprocess_distance=False,
                  preprocess_local=None,
                  preprocess_global=None,
                  feature_conf=None,
                  create_new=False,
                  ):
    
    rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    
    session = self.dataset.gcdb.local_sess()
    stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    base = session.execute(stmt).first()[0]
    smds_base = session.execute(
      select(SMDataSplitBase).where(SMDataSplitBase.id == self.id)).first()[0]
    
    self.validate_data_directories(root_dir=self.split_dir)
    self.validate_class_directories()

    motif_pool = SMPool(
      session=session,
      class_root_dir=self.split_dir,
      
      min_motif_nodes=params.pool_min_nodes,
      max_motif_nodes=params.pool_max_nodes,
      pool_size=params.pool_size,
      randoms=params.pool_randoms,
      rings=params.pool_rings,
      cliques=params.pool_cliques,
      stars=params.pool_stars,
      lines=params.pool_lines,
      exp_node_edges=params.pool_exp_node_edges,
    )
    # pool_base = session.execute(
    #   select(SMPoolBase).where(SMPoolBase.id == motif_pool.id)).first()[0]
    smds_base.pool_id = motif_pool.id
    
    motif_pool_As = []
    for motif in motif_pool.pool:
      edge_index = np.load(open(motif.components['edge_index'], 'rb'))
      edge_attr = np.load(open(motif.components['edge_attr'], 'rb'))
      csr = sps.csr_matrix((edge_attr, (edge_index[0], edge_index[1])))
      motif_pool_As.append(csr.toarray())
    
    self.pool_id = motif_pool.id

    structures = []
    if params.init_randoms:
      structures.append('random')
    if params.init_rings:
      structures.append('ring')
    if params.init_cliques:
      structures.append('clique')
    if params.init_stars:
      structures.append('star')
    if params.init_lines:
      structures.append('line')
    assert len(structures) > 0, 'must select at least one structure type'
    
    for i in range(params.n_samples):
      init_g_size = int(np.random.randint(
        low=params.init_min_nodes, high=params.init_max_nodes))
      if isinstance(params.n_passes, list):
        n_passes = int(np.random.choice(params.n_passes))
      elif isinstance(params.n_passes, int):
        n_passes = params.n_passes
      else:
        raise ValueError
      structure = np.random.choice(structures)
      
      if create_new:
        matching = []
      else:
        # stmt = select(GCSampleBase).join(SharedMotifsNetworkBase).where(
        #   and_(
        #
        #     SharedMotifsNetworkBase.pool_id ==
        #
        #
        #     SharedMotifsNetworkBase.init_g_size == init_g_size,
        #     SharedMotifsNetworkBase.init_g_exp_node_edges == params.init_g_exp_node_edges,
        #     SharedMotifsNetworkBase.motif_min_nodes == params.motif_min_nodes,
        #     SharedMotifsNetworkBase.motif_max_nodes == params.motif_max_nodes,
        #     SharedMotifsNetworkBase.motif_exp_node_edges == params.motif_exp_node_edges,
        #     SharedMotifsNetworkBase.motif_pool_size == params.motif_pool_size,
        #     SharedMotifsNetworkBase.expansion_prob == params.expansion_prob,
        #     SharedMotifsNetworkBase.maintain_motif_degree == params.maintain_motif_degree,
        #     SharedMotifsNetworkBase.n_passes == n_passes,
        #     GCSampleBase.id.notin_(sample_ids_added + self.exclude_sample_ids)
        #   )
        # )
        #
        # matching = session.execute(stmt).all()
        
        # effectively, as initialising a new pool, will never be drawing samples from the same
        # distribution again. So not really feasible to select samples from a database this way.
        # BUT can still used named identifiers for data splits and hence re-use samples that way.
        
        # alternatively, if this becomes laborious, can work something up to select matching pools...
        matching = []
      
      if len(matching) > 0:
        sample = rng.choice(matching, axis=0)[0]
        sample_ids_added.append(sample.id)
        file_index.append(sample.file)
        base.samples.append(sample)
        pkl_file = sample.file
      else:
        sample = SharedMotifsNetwork(
          session=session,
          sample_dir=self.split_dir,
          
          motif_pool=motif_pool_As,
          motif_pool_id=motif_pool.id,
          
          init_size=init_g_size,
          init_structure=structure,
          init_exp_node_edges=params.init_exp_node_edges,
          maintain_motif_degree=params.maintain_motif_degree,
          n_passes=n_passes,
          expansion_prob=params.expansion_prob,
        )
        sample_ids_added.append(sample.id)
        file_index.append(sample.filename)
        stmt = select(GCSampleBase).where(GCSampleBase.id == sample.id)
        sample_base = session.execute(stmt).first()[0]
        base.samples.append(sample_base)
        pkl_file = sample.filename
      
      sample: GCSample = pickle.load(open(pkl_file, 'rb'))
      
      if preprocess_distance and not sample.preprocessed_distance:
        sample.preprocess_distance_matrix(recompute=False)
      if any(f not in sample.preprocessed_global_features for f in preprocess_global) \
          or any(f not in sample.preprocessed_local_features for f in preprocess_local):
        sample.preprocess_graph_features(
          local_feature_names=preprocess_local, global_feature_names=preprocess_global)
      if not sample.verify_eig_conf_present(preprocess_eig):
        sample.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)
        
      if any(f not in sample.preprocessed_local_features for f in feature_conf['local_features']):
        sample.preprocess_graph_features(local_feature_names=feature_conf['local_features'],
                                         global_feature_names=[])
      if not sample.verify_eig_conf_present(feature_conf['eig_conf']):
        sample.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'], force_recompute=False)
      
      printProgressBar(iteration=i + 1,
                       total=params.n_samples,
                       prefix=f'Populating {self.split} dataset.',
                       suffix=f'({i + 1}/{params.n_samples})',
                       length=100)
      
      session.commit()
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    if isinstance(self.params.n_passes, list):
      n_passes = ','.join(map(str, self.params.n_passes))
    else:
      n_passes = self.params.n_passes
    
    base = SMDataSplitBase(
      id = self.id,
      split=self.split,
      n_samples=self.params.n_samples,
      
      init_min_nodes=self.params.init_min_nodes,
      init_max_nodes=self.params.init_max_nodes,
      init_randoms=self.params.init_randoms,
      init_rings=self.params.init_rings,
      init_cliques=self.params.init_cliques,
      init_stars=self.params.init_stars,
      init_lines=self.params.init_lines,
      init_exp_node_edges=self.params.init_exp_node_edges,
      expansion_prob=self.params.expansion_prob,
      maintain_motif_degree=self.params.maintain_motif_degree,
      n_passes=n_passes,
      
      pool_id=self.pool_id
    )
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.commit()


class SMDataset(GCDataset):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(SMDataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb, verbose=verbose)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = SMDataSplit(dataset=self,
                        split=split_name,
                        params=split_params,
                        exclude_sample_ids=exclude_sample_ids,
                        preprocess_eig=preprocess_eig,
                        preprocess_local=preprocess_local,
                        preprocess_global=preprocess_global,
                        preprocess_distance=preprocess_distance,
                        feature_conf=feature_conf,
                        transform=transform,
                        create=create,
                        named_identifier=named_identifier,
                        overwrite_identifier=overwrite_identifier,
                        )
    self.splits[split_name] = split
    return split


class SMPool(object):
  db_table = 'samples_sm_pools'
  
  def __init__(self,
               session,
               class_root_dir,

               min_motif_nodes,
               max_motif_nodes,
               pool_size,
               randoms=False,
               rings=False,
               cliques=False,
               stars=False,
               lines=False,
               exp_node_edges=None,
               ):
    self.class_root_dir = class_root_dir
    if randoms:
      assert isinstance(exp_node_edges, Number), 'must supply exp_node_edges if using randoms'
    self.min_motif_nodes = min_motif_nodes
    self.max_motif_nodes = max_motif_nodes
    self.pool_size = pool_size
    
    self.structures = []
    if randoms:
      self.structures.append('random')
    if rings:
      self.structures.append('ring')
    if cliques:
      self.structures.append('clique')
    if stars:
      self.structures.append('star')
    if lines:
      self.structures.append('line')
    assert len(self.structures) > 0, 'must select at least one structure type'
    self.randoms = randoms
    self.rings = rings
    self.cliques = cliques
    self.stars = stars
    self.lines = lines
    self.exp_node_edges = exp_node_edges
    
    self.sm_pool_dir = f'{class_root_dir}/{SM_POOLS_SFX}'
    self.id = self.add_sql_entry(session)
    self.filename = f'{self.sm_pool_dir}/{self.id}.pkl'
    pickle.dump(self, open(f'{self.filename}', 'wb'))
    
    self.pool = self.generate_pool(session)
    pickle.dump(self, open(f'{self.filename}', 'wb'))
    
  def add_sql_entry(self, session):
    base = SMPoolBase(
      pool_size=self.pool_size,
      min_motif_nodes=self.min_motif_nodes,
      max_motif_nodes=self.max_motif_nodes,
      randoms=self.randoms,
      rings=self.rings,
      cliques=self.cliques,
      stars=self.stars,
      lines=self.lines,
      exp_node_edges=self.exp_node_edges,
      file='temp',
    )
    session.add(base)
    session.flush()
    if os.path.exists(f'{self.sm_pool_dir}/{base.id}.pkl'):
      new_id = max([int(f.stem) for f in Path(self.sm_pool_dir).glob('*.pkl')]) + 1
      base.id = new_id
    base.file = f'{self.sm_pool_dir}/{base.id}.pkl'
    id = base.id
    session.commit()
    return id
    
  def generate_pool(self, session):
    pool = []
    for i in range(self.pool_size):
      n_nodes = np.random.randint(low=self.min_motif_nodes, high=self.max_motif_nodes+1)
      structure = np.random.choice(self.structures)
      motif = SMMotif(session=session,
                      sm_motif_dir=f'{self.class_root_dir}/{SM_MOTIFS_SFX}',
                      n_nodes=n_nodes,
                      structure=structure,
                      exp_node_edges=self.exp_node_edges)
      pool.append(motif)
    return pool
    
    

class SMMotif(GCSample):
  db_table = 'samples_sm_motifs'
  
  def __init__(self,
               session,
               sm_motif_dir,

               n_nodes,
               structure,
               exp_node_edges=None,

               ):
    assert structure in ['random', 'ring', 'clique', 'star', 'line']
    if structure == 'random':
      assert isinstance(exp_node_edges, Number)
    
    self.n_nodes = n_nodes
    self.structure = structure
    self.exp_node_edges = exp_node_edges
    
    super(SMMotif, self).__init__(
      session=session,
      sample_dir=sm_motif_dir
    )
    
  def add_class_specific_sql_entry(self, session):
    base = SMMotifBase(
      id=self.id,
      n_nodes=self.n_nodes,
      structure=self.structure,
      exp_node_edges=self.exp_node_edges
    )
    session.add(base)
    session.commit()
    
  
  def generate(self):
    A = self.create_motif(n_nodes=self.n_nodes,
                          structure=self.structure,
                          exp_node_edges=self.exp_node_edges)
    
    self.save_adjacency_matrix_component(A=A, long_index=False)
    
  @staticmethod
  def create_motif(n_nodes, structure, exp_node_edges):
    if structure == 'random':
      A = generate_random_graph(
        n_nodes=n_nodes, exp_node_edges=exp_node_edges, ensure_connected=True)
    elif structure == 'ring':
      A = np.fromfunction(lambda i, j: np.abs(i-j) == 1,
                          shape=[n_nodes]*2).astype(np.float32)
      A[0, -1] = 1
      A[-1, 0] = 1
    elif structure == 'clique':
      A = np.fromfunction(lambda i, j: i != j, shape=[n_nodes]*2).astype(np.float32)
    elif structure == 'star':
      A = np.zeros([n_nodes]*2, dtype=np.float32)
      A[1:, 0] = 1
      A[0, 1:] = 1
    elif structure == 'line':
      A = np.fromfunction(lambda i, j: np.abs(i-j) == 1,
                          shape=[n_nodes]*2).astype(np.float32)
    else:
      raise NotImplementedError()
    return A
    


class SharedMotifsNetwork(GCSample):
  db_table = 'samples_sm'

  def __init__(self,
               session,
               sample_dir,

               motif_pool,
               motif_pool_id,
               
               init_size,
               init_structure,
               init_exp_node_edges,
               maintain_motif_degree,
               n_passes,
               expansion_prob,

               ):
  
    self.sample_dir = sample_dir
    self.motif_pool = motif_pool
    self.motif_pool_id = motif_pool_id
    self.init_size = init_size
    self.init_structure = init_structure
    self.init_exp_node_edges = init_exp_node_edges
    self.maintain_motif_degree = maintain_motif_degree
    self.n_passes = n_passes
    self.expansion_prob = expansion_prob
    

    super(SharedMotifsNetwork, self).__init__(
      session=session,
      sample_dir=sample_dir,
    )

  def add_class_specific_sql_entry(self, session):
    base = SharedMotifsNetworkBase(
      id=self.id,
      pool_id=self.motif_pool_id,
      
      init_size=int(self.init_size),
      init_structure=self.init_structure,
      init_exp_node_edges=float(self.init_exp_node_edges),
      maintain_motif_degree=bool(self.maintain_motif_degree),
      n_passes=int(self.n_passes),
      expansion_prob=float(self.expansion_prob),
    )
    session.add(base)
    session.commit()

  def generate(self):
    node_labels = {}
    adj = SMMotif.create_motif(n_nodes=self.init_size,
                               structure=self.init_structure,
                               exp_node_edges=self.init_exp_node_edges)
    
    communities = np.zeros(shape=(len(adj), 1), dtype=np.int32)
    communities = np.concatenate(
      [communities,
       np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
  
    for P in range(self.n_passes):
      communities = np.concatenate(
        [communities,
         np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
      n = 0
      n_nodes = len(adj)
    
      while n < n_nodes:
        e = np.random.rand()
        if e < self.expansion_prob:
          # A B C
          # D E F
          # G H I
        
          # todo: peculiar error, .choice fails if all motifs have same number of nodes as size of motif pool...
          # E = np.random.choice(a=self.motif_pool)
          E = self.motif_pool[np.random.randint(len(self.motif_pool))]
          motif_size = E.shape[0]
          block = np.zeros(shape=(n_nodes + motif_size - 1, n_nodes + motif_size - 1))
          block[n:n + motif_size, n:n + motif_size] = E
        
          block[:n, :n] = adj[:n, :n]  # A
          block[:n, n + motif_size:] = adj[:n, n + 1:]  # C
          block[n + motif_size:, :n] = adj[:n, n + 1:].T  # G
          block[n + motif_size:, n + motif_size:] = adj[n + 1:, n + 1:]  # I
        
          # E = generate_random_graph(n_nodes=motif_size, sparsity=self.motif_sparsity)
        
          # this creates a zero matrix representing the edges from the motif to the nodes preceding
          # it, then places the original vector inside at random indices. Hence node degree is
          # preserved, original edges are affixed to random nodes in the motif.
          if n > 0:
            B_old = adj[:n, n]
            choice = np.random.choice(motif_size, size=(n,))
            np.put_along_axis(
              block[:n, n:n + motif_size], choice[:, None], B_old[:, None], axis=1)  # B
            block[n:n + motif_size, :n] = block[:n, n:n + motif_size].T  # D
          
            if not self.maintain_motif_degree:
              # add some random 1s, but only in rows where there are 1s already (+ same for F)
              raise NotImplementedError()
        
          if n < n_nodes:
            F_old = adj[n, n + 1:]
            choice = np.random.choice(motif_size, size=(n_nodes - n - 1,))
            np.put_along_axis(
              block[n:n + motif_size, n + motif_size:], choice[None, :], F_old[None, :],
              axis=0)  # F
            block[n + motif_size:, n:n + motif_size] = block[n:n + motif_size,
                                                       n + motif_size:].T  # H
        
          adj = block
          repeats = np.ones(shape=(communities.shape[0]), dtype=np.int32)
          repeats[n] = motif_size
          communities = communities.repeat(repeats=repeats, axis=0)
          communities[:, -1] = np.arange(communities.shape[0], dtype=np.int32)
        
          n_nodes += (motif_size - 1)
          n += motif_size - 1
        n += 1
  
    node_labels.update(
      {f'communities_L{L}': communities[:, L] for L in range(communities.shape[1])}
    )

    adj, node_labels = self.permute_nodes(adj=adj, node_labels_dict=node_labels)

    del self.motif_pool
    self.save_adjacency_matrix_component(A=adj, long_index=False)
    self.save_node_labels(node_labels)
    for lbl in node_labels.keys():
      self.communities_to_lp_labels(lbl)
