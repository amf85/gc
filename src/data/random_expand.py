import numpy as np
from sqlalchemy import select, and_, or_

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample
from sql.sql_base import RandomlyExpandedNetworkBase, GCSampleBase, REDataSplitBase, GCDataSplitBase
from util.util import printProgressBar
import pickle

class REDataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_re'
  
  @property
  def split_class(self):
    return 'RandomlyExpandedNetwork'
  
  class RESplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,
                 
                 init_g_sizes,
                 motif_min_nodes,
                 motif_max_nodes,
                 n_passes,
                 expansion_prob=0.5,
                 init_g_sparsity=0.5,
                 motif_sparsity=0.5,
                 maintain_motif_degree=True,
                 
                 **kwargs,
                 ):
      super(REDataSplit.RESplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )
      self.init_g_sizes = init_g_sizes
      self.motif_min_nodes = motif_min_nodes
      self.motif_max_nodes = motif_max_nodes
      self.n_passes = n_passes
      self.expansion_prob = expansion_prob
      self.init_g_sparsity = init_g_sparsity
      self.motif_sparsity = motif_sparsity
      self.maintain_motif_degree = maintain_motif_degree
  
  def __init__(self,
               dataset,
               params: RESplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    super(REDataSplit, self).__init__(
      dataset=dataset,
      params=params,
      exclude_sample_ids=exclude_sample_ids,
      split=split,
      preprocess_eig=preprocess_eig,
      preprocess_local=preprocess_local,
      preprocess_global=preprocess_global,
      preprocess_distance=preprocess_distance,
      feature_conf=feature_conf,
      transform=transform,
      create=create,
      named_identifier=named_identifier,
      overwrite_identifier=overwrite_identifier,
    )

  def add_samples(self,
                  params: RESplitParams,
                  preprocess_eig=None,
                  preprocess_distance=False,
                  preprocess_local=None,
                  preprocess_global=None,
                  create_new=False,
                  feature_conf=None,
                  ):
    rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    
    session = self.dataset.gcdb.local_sess()
    stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    base = session.execute(stmt).first()[0]

    self.validate_data_directories(root_dir=self.split_dir)

    for i in range(params.n_samples):
      init_g_size = int(np.random.choice(params.init_g_sizes))
      n_passes = int(np.random.choice(params.n_passes))
      
      if create_new:
        matching = []
      else:
        stmt = select(GCSampleBase).join(RandomlyExpandedNetworkBase).where(
          and_(
            RandomlyExpandedNetworkBase.init_g_size == init_g_size,
            RandomlyExpandedNetworkBase.motif_min_nodes == params.motif_min_nodes,
            RandomlyExpandedNetworkBase.motif_max_nodes == params.motif_max_nodes,
            RandomlyExpandedNetworkBase.n_passes == n_passes,
            RandomlyExpandedNetworkBase.expansion_prob == params.expansion_prob,
            RandomlyExpandedNetworkBase.init_g_sparsity == params.init_g_sparsity,
            RandomlyExpandedNetworkBase.motif_sparsity == params.motif_sparsity,
            RandomlyExpandedNetworkBase.maintain_motif_degree == params.maintain_motif_degree,
            GCSampleBase.id.notin_(sample_ids_added + self.exclude_sample_ids)
          )
        )
        
        matching = session.execute(stmt).all()
      
      if len(matching) > 0:
        sample = rng.choice(matching, axis=0)[0]
        sample_ids_added.append(sample.id)
        file_index.append(sample.file)
        base.samples.append(sample)
        pkl_file = sample.file
      else:
        sample = RandomlyExpandedNetwork(
          session=session,
          sample_dir=self.split_dir,
          
          init_g_size=init_g_size,
          motif_min_nodes=params.motif_min_nodes,
          motif_max_nodes=params.motif_max_nodes,
          n_passes=n_passes,
          expansion_prob=params.expansion_prob,
          init_g_sparsity=params.init_g_sparsity,
          motif_sparsity=params.motif_sparsity,
          maintain_motif_degree=params.maintain_motif_degree,
        )
        sample_ids_added.append(sample.id)
        file_index.append(sample.filename)
        stmt = select(GCSampleBase).where(GCSampleBase.id == sample.id)
        sample_base = session.execute(stmt).first()[0]
        base.samples.append(sample_base)
        pkl_file = sample.filename
      
      sample: GCSample = pickle.load(open(pkl_file, 'rb'))

      if preprocess_distance and not sample.preprocessed_distance:
        sample.preprocess_distance_matrix(recompute=False)
      if any(f not in sample.preprocessed_global_features for f in preprocess_global) \
          or any(f not in sample.preprocessed_local_features for f in preprocess_local):
        sample.preprocess_graph_features(
          local_feature_names=preprocess_local, global_feature_names=preprocess_global)
      if not sample.verify_eig_conf_present(preprocess_eig):
        sample.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)
      
      if any(f not in sample.preprocessed_local_features for f in feature_conf['local_features']):
        sample.preprocess_graph_features(local_feature_names=feature_conf['local_features'],
                                         global_feature_names=[])
      if not sample.verify_eig_conf_present(feature_conf['eig_conf']):
        sample.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'], force_recompute=False)
      
      printProgressBar(iteration=i+1,
                       total=params.n_samples,
                       prefix=f'Populating {self.split} dataset.',
                       suffix=f'({i+1}/{params.n_samples})',
                       length=100)
      
      session.commit()
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    init_g_sizes = ','.join(map(str, self.params.init_g_sizes))
    n_passes = ','.join(map(str, self.params.n_passes))
    base = REDataSplitBase(
      id=self.id,
      split=self.split,
      n_samples=self.params.n_samples,
      
      init_g_sizes=init_g_sizes,
      motif_min_nodes=self.params.motif_min_nodes,
      motif_max_nodes=self.params.motif_max_nodes,
      n_passes=n_passes,
      expansion_prob=self.params.expansion_prob,
      init_g_sparsity=self.params.init_g_sparsity,
      motif_sparsity=self.params.motif_sparsity,
      maintain_motif_degree=self.params.maintain_motif_degree,
      
      directory=self.split_dir
    )
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.commit()


class REDataset(GCDataset):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(REDataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb, verbose=verbose)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = REDataSplit(dataset=self,
                        split=split_name,
                        params=split_params,
                        exclude_sample_ids=exclude_sample_ids,
                        preprocess_eig=preprocess_eig,
                        preprocess_local=preprocess_local,
                        preprocess_global=preprocess_global,
                        preprocess_distance=preprocess_distance,
                        feature_conf=feature_conf,
                        transform=transform,
                        create=create,
                        named_identifier=named_identifier,
                        overwrite_identifier=overwrite_identifier,
                        )
    self.splits[split_name] = split
    return split


class RandomlyExpandedNetwork(GCSample):
  db_table = 'samples_re'
  
  def __init__(self,
               session,
               sample_dir,

               init_g_size,
               motif_min_nodes,
               motif_max_nodes,
               n_passes,
               expansion_prob=0.5,
               init_g_sparsity=0.5,
               motif_sparsity=0.5,
               maintain_motif_degree=True,
               ):
    self.init_g_size = init_g_size
    self.motif_min_nodes = motif_min_nodes
    self.motif_max_nodes = motif_max_nodes
    self.n_passes = n_passes
    self.expansion_prob = expansion_prob
    self.init_g_sparsity = init_g_sparsity
    self.motif_sparsity = motif_sparsity
    self.maintain_motif_degree = maintain_motif_degree
    self.features = None
    self.feature_dict = {}
    
    super(RandomlyExpandedNetwork, self).__init__(
      session=session,
      sample_dir=sample_dir,
    )
  
  def add_class_specific_sql_entry(self, session):
    base = RandomlyExpandedNetworkBase(
      id=self.id,
      init_g_size=int(self.init_g_size),
      motif_min_nodes=int(self.motif_min_nodes),
      motif_max_nodes=int(self.motif_max_nodes),
      n_passes=int(self.n_passes),
      expansion_prob=float(self.expansion_prob),
      init_g_sparsity=float(self.init_g_sparsity),
      motif_sparsity=float(self.motif_sparsity),
      maintain_motif_degree=self.maintain_motif_degree,
    )
    session.add(base)
    session.commit()
  
  def generate(self):
    adj = generate_random_graph(n_nodes=self.init_g_size, sparsity=self.init_g_sparsity)
    labels = {}
    communities = np.zeros(shape=(len(adj), 1), dtype=np.int32)
    communities = np.concatenate(
      [communities,
       np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
    
    for P in range(self.n_passes):
      communities = np.concatenate(
        [communities,
         np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
      n = 0
      n_nodes = len(adj)
      
      while n < n_nodes:
        e = np.random.rand()
        if e < self.expansion_prob:
          # A B C
          # D E F
          # G H I
          
          motif_size = np.random.randint(low=self.motif_min_nodes, high=self.motif_max_nodes + 1)
          block = np.zeros(shape=(n_nodes + motif_size - 1, n_nodes + motif_size - 1))
          
          block[:n, :n] = adj[:n, :n]  # A
          block[:n, n + motif_size:] = adj[:n, n + 1:]  # C
          block[n + motif_size:, :n] = adj[:n, n + 1:].T  # G
          block[n + motif_size:, n + motif_size:] = adj[n + 1:, n + 1:]  # I
          
          E = generate_random_graph(n_nodes=motif_size, sparsity=self.motif_sparsity)
          block[n:n + motif_size, n:n + motif_size] = E
          
          # this creates a zero matrix representing the edges from the motif to the nodes preceding
          # it, then places the original vector inside at random indices. Hence node degree is
          # preserved, original edges are affixed to random nodes in the motif.
          if n > 0:
            B_old = adj[:n, n]
            choice = np.random.choice(motif_size, size=(n,))
            np.put_along_axis(
              block[:n, n:n + motif_size], choice[:, None], B_old[:, None], axis=1)  # B
            block[n:n + motif_size, :n] = block[:n, n:n + motif_size].T  # D
            
            if not self.maintain_motif_degree:
              # add some random 1s, but only in rows where there are 1s already (+ same for F)
              raise NotImplementedError()
          
          if n < n_nodes:
            F_old = adj[n, n + 1:]
            choice = np.random.choice(motif_size, size=(n_nodes - n - 1,))
            np.put_along_axis(
              block[n:n + motif_size, n + motif_size:], choice[None, :], F_old[None, :],
              axis=0)  # F
            block[n + motif_size:, n:n + motif_size] = block[n:n + motif_size,
                                                       n + motif_size:].T  # H
          
          adj = block
          repeats = np.ones(shape=(communities.shape[0]), dtype=np.int32)
          repeats[n] = motif_size
          communities = communities.repeat(repeats=repeats, axis=0)
          communities[:, -1] = np.arange(communities.shape[0], dtype=np.int32)
          
          n_nodes += (motif_size - 1)
          n += motif_size - 1
        n += 1
    
    labels.update(
      {f'communities_L{L}': communities[:, L] for L in range(communities.shape[1])}
    )
    self.save_node_labels(labels)
    self.save_adjacency_matrix_component(A=adj, long_index=False)
