import numpy as np
from sqlalchemy import select, and_, or_

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample, is_connected
from sql.sql_base import StochasticBlockModelBase, GCSampleBase, SBMDataSplitBase, GCDataSplitBase
from util.util import printProgressBar

import pickle

class SBMDataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_sbm'
  
  @property
  def split_class(self):
    return 'StochasticBlockModel'
  
  class SBMSplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,

                 min_nodes,
                 max_nodes,
                 min_clusters,
                 max_clusters,
                 priors_a,
                 priors_b,
                 intra_connect_min,
                 intra_connect_max,
                 inter_connect_min,
                 inter_connect_max,
                 
                 **kwargs,
                 ):
      super(SBMDataSplit.SBMSplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )
      self.min_nodes = min_nodes
      self.max_nodes = max_nodes
      self.min_clusters = min_clusters
      self.max_clusters = max_clusters
      self.priors_a = priors_a
      self.priors_b = priors_b
      self.intra_connect_min = intra_connect_min
      self.intra_connect_max = intra_connect_max
      self.inter_connect_min = inter_connect_min
      self.inter_connect_max = inter_connect_max
  
  def __init__(self,
               dataset,
               params: SBMSplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    super(SBMDataSplit, self).__init__(
      dataset=dataset,
      params=params,
      exclude_sample_ids=exclude_sample_ids,
      split=split,
      preprocess_eig=preprocess_eig,
      preprocess_local=preprocess_local,
      preprocess_global=preprocess_global,
      preprocess_distance=preprocess_distance,
      feature_conf=feature_conf,
      transform=transform,
      create=create,
      named_identifier=named_identifier,
      overwrite_identifier=overwrite_identifier,
    )

  def add_samples(self,
                  params: SBMSplitParams,
                  preprocess_eig=None,
                  preprocess_distance=False,
                  preprocess_local=None,
                  preprocess_global=None,
                  feature_conf=None,
                  create_new=False,
                  ):
    rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    
    session = self.dataset.gcdb.local_sess()
    stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    base = session.execute(stmt).first()[0]
    
    self.validate_data_directories(root_dir=self.split_dir)
    
    for i in range(params.n_samples):
      n_nodes = np.random.randint(low=params.min_nodes, high=params.max_nodes)
      n_clusters = np.random.randint(low=params.min_clusters, high=params.max_clusters)
      
      if create_new:
        matching = []
      else:
        stmt = select(GCSampleBase).join(StochasticBlockModelBase).where(
          and_(
            StochasticBlockModelBase.n_nodes == n_nodes,
            StochasticBlockModelBase.n_clusters == n_clusters,
            StochasticBlockModelBase.priors_a == params.priors_a,
            StochasticBlockModelBase.priors_b == params.priors_b,
            StochasticBlockModelBase.intra_connect_min == params.intra_connect_min,
            StochasticBlockModelBase.intra_connect_max == params.intra_connect_max,
            StochasticBlockModelBase.inter_connect_min == params.inter_connect_min,
            StochasticBlockModelBase.inter_connect_max == params.inter_connect_max,
            GCSampleBase.id.notin_(sample_ids_added + self.exclude_sample_ids)
          )
        )
        
        matching = session.execute(stmt).all()
      
      if len(matching) > 0:
        sample = rng.choice(matching, axis=0)[0]
        sample_ids_added.append(sample.id)
        file_index.append(sample.file)
        base.samples.append(sample)
        pkl_file = sample.file
      else:
        sample = StochasticBlockModel(
          session=session,
          sample_dir=self.split_dir,
          
          n_nodes=n_nodes,
          n_clusters=n_clusters,
          priors_a=params.priors_a,
          priors_b=params.priors_b,
          intra_connect_min=params.intra_connect_min,
          intra_connect_max=params.intra_connect_max,
          inter_connect_min=params.inter_connect_min,
          inter_connect_max=params.inter_connect_max,
        )
        sample_ids_added.append(sample.id)
        file_index.append(sample.filename)
        stmt = select(GCSampleBase).where(GCSampleBase.id == sample.id)
        sample_base = session.execute(stmt).first()[0]
        base.samples.append(sample_base)
        pkl_file = sample.filename

      sample: GCSample = pickle.load(open(pkl_file, 'rb'))

      if preprocess_distance and not sample.preprocessed_distance:
        sample.preprocess_distance_matrix(recompute=False)
      if any(f not in sample.preprocessed_global_features for f in preprocess_global) \
          or any(f not in sample.preprocessed_local_features for f in preprocess_local):
        sample.preprocess_graph_features(
          local_feature_names=preprocess_local, global_feature_names=preprocess_global)
      if not sample.verify_eig_conf_present(preprocess_eig):
        sample.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)
      
      if any(f not in sample.preprocessed_local_features for f in feature_conf['local_features']):
        sample.preprocess_graph_features(local_feature_names=feature_conf['local_features'],
                                         global_feature_names=[])
      if not sample.verify_eig_conf_present(feature_conf['eig_conf']):
        sample.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'], force_recompute=False)
        
      printProgressBar(iteration=i+1,
                       total=params.n_samples,
                       prefix=f'Populating {self.split} dataset.',
                       suffix=f'({i + 1}/{params.n_samples})',
                       length=100)
      
      session.commit()
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    base = SBMDataSplitBase(
      id=self.id,
      split=self.split,
      n_samples=self.params.n_samples,
      
      min_nodes=self.params.min_motif_nodes,
      max_nodes=self.params.max_motif_nodes,
      min_clusters=self.params.min_clusters,
      max_clusters=self.params.max_clusters,
      priors_a=self.params.priors_a,
      priors_b=self.params.priors_b,
      intra_connect_min=self.params.intra_connect_min,
      intra_connect_max=self.params.intra_connect_max,
      inter_connect_min=self.params.inter_connect_min,
      inter_connect_max=self.params.inter_connect_max,
      
      directory=self.split_dir
    )
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.flush()
    session.commit()


class SBMDataset(GCDataset):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(SBMDataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb, verbose=verbose)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = SBMDataSplit(dataset=self,
                         split=split_name,
                         params=split_params,
                         exclude_sample_ids=exclude_sample_ids,
                         preprocess_eig=preprocess_eig,
                         preprocess_local=preprocess_local,
                         preprocess_global=preprocess_global,
                         preprocess_distance=preprocess_distance,
                         feature_conf=feature_conf,
                         transform=transform,
                         create=create,
                         named_identifier=named_identifier,
                         overwrite_identifier=overwrite_identifier,
                         )
    self.splits[split_name] = split
    return split


class StochasticBlockModel(GCSample):
  db_table = 'samples_sbm'
  
  def __init__(self,
               session,
               sample_dir,

               n_nodes,
               n_clusters,
               priors_a,
               priors_b,
               intra_connect_min,
               intra_connect_max,
               inter_connect_min,
               inter_connect_max,
               ):
    self.n_nodes = n_nodes
    self.n_clusters = n_clusters
    self.priors_a = priors_a
    self.priors_b = priors_b
    self.intra_connect_min = intra_connect_min
    self.intra_connect_max = intra_connect_max
    self.inter_connect_min = inter_connect_min
    self.inter_connect_max = inter_connect_max
    
    self.cluster_prior = np.random.randint(low=priors_a,
                                           high=priors_b,
                                           size=n_clusters).astype(np.float32)
    self.cluster_prior /= np.sum(self.cluster_prior)
    
    intras = np.random.rand(n_clusters)
    intras *= (intra_connect_max - intra_connect_min)
    intras += intra_connect_min
    
    connectivities = []
    for i in range(n_clusters):
      intra = intras[i]
      connectivity = np.random.rand(n_clusters - 1)
      connectivity *= (inter_connect_max - inter_connect_min)
      connectivity += inter_connect_min
      
      # connectivity = np.random.randint(low=inter_connect_min,
      #                                  high=inter_connect_max,
      #                                  size=n_clusters-1).astype(np.float32)
      # connectivity /= np.sum(connectivity)
      # connectivity *= (1 - intra)
      connectivity = np.insert(connectivity, i, intra)
      connectivities.append(connectivity)
    self.connectivity = np.vstack(connectivities)
    
    
    self.features = None
    self.feature_dict = {}
    self.A = None
    
    super(StochasticBlockModel, self).__init__(
      session=session,
      sample_dir=sample_dir,
    )
  
  def add_class_specific_sql_entry(self, session):
    base = StochasticBlockModelBase(
      id=self.id,
      n_nodes=self.n_nodes,
      n_clusters=self.n_clusters,
      priors_a=self.priors_a,
      priors_b=self.priors_b,
      intra_connect_min=self.intra_connect_min,
      intra_connect_max=self.intra_connect_max,
      inter_connect_min=self.inter_connect_min,
      inter_connect_max=self.inter_connect_max
    )
    session.add(base)
    session.commit()
  
  def generate(self):
    labels = {}
    A, X, valid = None, None, False
    while not valid:
      X = np.random.choice(a=len(self.cluster_prior), size=self.n_nodes, p=self.cluster_prior).astype(int)
      adj_probs = [[self.connectivity[i][j] for j in X] for i in X]
      for i in range(len(adj_probs)):
        adj_probs[i][i] = 0
      A = np.vectorize(lambda p: 1 if np.random.rand() < p else 0)(adj_probs)
      A = np.tril(A) + np.tril(A).T
      valid = is_connected(A)
    
    labels.update({'communities_L1': X})
    self.save_node_labels(labels)
    self.save_adjacency_matrix_component(A=A, long_index=False)
