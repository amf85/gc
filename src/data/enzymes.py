import shutil

import numpy as np
from sqlalchemy import select, and_, or_
import json
import os
import pickle
import scipy.sparse as sps
from torch_geometric.utils import to_dense_adj
from data.gc_data import is_connected

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample
from sql.sql_base import (
  MotifExpandedNetworkBase,
  GCSampleBase,
  MEDataSplitBase,
  GCDataSplitBase,
  GCEnzymeBase,
  EnzymesDataSplitBase
)
from util.util import printProgressBar, validate_directory
from util.constants import *
from torch_geometric.datasets import TUDataset

class EnzymesDataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_enz'
  
  @property
  def split_class(self):
    return 'GCEnzyme'
  
  class EnzymesSplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,
                 
                 **kwargs,
                 ):
      super(EnzymesDataSplit.EnzymesSplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )
  
  def __init__(self,
               dataset,
               params: EnzymesSplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    super(EnzymesDataSplit, self).__init__(
      dataset=dataset,
      params=params,
      exclude_sample_ids=exclude_sample_ids,
      split=split,
      preprocess_eig=preprocess_eig,
      preprocess_local=preprocess_local,
      preprocess_global=preprocess_global,
      preprocess_distance=preprocess_distance,
      feature_conf=feature_conf,
      transform=transform,
      create=create,
      named_identifier=named_identifier,
      overwrite_identifier=overwrite_identifier,
    )
    
  # def create_one_sample(self):
  def convert_source_data(self):
    enzymes = TUDataset(root=f'{ENZYMES_ROOT}', name='ENZYMES', use_node_attr=True)
    print('test')
    # edge_index_dir = validate_directory(f'{self.split_dir}/{ADJ_INDEX_SFX}')
    # edge_attr_dir = validate_directory(f'{self.split_dir}/{ADJ_ATTR_SFX}')
    # graph_labels_dir = validate_directory(f'{self.split_dir}/{GRAPH_LBL_SFX}')
    # node_labels_dir = validate_directory(f'{self.split_dir}/{NODE_LBL_SFX}')
    # node_attrs_dir = validate_directory(f'{self.split_dir}/{NODE_ATTR_SFX}')
    if os.path.exists(f'{self.split_dir}/rejected_samples.pkl'):
      rejected_samples = pickle.load(open(f'{self.split_dir}/rejected_samples.pkl', 'rb'))
    else:
      rejected_samples = []
    recompute = False
    for i in range(1, len(enzymes.data['y'])+1):
      sample_file = f'{self.split_dir}/{SAMPLES_SFX}/{i}.pkl'
      if not os.path.exists(sample_file) and (i not in rejected_samples):
        recompute = True
        break
      sample_pkl = pickle.load(open(f'{self.split_dir}/{SAMPLES_SFX}/{i}.pkl', 'rb'))
      if sample_pkl.initialised == False:
        recompute = True
        break

    rejected_samples = []

    if recompute:
      shutil.rmtree(self.split_dir)
      self.validate_data_directories(root_dir=self.split_dir)
      for i in range(len(enzymes.data['y'])):

        n_nodes = enzymes.data.num_nodes[i]

        edge_index_start, edge_index_end = enzymes.slices['edge_index'][i], enzymes.slices['edge_index'][i+1]
        edge_index = enzymes.data['edge_index'][:, edge_index_start:edge_index_end]
        edge_attr = np.ones(edge_index.shape[1])
        graph_label_index = enzymes.slices['y'][i]
        graph_label_raw = enzymes.data['y'][graph_label_index]
        graph_labels_dict = {'ec_class': graph_label_raw.numpy()[None]}

        adj = to_dense_adj(edge_index)[0].numpy()
        if not is_connected(adj, reverse=False):
          pass
        if i == 37:
          print('test')
          # rejected_samples.append(i+1)
          # continue

        nodes_start, nodes_end = enzymes.slices['x'][i], enzymes.slices['x'][i + 1]
        node_attrs = enzymes.data['x'][nodes_start:nodes_end, :-enzymes.num_node_labels]

        node_attrs_dict = {f'c{j}': node_attrs[:, j] for j in range(6)}
        node_attrs_dict.update({f'd{j}': node_attrs[:, j + 6] for j in range(12)})

        node_labels_raw = enzymes.data['x'][nodes_start:nodes_end, -enzymes.num_node_labels:]
        node_labels_int = np.where(node_labels_raw.numpy() == 1)[1]
        node_labels_int += 1
        node_labels_dict = {'sse_type': node_labels_int}


        sample = GCEnzyme(
          session=self.dataset.gcdb.local_sess(),
          sample_dir=self.split_dir,

          edge_index=edge_index,
          num_nodes=n_nodes,
          node_attrs_dict=node_attrs_dict,
          graph_labels_dict=graph_labels_dict,
          node_labels_dict=node_labels_dict
        )

      pickle.dump(rejected_samples, open(f'{self.split_dir}/rejected_samples.pkl', 'wb'))
      with open(f'{self.split_dir}/rejected_samples.json', 'w') as f:
        f.write(json.dumps(rejected_samples, indent=2))




      # print('test')



  
  def add_samples(self,
                  params: EnzymesSplitParams,
                  preprocess_eig=None,
                  preprocess_distance=False,
                  preprocess_local=None,
                  preprocess_global=None,
                  feature_conf=None,
                  create_new=False,
                  ):
    self.validate_data_directories(root_dir=self.split_dir)
    self.convert_source_data()
    rejected_samples = pickle.load(open(f'{self.split_dir}/rejected_samples.pkl', 'rb'))
    
    rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    
    session = self.dataset.gcdb.local_sess()
    stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    base = session.execute(stmt).first()[0]

    # sample_num = 1
    # i = 1
    # while i < params.n_samples + 1:
    for i in range(1, params.n_samples+1):
      # if sample_num in rejected_samples:
      if i in rejected_samples:
        # i += 1
        continue
      pkl_file = f'{self.split_dir}/{SAMPLES_SFX}/{i}.pkl'
      
      sample: GCSample = pickle.load(open(pkl_file, 'rb'))
      sample_ids_added.append(i)
      file_index.append(pkl_file)
      stmt = select(GCSampleBase).where(GCSampleBase.id == i)
      sample_base = session.execute(stmt).first()[0]
      base.samples.append(sample_base)


      if preprocess_distance and not sample.preprocessed_distance:
        sample.preprocess_distance_matrix(recompute=False)
      if any(f not in sample.preprocessed_global_features for f in preprocess_global) \
          or any(f not in sample.preprocessed_local_features for f in preprocess_local):
        sample.preprocess_graph_features(
          local_feature_names=preprocess_local, global_feature_names=preprocess_global)
      if not sample.verify_eig_conf_present(preprocess_eig):
        sample.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)
      
      if any(f not in sample.preprocessed_local_features for f in feature_conf['local_features']):
        sample.preprocess_graph_features(local_feature_names=feature_conf['local_features'],
                                         global_feature_names=[])
      if not sample.verify_eig_conf_present(feature_conf['eig_conf']):
        sample.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'], force_recompute=False)
      
      printProgressBar(iteration=i+1,
                       total=params.n_samples,
                       prefix=f'Preprocessing {self.split} dataset.',
                       suffix=f'({i+1}/{params.n_samples})',
                       length=100)
      
      session.commit()
      # sample_num += 1
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    base = EnzymesDataSplitBase(
      id=self.id,
      split=self.split,
      n_samples=self.params.n_samples,

      directory=self.split_dir
    )
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.commit()


class EnzymesDataset(GCDataset):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(EnzymesDataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb, verbose=verbose)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = EnzymesDataSplit(dataset=self,
                             split=split_name,
                             params=split_params,
                             exclude_sample_ids=exclude_sample_ids,
                             preprocess_eig=preprocess_eig,
                             preprocess_local=preprocess_local,
                             preprocess_global=preprocess_global,
                             preprocess_distance=preprocess_distance,
                             feature_conf=feature_conf,
                             transform=transform,
                             create=create,
                             named_identifier=named_identifier,
                             overwrite_identifier=overwrite_identifier,
                             )
    self.splits[split_name] = split
    return split


class GCEnzyme(GCSample):
  db_table = 'samples_enz'
  
  def __init__(self,
               session,
               sample_dir,

               edge_index,
               num_nodes,
               node_attrs_dict,
               graph_labels_dict,
               node_labels_dict,

               ):
    self.initialised = False
    super(GCEnzyme, self).__init__(
      session=session,
      sample_dir=sample_dir,
    )
    self.num_nodes = num_nodes
    adj = to_dense_adj(edge_index, max_num_nodes=num_nodes)[0].numpy()
    self.save_adjacency_matrix_component(A=adj, long_index=False)
    self.save_node_labels(node_labels_dict)
    for lbl in node_labels_dict.keys():
      self.communities_to_lp_labels(lbl)
    self.save_node_attrs(node_attrs_dict)
    self.save_graph_labels(graph_labels_dict)

    self.initialised = True
    pickle.dump(self, open(self.filename, 'wb'))
  
  def add_class_specific_sql_entry(self, session):
    base = GCEnzymeBase(
      id=self.id,
    )
    session.add(base)
    session.commit()
  
  def generate(self):
    ...
    
    
