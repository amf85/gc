import pickle

import numpy as np
from sqlalchemy import select, and_, or_

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample
from sql.sql_base import HierarchicalNetworkBase, HNDataSplitBase, GCSampleBase, GCDataSplitBase
from util.util import printProgressBar
import pickle


class HNDataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_hn'
  
  @property
  def split_class(self):
    return 'HierarchicalNetwork'
  
  class HNSplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,
                 
                 motif_sizes,
                 branching_factors,
                 depths,
                 
                 **kwargs,
                 ):
      super(HNDataSplit.HNSplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )
      self.motif_sizes = motif_sizes
      self.branching_factors = branching_factors
      self.depths = depths
  
  def __init__(self,
               dataset,
               params: HNSplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    super(HNDataSplit, self).__init__(
      dataset=dataset,
      params=params,
      exclude_sample_ids=exclude_sample_ids,
      split=split,
      preprocess_eig=preprocess_eig,
      preprocess_local=preprocess_local,
      preprocess_global=preprocess_global,
      preprocess_distance=preprocess_distance,
      feature_conf=feature_conf,
      transform=transform,
      create=create,
      named_identifier=named_identifier,
      overwrite_identifier=overwrite_identifier,
    )

  def add_samples(self,
                  params: HNSplitParams,
                  preprocess_eig=None,
                  preprocess_distance=False,
                  preprocess_local=None,
                  preprocess_global=None,
                  create_new=False,
                  feature_conf=None,
                  ):
    rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    
    session = self.dataset.gcdb.local_sess()
    stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    base = session.execute(stmt).first()[0]
    
    self.validate_data_directories(root_dir=self.split_dir)
    
    for i in range(params.n_samples):
      motif_size = int(np.random.choice(params.motif_sizes))
      branching_factor = int(np.random.choice(params.branching_factors))
      depth = int(np.random.choice(params.depths))
      
      if create_new:
        matching = []
      else:
        stmt = select(GCSampleBase).join(HierarchicalNetworkBase).where(
          and_(
            HierarchicalNetworkBase.motif_size == motif_size,
            HierarchicalNetworkBase.branching_factor == branching_factor,
            HierarchicalNetworkBase.depth == depth,
            GCSampleBase.id.notin_(sample_ids_added + self.exclude_sample_ids)
          )
        )
        matching = session.execute(stmt).all()
      
      if len(matching) > 0:
        sample = rng.choice(matching, axis=0)[0]
        sample_ids_added.append(sample.id)
        file_index.append(sample.file)
        base.samples.append(sample)
        pkl_file = sample.file
      else:
        sample = HierarchicalNetwork(
          session=session,
          sample_dir=self.split_dir,
          
          motif_adj=generate_random_graph(motif_size),
          branching_factor=branching_factor,
          depth=depth,
        )
        sample_ids_added.append(sample.id)
        file_index.append(sample.filename)
        stmt = select(GCSampleBase).where(GCSampleBase.id == sample.id)
        sample_base = session.execute(stmt).first()[0]
        base.samples.append(sample_base)
        pkl_file = sample.filename
      
      sample: GCSample = pickle.load(open(pkl_file, 'rb'))

      if preprocess_distance and not sample.preprocessed_distance:
        sample.preprocess_distance_matrix(recompute=False)
      if any(f not in sample.preprocessed_global_features for f in preprocess_global) \
          or any(f not in sample.preprocessed_local_features for f in preprocess_local):
        sample.preprocess_graph_features(
          local_feature_names=preprocess_local, global_feature_names=preprocess_global)
      if not sample.verify_eig_conf_present(preprocess_eig):
        sample.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)
        
      if any(f not in sample.preprocessed_local_features for f in feature_conf['local_features']):
        sample.preprocess_graph_features(local_feature_names=feature_conf['local_features'],
                                         global_feature_names=[])
      if not sample.verify_eig_conf_present(feature_conf['eig_conf']):
        sample.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'], force_recompute=False)
      
      printProgressBar(iteration=i+1,
                       total=params.n_samples,
                       prefix=f'Populating {self.split} dataset.',
                       suffix=f'({i+1}/{params.n_samples})',
                       length=100)
      
      session.commit()
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    motif_sizes = ','.join(map(str, self.params.motif_sizes))
    branching_factors = ','.join(map(str, self.params.branching_factors))
    depths = ','.join(map(str, self.params.depths))
    base = HNDataSplitBase(
      id=self.id,
      split=self.split,
      n_samples=self.params.n_samples,
      
      motif_sizes=motif_sizes,
      branching_factors=branching_factors,
      depths=depths,
      
      directory=self.split_dir,
    )
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.flush()
    session.commit()


class HNDataset(GCDataset):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(HNDataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb, verbose=verbose)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = HNDataSplit(dataset=self,
                        split=split_name,
                        params=split_params,
                        exclude_sample_ids=exclude_sample_ids,
                        preprocess_eig=preprocess_eig,
                        preprocess_local=preprocess_local,
                        preprocess_global=preprocess_global,
                        preprocess_distance=preprocess_distance,
                        feature_conf=feature_conf,
                        transform=transform,
                        create=create,
                        named_identifier=named_identifier,
                        overwrite_identifier=overwrite_identifier,
                        )
    self.splits[split_name] = split
    return split


class HierarchicalNetwork(GCSample):
  db_table = 'samples_hn'
  
  def __init__(self,
               session,
               sample_dir,

               motif_adj,
               depth,
               branching_factor,
               ):
    self.motif_adj = motif_adj
    self.depth = depth
    self.branching_factor = branching_factor
    # using 'c' for centre nodes; 'p' for periphery (due to be attached to a c); 'o' for ordinary
    self.node_types = np.array(['c'] + ['p'] * (len(motif_adj) - 1))
    self.features = np.random.randint(low=0, high=2, size=len(motif_adj))[:, None]
    # self.labels = self.init_labels()
    self.feature_dict = {}
    # self.A = motif_adj
    
    super(HierarchicalNetwork, self).__init__(
      session=session,
      sample_dir=sample_dir,
    )
  
  def add_class_specific_sql_entry(self, session):
    base = HierarchicalNetworkBase(
      id=self.id,
      motif_size=len(self.motif_adj),
      depth=self.depth,
      branching_factor=self.branching_factor
    )
    session.add(base)
    session.commit()
  
  def init_labels(self):
    graph_labels = {
      'hierarchical_mode': np.random.choice([-1, 1], size=len(self.motif_adj)),
      'c_node_sum': np.sum(self.features[self.node_types == 'c']),
      'all_node_sum': np.sum(self.features),
      'max_cluster_sum': np.sum(self.features),
    }
    node_labels = {f'communities_L0': np.array([0] * len(self.motif_adj))}
    graph_labels['hierarchical_mode'] = np.clip(
      np.sum(graph_labels['hierarchical_mode']), a_min=-1, a_max=1)
    return node_labels, graph_labels
  
  def generate(self):
    node_labels, graph_labels = self.init_labels()
    # node_labels = {}
    A = self.motif_adj
    
    for L in range(self.depth):
      node_tile = self.node_types.copy()
      level_X = [np.random.randint(low=0, high=2, size=len(node_tile))
                 for _ in range(self.branching_factor)]
      self.features = np.concatenate([self.features, np.concatenate(level_X)[:, None]], axis=0)
      
      # update node/graph labels before modifying node types during layer generation
      level_mode = [np.random.choice([-1, 1], size=len(node_tile))
                    for _ in range(self.branching_factor)]
      graph_labels['hierarchical_mode'] = np.clip(np.sum(
        [graph_labels['hierarchical_mode']] +
        [np.clip(np.sum(info), -1, 1) for info in level_mode]),
        -1, 1)
      graph_labels['all_node_sum'] += np.sum([np.sum(info) for info in level_X])
      graph_labels['max_cluster_sum'] = max(graph_labels['max_cluster_sum'],
                                           max(np.sum(level_X, axis=1)))
      graph_labels['c_node_sum'] += np.sum([motif[node_tile == 'c'][0] for motif in level_X])
      
      # generate the next layer
      # don't want to add edges from any existing nodes, so set their type to 'o'
      self.node_types[self.node_types == 'p'] = 'o'
      # in the new motifs, set the centre nodes to ordinary nodes, don't want new edges here
      node_tile[node_tile == 'c'] = 'o'
      node_types = np.tile(node_tile, (1, self.branching_factor))
      node_types = np.concatenate([self.node_types, node_types.squeeze()], axis=0)
      empty_block = np.zeros(A.shape)
      branch_eye = np.eye(self.branching_factor + 1)
      # tile the motifs as disconnected components
      A = np.array(np.bmat(
        [[A if int(e) == 1 else empty_block for e in row] for row in branch_eye]))
      # connect up the periphery nodes to the centre nodes (generally, should only be one c node)
      p_indices = np.argwhere(node_types == 'p')
      c_indices = np.argwhere(node_types == 'c')
      for p in p_indices:
        for c in c_indices:
          A[p, c], A[c, p] = 1, 1
      self.node_types = node_types
      
      # update community labels
      for j in range(L + 1, 0, -1):
        prev = node_labels[f'communities_L{j - 1}']
        new = np.concatenate([prev + (i * (self.branching_factor + 1) ** (j - 1)) for i in
                              range(1, self.branching_factor + 1)])
        node_labels[f'communities_L{j}'] = np.concatenate([prev, new])
      node_labels[f'communities_L{0}'] = np.array([0] * len(A))
    self.feature_dict.update({'random_binary': 0})
    graph_labels['c_node_modulo'] = graph_labels['c_node_sum'] % 2
    self.save_node_labels(node_labels)
    self.save_graph_labels(graph_labels)
    self.save_adjacency_matrix_component(A=A, long_index=False)
  
  def add_per_node_max_cluster(self, label, labels):
    if label[0] in labels.keys():
      return self
    if label[1] not in labels.keys():
      raise ValueError()
    communities = labels[label[1]]
    rb = self.features[:, self.feature_dict['random_binary']]
    sums = [np.sum(rb[np.where(communities == i)]) for i in range(np.max(communities) + 1)]
    sums_args_where_max = np.where(sums == np.max(sums))
    # is_max = np.zeros(shape=(len(sums),), dtype=np.int32)
    # is_max[sums_args_where_max] = 1
    output = np.zeros(shape=(communities.shape[0]))
    output[np.where(np.isin(communities, sums_args_where_max))] = 1
    labels[label[0]] = output
    pickle.dump(self, open(f'{self.filename}', 'wb'))
    self.save_node_labels(labels)
    return self
