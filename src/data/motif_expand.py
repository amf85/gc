import numpy as np
from sqlalchemy import select, and_, or_
import json
import pickle

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample
from sql.sql_base import MotifExpandedNetworkBase, GCSampleBase, MEDataSplitBase, GCDataSplitBase
from util.util import printProgressBar, validate_directory
from util.constants import *

class MEDataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_me'
  
  @property
  def split_class(self):
    return 'MotifExpandedNetwork'
  
  class MESplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,

                 init_g_min_nodes=4,
                 init_g_max_nodes=30,
                 init_g_exp_node_edges=2,
                 motif_min_nodes=3,
                 motif_max_nodes=7,
                 motif_exp_node_edges=3.5,
                 motif_pool_size=5,
                 expansion_prob=1.0,
                 maintain_motif_degree=True,
                 n_passes=1,
                 
                 **kwargs,
                 ):
      super(MEDataSplit.MESplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )
      self.init_g_min_nodes = init_g_min_nodes
      self.init_g_max_nodes = init_g_max_nodes
      self.init_g_exp_node_edges = init_g_exp_node_edges
      self.motif_min_nodes = motif_min_nodes
      self.motif_max_nodes = motif_max_nodes
      self.motif_exp_node_edges = motif_exp_node_edges
      self.motif_pool_size = motif_pool_size
      self.expansion_prob = expansion_prob
      self.maintain_motif_degree = maintain_motif_degree
      self.n_passes = n_passes
  
  def __init__(self,
               dataset,
               params: MESplitParams,
               exclude_sample_ids,
               split='train',
               preprocess_eig=None,
               preprocess_local=None,
               preprocess_global=None,
               preprocess_distance=False,
               feature_conf=None,
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    super(MEDataSplit, self).__init__(
      dataset=dataset,
      params=params,
      exclude_sample_ids=exclude_sample_ids,
      split=split,
      preprocess_eig=preprocess_eig,
      preprocess_local=preprocess_local,
      preprocess_global=preprocess_global,
      preprocess_distance=preprocess_distance,
      feature_conf=feature_conf,
      transform=transform,
      create=create,
      named_identifier=named_identifier,
      overwrite_identifier=overwrite_identifier,
    )
    
  # def create_one_sample(self):
  
  def add_samples(self,
                  params: MESplitParams,
                  preprocess_eig=None,
                  preprocess_distance=False,
                  preprocess_local=None,
                  preprocess_global=None,
                  feature_conf=None,
                  create_new=False,
                  ):
    
    rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    
    session = self.dataset.gcdb.local_sess()
    stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    base = session.execute(stmt).first()[0]
    
    self.validate_data_directories(root_dir=self.split_dir)
    
    for i in range(params.n_samples):
      init_g_size = int(np.random.randint(
        low=params.init_g_min_nodes, high=params.init_g_max_nodes))
      n_passes = int(np.random.choice(params.n_passes))
      
      if create_new:
        matching = []
      else:
        stmt = select(GCSampleBase).join(MotifExpandedNetworkBase).where(
          and_(
            MotifExpandedNetworkBase.init_g_size == init_g_size,
            MotifExpandedNetworkBase.init_g_exp_node_edges == params.init_g_exp_node_edges,
            MotifExpandedNetworkBase.motif_min_nodes == params.motif_min_nodes,
            MotifExpandedNetworkBase.motif_max_nodes == params.motif_max_nodes,
            MotifExpandedNetworkBase.motif_exp_node_edges == params.motif_exp_node_edges,
            MotifExpandedNetworkBase.motif_pool_size == params.motif_pool_size,
            MotifExpandedNetworkBase.expansion_prob == params.expansion_prob,
            MotifExpandedNetworkBase.maintain_motif_degree == params.maintain_motif_degree,
            MotifExpandedNetworkBase.n_passes == n_passes,
            GCSampleBase.id.notin_(sample_ids_added + self.exclude_sample_ids)
          )
        )
        
        matching = session.execute(stmt).all()
      
      if len(matching) > 0:
        sample = rng.choice(matching, axis=0)[0]
        sample_ids_added.append(sample.id)
        file_index.append(sample.file)
        base.samples.append(sample)
        pkl_file = sample.file
      else:
        sample = MotifExpandedNetwork(
          session=session,
          sample_dir=self.split_dir,
          
          init_g_size=init_g_size,
          init_g_exp_node_edges=params.init_g_exp_node_edges,
          motif_min_nodes=params.motif_min_nodes,
          motif_max_nodes=params.motif_max_nodes,
          motif_exp_node_edges=params.motif_exp_node_edges,
          motif_pool_size=params.motif_pool_size,
          expansion_prob=params.expansion_prob,
          maintain_motif_degree=params.maintain_motif_degree,
          n_passes=n_passes,
        )
        sample_ids_added.append(sample.id)
        file_index.append(sample.filename)
        stmt = select(GCSampleBase).where(GCSampleBase.id == sample.id)
        sample_base = session.execute(stmt).first()[0]
        base.samples.append(sample_base)
        pkl_file = sample.filename
      
      sample: GCSample = pickle.load(open(pkl_file, 'rb'))

      if preprocess_distance and not sample.preprocessed_distance:
        sample.preprocess_distance_matrix(recompute=False)
      if any(f not in sample.preprocessed_global_features for f in preprocess_global) \
          or any(f not in sample.preprocessed_local_features for f in preprocess_local):
        sample.preprocess_graph_features(
          local_feature_names=preprocess_local, global_feature_names=preprocess_global)
      if not sample.verify_eig_conf_present(preprocess_eig):
        sample.preprocess_eigenfeatures(eig_config=preprocess_eig, force_recompute=False)
      
      if any(f not in sample.preprocessed_local_features for f in feature_conf['local_features']):
        sample.preprocess_graph_features(local_feature_names=feature_conf['local_features'],
                                         global_feature_names=[])
      if not sample.verify_eig_conf_present(feature_conf['eig_conf']):
        sample.preprocess_eigenfeatures(eig_config=feature_conf['eig_conf'], force_recompute=False)
      
      printProgressBar(iteration=i+1,
                       total=params.n_samples,
                       prefix=f'Populating {self.split} dataset.',
                       suffix=f'({i+1}/{params.n_samples})',
                       length=100)
      
      session.commit()
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    n_passes = ','.join(map(str, self.params.n_passes))
    base = MEDataSplitBase(
      id=self.id,
      split=self.split,
      n_samples=self.params.n_samples,
      
      init_g_min_nodes=self.params.init_g_min_nodes,
      init_g_max_nodes=self.params.init_g_max_nodes,
      init_g_exp_node_edges=self.params.init_g_exp_node_edges,
      motif_min_nodes=self.params.motif_min_nodes,
      motif_max_nodes=self.params.motif_max_nodes,
      motif_exp_node_edges=self.params.motif_exp_node_edges,
      motif_pool_size=self.params.motif_pool_size,
      expansion_prob=self.params.expansion_prob,
      maintain_motif_degree=self.params.maintain_motif_degree,
      n_passes=n_passes,
      
      directory=self.split_dir
    )
    session = self.dataset.gcdb.local_sess()
    session.add(base)
    session.commit()


class MEDataset(GCDataset):
  def __init__(self, dataset_dir, gcdb, verbose=1):
    super(MEDataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb, verbose=verbose)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                preprocess_eig=None,
                preprocess_local=None,
                preprocess_global=None,
                preprocess_distance=False,
                feature_conf=None,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = MEDataSplit(dataset=self,
                        split=split_name,
                        params=split_params,
                        exclude_sample_ids=exclude_sample_ids,
                        preprocess_eig=preprocess_eig,
                        preprocess_local=preprocess_local,
                        preprocess_global=preprocess_global,
                        preprocess_distance=preprocess_distance,
                        feature_conf=feature_conf,
                        transform=transform,
                        create=create,
                        named_identifier=named_identifier,
                        overwrite_identifier=overwrite_identifier,
                        )
    self.splits[split_name] = split
    return split


class MotifExpandedNetwork(GCSample):
  db_table = 'samples_me'
  
  def __init__(self,
               session,
               sample_dir,

               init_g_size=20,
               init_g_exp_node_edges=1.5,
               motif_min_nodes=3,
               motif_max_nodes=7,
               motif_exp_node_edges=3.5,
               motif_pool_size=5,
               expansion_prob=1.0,
               maintain_motif_degree=True,
               n_passes=1,
               ):
    self.init_g_size = init_g_size
    self.init_g_exp_node_edges = init_g_exp_node_edges
    self.init_g_sparsity = 1 - (2 * init_g_exp_node_edges) / (init_g_size - 1)
    self.motif_min_nodes = motif_min_nodes
    self.motif_max_nodes = motif_max_nodes
    self.motif_exp_node_edges = motif_exp_node_edges
    self.motif_pool_size = motif_pool_size
    self.expansion_prob = expansion_prob
    self.maintain_motif_degree = maintain_motif_degree
    self.n_passes = n_passes
    
    
    super(MotifExpandedNetwork, self).__init__(
      session=session,
      sample_dir=sample_dir,
    )
  
  def add_class_specific_sql_entry(self, session):
    base = MotifExpandedNetworkBase(
      id=self.id,
      init_g_size=int(self.init_g_size),
      init_g_exp_node_edges=float(self.init_g_exp_node_edges),
      init_g_sparsity=float(self.init_g_sparsity),
      motif_min_nodes=int(self.motif_min_nodes),
      motif_max_nodes=int(self.motif_max_nodes),
      motif_exp_node_edges=float(self.motif_exp_node_edges),
      motif_pool_size=int(self.motif_pool_size),
      expansion_prob=float(self.expansion_prob),
      maintain_motif_degree=self.maintain_motif_degree,
      n_passes=int(self.n_passes),
    )
    session.add(base)
    session.commit()
  
  def generate(self):
    node_labels = {}
    adj = generate_random_graph(n_nodes=self.init_g_size,
                              sparsity=self.init_g_sparsity,
                              ensure_connected=True)
  
    motif_pool = []
    for i in range(self.motif_pool_size):
      motif = generate_random_graph(
        n_nodes=np.random.randint(low=self.motif_min_nodes, high=self.motif_max_nodes + 1),
        exp_node_edges=self.motif_exp_node_edges,
        # sparsity=motif_sparsity,
        ensure_connected=True)
      motif_pool.append(motif)
    
    communities = np.zeros(shape=(len(adj), 1), dtype=np.int32)
    communities = np.concatenate(
      [communities,
       np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
    
    for P in range(self.n_passes):
      communities = np.concatenate(
        [communities,
         np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
      n = 0
      n_nodes = len(adj)
      
      while n < n_nodes:
        e = np.random.rand()
        if e < self.expansion_prob:
          # A B C
          # D E F
          # G H I
          
          # todo: peculiar error, fails if all motifs have same number of nodes as size of motif pool...
          # E = np.random.choice(a=self.motif_pool)
          E = motif_pool[np.random.randint(len(motif_pool))]
          motif_size = E.shape[0]
          block = np.zeros(shape=(n_nodes + motif_size - 1, n_nodes + motif_size - 1))
          block[n:n + motif_size, n:n + motif_size] = E
          
          block[:n, :n] = adj[:n, :n]  # A
          block[:n, n + motif_size:] = adj[:n, n + 1:]  # C
          block[n + motif_size:, :n] = adj[:n, n + 1:].T  # G
          block[n + motif_size:, n + motif_size:] = adj[n + 1:, n + 1:]  # I
          
          # E = generate_random_graph(n_nodes=motif_size, sparsity=self.motif_sparsity)
          
          # this creates a zero matrix representing the edges from the motif to the nodes preceding
          # it, then places the original vector inside at random indices. Hence node degree is
          # preserved, original edges are affixed to random nodes in the motif.
          if n > 0:
            B_old = adj[:n, n]
            choice = np.random.choice(motif_size, size=(n,))
            np.put_along_axis(
              block[:n, n:n + motif_size], choice[:, None], B_old[:, None], axis=1)  # B
            block[n:n + motif_size, :n] = block[:n, n:n + motif_size].T  # D
            
            if not self.maintain_motif_degree:
              # add some random 1s, but only in rows where there are 1s already (+ same for F)
              raise NotImplementedError()
          
          if n < n_nodes:
            F_old = adj[n, n + 1:]
            choice = np.random.choice(motif_size, size=(n_nodes - n - 1,))
            np.put_along_axis(
              block[n:n + motif_size, n + motif_size:], choice[None, :], F_old[None, :],
              axis=0)  # F
            block[n + motif_size:, n:n + motif_size] = block[n:n + motif_size,
                                                       n + motif_size:].T  # H
          
          adj = block
          repeats = np.ones(shape=(communities.shape[0]), dtype=np.int32)
          repeats[n] = motif_size
          communities = communities.repeat(repeats=repeats, axis=0)
          communities[:, -1] = np.arange(communities.shape[0], dtype=np.int32)
          
          n_nodes += (motif_size - 1)
          n += motif_size - 1
        n += 1
    
    node_labels.update(
      {f'communities_L{L}': communities[:, L] for L in range(communities.shape[1])}
    )

    adj, node_labels = self.permute_nodes(adj=adj, node_labels_dict=node_labels)
    
    self.save_node_labels(node_labels)
    self.save_adjacency_matrix_component(A=adj, long_index=False)
    for lbl in node_labels.keys():
      self.communities_to_lp_labels(lbl)
    
    
