import numpy as np
from sqlalchemy import select, and_, or_
import torch
from torch_geometric.utils import to_undirected, to_dense_adj, dense_to_sparse, get_laplacian
from torch._linalg_utils import symeig
from atom3d.util.graph import one_of_k_encoding_unk, prot_atoms
import scipy.spatial as ss
import scipy as sp
import scipy.sparse
import pandas as pd
import os
import networkx as nx
from pathlib import Path
import json
import pickle

from util.util import GCTime, validate_directory

from data.gc_data import GCDataset, GCDataSplit, generate_random_graph, GCSample
# from sql.sql_base import MSPAtomBase, GCSampleBase, MADataSplitBase, GCDataSplitBase
from util.util import printProgressBar
from util.constants import *

import atom3d.datasets.datasets as da
import atom3d.util.transforms as tr
from atom3d.datasets import LMDBDataset

class MADataSplit(GCDataSplit):
  @property
  def db_table(self):
    return 'datasplits_ma'
  
  @property
  def split_class(self):
    return 'MSPAtom'
  
  class MASplitParams(GCDataSplit.GCSplitParams):
    def __init__(self,
                 n_samples,

                 # init_g_min_nodes=4,
                 # init_g_max_nodes=30,
                 # init_g_exp_node_edges=2,
                 # motif_min_nodes=3,
                 # motif_max_nodes=7,
                 # motif_exp_node_edges=3.5,
                 # motif_pool_size=5,
                 # expansion_prob=1.0,
                 # maintain_motif_degree=True,
                 # n_passes=1,
                 
                 **kwargs,
                 ):
      super(MADataSplit.MASplitParams, self).__init__(
        n_samples=n_samples,
        **kwargs,
      )
      # self.init_g_min_nodes = init_g_min_nodes
      # self.init_g_max_nodes = init_g_max_nodes
      # self.init_g_exp_node_edges = init_g_exp_node_edges
      # self.motif_min_nodes = motif_min_nodes
      # self.motif_max_nodes = motif_max_nodes
      # self.motif_exp_node_edges = motif_exp_node_edges
      # self.motif_pool_size = motif_pool_size
      # self.expansion_prob = expansion_prob
      # self.maintain_motif_degree = maintain_motif_degree
      # self.n_passes = n_passes
  
  def __init__(self,
               dataset,
               params: MASplitParams,
               exclude_sample_ids,
               split='train',
               transform=None,
               create=None,
               named_identifier=None,
               overwrite_identifier=False,
               ):
    super(MADataSplit, self).__init__(dataset=dataset,
                                      params=params,
                                      exclude_sample_ids=exclude_sample_ids,
                                      split=split,
                                      transform=transform,
                                      create=create,
                                      named_identifier=named_identifier,
                                      overwrite_identifier=overwrite_identifier,
                                      )
  
  def add_samples(self, params: MASplitParams, create_new=False):
    
    # rng = np.random.default_rng()
    sample_ids_added = []
    file_index = []
    #
    # session = self.dataset.gcdb.local_sess()
    # stmt = select(GCDataSplitBase).where(GCDataSplitBase.id == self.id)
    # base = session.execute(stmt).first()[0]
    #
    # for i in range(params.n_samples):
    #   printProgressBar(iteration=i+1,
    #                    total=params.n_samples,
    #                    prefix=f'Populating {self.split} dataset ({params.n_samples} samples).',
    #                    suffix='Complete.',
    #                    length=100)
    #
    #   init_g_size = int(np.random.randint(
    #     low=params.init_g_min_nodes, high=params.init_g_max_nodes))
    #   n_passes = int(np.random.choice(params.n_passes))
    #
    #   if create_new:
    #     matching = []
    #   else:
    #     general_criteria = GCDataSplit.get_search_criteria(params)
    #
    #     stmt = select(GCSampleBase).join(MotifExpandedNetworkBase).where(
    #       and_(
    #         MotifExpandedNetworkBase.init_g_size == init_g_size,
    #         MotifExpandedNetworkBase.init_g_exp_node_edges == params.init_g_exp_node_edges,
    #         MotifExpandedNetworkBase.motif_min_nodes == params.motif_min_nodes,
    #         MotifExpandedNetworkBase.motif_max_nodes == params.motif_max_nodes,
    #         MotifExpandedNetworkBase.motif_exp_node_edges == params.motif_exp_node_edges,
    #         MotifExpandedNetworkBase.motif_pool_size == params.motif_pool_size,
    #         MotifExpandedNetworkBase.expansion_prob == params.expansion_prob,
    #         MotifExpandedNetworkBase.maintain_motif_degree == params.maintain_motif_degree,
    #         MotifExpandedNetworkBase.n_passes == n_passes,
    #         *general_criteria,
    #         GCSampleBase.id.notin_(sample_ids_added + self.exclude_sample_ids)
    #       )
    #     )
    #
    #     matching = session.execute(stmt).all()
    #
    #   if len(matching) > 0:
    #     sample = rng.choice(matching, axis=0)[0]
    #     sample_ids_added.append(sample.id)
    #     file_index.append(sample.file)
    #     base.samples.append(sample)
    #   else:
    #     sample = MotifExpandedNetwork(
    #       session=session,
    #       dir=self.split_dir,
    #
    #       init_g_size=init_g_size,
    #       init_g_exp_node_edges=params.init_g_exp_node_edges,
    #       motif_min_nodes=params.motif_min_nodes,
    #       motif_max_nodes=params.motif_max_nodes,
    #       motif_exp_node_edges=params.motif_exp_node_edges,
    #       motif_pool_size=params.motif_pool_size,
    #       expansion_prob=params.expansion_prob,
    #       maintain_motif_degree=params.maintain_motif_degree,
    #       n_passes=n_passes,
    #
    #       preprocess_spectral=params.preprocess_spectral,
    #       preprocess_distance=params.preprocess_distance,
    #       preprocess_local_features=params.preprocess_local_features,
    #       preprocess_global_features=params.preprocess_global_features,
    #       preprocess_top_down_methods=params.preprocess_top_down_methods,
    #       top_down_n_splits=params.top_down_n_splits,
    #       top_down_vectors_per_split=params.top_down_vectors_per_split,
    #       top_down_assignments=params.top_down_assignments,
    #     )
    #     sample_ids_added.append(sample.id)
    #     file_index.append(sample.filename)
    #     stmt = select(GCSampleBase).where(GCSampleBase.id == sample.id)
    #     sample_base = session.execute(stmt).first()[0]
    #     base.samples.append(sample_base)
    # print('\n')
    # session.commit()
    return file_index, sample_ids_added
  
  def add_sql_entry(self):
    n_passes = ','.join(map(str, self.params.n_passes))
    # base = MADataSplitBase(
    #   id=self.id,
    #   split=self.split,
    #   n_samples=self.params.n_samples,
    #
    #   init_g_min_nodes=self.params.init_g_min_nodes,
    #   init_g_max_nodes=self.params.init_g_max_nodes,
    #   init_g_exp_node_edges=self.params.init_g_exp_node_edges,
    #   motif_min_nodes=self.params.motif_min_nodes,
    #   motif_max_nodes=self.params.motif_max_nodes,
    #   motif_exp_node_edges=self.params.motif_exp_node_edges,
    #   motif_pool_size=self.params.motif_pool_size,
    #   expansion_prob=self.params.expansion_prob,
    #   maintain_motif_degree=self.params.maintain_motif_degree,
    #   n_passes=n_passes,
    #
    #   directory=self.split_dir
    # )
    # session = self.dataset.gcdb.local_sess()
    # session.add(base)
    # session.commit()


class MADataset(GCDataset):
  def __init__(self, dataset_dir, gcdb):
    super(MADataset, self).__init__(dataset_dir=dataset_dir, gcdb=gcdb)
  
  def add_split(self,
                split_name,
                split_params,
                exclude_sample_ids,
                transform=None,
                create=None,
                named_identifier=None,
                overwrite_identifier=False,
                ):
    split = MADataSplit(dataset=self,
                        params=split_params,
                        exclude_sample_ids=exclude_sample_ids,
                        split=split_name,
                        transform=transform,
                        create=create,
                        named_identifier=named_identifier,
                        overwrite_identifier=overwrite_identifier,
                        )
    self.splits[split_name] = split
    return split


class MSPAtom(GCSample):
  db_table = 'samples_ma'
  
  def __init__(self,
               session,
               sample_dir,

               # init_g_size=20,
               # init_g_exp_node_edges=1.5,
               # motif_min_nodes=3,
               # motif_max_nodes=7,
               # motif_exp_node_edges=3.5,
               # motif_pool_size=5,
               # expansion_prob=1.0,
               # maintain_motif_degree=True,
               # n_passes=1,

               **kwargs,
               ):
    # self.init_g_size = init_g_size
    # self.init_g_exp_node_edges = init_g_exp_node_edges
    # self.init_g_sparsity = 1 - (2 * init_g_exp_node_edges) / (init_g_size - 1)
    # self.motif_min_nodes = motif_min_nodes
    # self.motif_max_nodes = motif_max_nodes
    # self.motif_exp_node_edges = motif_exp_node_edges
    # self.motif_pool_size = motif_pool_size
    # self.expansion_prob = expansion_prob
    # self.maintain_motif_degree = maintain_motif_degree
    # self.n_passes = n_passes
    # self.A = generate_random_graph(n_nodes=init_g_size,
    #                                sparsity=self.init_g_sparsity,
    #                                ensure_connected=True)
    #
    # self.motif_pool = []
    # for i in range(motif_pool_size):
    #   motif = generate_random_graph(
    #     n_nodes=np.random.randint(low=motif_min_nodes, high=motif_max_nodes+1),
    #     exp_node_edges=motif_exp_node_edges,
    #     # sparsity=motif_sparsity,
    #     ensure_connected=True)
    #   self.motif_pool.append(motif)
    #
    # self.labels = {}
    # self.features = None
    # self.feature_dict = {}
    
    super(MSPAtom, self).__init__(
      session=session,
      sample_dir=sample_dir,
      **kwargs,
    )
  
  def add_class_specific_sql_entry(self, session):
    # base = MSPAtomBase(
    #   id=self.id,
    #   init_g_size=int(self.init_g_size),
    #   init_g_exp_node_edges=float(self.init_g_exp_node_edges),
    #   init_g_sparsity=float(self.init_g_sparsity),
    #   motif_min_nodes=int(self.motif_min_nodes),
    #   motif_max_nodes=int(self.motif_max_nodes),
    #   motif_exp_node_edges=float(self.motif_exp_node_edges),
    #   motif_pool_size=int(self.motif_pool_size),
    #   expansion_prob=float(self.expansion_prob),
    #   maintain_motif_degree=self.maintain_motif_degree,
    #   n_passes=int(self.n_passes),
    # )
    # session.add(base)
    session.commit()
  
  def generate(self):
    communities = np.zeros(shape=(len(self.A), 1), dtype=np.int32)
    communities = np.concatenate(
      [communities,
       np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
    
    for P in range(self.n_passes):
      communities = np.concatenate(
        [communities,
         np.expand_dims(np.arange(communities.shape[0], dtype=np.int32), axis=1)], axis=1)
      n = 0
      n_nodes = len(self.A)
      
      while n < n_nodes:
        e = np.random.rand()
        if e < self.expansion_prob:
          # A B C
          # D E F
          # G H I
          
          # todo: peculiar error, fails if all motifs have same number of nodes as size of motif pool...
          # E = np.random.choice(a=self.motif_pool)
          E = self.motif_pool[np.random.randint(len(self.motif_pool))]
          motif_size = E.shape[0]
          block = np.zeros(shape=(n_nodes + motif_size - 1, n_nodes + motif_size - 1))
          block[n:n + motif_size, n:n + motif_size] = E
          
          block[:n, :n] = self.A[:n, :n]  # A
          block[:n, n + motif_size:] = self.A[:n, n + 1:]  # C
          block[n + motif_size:, :n] = self.A[:n, n + 1:].T  # G
          block[n + motif_size:, n + motif_size:] = self.A[n + 1:, n + 1:]  # I
          
          # E = generate_random_graph(n_nodes=motif_size, sparsity=self.motif_sparsity)
          
          # this creates a zero matrix representing the edges from the motif to the nodes preceding
          # it, then places the original vector inside at random indices. Hence node degree is
          # preserved, original edges are affixed to random nodes in the motif.
          if n > 0:
            B_old = self.A[:n, n]
            choice = np.random.choice(motif_size, size=(n,))
            np.put_along_axis(
              block[:n, n:n + motif_size], choice[:, None], B_old[:, None], axis=1)  # B
            block[n:n + motif_size, :n] = block[:n, n:n + motif_size].T  # D
            
            if not self.maintain_motif_degree:
              # add some random 1s, but only in rows where there are 1s already (+ same for F)
              raise NotImplementedError()
          
          if n < n_nodes:
            F_old = self.A[n, n + 1:]
            choice = np.random.choice(motif_size, size=(n_nodes - n - 1,))
            np.put_along_axis(
              block[n:n + motif_size, n + motif_size:], choice[None, :], F_old[None, :],
              axis=0)  # F
            block[n + motif_size:, n:n + motif_size] = block[n:n + motif_size,
                                                       n + motif_size:].T  # H
          
          self.A = block
          repeats = np.ones(shape=(communities.shape[0]), dtype=np.int32)
          repeats[n] = motif_size
          communities = communities.repeat(repeats=repeats, axis=0)
          communities[:, -1] = np.arange(communities.shape[0], dtype=np.int32)
          
          n_nodes += (motif_size - 1)
          n += motif_size - 1
        n += 1
    
    self.labels.update(
      {f'communities_L{L}': communities[:, L] for L in range(communities.shape[1])}
    )

def node_pos_to_edges(node_pos, edge_dist_cutoff=4.5):
  '''
  Logic modified from atom3d
  '''
  kd_tree = ss.KDTree(node_pos)
  edge_tuples = list(kd_tree.query_pairs(edge_dist_cutoff))
  edges = torch.LongTensor(edge_tuples).t().contiguous()
  edges = to_undirected(edges)

  edge_weights = torch.FloatTensor(
    [1.0 / (np.linalg.norm(node_pos[i] - node_pos[j]) + 1e-5) for i, j in edges.t()]).view(-1)
  return edges, edge_weights
  
def atoms_df_to_adj(atoms_df, dense=False, numpy=True, edge_dist_cutoff=4.5):
  node_pos = torch.FloatTensor(atoms_df[['x', 'y', 'z']].to_numpy())
  edge_index, edge_attr = node_pos_to_edges(node_pos, edge_dist_cutoff=edge_dist_cutoff)
  if dense:
    adj = to_dense_adj(edge_index=edge_index, edge_attr=edge_attr)
    if numpy:
      adj = adj.numpy()[0]
    return adj
  if numpy:
    edge_index, edge_attr = edge_index.numpy().astype(np.int32), edge_attr.numpy()
  return edge_index, edge_attr

def extract_adj_from_atoms_df_dir(prepared_atoms_df_dir=MSP_ATOMS_DF_DIR,
                                  target_dir=MSP_ADJ_DIR,
                                  edge_dist_cutoff=4.5
                                  ):
  validate_directory(target_dir)
  df_dir = Path(prepared_atoms_df_dir)
  i = 0
  atoms_df_files = sorted(df_dir.glob('*.pkl'))
  for df_pkl in atoms_df_files:
    sample_name = df_pkl.stem
    edge_index_file = f'{target_dir}/{sample_name}-edge_index.npy'
    edge_attr_file = f'{target_dir}/{sample_name}-edge_attr.npy'
    if os.path.exists(edge_index_file) and os.path.exists(edge_attr_file):
      i += 1
      continue
    df = pd.read_pickle(df_pkl)
    edge_index, edge_attr = atoms_df_to_adj(atoms_df=df,
                                            numpy=True,
                                            edge_dist_cutoff=edge_dist_cutoff)
    np.save(arr=edge_index, file=f'{target_dir}/{sample_name}-edge_index.npy')
    np.save(arr=edge_attr, file=f'{target_dir}/{sample_name}-edge_attr.npy')
    # print(f'Extracted graphs: {i+1}/{len(atoms_df_files)}')
    i += 1
    printProgressBar(iteration=i,
                     total=len(atoms_df_files),
                     prefix='Extracted graphs:',
                     suffix=f'({i}/{len(atoms_df_files)})',
                     decimals=2,
                     length=50)
  print('\nDone.')
    
    

def test_eig_methods(atoms_df):
  timer = GCTime()
  timer.start_timer('transform')
  node_pos = torch.FloatTensor(atoms_df[['x', 'y', 'z']].to_numpy())
  edge_index, edge_attr = node_pos_to_edges(node_pos)
  L_index, L_attr = get_laplacian(edge_index=edge_index, edge_weight=edge_attr)#, normalization='sym')
  L = to_dense_adj(edge_index=L_index, edge_attr=L_attr)
  transform_time = timer.elapsed_perf_time('transform')
  print(f'transform time = {transform_time}')
  
  timer.start_timer('gpu_transfer')
  L_gpu = L.to(torch.device('cuda'))
  gpu_transfer_time = timer.elapsed_perf_time('gpu_transfer')
  print(f'gpu_transfer_time = {gpu_transfer_time}')
  
  timer.start_timer('t_symeig_gpu')
  L_symeig_gpu = torch.symeig(L_gpu, eigenvectors=True, upper=True)
  symeig_time_gpu = timer.elapsed_perf_time('t_symeig_gpu')
  print(f'torch.symeig_gpu = {symeig_time_gpu}')
  
  timer.start_timer('t_lobpcg_gpu')
  L_lobpcg_gpu = torch.lobpcg(L_gpu, k=6, largest=False, method='ortho', tol=None)
  lobpcg_time_gpu = timer.elapsed_perf_time('t_lobpcg_gpu')
  print(f'torch.lobpcg_gpu = {lobpcg_time_gpu}')
  
  # basic method not robust, on very rare occasions will crash if input is singular
  # timer.start_timer('t_lobpcg_gpu_basic')
  # L_lobpcg_gpu_basic = torch.lobpcg(L_gpu, k=6, largest=False, method='basic', tol=None)
  # lobpcg_time_gpu_basic = timer.elapsed_perf_time('t_lobpcg_gpu_basic')
  # print(f'torch.lobpcg_gpu_basic = {lobpcg_time_gpu_basic}')
  # timer.start_timer('t_eig_gpu')
  # L_eig_gpu = torch.eig(L_gpu[0], eigenvectors=True)
  # t_eig_time_gpu = timer.elapsed_perf_time('t_eig_gpu')
  # print(f'torch.eig_gpu = {t_eig_time_gpu}')
  
  # timer.start_timer('t_symeig')
  # L_symeig = torch.symeig(L, eigenvectors=True, upper=True)
  # symeig_time = timer.elapsed_perf_time('t_symeig')
  # print(f'torch.symeig = {symeig_time}')
  # timer.start_timer('t_eig')
  # L_eig = torch.eig(L[0], eigenvectors=True)
  # t_eig_time = timer.elapsed_perf_time('t_eig')
  # print(f'torch.eig = {t_eig_time}')
  
  numpy_L = L.numpy()[0]
  
  # timer.start_timer('np_linalg_eig')
  # np_linalg_eig = np.linalg.eig(numpy_L)
  # np_linalg_eig_time = timer.elapsed_perf_time('np_linalg_eig')
  # print(f'np_linalg_eig = {np_linalg_eig_time}')
  # timer.start_timer('np_linalg_eigh')
  # np_linalg_eigh = np.linalg.eigh(numpy_L)
  # np_linalg_eigh_time = timer.elapsed_perf_time('np_linalg_eigh')
  # print(f'np_linalg_eigh = {np_linalg_eigh_time}')

  # timer.start_timer('sp_linalg_eig')
  # sp_linalg_eig = sp.linalg.eig(numpy_L, left=True)
  # sp_linalg_eig_time = timer.elapsed_perf_time('sp_linalg_eig')
  # print(f'sp_linalg_eig = {sp_linalg_eig_time}')
  # timer.start_timer('sp_linalg_eigh_1_6')
  # sp_linalg_eigh_1_6 = sp.linalg.eigh(numpy_L, eigvals_only=False, subset_by_index=[1, 6])
  # sp_linalg_eigh_1_6_time = timer.elapsed_perf_time('sp_linalg_eigh_1_6')
  # print(f'sp_linalg_eigh_1_6 = {sp_linalg_eigh_1_6_time}')
  timer.start_timer('sp_sparse_linalg_eigsh_k6')
  sp_sparse_linalg_eigsh_k6 = sp.sparse.linalg.eigsh(numpy_L, return_eigenvectors=True, k=6, which='SM', tol=0)
  sp_sparse_linalg_eigsh_k6_time = timer.elapsed_perf_time('sp_sparse_linalg_eigsh_k6')
  print(f'sp_sparse_linalg_eigsh_k6 = {sp_sparse_linalg_eigsh_k6_time}')
  timer.start_timer('sp_sparse_linalg_eigsh_k6_1e5')
  sp_sparse_linalg_eigsh_k6_1e5 = sp.sparse.linalg.eigsh(numpy_L, return_eigenvectors=True, k=6, which='SM', tol=1e-5)
  sp_sparse_linalg_eigsh_k6_1e5_time = timer.elapsed_perf_time('sp_sparse_linalg_eigsh_k6_1e5')
  print(f'sp_sparse_linalg_eigsh_k6_1e5 = {sp_sparse_linalg_eigsh_k6_1e5_time}')
  # timer.start_timer('sp_linalg_eigh')
  # sp_linalg_eigh = sp.linalg.eigh(numpy_L, eigvals_only=False)
  # sp_linalg_eigh_time = timer.elapsed_perf_time('sp_linalg_eigh')
  # print(f'sp_linalg_eigh = {sp_linalg_eigh_time}')
  
  timer.start_timer('prep_nx')
  numpy_A = to_dense_adj(edge_index=edge_index, edge_attr=edge_attr).numpy()[0]
  nxgraph = nx.from_numpy_array(numpy_A)
  nx_prep_time = timer.elapsed_perf_time('prep_nx')
  print(f'nx_prep = {nx_prep_time}')
  timer.start_timer('nx_fiedler_vec')
  nx_fiedler = nx.linalg.algebraicconnectivity.fiedler_vector(nxgraph, normalized=True)
  nx_fiedler_time = timer.elapsed_perf_time('nx_fiedler_vec')
  print(f'nx_fiedler = {nx_fiedler_time}')

  
  print('test')
  
def compute_msp_graph_features(global_features_dir=MSP_GLOBAL_FEATURES_DIR,
                               local_features_dir=MSP_LOCAL_FEATURES_DIR,
                               atoms_adj_dir=MSP_ADJ_DIR,
                               atoms_df_dir=MSP_ATOMS_DF_DIR,
                               version=0,
                               ):
  global_feature_names = GCSample.GRAPH_LEVEL_FEATURES
  global_feature_names.remove('transitivity_sq')
  local_feature_names = GCSample.node_level_features
  local_feature_names.remove('clustering_coeff_sq')
  
  validate_directory(global_features_dir)
  validate_directory(local_features_dir)
  
  processed = len(list(Path(local_features_dir).glob(f'*-local_features-v{version}.npy')))
  n_files = len(os.listdir(atoms_df_dir))
  
  while processed < n_files:
    processed = len(list(Path(local_features_dir).glob(f'*-local_features-v{version}.npy')))
    n_files = len(os.listdir(atoms_df_dir))
    adj_attr_files = sorted(Path(atoms_adj_dir).glob('*-edge_attr.npy'))
    for f in adj_attr_files:
      sample_name = f.stem[:-len('-edge_attr')]
      if os.path.exists(f'{local_features_dir}/{sample_name}-local_features-v{version}.npy'):
        continue
      
      adjacency_index = np.load(f'{atoms_adj_dir}/{sample_name}-edge_index.npy')
      adjacency_attr = np.load(f'{atoms_adj_dir}/{sample_name}-edge_attr.npy')
      adjacency_csr = sp.sparse.csr_matrix((adjacency_attr,
                                            (adjacency_index[0], adjacency_index[1])))
      adjacency = adjacency_csr.toarray()
      
      g = nx.from_numpy_array(adjacency)
      # g = g.to_undirected()

      sample_global_features = {}
      sample_local_features = []
      local_feature_indices = {}
      
      n_nodes = g.number_of_nodes()
      n_edges = g.number_of_edges()
      sample_global_features['n_nodes'] = n_nodes
      sample_global_features['n_edges'] = n_edges
      sample_global_features['density'] = (n_edges*2) / (n_nodes * (n_nodes - 1))

      if 'degree_centrality' in local_feature_names:
        degree_centrality = np.array([d[1] / n_nodes for d in g.degree])
        sample_local_features.append(degree_centrality)
        local_feature_indices['degree_centrality'] = len(local_feature_indices)

      if any(f in local_feature_names or f in global_feature_names for f in
             ['clustering_coeff', 'transitivity']):
        triangles = np.array([t[2] for t in nx.algorithms.cluster._triangles_and_degree_iter(g)])
        connected_triplets = np.array([d[1] * (d[1] - 1) for d in g.degree])
        clustering_coeff = np.divide(triangles, connected_triplets,
                                     out=np.zeros_like(triangles).astype(np.float),
                                     where=triangles != 0)
        # occasionally get nodes with no connected triplets (single edge nodes)
        # so se clustering coeff for these to 0
        clustering_coeff[np.isnan(clustering_coeff)] = 0
        sample_local_features.append(clustering_coeff)
        local_feature_indices['clustering_coeff'] = len(local_feature_indices)
        sample_global_features['transitivity'] = np.mean(clustering_coeff)

      if any(f in local_feature_names or f in global_feature_names for f in
             ['clustering_coeff_sq', 'transitivity_sq']):
        clustering_coeff_sq = np.array(list(nx.algorithms.cluster.square_clustering(g).values()))
        sample_local_features.append(clustering_coeff_sq)
        local_feature_indices['clustering_coeff_sq'] = len(local_feature_indices)
        sample_global_features['transitivity_sq'] = np.mean(clustering_coeff_sq)

      if 'average_neighbour_degree' in local_feature_names:
        avg_neighbour_degree = np.array(list(
          nx.algorithms.assortativity.average_neighbor_degree(g).values()))
        sample_local_features.append(avg_neighbour_degree / n_nodes)
        local_feature_indices['average_neighbour_degree'] = len(local_feature_indices)

      # if 'closeness_centrality' in local_feature_names:
      #   sum_distances = np.sum(self.distance_matrix, axis=0)
      #   closeness_centrality = (n_nodes - 1) / sum_distances
      #   sample_local_features.append(closeness_centrality)
      #   local_feature_indices['closeness_centrality'] = len(local_feature_indices)

      # if any(f in local_feature_names or f in global_feature_names for f in
      #        ['eccentricity', 'is_periphery', 'radius', 'diameter', 'periphery_coeff']):
      #   eccentricity = np.max(self.distance_matrix, axis=0)
      #   radius = np.min(eccentricity)
      #   diameter = np.max(eccentricity)
      #   is_periphery = (eccentricity == diameter).astype(np.int32)
      #   periphery_coeff = np.sum(is_periphery) / n_nodes
      #   sample_local_features.append(eccentricity)
      #   local_feature_indices['eccentricity'] = len(local_feature_indices)
      #   sample_local_features.append(is_periphery)
      #   local_feature_indices['is_periphery'] = len(local_feature_indices)
      #   sample_global_features['radius'] = radius
      #   sample_global_features['diameter'] = diameter
      #   sample_global_features['periphery_coeff'] = periphery_coeff

      if 'degree_assortativity' in global_feature_names:
        sample_global_features['degree_assortativity'] = \
          nx.algorithms.assortativity.degree_pearson_correlation_coefficient(g)

      local_features = np.concatenate([f[:, None] for f in sample_local_features], axis=1)
      with open(f'{local_features_dir}/local_features-v{version}_indices.json', 'w') as lff:
        lff.write(json.dumps(local_feature_indices, indent=4, sort_keys=True))
      with open(f'{global_features_dir}/{sample_name}-global_features-v{version}.json', 'w') as gff:
        gff.write(json.dumps(sample_global_features, indent=4, sort_keys=True))
      np.save(arr=local_features,
              file=f'{local_features_dir}/{sample_name}-local_features-v{version}.npy')
      processed = len(list(Path(local_features_dir).glob(f'*-local_features-v{version}.npy')))
      printProgressBar(iteration=processed,
                       total=n_files,
                       prefix='Sample features preprocessed:',
                       suffix=f'({processed}/{n_files})',
                       decimals=2,
                       length=50)

def run_top_down_decomposition(decomposition_dir=MSP_EIGS_ROOT,
                               atoms_adj_dir=MSP_ADJ_DIR,
                               atoms_df_dir=MSP_ATOMS_DF_DIR
                               ):
  n_splits = 15
  gpu_splits = 0
  # todo: fix normalised_laplacian method (crashes + slow)
  # todo: fix adjacency method (v. slow, crashes?)
  per_layer_adjacency_variants = ['laplacian'] * n_splits
  per_layer_eig_algorithms = ['pyt_lobpcg'] * gpu_splits
  per_layer_eig_algorithms += ['sp_eigsh'] * (n_splits - gpu_splits)
  per_layer_split_methods = ['median'] * n_splits
  # per_layer_split_methods += ['ncut'] * 13  # todo: fix search_ncut method for sparse (too long for dense)
  per_layer_n_vectors = [12] * n_splits
  per_layer_tols = [None] * gpu_splits
  per_layer_tols += [0.] * (n_splits - gpu_splits)
  per_layer_gpus = [True] * gpu_splits
  per_layer_gpus += [False] * (n_splits - gpu_splits)
  init_top_down_decomposition(decomposition_dir, atoms_df_dir)
  
  print(f'Decomposing for "{per_layer_split_methods[0]}" method')
  # print('Decomposing for "adjacency" method')
  
  final_target_dir = MSP_EIGS_ROOT
  for n in range(n_splits):
    final_target_dir += f'/{per_layer_adjacency_variants[n]}/' \
                        f'{per_layer_eig_algorithms[n]}/' \
                        f'{per_layer_split_methods[n]}'
  final_target_dir += '/assignments'
  total_decompositions = len(list(Path(final_target_dir).glob('*-assignments.npy')))
  n_files = len(os.listdir(atoms_df_dir))
  
  while total_decompositions < n_files:
    total_decompositions = len(list(Path(final_target_dir).glob('*-assignments.npy')))
    adj_attr_files = sorted(Path(atoms_adj_dir).glob('*-edge_attr.npy'))
    for f in adj_attr_files:
      sample_name = f.stem[:-len('-edge_attr')]
      if os.path.exists(f'{final_target_dir}/{sample_name}-assignments.npy'):
        continue
      last_n = 0
      try:
        start_dir = MSP_EIGS_ROOT
        # timer = GCTime()
        for n in range(n_splits):
          last_n = n
          # timer.start_timer('split')
          # print(n)
          adjacency_variant = per_layer_adjacency_variants[n]
          eig_algorithm = per_layer_eig_algorithms[n]
          split_method = per_layer_split_methods[n]
          n_vectors = per_layer_n_vectors[n]
          tol = per_layer_tols[n]
          gpu = per_layer_gpus[n]
          top_down_decomposition(sample_name=sample_name,
                                 start_dir=start_dir,
                                 adjacency_dir=MSP_ADJ_DIR,
                                 adjacency_variant=adjacency_variant,
                                 eig_algorithm=eig_algorithm,
                                 n_vectors=n_vectors,
                                 split_method=split_method,
                                 gpu=gpu,
                                 tol=tol)
          start_dir += f'/{adjacency_variant}/{eig_algorithm}/{split_method}'
          # print(f'split {n} = {timer.elapsed_perf_time("split")}')
  
        total_decompositions = len(list(Path(final_target_dir).glob('*-assignments.npy')))
        printProgressBar(iteration=total_decompositions,
                         total=n_files,
                         prefix='Completed decompositions:',
                         suffix=f'({total_decompositions+1}/{n_files})',
                         decimals=2,
                         length=50)
      except Exception as e:
        print(f'\nError occured for file {sample_name}')
        validate_directory(MSP_FAILS)
        with open(f'{MSP_FAILS}/{sample_name}-{per_layer_split_methods[last_n]}-{last_n}.fail',
                  'w') as f:
          f.write('')
        print(e)
        total_decompositions = len(list(Path(final_target_dir).glob('*-assignments.npy')))
        printProgressBar(iteration=total_decompositions,
                         total=n_files,
                         prefix='Completed decompositions:',
                         suffix=f'({total_decompositions + 1}/{n_files})',
                         decimals=2,
                         length=50)
        
      # print('\n')
    
    
  
def init_top_down_decomposition(decomposition_root=MSP_EIGS_ROOT,
                                atoms_df_dir=MSP_ATOMS_DF_DIR
                                ):
  assignments_dir = validate_directory(f'{decomposition_root}/assignments')
  for f in sorted(Path(atoms_df_dir).glob('*.pkl')):
    assignments_fname = f'{assignments_dir}/{f.stem}-assignments.npy'
    if os.path.exists(assignments_fname):
      continue
    df = pd.read_pickle(f)
    init_assignments = np.zeros(shape=(len(df), 1)).astype(np.bool)
    init_assignments = np.packbits(init_assignments, axis=0)
    np.save(arr=init_assignments, file=assignments_fname)
  print('Initialised eigendecomposition root dir.')
  

def top_down_decomposition(sample_name,
                           start_dir,
                           adjacency_dir,
                           adjacency_variant='laplacian',
                           eig_algorithm='sp_eigsh',
                           n_vectors=1,
                           split_method='median',
                           gpu=False,
                           tol=None,
                           ):
  assignments_fname = f'/{sample_name}-assignments.npy'
  eigenvectors_fname = f'/{sample_name}-eigenvectors.npy'
  eigenvalues_fname = f'/{sample_name}-eigenvalues.npy'
  source_assignments_dir = f'{start_dir}/assignments'
  source_assignments_fname = f'{source_assignments_dir}/{assignments_fname}'
  adjacency_index_fname = f'{adjacency_dir}/{sample_name}-edge_index.npy'
  adjacency_attr_fname = f'{adjacency_dir}/{sample_name}-edge_attr.npy'
  assert os.path.exists(adjacency_index_fname)
  assert os.path.exists(adjacency_attr_fname)
  assert os.path.exists(source_assignments_fname)
  assert adjacency_variant in ('adjacency', 'laplacian', 'normalised_laplacian')
  assert eig_algorithm in ('pyt_eig',
                           'pyt_symeig',
                           'pyt_lobpcg',
                           'np_eig',
                           'np_eigh',
                           'sp_eig',
                           'sp_eigh',
                           'sp_eigs',
                           'sp_eigsh')
  eig_algorithm_dir = validate_directory(f'{start_dir}/{adjacency_variant}/{eig_algorithm}')
  eigenvectors_dir = validate_directory(f'{eig_algorithm_dir}/eigenvectors/smallest_{n_vectors}')
  eigenvalues_dir = validate_directory(f'{eig_algorithm_dir}/eigenvalues/smallest_{n_vectors}')
  split_method_dir = validate_directory(f'{eig_algorithm_dir}/{split_method}')
  tgt_assignments_dir = validate_directory(f'{split_method_dir}/assignments')
  
  adjacency_index = np.load(adjacency_index_fname)
  adjacency_attr = np.load(adjacency_attr_fname)
  adjacency_csr = sp.sparse.csr_matrix((adjacency_attr, (adjacency_index[0], adjacency_index[1])))#.toarray()
  
  assignments = np.unpackbits(np.load(source_assignments_fname), axis=0, count=adjacency_csr.shape[0])
  
  eigenvectors = np.zeros(shape=(adjacency_csr.shape[0], n_vectors)).astype(np.float32)
  eigenvalues = np.zeros(shape=(adjacency_csr.shape[0], n_vectors)).astype(np.float32)
  assignment_col = np.zeros(shape=(adjacency_csr.shape[0], 1)).astype(np.bool)
  
  unique_partitions = np.unique(assignments, axis=0)
  for partition in unique_partitions:
    partition_idxs = np.where((assignments == partition).all(axis=1))[0]
    if len(partition_idxs) < 2:
      continue
    partition_adj = adjacency_csr[np.ix_(partition_idxs, partition_idxs)]
    sorted_eig_vals, sorted_eig_vecs = top_down_eigenfeatures(
      A=partition_adj,
      adjacency_variant=adjacency_variant,
      eig_algorithm=eig_algorithm,
      n_vectors=n_vectors+1,  # +1 as will be ignoring the very smallest
      gpu=gpu,
      tol=tol)
    
    # first save the eigenfeatures
    eigenvectors[partition_idxs, :] = sorted_eig_vecs[:, 1:]
    eigenvalues[partition_idxs, :] = sorted_eig_vals[1:]
    
    # then perform the split and generate assignments
    fiedler_vec = sorted_eig_vecs[:, 1]
    idxs_1, idxs_2 = GCSample.split_on_fiedler(fiedler_vec, split_method, A=partition_adj)
    partition_assignments = np.zeros(shape=(partition_adj.shape[0], 1))
    partition_assignments[idxs_2] = 1
    assignment_col[partition_idxs] = partition_assignments
    
  np.save(arr=eigenvectors, file=f'{eigenvectors_dir}/{eigenvectors_fname}')
  np.save(arr=eigenvalues, file=f'{eigenvalues_dir}/{eigenvalues_fname}')
  assignments = np.concatenate([assignments, assignment_col], axis=1)
  np.save(arr=np.packbits(assignments, axis=0), file=f'{tgt_assignments_dir}/{assignments_fname}')
    
    
def top_down_eigenfeatures(A,
                           adjacency_variant,
                           eig_algorithm,
                           n_vectors,
                           gpu=False,
                           tol=0.,
                           ):
  if A.shape[0] < n_vectors:
    spare_vecs = n_vectors - A.shape[0]
    n_vectors = A.shape[0]
  else:
    spare_vecs = 0
    
  AV = A
  if adjacency_variant == 'laplacian':
    AV = sp.sparse.csgraph.laplacian(A, normed=False)
  elif adjacency_variant == 'normalised_laplacian':
    AV = sp.sparse.csgraph.laplacian(A, normed=True)
    
  if eig_algorithm.startswith('pyt'):
    indices = np.vstack([AV.row, AV.col])
    values = AV.data
    size = AV.shape
    AV = torch.sparse_coo_tensor(indices=indices, values=values, size=size)#.to_dense()
    AV = AV.to(torch.device('cuda')) if gpu else AV
  
  if eig_algorithm == 'pyt_eig':
    eig_vals, eig_vecs = torch.eig(AV, eigenvectors=True)
  elif eig_algorithm == 'pyt_symeig':
    eig_vals, eig_vecs = torch.symeig(AV, eigenvectors=True)
  elif eig_algorithm == 'pyt_lobpcg':
    eig_vals, eig_vecs = torch.lobpcg(AV, k=n_vectors, largest=False, tol=tol, method='ortho')
  
  elif eig_algorithm == 'np_eig':
    eig_vals, eig_vecs = np.linalg.eig(AV)
  elif eig_algorithm == 'np_eigh':
    eig_vals, eig_vecs = np.linalg.eigh(AV)
  
  elif eig_algorithm == 'sp_eig':
    AV = AV.toarray()
    eig_vals, eig_vecs = sp.linalg.eig(AV)
  elif eig_algorithm == 'sp_eigh':
    AV = AV.toarray()
    eig_vals, eig_vecs = sp.linalg.eigh(AV,
                                        eigvals_only=False,
                                        subset_by_index=[0, n_vectors-1],
                                        driver='evr')
  elif eig_algorithm == 'sp_eigs':
    if n_vectors >= AV.shape[0]:
      AV = AV.toarray()
      eig_vals, eig_vecs = sp.linalg.eig(AV)
    else:
      eig_vals, eig_vecs = sp.sparse.linalg.eigs(AV,
                                                 k=n_vectors,
                                                 which='SR',  # or SM, as eigs should be real anyway
                                                 tol=tol,
                                                 return_eigenvectors=True)
  elif eig_algorithm == 'sp_eigsh':
    if n_vectors >= AV.shape[0]:
      AV = AV.toarray()
      eig_vals, eig_vecs = sp.linalg.eigh(AV,
                                          eigvals_only=False,
                                          subset_by_index=[0, n_vectors-1],
                                          driver='evr')
    else:
      eig_vals, eig_vecs = sp.sparse.linalg.eigsh(AV,
                                                  k=n_vectors,
                                                  which='SM',
                                                  tol=tol,
                                                  return_eigenvectors=True)
  else:
    raise ValueError()
  
  if eig_algorithm.startswith('pyt'):
    if gpu:
      eig_vals, eig_vecs = eig_vals.cpu(), eig_vecs.cpu()
    eig_vals, eig_vecs = eig_vals.numpy(), eig_vecs.numpy()
  
  eig_val_order = np.argsort(np.abs(eig_vals))
  ordered_vals = eig_vals[eig_val_order]
  ordered_vecs = eig_vecs[:, eig_val_order]
  
  if spare_vecs > 0:
    ordered_vecs = np.concatenate(
      [ordered_vecs, np.zeros(shape=(ordered_vecs.shape[0], spare_vecs))], axis=1)
    ordered_vals = np.concatenate([ordered_vals, np.zeros(spare_vecs)])
  
  return ordered_vals[:n_vectors+spare_vecs], ordered_vecs[:, :n_vectors+spare_vecs]
  

  
def extract_lmdb(lmdb_data_file, target_loc):
  lmdb_dataset = LMDBDataset(data_file=lmdb_data_file)
  atoms_df_dir = validate_directory(f'{target_loc}/atoms_df')
  labels_cols = ['wild_type', 'mutated', 'label']
  if os.path.exists(f'{MSP_EXTRACT_DIR}/labels.csv'):
    labels_df = pd.read_csv(f'{MSP_EXTRACT_DIR}/labels.csv')#, header=labels_cols)
  else:
    labels_df = pd.DataFrame(columns=labels_cols)
  for entry in lmdb_dataset:
    wild = entry['original_atoms']
    mutated = entry['mutated_atoms']
    label = entry['label']
    wild_name = wild.ensemble[0][:-4]
    mutated_name = mutated.ensemble[0][:-4]
    labels_row = [wild_name, mutated_name, label]
    if wild_name not in labels_df.wild_type.unique():
      wild.to_pickle(f'{atoms_df_dir}/{wild_name}.pkl')
    if mutated_name not in labels_df.mutated.unique():
      mutated.to_pickle(f'{atoms_df_dir}/{mutated_name}.pkl')
      labels_df.loc[len(labels_df)] = labels_row
    labels_df.to_csv(f'{MSP_EXTRACT_DIR}/labels.csv', header=True, index=False)
    print(f'Extracted {len(labels_df)}/{len(lmdb_dataset)}')
    
    
def download_msp():
  da.download_dataset(name='msp', out_path=MSP_DOWNLOAD_DIR)

def test():
  # extract_lmdb(MSP_LMDB_DIR, target_loc=MSP_EXTRACT)
  # da.download_dataset(name='msp', out_path=f'{MSP_DOWNLOAD})
  dataset = LMDBDataset(data_file=f'{MSP_LMDB_DIR}')
  for entry in dataset:
    mutated_df = entry['mutated_atoms']
    sample = test_eig_methods(mutated_df)
    wild_type_df = entry['original_atoms']

if __name__ == '__main__':
    # test()
    # extract_adj_from_atoms_df_dir()
    run_top_down_decomposition(decomposition_dir=MSP_EIGS_ROOT, atoms_df_dir=MSP_ATOMS_DF_DIR)
    # compute_msp_graph_features()