FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04
LABEL maintainer = "amf85@cl.cam.ac.uk"
ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"
WORKDIR /

RUN apt update && \
	apt install -y software-properties-common && \
	add-apt-repository ppa:deadsnakes/ppa && \
	apt install -y python3.7

RUN add-apt-repository universe && \
    apt install -y python3-pip && \
    pip3 install --upgrade pip
RUN apt-get update && apt-get install -y git
RUN apt-get install -y wget && rm -rf /var/lib/apt/lists/*
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    mkdir /root/.conda && \
    bash Miniconda3-latest-Linux-x86_64.sh -b && \
    rm -f Miniconda3-latest-Linux-x86_64.sh

#RUN conda install python=3.6
RUN conda install python=3.7

# graphein install instructions
RUN conda install scipy numpy scikit-learn matplotlib pandas cython
RUN pip3 install ticc==0.1.4
# slight change for vmd-python to graphein readme
RUN conda install -c conda-forge vmd-python
RUN git clone https://github.com/getcontacts/getcontacts.git
RUN echo "export PATH=`pwd`/getcontacts:\$PATH" >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"

RUN conda install biopython
RUN conda install -c conda-forge rdkit==2018.09.3

RUN conda install -c salilab dssp

RUN conda install pytorch==1.7.0 torchvision==0.8.0 torchaudio==0.7.0 cudatoolkit=10.2 -c pytorch
RUN pip3 install dgl==0.4.3
RUN conda install -c dglteam dgllife dgl-cuda10.2

RUN pip3 install --no-index torch-scatter -f https://pytorch-geometric.com/whl/torch-1.7.0+cu102.html
RUN pip3 install --no-index torch-sparse -f https://pytorch-geometric.com/whl/torch-1.7.0+cu102.html
RUN pip3 install --no-index torch-cluster -f https://pytorch-geometric.com/whl/torch-1.7.0+cu102.html
RUN pip3 install --no-index torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.7.0+cu102.html
RUN pip3 install torch-geometric

RUN conda install -c schrodinger pymol

RUN git clone -b protein_vis https://www.github.com/a-r-j/graphein
WORKDIR /graphein
RUN pip3 install -e .
WORKDIR /

# other imports rfom trying to run graphein
RUN pip3 install pydantic
RUN pip3 install xarray
RUN pip3 install multipledispatch

# atom3d
RUN git clone https://github.com/drorlab/atom3d
WORKDIR /atom3d
RUN make requirements
WORKDIR /

ENV DGLBACKEND=pytorch

#RUN conda install -c dgl-cuda10.2
#RUN conda install -c pytorch3d pytorch3d

# graphviz
RUN apt-get update -y && apt update -y && apt install -y graphviz graphviz-dev
RUN pip3 install pygraphviz

RUN pip3 install seaborn
RUN pip3 install prettytable
RUN pip3 install better-abc
RUN pip3 install SQLAlchemy
RUN pip3 install SQLAlchemy-ImageAttach
RUN pip3 install pyrr
RUN pip3 install bayesian-optimization